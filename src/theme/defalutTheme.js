import {calcFontSize} from '../utils/dimensions';

export const colors = {
  white: '#fff',
  black: 'rgb(0,0,0)',
  blueLight: '#40A8E2',
  transparentBlack: 'rgba(0, 0, 0, 0.3)',
  gray: 'gray',
  grayLight: '#F0ECEB',
  green: '#5AEB68',
  borderColorInput: 'rgba(180, 190, 201, 0.303017)',
  errorColor: '#FF5A5A',
  colorText: 'black',
  yellowLight: '#EBB835',
  yellow: '#edbd41',
  backgroundColorOnPress: '#FFEDB1',
  darkGray: '#939393',
  darkGray1: '#EEF1F5',
  darkGray2: '#939393',
  darkGray3: 'rgba(180, 190, 201, 0.303017)',
  gray4: '#EBE6E6',
  gray5: '#939393',
  gray6: '#BEBEBE',
  gray7: 'rgba(180, 190, 201, 0.3)',
  gray8: '#E6E7E8',
  backgroundColorModal: '#FFFFFF',
  textDisabledColor: '#d9e1e8',
  inValid: '#939393',
  borderRadioButton: '#DBE2EA',
  ghostWhite: '#F9F8F8',
  backgroundRemarks: '#F8F6F4',
  backgroundModalRemarks: 'rgba(0,0,0,0.85)',
  backgroundDetailes: 'rgba(255,255,255,0.86)',
};

export const fontSizes = {
  s10: calcFontSize(10),
  s12: calcFontSize(12),
  s13: calcFontSize(13),
  s14: calcFontSize(14),
  s15: calcFontSize(15),
  s16: calcFontSize(16),
  s17: calcFontSize(17),
  s18: calcFontSize(18),
  s19: calcFontSize(19),
  s20: calcFontSize(20),
  s21: calcFontSize(21),
  s22: calcFontSize(22),
  s24: calcFontSize(24),
  s28: calcFontSize(28),
  s30: calcFontSize(30),
  s32: calcFontSize(32),
  s36: calcFontSize(36),
  s40: calcFontSize(40),
};

export const bigFontSizes = {
  s10: calcFontSize(13),
  s12: calcFontSize(15),
  s13: calcFontSize(16),
  s14: calcFontSize(17),
  s15: calcFontSize(18),
  s16: calcFontSize(19),
  s17: calcFontSize(20),
  s18: calcFontSize(21),
  s19: calcFontSize(22),
  s20: calcFontSize(23),
  s21: calcFontSize(24),
  s22: calcFontSize(25),
  s24: calcFontSize(27),
  s28: calcFontSize(31),
  s30: calcFontSize(33),
  s32: calcFontSize(35),
  s36: calcFontSize(39),
  s40: calcFontSize(43),
};

// export const fontFamilies = {
// 	GilroyBold: 'Gilroy-Bold',
// 	GilroyMedium: 'Gilroy-Medium',
// 	LucidaGrande: 'LucidaGrande',
// 	SFProRegular: 'SFProText-Regular',
// 	SFProSemibold: 'SFProText-Semibold',
// 	TTNormsMedium: 'TTNorms-Medium',
// };

const defaultTheme = {
  colors,
  fontSizes,
  //fontFamilies
};

export default defaultTheme;
