import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// components
import AddPassengers from '../../screens/StationsAndPassengers/screens/AddPassengers/AddPassengers';
import AddStation from '../../screens/StationsAndPassengers/screens/AddStation/AddStation';
import EditPassenger from '../../screens/StationsAndPassengers/screens/EditPassenger/EditPassenger';
import StationsAndPassengers from '../../screens/StationsAndPassengers/StationsAndPassengers';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
};

const StationsAndPassengersStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,

        transitionSpec: {
          open: config,
          close: config,
        },
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Stations" component={StationsAndPassengers} />
      <Stack.Screen name="AddPassengers" component={AddPassengers} />
      <Stack.Screen name="AddStation" component={AddStation} />
      <Stack.Screen name="EditPassenger" component={EditPassenger} />
    </Stack.Navigator>
  );
};

export default StationsAndPassengersStack;
