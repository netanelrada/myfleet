import React, {createRef} from 'react';

import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
// components
import Main from '../../screens/Main/Main';
import SingleTrip from '../../screens/SingleTrip/SingleTrip';
import StationsAndPassengersStack from './StationsAndPassengersStack';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
};

const TripStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,

        transitionSpec: {
          open: config,
          close: config,
        },
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Main" component={Main} />
      <Stack.Screen name="SingleTrip" component={SingleTrip} />
      <Stack.Screen
        name="StationsAndPassengers"
        component={StationsAndPassengersStack}
      />
    </Stack.Navigator>
  );
};

export default TripStack;
