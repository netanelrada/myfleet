import React, { createRef } from 'react';

import {
	createStackNavigator,
	CardStyleInterpolators,
} from '@react-navigation/stack';
// components
import NewTrip from '../../screens/NewTrip/NewTrip';
import ClientSelect from '../../screens/NewTrip/screens/ClientSelect/ClientSelect';
import LineDescriptionSelect from '../../screens/NewTrip/screens/LineDescriptionSelect/LineDescriptionSelect';
import CarTypeSelect from '../../screens/NewTrip/screens/CarTypeSelect/CarTypeSelect';
import DriverSelect from '../../screens/NewTrip/screens/DriverSelect/DriverSelect';
import CarSelect from '../../screens/NewTrip/screens/CarSelect/CarSelect';

const Stack = createStackNavigator();

const config = {
	animation: 'timing',
};

const NewTripStack = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerShown: false,
				gestureEnabled: false,

				transitionSpec: {
					open: config,
					close: config,
				},
				cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
			}}
		>
			<Stack.Screen name='NewTrip' component={NewTrip} />
			<Stack.Screen name='ClientSelect' component={ClientSelect} />
			<Stack.Screen
				name='LineDescriptionSelect'
				component={LineDescriptionSelect}
			/>
			<Stack.Screen name='CarTypeSelect' component={CarTypeSelect} />
			<Stack.Screen name='DriverSelect' component={DriverSelect} />
			<Stack.Screen name='CarSelect' component={CarSelect} />
		</Stack.Navigator>
	);
};

export default NewTripStack;
