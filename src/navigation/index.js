import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
// components
import Initial from '../screens/Initial/Initial';
import Home from '../screens/Home/Home';
import Settings from '../screens/Settings/Settings';
import DriverReplacement from '../screens/DriverReplacement/DriverReplacement';
import CarReplacement from '../screens/CarReplacement/CarReplacement';
import EditComments from '../screens/EditComments/EditComments';
import EditTrip from '../screens/EditTrip/EditTrip';
import EditScheduleTrip from '../screens/EditScheduleTrip/EditScheduleTrip';
import FilterTrips from '../screens/FilterTrips/FilterTrips';
/// stack new trip
import NewTripStack from './Stacks/NewTripStack';

import {navigationRef} from './navigationService';
const Stack = createStackNavigator();

const config = {
  animation: 'timing',
};

const MainStack = ({isConnect = false}) => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          gestureEnabled: false,
          transitionSpec: {
            open: config,
            close: config,
          },
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        {isConnect ? (
          <>
            <Stack.Screen name="Home" component={Home} />
          </>
        ) : (
          <>
            <Stack.Screen name="Initial" component={Initial} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Settings" component={Settings} />
            <Stack.Screen
              name="DriverReplacement"
              component={DriverReplacement}
            />
            <Stack.Screen name="CarReplacement" component={CarReplacement} />
            <Stack.Screen name="EditComments" component={EditComments} />
            <Stack.Screen name="EditTrip" component={EditTrip} />
            <Stack.Screen
              name="EditScheduleTrip"
              component={EditScheduleTrip}
            />
            <Stack.Screen name="FilterTrips" component={FilterTrips} />
            <Stack.Screen name="NewTripStack" component={NewTripStack} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainStack;
