export const createPayload = data => {
	const res = Object.keys(data).reduce((acc, curr) => {
		acc[`$${curr}`] = data[curr];
		return acc;
	}, {});
	return res;
};
