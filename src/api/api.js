import {getUniqueId} from 'react-native-device-info';
import {instance} from './middleware';

import querystring from 'querystring';
import {createPayload} from './utilis';

const uuid = getUniqueId();

export const login = async (payload) => {
  const data = querystring.stringify({
    $company_code: payload.companyCode,
    $user_code: payload.userCode,
    $user_password: payload.password,
    $unique_id: uuid,
  });
  return instance.post(
    `https://myfleet.y-it.co.il/myfleet/ws_myfleet_Login`,
    data,
  );
};

export const loginByUUID = async () => {
  const data = querystring.stringify({
    $unique_id: uuid,
  });
  return instance.post(
    `https://myfleet.y-it.co.il/MyFleet/ws_MyFleet_By_UUID`,
    data,
  );
};

export const getTrips = async (payload) => {
  const {wsString, userToken, relativeDate, ...restProp} = payload;

  const data = querystring.stringify(
    createPayload({
      userToken,
      relativeDate,
      ...restProp,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_Lines`, data);
};

export const getClinets = async (payload) => {
  const {wsString, userToken} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_Clients`, data);
};

export const getDrivers = async (payload) => {
  const {wsString, userToken, relativeDate, onlyFree} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      relativeDate,
      onlyFree,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_Drivers`, data);
};

export const getCars = async (payload) => {
  const {wsString, userToken, relativeDate, onlyFree} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      relativeDate,
      onlyFree,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_Cars`, data);
};

export const getCarsType = async (payload) => {
  const {wsString, userToken} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_CarTypes`, data);
};

export const getLineTypes = async (payload) => {
  const {wsString, userToken} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
    }),
  );
  return instance.post(`${wsString}/ws_MyFleet_Get_LineTypes`, data);
};

export const setLineDriver = async (payload) => {
  const {
    wsString,
    userToken,
    lines,
    driverCode,
    action,
    updateCar = '1',
  } = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lines,
      driverCode,
      action,
      updateCar,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Driver`, data);
};

export const setLineCar = async (payload) => {
  const {
    wsString,
    userToken,
    lines,
    carCode,
    action,
    updateDriver = '1',
  } = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lines,
      carCode,
      action,
      updateDriver,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Car`, data);
};

export const getLineData = async (payload) => {
  const {wsString, userToken, lineCode} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lineCode,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Get_Line`, data);
};

export const setLineRemarks = async (payload) => {
  const {
    wsString,
    userToken,
    lines,
    shortRemarks,
    longRemarks,
    action,
  } = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lines,
      shortRemarks,
      longRemarks,
      action,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Remarks`, data);
};

export const sendTripsToDrivers = async (payload) => {
  const {wsString, userToken, lines, status, action} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lines,
      status,
      action,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Status`, data);
};

export const setIsActiveTrip = async (payload) => {
  const {wsString, userToken, lines, active, action} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
      lines,
      active,
      action,
    }),
  );
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Active`, data);
};

export const getCourses = async (payload) => {
  const {wsString, userToken, ...rest} = payload;
  const data = querystring.stringify(createPayload({userToken, ...rest}));
  return instance.post(`${wsString}/ws_myfleet_Get_Courses`, data);
};

export const setLineDetails = async (payload) => {
  const {wsString, userToken, ...restData} = payload;
  const data = querystring.stringify(createPayload({userToken, ...restData}));
  return instance.post(`${wsString}/ws_myfleet_Set_Line_Details`, data);
};

export const getLocation = async (payload) => {
  const {gpsServer, token, driverCode, accountCode, ...rest} = payload;
  const data = querystring.stringify({token, driverCode, accountCode, ...rest});
  return instance.post(`${gpsServer}/ws_MyFleet_Get_CurrentPos`, data);
};
export const getGpsData = async (payload) => {
  const {gpsServer, token, driverCode, accountCode, ...rest} = payload;
  const data = querystring.stringify({token, driverCode, accountCode, ...rest});
  return instance.post(`${gpsServer}/ws_MyFleet_Get_GPSData`, data);
};

export const setNewLine = async (payload) => {
  const {wsString, userToken, ...restData} = payload;
  const data = querystring.stringify(createPayload({userToken, ...restData}));
  return instance.post(`${wsString}/ws_MyFleet_Set_Line`, data);
};

export const deleteStationsAndPassengers = async (payload) => {
  const {
    wsString,
    userToken,
    lineCode,
    stationsCodes = '',
    passCodes = '',
  } = payload;
  const data = querystring.stringify(
    createPayload({userToken, lineCode, stationsCodes, passCodes}),
  );
  return instance.post(`${wsString}/ws_MyFleet_Del_Diary_Stations`, data);
};

export const getPassengersDetails = async (payload) => {
  const {wsString, userToken, clientCode} = payload;

  const data = querystring.stringify(createPayload({userToken, clientCode}));

  return instance.post(`${wsString}/ws_MyFleet_Get_Passengers_Details`, data);
};

export const setDiaryStation = async (payload) => {
  const {
    wsString,
    userToken,
    clientCode,
    lineCode,
    startTime,
    ...restData
  } = payload;

  const data = querystring.stringify(
    createPayload({
      userToken,
      clientCode,
      lineCode,
      startTime,
      ...restData,
    }),
  );

  return instance.post(`${wsString}/ws_MyFleet_Set_Diary_Stations`, data);
};

const GOOGLE_API_KEY = 'AIzaSyAj0nnRDdlFyLlonv0UhveXh3PFi-Teyas';

export const getPlacesByText = async ({
  type = '',
  input = '',
  language = 'he',
}) => {
  // const data = querystring.stringify(
  //   createPayload({
  //     userToken,
  //     clientCode,
  //     lineCode,
  //     startTime,
  //     ...restData,
  //   }),
  // );

  return instance.get(
    `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&language=${language}&types=${type}&key=${GOOGLE_API_KEY}`,
  );
};

export const getBranches = async (payload) => {
  const {wsString, userToken} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
    }),
  );

  const config = {isIgnorError: true};
  return instance.post(`${wsString}/ws_MyFleet_Get_Branches`, data, config);
};

export const logout = async (payload) => {
  const {userToken} = payload;
  const data = querystring.stringify(
    createPayload({
      userToken,
    }),
  );

  return instance.post(
    `https://interfaceserviceapi.y-it.co.il/wapdb/ws_myfleet_disconnect`,
    data,
  );
};

// wsString.replace(
// 	'http://proxy1.y-it.co.il:8080/wapdb/ws_Proxy/',
// 	'http://israelproxy.y-it.co.il/ProxyService.svc/Proxy?url='
// )
