import React from 'react';
import styled, { useTheme } from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
import Text from '../../common/Text/Text';

const Day = ({ dayName, date, isSelected = false, onPressDay }) => {
	const { colors } = useTheme();
	const color = isSelected ? colors.yellow : colors.gray;

	return (
		<s.Container onPress={onPressDay}>
			<Text numberOfLines={1} color={color}>
				{dayName}
			</Text>
			<Text color={color}>{date}</Text>
			<s.Point show={isSelected} />
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		min-width: ${calcWidth(35)}px;
		height: ${calcHeight(50)}px;
		align-items: center;
		align-self: center;
		justify-content: space-around;
		margin-horizontal: ${calcWidth(7)}px;
	`,
	Point: styled.View`
		width: ${calcWidth(4)}px;
		height: ${calcWidth(4)}px;
		border-radius: ${calcWidth(2)}px;
		${({ show, theme }) =>
			show ? `background-color: ${theme.colors.yellow}` : null}
	`,
};

export default Day;
