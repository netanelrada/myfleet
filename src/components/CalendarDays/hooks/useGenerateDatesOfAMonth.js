import { useState, useEffect } from 'react';
import { generateDatesOfAMonth } from '../../../utils/time';

const useGenerateDatesOfAMonth = (month, isRtl) => {
	const [daysData, setDaysData] = useState([]);
	useEffect(() => {
		let datesOfAMonth = generateDatesOfAMonth(month - 1);
		isRtl && datesOfAMonth.reverse();
		setDaysData(datesOfAMonth);
	}, [month]);

	return { daysData };
};

export default useGenerateDatesOfAMonth;
