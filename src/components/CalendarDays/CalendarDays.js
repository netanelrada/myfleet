import React, { useState, useEffect, useRef } from 'react';
import { FlatList, InteractionManager } from 'react-native';
import Day from './Day/Day';
import useGenerateDatesOfAMonth from './hooks/useGenerateDatesOfAMonth';
import { getDays, getCurrentDate } from '../../utils/time';
import { calcHeight, calcWidth } from '../../utils/dimensions';
import { useTranslation } from 'react-i18next';

const MIN_DAYS_MONTH = 29;

const CalendarDays = ({ month, selectedDay, onSetDay }) => {
	const { i18n } = useTranslation();
	const isRtl = i18n.dir() === 'rtl';
	const ref = useRef(null);
	const [currentDate, setSelectedDay] = useState(getCurrentDate);
	const { daysData } = useGenerateDatesOfAMonth(month, isRtl);
	const days = getDays();

	useEffect(() => {
		selectedDay && setSelectedDay(selectedDay);
	}, [selectedDay]);

	useEffect(() => {
		InteractionManager.runAfterInteractions(() => {
			ref.current.scrollToIndex({
				animated: true,
				index: getInitialScrollIndex(),
			});
		});
	}, [month, selectedDay]);

	const onPressDay = indexDay => {
		setSelectedDay(indexDay);
		onSetDay(indexDay);
	};

	const getItemLayout = (data, index) => ({
		length: calcHeight(50),
		offset: calcWidth(50) * index,
		index,
	});

	const renderItem = ({ item: { date, dayInWeek } }) => (
		<Day
			dayName={days[dayInWeek]}
			date={date}
			isSelected={date === currentDate}
			onPressDay={() => onPressDay(date)}
		/>
	);

	const initialScrollIndex = () => {
		let index = 0;
		if (isRtl) {
			const start = Math.abs(daysData.length - currentDate);
			index = Math.min(Math.max(0, daysData.length - 1), start - 2);
		} else {
			index = Math.max(0, currentDate - 3);
		}
		return index;
	};

	const getInitialScrollIndex = () => {
		let index = 0;
		if (isRtl) {
			const start = Math.max(Math.abs(MIN_DAYS_MONTH - selectedDay) - 2, 0);
			index = Math.min(MIN_DAYS_MONTH, start);
		} else {
			index = Math.max(0, selectedDay - 4);
		}

		return index;
	};

	return (
		<FlatList
			ref={ref}
			data={daysData}
			showsHorizontalScrollIndicator={false}
			horizontal
			getItemLayout={getItemLayout}
			keyExtractor={item => item.date.toString()}
			renderItem={renderItem}
		/>
	);
};

export default CalendarDays;
