import React from 'react';
import styled from 'styled-components';
import Text from '../common/Text/Text';
import {calcHeight, calcWidth} from '../../utils/dimensions';

const LineDescription = ({lineDescription = ''}) => {
  return (
    <s.Container>
      <Text textAlign="right">{lineDescription}</Text>
    </s.Container>
  );
};

const s = {
  Container: styled.View`
    width: ${calcWidth(332)}px;
    height: ${calcHeight(58)}px;
    background-color: ${({theme}) => theme.colors.white};
    border-radius: 9px;
    border: 1px solid ${({theme}) => theme.colors.gray7};
    padding: ${calcWidth(5)}px;
    margin-top: ${calcWidth(10)}px;
  `,
};
export default LineDescription;
