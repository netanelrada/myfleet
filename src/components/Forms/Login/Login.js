import React, { useContext, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import FormInput from './FormInput/FormInput';
import s from './styles';
import Button from '../../common/Buttons/Button';
import Text from '../../common/Text/Text';
import { useTheme } from 'styled-components';
import { useTranslation } from 'react-i18next';
import { ConstansLoginFormFileds } from './LoginFormFileds';
import md5 from 'md5';
import { FillContext } from '../../../screens/Initial/Context';

const Login = ({ submit }) => {
	const theme = useTheme();
	const { t } = useTranslation();
	const { control, handleSubmit, errors, formState } = useForm();
	const [, setFill] = useContext(FillContext);
	const { dirtyFields } = formState;

	const numberValidFiled = Object.keys(dirtyFields).length;

	useEffect(() => {
		try {
			updateFill(numberValidFiled);
		} catch (error) {}
	}, [numberValidFiled]);

	const updateFill = numberValidFiled => {
		switch (numberValidFiled) {
			case 0:
				setFill(2);
				break;
			case 1:
				setFill(33);
				break;
			case 2:
				setFill(66);
				break;
			case 3:
				setFill(100);
				break;

			default:
				break;
		}
	};

	const propsButton = {
		content: (
			<Text fontSize='s16' fontWeight={500} color={theme.colors.white}>
				{t('connect')}
			</Text>
		),
		width: 264,
		height: 48,
		backgroundColor: theme.colors.blueLight,
	};

	const onSubmit = data => {
		try {
			const payload = {
				...data,
				password: md5(data.password),
			};
			submit(payload);
		} catch (error) {
			console.log(error);
		}
	};



	return (
		<s.Container>
			{ConstansLoginFormFileds().map(({ key, label, name, validation }) => (
				<FormInput
					key={key}
					control={control}
					name={name}
					labelName={label}
					errors={errors}
					validation={validation}
				/>
			))}
			<Button {...propsButton} onPress={handleSubmit(onSubmit)} />
		</s.Container>
	);
};

//Login.propTypes = {};

export default Login;
