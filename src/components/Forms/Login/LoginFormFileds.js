import i18n from '../../../i18n';
import { validationSechema } from '../validations';

export const ConstansLoginFormFileds = () => [
	{
		key: 1,
		name: 'companyCode',
		label: i18n.t('clientCode'),
		validation: {
			...validationSechema.required(i18n.t('fieldRequired')),
			...validationSechema.onlyNumber(i18n.t('onlyNumber')),
		},
	},
	{
		key: 2,
		name: 'userCode',
		label: i18n.t('employeeCode'),
		validation: {
			...validationSechema.required(i18n.t('fieldRequired')),
			...validationSechema.onlyNumber(i18n.t('onlyNumber')),
		},
	},
	{
		key: 3,
		name: 'password',
		label: i18n.t('password'),
		validation: {
			...validationSechema.required(i18n.t('fieldRequired')),
		},
	},
];
