import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	align-items: center;
	justify-content: space-around;
	flex: 1;
`;

export default styles;
