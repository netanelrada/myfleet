import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(264)}px;
	align-items: flex-end;
`;

export default styles;
