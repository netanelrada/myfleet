import React from 'react';
import { Controller } from 'react-hook-form';
import s from './styles';
import Text from '../../../common/Text/Text';
import Input from './Input/Input';
import { useTheme } from 'styled-components';
import { useTranslation } from 'react-i18next';

const FormInput = ({
	labelName = '',
	errors = false,
	control,
	name,
	validation,
}) => {
	const theme = useTheme();
	const { t } = useTranslation();

	return (
		<s.Container>
			<Text fontSize='s14'>{labelName}</Text>
			<Controller
				control={control}
				render={({ onChange, onBlur, value = '' }) => (
					<Input
						onBlur={onBlur}
						onChangeText={value => onChange(value)}
						value={value}
						isPassword={name === 'password'}
						isRequiredError={errors[name] && errors[name].type === 'required'}
					/>
				)}
				name={name}
				rules={{
					...validation,
				}}
				defaultValue=''
			/>

			{errors[name] && (
				<Text color={theme.colors.errorColor} fontSize='s14'>
					{errors[name].message}
				</Text>
			)}
		</s.Container>
	);
};

export default FormInput;
