import styled from 'styled-components';
import { calcWidth } from '../../../../../utils/dimensions';

const styles = {};

styles.View = styled.View`
	flex-direction: row;
	align-items: center;
`;

styles.IconContainer = styled.TouchableOpacity`
	position: absolute;
	left: ${calcWidth(10)}px;
	z-index: 999;
`;

styles.TextInput = styled.TextInput`
	width: ${calcWidth(264)}px;
	aspect-ratio: ${264 / 44};
	border-radius: 4px;
	letter-spacing: 10px;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	border: 1px solid
		${({ theme, isRequiredError }) =>
			isRequiredError
				? theme.colors.errorColor
				: theme.colors.borderColorInput};
	align-items: center;
	justify-content: center;
`;

export default styles;
