import React, { useState } from 'react';
import s from './styles';
import { icons } from '../../../../../assets';
import { PlatfromOS } from '../../../../../utils/utilis';

const Input = props => {
	const [isVisable, setisVisable] = useState(props.isPassword);

	return (
		<s.View>
			{props.isPassword && (
				<s.IconContainer onPress={() => setisVisable(preState => !preState)}>
					{isVisable ? <icons.dont_see /> : <icons.see />}
				</s.IconContainer>
			)}
			<s.TextInput
				keyboardType={props.isPassword ? 'default' : 'numeric'}
				secureTextEntry={isVisable}
				textAlign='right'
				{...props}
			></s.TextInput>
		</s.View>
	);
};

export default Input;
