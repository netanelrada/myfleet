export const validationSechema = {
  required: (message) => {
    return {required: {value: true, message}};
  },
  onlyNumber: (message) => {
    return {validate: (data) => (isNaN(data) ? message : true)};
  },
};
