import React from 'react';
import s from './styles';
import Text from '../../common/Text/Text';
import { useTranslation } from 'react-i18next';
import Button from '../../common/Buttons/Button';
import { useTheme } from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
import { Modal } from 'react-native';

const IOSTimePicker = ({ children, onPress = () => {}, label = '' }) => {
	const { t } = useTranslation();
	const { colors } = useTheme();
	const styleBtn = {
		width: 360,
		height: calcHeight(44),
		backgroundColor: colors.blueLight,
	};
	return (
		<Modal visible={true} transparent={true}>
			<s.Container>
				<s.SafeAreaView>
					<s.View>
						<Text textAlign='center'>{label}</Text>
						{children}
						<Button
							onPress={onPress}
							content={
								<Text
									fontSize='s16'
									fontFamily='Rubik-Medium'
									fontWeight='500'
									color='white'
								>
									{t('ok')}
								</Text>
							}
							{...styleBtn}
						/>
					</s.View>
				</s.SafeAreaView>
			</s.Container>
		</Modal>
	);
};

export default IOSTimePicker;
