import styled from 'styled-components';
const styles = {};

styles.Container = styled.View`
	flex: 1;
	background-color: ${({ theme }) => theme.colors.transparentBlack};
`;

styles.SafeAreaView = styled.SafeAreaView`
	flex: 1;
`;

styles.View = styled.View`
	background-color: ${({ theme }) => theme.colors.white};
	position: absolute;
	bottom: 0;
`;

export default styles;
