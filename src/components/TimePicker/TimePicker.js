import React, {useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {PlatfromOS} from '../../utils/utilis';
import IOSTimePicker from './IOSTimePicker/IOSTimePicker';

const TimePicker = ({onConfirm = () => {}, label = ''}) => {
  const [date, setDate] = useState(new Date());

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  const onClickOK = () => {
    onConfirm(date);
  };

  const onClickOkAndroid = (event, selectedDate) => {
    //const currentDate = selectedDate || date;
    onConfirm(selectedDate);
  };

  const getDateTimePicker = () => {
    if (PlatfromOS === 'ios') {
      return (
        <IOSTimePicker label={label} onPress={onClickOK}>
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode="time"
            is24Hour={true}
            display="spinner"
            onChange={onChange}
          />
        </IOSTimePicker>
      );
    } else {
      return (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode="time"
          is24Hour={true}
          display="clock"
          onChange={onClickOkAndroid}
        />
      );
    }
  };
  return <>{getDateTimePicker()}</>;
};

export default TimePicker;
