import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';

const styles = {};

styles.Container = styled.ScrollView`
	width: ${calcWidth(265)}px;
	padding-horizontal: ${calcWidth(5)}px;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	color: ${({ theme }) => theme.colors.gray6};
`;

styles.Row = styled.View`
	flex-direction: row-reverse;
	min-height: ${calcHeight(45)}px;
`;

styles.Hr = styled.View`
	border-bottom-color: rgba(0, 0, 0, 0.12);
	border-bottom-width: 1px;
	width: 100%;
	margin-top: 5px;
`;

styles.ErrorsContainer = styled.View`
	align-items: flex-end;
	margin-top: 5px;
`;
styles.AlertsContainer = styled.View`
	margin-top: 5px;
	align-items: flex-end;
`;

export default styles;
