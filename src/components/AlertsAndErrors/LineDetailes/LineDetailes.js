import React from 'react';
import {ScrollView} from 'react-native';
import s from './styles';
import {useTranslation} from 'react-i18next';
import Text from '../../common/Text/Text';

const LineDetailes = ({
  lineDescription = '',
  startTime = '',
  errors = [],
  alerts = [],
  mssages = [],
}) => {
  const {t} = useTranslation();
  const [_, sTime] = startTime.split(' ');

  const isErrorsEmpty = errors.length === 0;
  const isAlertsEmpty = alerts.length === 0;

  return (
    <s.Container>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text fontWeight="500" textAlign="right">
          <s.Label>{`${t('lineDescription')}:`}</s.Label> {lineDescription}
        </Text>
        <Text fontWeight="500" textAlign="right">
          <s.Label>{`${t('startTime')}:`}</s.Label> {sTime}
        </Text>
        <s.Hr />
        {!isErrorsEmpty && (
          <s.ErrorsContainer>
            <Text fontWeight="500" fontFamily="Rubik-Medium" fontSize="s15">
              {t('errors')}
            </Text>
            {errors.map(({type}, i) => (
              <Text key={i} textAlign="right">{`• ${mssages[type]}`}</Text>
            ))}
          </s.ErrorsContainer>
        )}
        {!isAlertsEmpty && (
          <s.AlertsContainer>
            <Text fontWeight="500" fontFamily="Rubik-Medium" fontSize="s15">
              {t('alerts')}
            </Text>
            {alerts.map(({type}, i) => (
              <Text key={i} textAlign="right">{`• ${mssages[type]}`}</Text>
            ))}
          </s.AlertsContainer>
        )}
      </ScrollView>
    </s.Container>
  );
};

export default LineDetailes;
