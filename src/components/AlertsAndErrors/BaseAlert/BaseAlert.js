import React, { Children } from 'react';
import Modal from 'react-native-modal';
import s from './styles';
import Icon from '../../common/Icon/Icon';

const BaseAlert = ({
	isVisible = false,
	children,
	iconName = 'warning',
	backgroundColorModal = 'white',
}) => {
	return (
		<Modal
			style={{ justifyContent: 'center', alignItems: 'center' }}
			isVisible={isVisible}
			animationOutTiming={700}
			animationInTiming={1000}
			useNativeDriver={true}
			hideModalContentWhileAnimating={true}
		>
			<s.Container>
				<s.IconContainer backgroundColor={backgroundColorModal}>
					<Icon name={iconName} />
				</s.IconContainer>
				{children}
			</s.Container>
		</Modal>
	);
};

export default BaseAlert;
