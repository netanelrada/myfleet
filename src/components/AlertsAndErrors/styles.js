import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../utils/dimensions';
import { TouchableOpacity } from 'react-native';

const styles = {};

styles.Container = styled.View`
	width: 100%;
	flex: 1;
`;

styles.IndacitorContainer = styled.View`
	position: absolute;
	right: ${calcWidth(16)};
	top: ${calcWidth(8)};
`;

styles.BodyContainer = styled.View`
	align-items: center;
	margin-top: 15%;
	justify-content: space-around;
	flex: 1;
	width: 100%;
	flex-direction: row;
`;

styles.ButtonsContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-around;
	align-items: center;
	margin-vertical: ${calcHeight(15)}px;
	width: 100%;
`;

styles.TouchableOpacity = styled.TouchableOpacity``;

styles.ScrollContainer = styled.View`
	flex: 0.8;
`;
styles.NextContainer = styled.TouchableOpacity`
	flex: 0.1;
	align-items: center;
`;
styles.PrevContainer = styled.TouchableOpacity`
	flex: 0.1;
	align-items: center;
`;

export default styles;
