import React, {useState, useRef, useEffect} from 'react';
import {ScrollView, FlatList} from 'react-native';
import BaseAlert from './BaseAlert/BaseAlert';
import {useTheme} from 'styled-components';
import s from './styles';
import Text from '../common/Text/Text';
import {useTranslation} from 'react-i18next';
import LineDetailes from './LineDetailes/LineDetailes';
import Icon from '../common/Icon/Icon';
import {calcWidth} from '../../utils/dimensions';

const AlertsAndErrors = ({
  isVisible = false,
  closeModal,
  res = {
    errors: 0,
    alerts: 0,
    lines: [],
  },
  errorMessages = [],
  onConfirm = () => {},
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const [currIndex, setcurrIndex] = useState(0);
  const flatListRef = useRef(null);
  const lengthList = Math.max(res.lines.length - 1, 0);
  const isError = Boolean(res.errors);

  useEffect(() => {
    setcurrIndex(0);
  }, [isVisible]);
  const modalData = {
    iconName: isError ? 'warning' : 'alert',
    backgroundColorModal: isError ? colors.errorColor : colors.yellowLight,
  };

  const arrowIconSize = {
    width: 20,
    height: 60,
  };

  const handleOnScroll = (e) => {
    // 	//	try {
    // 	// 	console.log('e', e);
    // 	// 	let contentOffset = e.nativeEvent.contentOffset;
    // 	// 	let viewSize = e.nativeEvent.layoutMeasurement;
    // 	// 	// Divide the horizontal offset by the width of the view to see which page is visible
    // 	// 	let pageNum = Math.round(contentOffset.x / viewSize.width);
    // 	// 	console.log('pageNum', pageNum);
    // 	// 	setcurrIndex(pageNum);
    // 	// } catch (error) {
    // 	// 	console.log('error', error);
    // 	// }
  };

  const getItemLayout = (data, index) => ({
    he: calcWidth(270),
    offset: calcWidth(270) * index,
    index,
  });

  const onPressNext = () => {
    const newIndex = currIndex + 1;

    if (newIndex <= lengthList) {
      const scrollToData = {animated: true, index: newIndex};
      flatListRef.current.scrollToIndex(scrollToData);
      setcurrIndex(newIndex);
    }
  };
  const onPressPrev = () => {
    const newIndex = currIndex - 1;
    if (newIndex >= 0) {
      const scrollToData = {animated: true, index: newIndex};
      flatListRef.current.scrollToIndex(scrollToData);
      setcurrIndex(newIndex);
    }
  };

  return (
    <BaseAlert isVisible={isVisible} {...modalData}>
      <s.Container>
        <s.IndacitorContainer>
          <Text color={colors.darkGray}>{`${currIndex}/${lengthList}`}</Text>
        </s.IndacitorContainer>

        <s.BodyContainer>
          <s.PrevContainer onPress={onPressPrev}>
            <Icon name="leftArrow" {...arrowIconSize} />
          </s.PrevContainer>
          <s.ScrollContainer>
            <FlatList
              data={res.lines}
              horizontal
              scrollEnabled={false}
              pagingEnabled
              ref={flatListRef}
              getItemLayout={(data, index) => ({
                length: calcWidth(265),
                offset: calcWidth(265) * index,
                index,
              })}
              onScroll={(e) => handleOnScroll(e)}
              renderItem={({
                item: {lineDescription, startTime, errors, alerts},
                index,
              }) => (
                <LineDetailes
                  key={index}
                  lineDescription={lineDescription}
                  startTime={startTime}
                  errors={errors}
                  alerts={alerts}
                  mssages={errorMessages}
                />
              )}
              keyExtractor={(_, index) => index.toString()}
            />
          </s.ScrollContainer>
          <s.NextContainer onPress={onPressNext}>
            <Icon name="rightArrow" {...arrowIconSize} />
          </s.NextContainer>
        </s.BodyContainer>
        <s.ButtonsContainer>
          {!isError && (
            <s.TouchableOpacity onPress={onConfirm}>
              <Text
                color={colors.blueLight}
                fontSize="s16"
                fontFamily="Rubik-Medium">
                {t('doneAnyWay')}
              </Text>
            </s.TouchableOpacity>
          )}

          <s.TouchableOpacity onPress={closeModal}>
            <Text fontSize="s16" fontFamily="Rubik-Medium">
              {t('close')}
            </Text>
          </s.TouchableOpacity>
        </s.ButtonsContainer>
      </s.Container>
    </BaseAlert>
  );
};

export default AlertsAndErrors;
