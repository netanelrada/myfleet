import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${({ width }) => calcWidth(width)}px;
	height: ${calcWidth(44)}px;
	border: 1px solid
		${({ theme: { colors }, isValidate }) =>
			isValidate ? colors.borderColorInput : colors.errorColor};
	flex-direction: row-reverse;
	border-radius: 4px;
`;
styles.InputContainer = styled.View`
	flex-direction: row-reverse;
	flex: 0.75;
	justify-content: center;
	align-items: center;
`;

styles.Input = styled.TextInput`
	height: ${calcHeight(44)}px;
	width: 40%;
	height: 100%;
	justify-content: center;
	align-items: center;
	text-align: center;
`;

styles.IconContainer = styled.TouchableOpacity`
	justify-content: center;
	align-items: center;
	height: 100%;
	flex: 0.25;
	border-end-width: 1px;
	border-end-color: ${({ theme }) => theme.colors.borderColorInput};
`;

export default styles;
