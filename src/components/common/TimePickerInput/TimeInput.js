import React, { useState, useEffect } from 'react';
import { Keyboard } from 'react-native';
import s from './styles';
import Icon from '../Icon/Icon';
import Text from '../Text/Text';
import TimePicker from '../../TimePicker/TimePicker';
import { formatToTwoDig, validateTime } from '../../../utils/time';
import { PlatfromOS } from '../../../utils/utilis';

const getHoursAndMinutes = time => {
	const [hours, minutes] = time.split(':');
	const min = minutes ? minutes : '00';
	const hour = hours ? hours : '00';
	return { hour, min };
};

const TimePickerInput = ({
	initialTime,
	label,
	updateTime = () => {},
	isValid = true,
	width = 142,
	onChange,
}) => {
	const [time, setTime] = useState(getHoursAndMinutes(initialTime));
	const [isShowTimePicker, setIsShowTimePicker] = useState(false);
	const [isValidate, setisValidate] = useState(true);

	useEffect(() => {
		const fullTime = time.hour + ':' + time.min;
		updateTime({
			time: fullTime,
			isValid: true,
		});
		onChange && onChange(fullTime);
		setisValidate(true);
	}, [time]);

	useEffect(() => {
		!isValid && setisValidate(false);
	}, [isValid]);

	const onSetTimeByTimePicker = timeSelected => {
		setIsShowTimePicker(false);
		if (!timeSelected) return;
		onUpdateTime(timeSelected);
	};

	const onUpdateTime = timeSelected => {
		const min = formatToTwoDig(timeSelected.getMinutes()).toString();
		const hour = formatToTwoDig(timeSelected.getHours()).toString();
		setTime({
			min,
			hour,
		});
	};
	const openTimePicker = () => {
		Keyboard.dismiss();
		setIsShowTimePicker(true);
	};

	const isOnlyNumber = val => /^\d+$/.test(val) || val === '';
	const keyboardType = PlatfromOS === 'ios' ? 'number-pad' : 'phone-pad';
	return (
		<s.Container width={width} isValidate={isValidate}>
			<s.InputContainer>
				<s.Input
					value={time.min}
					onChangeText={text =>
						isOnlyNumber(text) &&
						setTime(preState => ({ ...preState, min: text }))
					}
					keyboardType={keyboardType}
					maxLength={2}
				/>
				<Text color='gray'>:</Text>
				<s.Input
					value={time.hour}
					onChangeText={text =>
						isOnlyNumber(text) &&
						setTime(preState => ({ ...preState, hour: text }))
					}
					keyboardType={keyboardType}
					maxLength={2}
				/>
			</s.InputContainer>
			<s.IconContainer onPress={openTimePicker}>
				<Icon name='blueClock' />
			</s.IconContainer>

			{isShowTimePicker && (
				<TimePicker onConfirm={onSetTimeByTimePicker} label={label} />
			)}
		</s.Container>
	);
};

export default TimePickerInput;
