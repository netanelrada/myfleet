import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const Text = ({
	children,
	textAlign = null,
	fontSize = 's14',
	color = null,
	fontFamily = 'Rubik-Regular',
	uppercase = null,
	lineHeight = null,
	numberOfLines = null,
	fontWeight = 'normal',
	underLine = null,
	capitalize = null,
}) => {
	return (
		<>
			<styles.Text
				textAlign={textAlign}
				fontSize={fontSize}
				color={color}
				fontFamily={fontFamily}
				uppercase={uppercase}
				lineHeight={lineHeight}
				numberOfLines={numberOfLines}
				fontWeight={fontWeight}
				underLine={underLine}
				capitalize={capitalize}
			>
				{children}
			</styles.Text>
		</>
	);
};

// Text.prototype = {
// 	children: PropTypes.element.isRequired,
// 	textAlign: PropTypes.string,
// 	fontSize: PropTypes.number,
// 	color: PropTypes.string,
// 	fontFamily: PropTypes.string,
// 	uppercase: PropTypes.bool,
// 	lineHeight: PropTypes.number,
// 	numberOfLines: PropTypes.number,
// 	fontWeight: PropTypes.number,
// 	underLine: PropTypes.bool,
// 	capitalize: PropTypes.bool,
// };

export default Text;
