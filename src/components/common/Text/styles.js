import styled from 'styled-components';
const styles = {};

styles.Text = styled.Text`
	font-weight: ${({ fontWeight }) => fontWeight};
	font-size: ${({ theme, fontSize }) => theme.fontSizes[fontSize]}px;
	color: ${({ color, theme }) => (color ? color : theme.colors.colorText)};
	${({ fontFamily }) => fontFamily && `font-family: ${fontFamily};`}
	${({ textAlign }) => textAlign && ` text-align: ${textAlign};`}
	${({ uppercase }) => uppercase && ` text-transform: uppercase;`}
	${({ capitalize }) => capitalize && `text-transform: capitalize;`}
	${({ underLine }) => underLine && `text-decoration-line: underline;`}
	${({ lineHeight }) => lineHeight && `line-height: ${lineHeight}px;`}
`;

export default styles;
