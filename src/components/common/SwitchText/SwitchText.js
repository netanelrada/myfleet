import React, {useState} from 'react';
import {Switch} from 'react-native-switch';
import {useTranslation} from 'react-i18next';
import {useTheme} from 'styled-components';
import {calcWidth} from '../../../utils/dimensions';

const SwitchText = ({state = false, setState = () => {}}) => {
  const {t} = useTranslation();
  const {fontSizes, colors} = useTheme();

  return (
    <Switch
      value={state}
      onValueChange={(val) => setState(val)}
      disabled={false}
      activeText={t('big')}
      inActiveText={t('normal')}
      backgroundActive={colors.gray6}
      backgroundInactive={colors.gray6}
      circleSize={calcWidth(35)}
      circleActiveColor="white"
      circleInActiveColor="white"
      switchWidthMultiplier={2}
      switchLeftPx={5}
      switchRightPx={5}
      activeTextStyle={{
        color: 'black',
        fontFamily: 'Rubik-regular',
        fontSize: t('big').length > 6 ? 8 : fontSizes.s12,
        textAlign: 'center',
      }}
      inactiveTextStyle={{
        color: 'black',
        fontFamily: 'Rubik-regular',
        fontSize: t('normal').length > 6 ? 8 : fontSizes.s12,
        textAlign: 'center',
      }}
    />
  );
};

export default SwitchText;
