import React, {useRef, useEffect} from 'react';
import {Animated} from 'react-native';

const statusLoopE = {
  INITIAL: 'INITIAL',
  END: 'END',
  FINISH: 'FINISH',
};
export default function RotateAnimation({children, isToggle}) {
  const rotateAnimation = useRef(new Animated.Value(0));
  const translateX = useRef(new Animated.Value(0));
  const statusLoop = useRef(statusLoopE.INITIAL);

  useEffect(() => {
    startLoop();
    setTimeout(() => {
      stopLoop();
    }, 4000);

    return () => stopLoop();
  }, []);

  useEffect(() => {
    handleAnimation(Number(isToggle));
  }, [isToggle]);

  const startLoop = () => {
    Animated.timing(translateX.current, {
      toValue: statusLoop.current === statusLoopE.INITIAL ? 1 : 0,
      duration: 650,
      useNativeDriver: true,
    }).start(() => {
      if (statusLoop.current === statusLoopE.FINISH) return;
      statusLoop.current =
        statusLoop.current === statusLoopE.INITIAL
          ? statusLoopE.END
          : statusLoopE.INITIAL;
      startLoop();
    });
  };

  const stopLoop = () => {
    statusLoop.current = statusLoopE.FINISH;
    translateX.current.setValue(0.5);
  };
  const handleAnimation = async (toValue = 0) => {
    Animated.timing(rotateAnimation.current, {
      toValue,
      duration: 100,
      useNativeDriver: true,
    }).start();
  };

  const interpolateRotating = rotateAnimation.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['180deg', '0deg'],
  });

  return (
    <Animated.View
      style={{
        transform: [
          {
            rotateZ: interpolateRotating,
          },
          {
            translateX: translateX.current.interpolate({
              inputRange: [0, 0.5, 1],
              outputRange: [5, 0, -5],
            }),
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
}
