import React from 'react';
import styled from 'styled-components';

const Overlay = ({isVisable = false, onBackgroundClick = () => {}}) => {
  return (
    <>{isVisable && <s.Container onPress={onBackgroundClick}></s.Container>}</>
  );
};

const s = {
  Container: styled.Pressable`
    position: absolute;
    right: 0;
    left: 0;
    bottom: 0;
    top: 0;
    background-color: ${({theme}) => theme.colors.borderColorInput};
  `,
};

export default Overlay;
