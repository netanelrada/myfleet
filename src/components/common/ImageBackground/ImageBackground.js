import React from 'react';
import styled from 'styled-components';
import { images } from '../../../assets/index';

const ImageBackground = ({ children }) => {
	return (
		<s.ImageBackground source={images.backgroundSplash}>
			<s.SafeAreaView>{children}</s.SafeAreaView>
		</s.ImageBackground>
	);
};

const s = {
	SafeAreaView: styled.SafeAreaView`
		flex: 1;
	`,
	ImageBackground: styled.ImageBackground`
		flex: 1;
	`,
};

export default ImageBackground;
