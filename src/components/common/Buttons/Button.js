import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';

const Button = ({ onPress, content, ...style }) => {
	return (
		<s.Btn onPress={onPress} {...style}>
			{content}
		</s.Btn>
	);
};

const s = {
	Btn: styled.TouchableOpacity`
		width: ${({ width }) => calcWidth(width)}px;
		aspect-ratio: ${({ width, height }) => width / height};
		${({ backgroundColor }) =>
			backgroundColor && `background-color: ${backgroundColor}`};
		border-radius: ${({ borderRadius }) => borderRadius}px;
		justify-content: center;
		align-items: center;
		${({ borderWidth }) => borderWidth && `border-width: ${borderWidth}px`};
		${({ borderColor }) => borderColor && `border-color: ${borderColor}`};
	`,
};

Button.propTypes = {
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	backgroundColor: PropTypes.string,
	content: PropTypes.element,
	borderRadius: PropTypes.number,
};

Button.defaultProps = {
	width: 264,
	height: 48,
	borderRadius: 4,
};

export default Button;
