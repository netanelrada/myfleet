import React from 'react';
import { Switch } from 'react-native';

const CustomSwitch = ({
	color = {
		true: '#BEBEBE',
		false: '#FCCB00',
	},
	toggleSwitch = () => {},
	isEnabled = false,
}) => {
	return (
		<Switch
			style={{ flexDirection: 'row-reverse' }}
			trackColor={{ true: '#EBE6E6', false: '#BEBEBE' }}
			thumbColor={!isEnabled ? '#BEBEBE' : '#FFEDB1'}
			onValueChange={toggleSwitch}
			value={!isEnabled}
		/>
	);
};

export default CustomSwitch;
