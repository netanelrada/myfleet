import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
  width: ${calcWidth(296)}px;
  min-height: ${calcHeight(160)}px;
  flex-direction: column;
  justify-content: center;
  border-radius: 9px;
  background-color: ${({theme}) => theme.colors.white};
`;

styles.IconContainer = styled.View`
  width: ${calcWidth(72)}px;
  aspect-ratio: 1;
  border-radius: ${calcWidth(36)}px;
  background-color: ${({theme}) => theme.colors.yellowLight};
  position: absolute;
  top: ${-calcHeight(35)}px;
  align-self: center;
  align-items: center;
  justify-content: center;
`;

styles.BodyContainer = styled.View`
  align-items: center;
  margin-top: 15%;
  justify-content: space-around;
  flex: 1;
`;
styles.ChildrenContainer = styled.View``;

styles.ButtonsContainer = styled.View`
  flex-direction: row-reverse;
  justify-content: space-around;
  align-items: center;
  margin-vertical: ${calcHeight(15)}px;
  width: 100%;
`;

export default styles;
