import React from 'react';
import Modal from 'react-native-modal';
import s from './styles';
import Button from '../Buttons/Button';
import Text from '../Text/Text';
import { useTheme } from 'styled-components';
import Icon from '../Icon/Icon';
import { calcWidth } from '../../../utils/dimensions';

const ModalConfirm = ({
	onConfirm = () => {},
	onCancel = () => {},
	isVisible = false,
	children,
	iconName = 'warning',
	textConfirnBtn = 'OK',
	textCancelBtn = 'CANCEL',
	isCancelHidden,
	isConfirmHidden = false,
	iconStyle = {},
	isExtraBtnHidden = true,
	onPressExtraBtn = () => {},
	textExtraBtn = '',
	onModalHide = () => {},
}) => {
	const theme = useTheme();
	const { blueLight, darkGray } = theme.colors;
	const sharedStyleBtn = {
		size: { width: calcWidth(70), height: 24 },
		textStyle: { fontSize: 's14', fontWeight: 500 },
	};
	const propsConfirmButton = {
		content: (
			<Text {...sharedStyleBtn.textStyle} color={blueLight}>
				{textConfirnBtn}
			</Text>
		),
		...sharedStyleBtn.size,
	};

	const propsCancelButton = {
		content: (
			<Text
				{...sharedStyleBtn.textStyle}
				color={isConfirmHidden ? blueLight : darkGray}
			>
				{textCancelBtn}
			</Text>
		),
		...sharedStyleBtn.size,
	};

	const propsExtraButton = {
		content: (
			<Text
				{...sharedStyleBtn.textStyle}
				color={isConfirmHidden ? blueLight : darkGray}
			>
				{textExtraBtn}
			</Text>
		),
		...sharedStyleBtn.size,
	};

	return (
		<Modal
			style={{ justifyContent: 'center', alignItems: 'center' }}
			isVisible={isVisible}
			animationOutTiming={700}
			animationInTiming={1000}
			useNativeDriver={true}
			hideModalContentWhileAnimating={true}
			onModalHide={onModalHide}
		>
			<s.Container>
				<s.IconContainer>
					<Icon name={iconName} {...iconStyle} />
				</s.IconContainer>
				<s.BodyContainer>
					<s.ChildrenContainer>{children}</s.ChildrenContainer>
					<s.ButtonsContainer>
						{!isConfirmHidden && (
							<Button onPress={onConfirm} {...propsConfirmButton} />
						)}
						{!isCancelHidden && (
							<Button onPress={onCancel} {...propsCancelButton} />
						)}
						{!isExtraBtnHidden && (
							<Button onPress={onPressExtraBtn} {...propsExtraButton} />
						)}
					</s.ButtonsContainer>
				</s.BodyContainer>
			</s.Container>
		</Modal>
	);
};

export default ModalConfirm;
