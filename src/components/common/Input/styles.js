import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
const styles = {};

styles.View = styled.View`
	flex-direction: row;
	align-items: center;
	${({ width }) => width && `width: ${calcWidth(width)}}px;`}
	height: ${({ height }) => calcHeight(height)}px;
	${({ backgroundColor }) =>
		backgroundColor && `background-color: ${backgroundColor}`};
	border-radius: ${({ borderRadius }) => borderRadius}px;
	padding-horizontal: ${calcWidth(5)}px;
`;

styles.IconContainer = styled.TouchableOpacity`
	flex: 0.1;
	align-items: center;
`;

styles.TextInput = styled.TextInput`
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	height: ${({ height }) => calcHeight(height)}px;
	padding-vertical: ${calcWidth(5)}px;
	flex: 0.9;
`;

export default styles;
