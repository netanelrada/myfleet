import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';
import s from './styles';

const Input = ({
	onChangeText,
	value = '',
	placeholder = '',
	iconName = null,
	...style
}) => {
	return (
		<s.View {...style}>
			<s.IconContainer>{iconName && <Icon name={iconName} />}</s.IconContainer>
			<s.TextInput
				onChangeText={text => onChangeText(text)}
				value={value}
				placeholder={placeholder}
				textAlign='right'
				{...style}
			></s.TextInput>
		</s.View>
	);
};

Input.propTypes = {
	height: PropTypes.number.isRequired,
	backgroundColor: PropTypes.string,
	borderRadius: PropTypes.number,
};

Input.defaultProps = {
	height: 48,
	borderRadius: 4,
};

export default Input;
