import React from 'react';
import { icons } from '../../../assets';

export default function Icon({ name, ...style }) {
	const IconCom = icons[name];
	return <IconCom {...style} />;
}
