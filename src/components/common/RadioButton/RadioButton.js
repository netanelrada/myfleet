import React, { useState } from 'react';
import styled from 'styled-components';
import { calcWidth } from '../../../utils/dimensions';
import Text from '../Text/Text';

const RadioButton = ({ width = 24, isSelected, onSelect, label = '' }) => {
	return (
		<s.Container>
			<Text fontSize='s12' fontWeight='500'>
				{label}
			</Text>
			<s.RadioContainer width={width} onPress={onSelect}>
				{isSelected && <s.Selected width={width} />}
			</s.RadioContainer>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		flex-direction: row;
		align-items: center;
	`,
	RadioContainer: styled.TouchableOpacity`
		width: ${({ width }) => calcWidth(width)}px;
		aspect-ratio: 1;
		border-radius: ${({ width }) => calcWidth(width) / 2}px;
		border: 1px solid ${({ theme }) => theme.colors.borderRadioButton};
		background-color: ${({ theme }) => theme.colors.ghostWhite};
		justify-content: center;
		align-items: center;
		margin-horizontal: ${calcWidth(4)}px;
	`,
	Selected: styled.View`
		background-color: #b9d9fd;
		width: 70%;
		height: 70%;
		border-radius: ${({ width }) => calcWidth(width) / 2}px;
	`,
};

export default RadioButton;
