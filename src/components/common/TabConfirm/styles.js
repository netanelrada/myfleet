import styled from 'styled-components';
import { calcHeight } from '../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: 100%;
	height: ${calcHeight(56)}px;
	box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
	elevation: 3;
	flex-direction: row-reverse;
	justify-content: space-around;
	align-items: center;
	background-color: ${({ theme }) => theme.colors.white};
`;

styles.Btn = styled.TouchableOpacity`
	width: 30%;
	align-items: center;
`;

export default styles;
