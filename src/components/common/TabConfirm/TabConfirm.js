import React from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from 'styled-components';
import Text from '../Text/Text';
import s from './styles';

const TabConfirm = ({ onSave, onCancel }) => {
	const { t } = useTranslation();
	const { colors } = useTheme();

	return (
		<s.Container>
			<s.Btn onPress={onSave}>
				<Text fontWeight='500' color={colors.blueLight} fontSize='s16'>
					{t('save')}
				</Text>
			</s.Btn>
			<s.Btn onPress={onCancel}>
				<Text color={colors.darkGray} fontWeight='400' fontSize='s16'>
					{t('cancel')}
				</Text>
			</s.Btn>
		</s.Container>
	);
};

export default TabConfirm;
