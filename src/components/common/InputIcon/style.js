import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(320)}px;
`;

styles.LabelContainer = styled.View`
	align-self: flex-end;
`;

styles.InputContainer = styled.View`
	flex-direction: row;
	width: ${calcWidth(320)}px;
	height: ${calcHeight(44)}px;
	border: 1px solid ${({ theme }) => theme.colors.borderColorInput};
	border-radius: 4px;
`;

styles.Input = styled.TextInput`
	height: ${calcHeight(44)}px;
	height: 100%;
	flex: 0.9;
`;

styles.IconContainer = styled.View`
	justify-content: center;
	align-items: center;
	height: 100%;

	flex: 0.1;
	border-end-width: 1px;
	border-end-color: ${({ theme }) => theme.colors.borderColorInput};
`;

export default styles;
