import React from 'react';
import Icon from '../Icon/Icon';
import s from './style';
import Text from '../Text/Text';

const Input = ({
	iconName = 'blueSearch',
	label = 'נהג',
	onChangeText,
	value,
}) => {
	return (
		<s.Container>
			<s.LabelContainer>
				<Text>{label}</Text>
			</s.LabelContainer>
			<s.InputContainer>
				<s.IconContainer>
					<Icon name={iconName} />
				</s.IconContainer>
				<s.Input
					value={value}
					onChangeText={text => onChangeText(text)}
					textAlign='right'
				/>
			</s.InputContainer>
		</s.Container>
	);
};

export default Input;
