import React, {Children, useState, useEffect} from 'react';

const SelectOption = ({
  children,
  updateSelectedOption,
  value,
  options = [],
}) => {
  const onSetRadioSelected = (indexSelected, key) => {
    updateSelectedOption(key);
  };

  return Children.map(children, (child, index) =>
    React.cloneElement(child, {
      isSelected: index === options.findIndex((el) => el.key === value),
      onSelect: () => onSetRadioSelected(index, child.key),
    }),
  );
};

export default SelectOption;
