import React from 'react';
import {Platform} from 'react-native';
import NativeCheckBox from '@react-native-community/checkbox';
import {calcWidth} from '../../../utils/dimensions';

const CheckBox = ({onPressCheckBox = () => {}, value = false}) => {
  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(15)};

  return (
    <NativeCheckBox
      disabled={false}
      style={[Platform.OS === 'ios' ? checkBoxStyle : null]}
      value={value}
      onValueChange={(newValue) => onPressCheckBox(newValue)}
      onAnimationType="stroke"
    />
  );
};

export default CheckBox;
