import React from 'react';
import { Modal } from 'react-native';

const CustomModal = ({
	children,
	isVisable = false,
	style = {},
	onRequestClose = () => {},
}) => {
	return (
		<Modal
			animationType='slide'
			visible={isVisable}
			style={{ ...style }}
			onRequestClose={onRequestClose}
		>
			{children}
		</Modal>
	);
};

export default CustomModal;
