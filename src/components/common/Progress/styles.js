import styled from 'styled-components';
import { calcHeight } from '../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: 100%;
	height: ${calcHeight(10)}px;
`;

styles.Progress = styled.View`
	height: 100%;
	width: ${({ fill }) => fill + '%'};
	background-color: ${({ theme }) => theme.colors.blueLight};
`;

export default styles;
