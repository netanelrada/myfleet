import React, { useContext } from 'react';
import { FillContext } from '../../../screens/Initial/Context';
import styled from 'styled-components';
import s from './styles';

const Progress = () => {
	const [fill] = useContext(FillContext);

	return (
		<s.Container>
			<s.Progress fill={fill} />
		</s.Container>
	);
};

export default Progress;
