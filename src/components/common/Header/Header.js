import React from 'react';
import s from './styles';
import Icon from '../Icon/Icon';

const Header = ({ onBack = () => {}, title = '' }) => {
	return (
		<s.Container>
			<s.LeftContainer>
				<s.Title>{title}</s.Title>
				<s.Btn onPress={onBack}>
					<Icon name='back' />
				</s.Btn>
			</s.LeftContainer>
		</s.Container>
	);
};

export default Header;
