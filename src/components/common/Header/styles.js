import styled from 'styled-components';
import { calcWidth } from '../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: 100%;
	height: 10%;
	flex-direction: row-reverse;
	justify-content: space-between;
	align-items: center;
	padding-horizontal: ${calcWidth(15)}px;
`;

styles.LeftContainer = styled.View`
	flex-direction: row;
	align-items: center;
`;

styles.Btn = styled.TouchableOpacity`
	width: ${calcWidth(40)}px;
	aspect-ratio: 1;
	border-radius: 9px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	justify-content: center;
	align-items: center;
`;

styles.Title = styled.Text`
	font-family: Rubik-Regular;
	font-weight: 500;
	font-size: ${({ theme }) => theme.fontSizes.s18}px;
	margin-right: ${calcWidth(9)}px;
`;

export default styles;
