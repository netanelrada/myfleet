import React from 'react';
import { Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { calcHeight } from '../../../utils/dimensions';

const CustomKeyboardAvoidView = ({ children }) => {
	const height = Platform.OS === 'android' ? calcHeight(120) : calcHeight(60);
	return (
		<KeyboardAwareScrollView
			enableOnAndroid
			extraHeight={height}
			extraScrollHeight={Platform.OS === 'android' ? height : null}
			contentContainerStyle={{
				flexGrow: 1,
			}}
		>
			{children}
		</KeyboardAwareScrollView>
	);
};

export default CustomKeyboardAvoidView;
