import React from 'react';
import styled from 'styled-components';
import Text from '../Text/Text';
import { APP_VERSION_NUMBER } from '../../../utils/enums';
import { useTranslation } from 'react-i18next';
import Icon from '../Icon/Icon';
import { calcWidth } from '../../../utils/dimensions';

const NumberVersion = () => {
	const { t } = useTranslation();
	const iconSize = calcWidth(32);
	return (
		<s.Container>
			<Text textAlign='center'>{`${t('version')}: ${APP_VERSION_NUMBER}`}</Text>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		flex-direction: row;
		align-items: center;
	`,
};
export default NumberVersion;
