import React from 'react';
import styled from 'styled-components';
import { calcWidth } from '../../../utils/dimensions';
import Text from '../Text/Text';

const CountWrap = ({
	children,
	count = 1,
	top = 8,
	right = 8,
	isWithBorder = true,
	disable = false,
	width = 24,
}) => {
	return (
		<s.Container pointerEvents={disable ? 'none' : 'auto'}>
			{children}
			{count > 0 && (
				<s.CountContainer
					width={width}
					top={top}
					right={right}
					isWithBorder={isWithBorder}
				>
					<Text>{count}</Text>
				</s.CountContainer>
			)}
		</s.Container>
	);
};

const s = {
	Container: styled.View``,
	ChildrenContainer: styled.View`
		position: absolute;
		flex-direction: column;
	`,
	CountContainer: styled.View`
		width: ${({ width }) => calcWidth(width)}px;
		aspect-ratio: 1;
		border-radius: ${calcWidth(12)}px;
		background-color: ${({ theme }) => theme.colors.white};
		${({ isWithBorder, theme }) =>
			isWithBorder && `border: 1px solid ${theme.colors.blueLight}`}
		justify-content: center;
		align-items: center;
		position: absolute;
		right: ${({ right }) => -calcWidth(right)}px;
		top: ${({ top }) => -calcWidth(top)}px;
	`,
};

export default CountWrap;
