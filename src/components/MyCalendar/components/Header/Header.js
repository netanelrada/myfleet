import React from 'react';
import styled from 'styled-components';
import Icon from '../../../common/Icon/Icon';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';
import { useTranslation } from 'react-i18next';
import Text from '../../../common/Text/Text';
import moment from 'moment';
const Header = ({ pressClose = () => {}, pressToday = () => {} }) => {
	const { t } = useTranslation();

	return (
		<s.Container>
			<s.Today onPress={pressToday}>
				<Icon name='calendar' />
				<Text>{` ${t('today')} ${moment().format('D.M.YYYY')} `}</Text>
			</s.Today>

			<s.BtnClose onPress={pressClose}>
				<Icon name='close' />
			</s.BtnClose>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
		padding-horizontal: ${calcWidth(10)}px;
		margin-vertical: ${calcHeight(5)}px;
		width: 100%;
	`,
	BtnClose: styled.TouchableOpacity`
		width: ${calcWidth(60)}px;
		justify-content: center;
		align-items: flex-end;
		height: ${calcHeight(30)}px;
	`,
	Today: styled.TouchableOpacity`
		flex-direction: row;
		align-items: center;
		width: ${calcWidth(141)}px;
		justify-content: space-around;
		aspect-ratio: ${141 / 36};
		border: 1px solid rgba(180, 190, 201, 0.303017);
	`,
};

export default Header;
