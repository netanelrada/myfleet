import React, { useState, useEffect } from 'react';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Modal from 'react-native-modal';
import moment from 'moment';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import Text from '../common/Text/Text';
import { getMonths, getShortDays } from '../../utils/time';
import { useTheme } from 'styled-components';
import { calcHeight } from '../../utils/dimensions';
import Header from './components/Header/Header';
import { PlatfromOS } from '../../utils/utilis';
//
LocaleConfig.locales['he'] = {
	monthNames: getMonths(),
	monthNamesShort: getMonths(),
	dayNames: getShortDays(),
	dayNamesShort: getShortDays(),
	today: 'היום',
};
LocaleConfig.defaultLocale = 'he';

const MyCalendar = ({
	isVisible,
	setDate = () => {},
	closeCalender = () => {},
	initialDate = moment().format('YYYY-MM-DD'),
}) => {
	const { t } = useTranslation();
	const theme = useTheme();

	const slectedDay = moment(initialDate._i, 'D.M.YYYY').format('YYYY-MM-DD');
	const dayPress = date => {
		date !== slectedDay && closeCalender();
		setDate(date);
	};

	const onPressToday = () => {
		try {
			today !== slectedDay && setDate(today);
			closeCalender();
		} catch (error) {
			console.log(error);
		}
	};

	const today = moment().format('YYYY-MM-DD');
	//	const isToday = today === daySelected;

	return (
		<>
			<Modal
				style={{ justifyContent: 'center', alignItems: 'center' }}
				isVisible={isVisible}
				animationIn='slideInDown'
				animationOut='slideOutUp'
				animationOutTiming={600}
				animationInTiming={600}
				onBackdropPress={closeCalender}
				useNativeDriver={true}
				hideModalContentWhileAnimating={true}
			>
				<styles.Container>
					<Header pressClose={closeCalender} pressToday={onPressToday} />
					<Calendar
						theme={{
							'stylesheet.calendar.main': {
								header: {
									backgroundColor: theme.colors.backgroundColorModal,
									flexDirection: 'row-reverse',
									justifyContent: 'space-between',
									alignItems: 'center',
								},
								week: {
									marginVertical: calcHeight(7),
									flexDirection: 'row-reverse',
									justifyContent: 'space-around',
									backgroundColor: theme.colors.backgroundColorModal,

									fontSize: theme.fontSizes.s28,
									alignItems: 'center',
								},
							},
							'stylesheet.calendar.header': {
								week: {
									flexDirection: 'row-reverse',
									justifyContent: 'space-around',
									alignItems: 'center',
									fontSize: theme.fontSizes.s28,
									backgroundColor: theme.colors.backgroundColorModal,
								},
							},
							backgroundColor: 'red',
							textDayHeaderFontSize: theme.fontSizes.s16,
							backgroundColor: theme.colors.backgroundColorModal,
							calendarBackground: theme.colors.backgroundColorModal,
							textSectionTitleColor: theme.colors.colorText,
							dayTextColor: theme.colors.colorText,
							textDisabledColor: theme.colors.textDisabledColor,
							textDayFontSize: theme.fontSizes.s16,
							monthTextColor: theme.colors.colorText,
							textMonthFontSize: theme.fontSizes.s24,
							textMonthFontWeight: '500',
							textDayFontWeight: '400',
							textDayHeaderFontWeight: '500',
						}}
						markedDates={{
							marked: true,
						}}
						// Initially visible month. Default = Date()
						current={slectedDay}
						// Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined

						// Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined

						// Handler which gets executed on day press. Default = undefined
						onDayPress={day => {
						
							dayPress(day.dateString);
						}}
						// Handler which gets executed on day long press. Default = undefined
						onDayLongPress={day => {
							
						}}
						markedDates={{
							[slectedDay]: {
								selected: true,
								marked: false,
								selectedColor: '#3EA9D5',
							},
						}}
					/>
				</styles.Container>
			</Modal>
		</>
	);
};

export default MyCalendar;
