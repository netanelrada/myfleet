import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(318)}px;
	min-height: ${calcHeight(360)}px;
	background-color: ${({ theme }) => theme.colors.backgroundColorModal};
	justify-content: space-around;
	padding-vertical: ${calcHeight(5)}px;
	border-radius: 9px;
`;

export default styles;
