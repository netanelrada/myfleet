import React, { useRef, useState, useEffect, useMemo } from 'react';
import { Animated, InteractionManager, View, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/dimensions';

const DrawerAnimation = ({ children, isClick = false }) => {
	const [menuState, setmenuState] = useState(false);
	const animatedController = useRef(new Animated.ValueXY({ x: 0, y: 0 }))
		.current;
	const isOpen = useRef(false);

	useEffect(() => {
		toggleListItem();
	}, [isClick]);

	const configAnim = animatedController.y.interpolate({
		inputRange: [0, 1],
		outputRange: [0, calcHeight(384)],
	});

	const toggleListItem = () => {
		if (isOpen.current) {
			Animated.spring(animatedController, {
				duration: 300,
				toValue: 0,
				useNativeDriver: true,
			}).start();
		} else {
			Animated.timing(animatedController, {
				duration: 300,
				toValue: 1,
				useNativeDriver: true,
			}).start();
		}
		isOpen.current = !isOpen.current;
	};

	const render = () => {
		if (Platform.OS === 'android') return children;
		else return isOpen.current ? children : null;
	};
	return (
		<Animated.View
			style={{
				transform: [{ translateY: configAnim }],
				backgroundColor: 'red',
			}}
		>
			{children}
		</Animated.View>
	);
};

export default DrawerAnimation;
