import React, {useState, cloneElement} from 'react';
import {FlatList, ActivityIndicator} from 'react-native';
import {useTranslation} from 'react-i18next';
import Input from '../common/Input/Input';
import {useTheme} from 'styled-components';
import s from './styles';
import TableHead from './components/TableHead/TableHead';
import {calcWidth, calcHeight} from '../../utils/dimensions';
import useFilter from './hooks/useFilter';
import {fetchDataStatusE} from '../../utils/enums';

const SerachList = ({
  tableHead,
  tableData,
  arrStyle = [],
  RowComponent,
  callBackFilter = () => {},
  onClickItem,
  statusData,
  keyItem = 'key',
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();

  const {data, onChangeText, inputValue} = useFilter(
    tableData,
    callBackFilter,
    statusData,
  );

  const inputStyle = {
    width: 300,
    height: 35,
    backgroundColor: colors.grayLight,
    borderRadius: 4,
  };

  return (
    <s.Container>
      <Input
        value={inputValue}
        onChangeText={(text) => onChangeText(text)}
        iconName="search"
        placeholder={t('freeSearch')}
        {...inputStyle}
      />
      <TableHead data={tableHead} arrStyle={arrStyle} />
      {statusData === fetchDataStatusE.LOADING ? (
        <ActivityIndicator size="large" color="#40A8E2" />
      ) : (
        <FlatList
          style={{
            width: '100%',
            flex: 1,
            paddingHorizontal: calcWidth(16),
          }}
          contentContainerStyle={{
            paddingBottom: calcHeight(60),
          }}
          data={data}
          keyExtractor={(item) => item[keyItem].toString()}
          renderItem={({item}) => {
            return cloneElement(RowComponent, {
              data: item,
              onPress: () => onClickItem(item),
            });
          }}
        />
      )}
    </s.Container>
  );
};

export default SerachList;
