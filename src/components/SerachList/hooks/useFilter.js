import {useEffect, useState} from 'react';

const useFilter = (initialState = [], callBackFilter, statusData) => {
  const [data, setData] = useState(initialState);
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    setInputValue('');
  }, [statusData]);

  useEffect(() => {
    setData(initialState);
  }, [initialState]);

  const onChangeText = (text) => {
    setInputValue(text);
    if (text) {
      const filterData = initialState.filter((data) =>
        callBackFilter(data, text),
      );
      setData(filterData);
    } else setData(initialState);
  };

  return {data, onChangeText, inputValue};
};

export default useFilter;
