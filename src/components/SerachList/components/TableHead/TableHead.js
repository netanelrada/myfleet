import React from 'react';
import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
import { View } from 'react-native';
import Text from '../../../common/Text/Text';

const TableHead = ({ data = [], arrStyle = [] }) => {
	return (
		<s.Container>
			{data.map((text, index) => (
				<s.Cell key={index} flex={arrStyle[index].flex}>
					<Text textAlign={arrStyle[index].textAlign}>{text}</Text>
				</s.Cell>
			))}
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		width: ${calcWidth(300)}px;
		height: ${calcHeight(52)}px;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: flex-start;
	`,
	Cell: styled.View`
		flex: ${({ flex }) => flex};
	`,
};

export default TableHead;
