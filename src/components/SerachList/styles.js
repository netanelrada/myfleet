import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	flex: 1;
	background-color: ${({ theme }) => theme.colors.white};
	margin-top: ${calcHeight(15)}px;
	align-items: center;
	padding-top: ${calcHeight(15)}px;
	border-radius: 9px;
	box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);
`;

export default styles;
