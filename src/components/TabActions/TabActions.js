import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import useGetTabActions from './hooks/useGetTabActions';
import TabAction from './components/TabAction';
import {calcHeight, calcWidth, deviceHeight} from '../../utils/dimensions';
import Menu from './components/Menu/Menu';
import {useSelector} from 'react-redux';
import {allTripsSelector} from '../../store/selectors/tripsSelectors';
import Stations from './components/Stations/Stations';
import useBackHandler from '../../hooks/useBackHandler';

const TabActions = ({isVisable = true}) => {
  const [isOptionActive, setIsOptionActive] = useState(false);
  const [isPassengesModalVisable, setisPassengesModalVisable] = useState(false);
  const tabActions = useGetTabActions({
    setIsOptionActive,
    setisPassengesModalVisable,
  });
  const allTrips = useSelector((state) => allTripsSelector(state));
  useBackHandler(() => {
    if (isOptionActive) {
      setIsOptionActive(false);
      return true;
    }
    return false;
  });

  useEffect(() => {
    restTabState();
  }, [allTrips]);

  useEffect(() => {
    !isVisable && restTabState();
  }, [isVisable]);

  const restTabState = () => setIsOptionActive(false);

  return (
    <>
      {isVisable && (
        <>
          {isOptionActive && <s.Overlay onPress={restTabState} />}
          <s.Container>
            <Menu isVisabe={isOptionActive} />
            <Stations
              isVisable={isPassengesModalVisable}
              closeModal={() => setisPassengesModalVisable(false)}
            />
            <s.TabContainer>
              {tabActions.map(
                ({key, icon, action, label, labelColor, disabled, count}) => (
                  <TabAction
                    key={key}
                    iconName={icon.iconName}
                    label={label}
                    disabled={disabled}
                    count={count}
                    action={action}
                    labelColor={labelColor}
                  />
                ),
              )}
            </s.TabContainer>
          </s.Container>
        </>
      )}
    </>
  );
};

const s = {
  Container: styled.View`
    position: absolute;
    width: 100%;
    bottom: 0;
  `,
  TabContainer: styled.View`
    width: 100%;
    height: ${deviceHeight * 0.085}px;
    background-color: ${({theme}) => theme.colors.white};
    justify-content: space-around;
    align-items: center;
    box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
    flex-direction: row;
    align-self: flex-end;
    z-index: 99999;
  `,
  Overlay: styled.Pressable`
    position: absolute;
    right: 0;
    left: 0;
    top: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.4);
  `,
};
export default TabActions;
