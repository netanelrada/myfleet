import React from 'react';
import useProxyApi from '../../../hooks/useProxyApi';
import { getLineData } from '../../../api/api';

const useGetLineData = () => {
	const { proxyApi } = useProxyApi();

	const onGetTripData = async lineCode => {
		try {
			const res = await proxyApi({
				cb: getLineData,
				restProp: {
					lineCode,
				},
			});
			return res.data.data[0];
		} catch (error) {
			console.log(error);
		}
	};

	return { onGetTripData };
};

export default useGetLineData;
