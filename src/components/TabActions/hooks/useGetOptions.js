import React from 'react';
import {useEffect, useState, useMemo} from 'react';

import {useSelector} from 'react-redux';
import {editTripsSelector} from '../../../store/selectors/tripsSelectors';
import {useTranslation} from 'react-i18next';
import {calcHeight, calcWidth} from '../../../utils/dimensions';
import {useNavigation} from '@react-navigation/native';
import Text from '../../common/Text/Text';
import useActionsModal from './useActionsModal';
import {initStateModal} from '../components/Menu/Menu';
import useGetErrorMsg from '../../../hooks/useGetErrorMsg';
import useErrorAlertsModals from '../../../hooks/useErrorAlertsModals';

const useGetOptions = (setStateModal) => {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [isMultipleEdit, setisMultiplelEdit] = useState(false);
  const editTrips = useSelector((state) => editTripsSelector(state));
  const {sendToDrivers, sendOnWhatsapp, deleteTrips} = useActionsModal({
    closeModal: () =>
      setStateModal((preState) => ({
        ...preState,
        isVisible: false,
      })),
  });
  const {deleteErrorMsg, updateStatusErrorMsg} = useGetErrorMsg();
  const {
    modalState,
    onCloseModal,
    setModalState,
    actionComplete,
  } = useErrorAlertsModals();

  useEffect(() => {
    editTrips.length > 1 ? setisMultiplelEdit(true) : setisMultiplelEdit(false);
  }, [editTrips.length]);

  const sizeIcon = {
    height: calcHeight(16),
    width: calcWidth(16),
  };
  const options = useMemo(() => {
    return [
      {
        key: 'changeDriver',
        label: t('changeDriver'),
        icon: {
          iconName: 'driver',
          style: {
            ...sizeIcon,
          },
        },
        action: () => navigate('DriverReplacement'),
        disabled: false,
      },
      {
        key: 'changeCar',
        label: t('changeCar'),
        icon: {
          iconName: 'car',
          style: {
            ...sizeIcon,
          },
        },
        action: () => navigate('CarReplacement'),
        disabled: false,
      },
      {
        key: 'editLineDescription',
        label: t('editLineDescription'),
        icon: {
          iconName: 'road',
          style: {
            ...sizeIcon,
          },
        },
        action: () => navigate('EditTrip', {trip: editTrips[0]}),
        disabled: isMultipleEdit,
      },
      {
        key: 'editTimeTrip',
        label: t('editTimeTrip'),
        icon: {
          iconName: 'clock',
          style: {
            ...sizeIcon,
          },
        },
        action: () => navigate('EditScheduleTrip', {trip: editTrips[0]}),
        disabled: isMultipleEdit,
      },
      {
        key: 'editComments',
        label: t('editComments'),
        icon: {
          iconName: 'message',
          style: {
            ...sizeIcon,
          },
        },
        action: () => navigate('EditComments'),
        disabled: isMultipleEdit,
      },
      {
        key: 'sendWhatsApp',
        label: t('sendWhatsApp'),
        icon: {
          iconName: 'whatsapp',
          style: {
            ...sizeIcon,
          },
        },
        action: () =>
          setStateModal({
            isVisible: true,
            onConfirm: sendOnWhatsapp,
            iconName: 'whatsapp',
            textConfirnBtn: t('sendMessage'),
            component: <Text fontSize="s20">{t('sheardTripInWhatsapp')}</Text>,
            iconStyle: {
              height: calcHeight(32),
              width: calcWidth(32),
            },
          }),
        disabled: false,
      },

      {
        key: 'sendToDrivers',
        label: t('sendToDrivers'),
        icon: {
          iconName: 'share',
          style: {
            ...sizeIcon,
          },
        },
        action: async () => {
          const {data = {}} = await sendToDrivers(0);
          const isComplete = actionComplete(data);

          if (!isComplete) {
            setModalState({
              isVisible: true,
              res: data,
              errorMessages: updateStatusErrorMsg,
            });
          } else {
            setStateModal({
              isVisible: true,
              onConfirm: () => sendToDrivers(2),
              iconName: 'share',
              textConfirnBtn: t('send'),
              component: <Text fontSize="s20">{t('sendTripToDrivers')}</Text>,
              iconStyle: {
                height: calcHeight(32),
                width: calcWidth(40),
              },
            });
          }
        },
        disabled: false,
      },
      {
        key: 'delete',
        label: t('delete'),
        icon: {
          iconName: 'trash',
          style: {
            ...sizeIcon,
          },
        },
        action: async () => {
          const {data = {}} = await deleteTrips(0);
          const isComplete = actionComplete(data);

          if (!isComplete) {
            setModalState({
              isVisible: true,
              res: data,
              errorMessages: deleteErrorMsg,
            });
          } else {
            setStateModal({
              isVisible: true,
              onConfirm: () => deleteTrips(2),
              iconName: 'trash',
              textConfirnBtn: t('delete'),
              component: <Text fontSize="s20">{t('deleteTrip')}</Text>,
              iconStyle: {
                height: calcHeight(32),
                width: calcWidth(32),
              },
            });
          }
        },
        disabled: false,
      },
      {
        key: 'editTrip',
        label: t('editTrip'), //editLineDescription
        icon: {
          iconName: 'edit',
          style: {
            colorIconContainer: 'blueLight',
            ...sizeIcon,
          },
        },
        action: () =>
          navigate('NewTripStack', {
            screen: 'NewTrip',
            params: {lineCode: editTrips[0].lineCode},
          }),
        disabled: isMultipleEdit,
      },
    ];
  }, [isMultipleEdit, editTrips.length]);

  return {options, modalState, onCloseModal};
};

export default useGetOptions;
