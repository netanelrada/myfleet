import {useCallback} from 'react';
import {Linking, InteractionManager} from 'react-native';
import {useSelector} from 'react-redux';
import {userSelector} from '../../../store/selectors/loginSelectors';
import {editTripsSelector} from '../../../store/selectors/tripsSelectors';
import {
  sendTripsToDrivers,
  setIsActiveTrip,
  getLineData,
} from '../../../api/api';
import useHandleResponse from '../../../hooks/useHandleResponse';
import {useTranslation} from 'react-i18next';
import moment from 'moment';

const useActionsModal = ({closeModal = () => {}}) => {
  const user = useSelector((state) => userSelector(state));
  const editTrips = useSelector((state) => editTripsSelector(state));
  const {handleResponse} = useHandleResponse({enableGoBack: false});
  const {t} = useTranslation();

  const sendToDrivers = useCallback(
    async (action = 2) => {
      try {
        const {userToken, wsString} = user;
        const lines = editTrips.map(({lineCode}) => lineCode).join(',');
        const payload = {
          userToken,
          wsString,
          lines,
          status: 1,
          action,
        };
        const res = await sendTripsToDrivers(payload);
        if (action === 0) {
          return res;
        } else {
          res.data.updateLines = lines;
          closeModal();
          handleResponse(res.data);
        }
      } catch (error) {}
    },
    [editTrips],
  );

  const sendOnWhatsapp = useCallback(async () => {
    try {
      const promises = [];
      const {userToken, wsString} = user;
      editTrips.forEach(({lineCode}) => {
        promises.push(getLineData({wsString, userToken, lineCode}));
      });
      let fullDataTrips = [];
      const res = await Promise.all(promises);
      fullDataTrips = res.map((resTrip) => resTrip.data.data[0]);
      const msg = generateMsg(fullDataTrips);
      const url = `whatsapp://send?text=${msg}`;
      const supported = await Linking.canOpenURL(url);

      if (supported) {
        await Linking.openURL(url);
      }
      closeModal();
    } catch (error) {
      console.log({error});
    }
  }, [editTrips]);

  const deleteTrips = useCallback(
    async (action = 2) => {
      try {
        const {userToken, wsString} = user;
        const lines = editTrips.map(({lineCode}) => lineCode).join(',');
        const payload = {
          userToken,
          wsString,
          lines,
          active: 0,
          action,
        };
        const res = await setIsActiveTrip(payload);
        if (action === 0) {
          return res;
        } else {
          res.data.updateLines = lines;
          closeModal();
          InteractionManager.runAfterInteractions(() =>
            handleResponse(res.data),
          );
        }
      } catch (error) {
        console.log('error', error);
        closeModal();
      }
    },
    [editTrips],
  );

  const generateMsg = (trips = []) => {
    const messages = [];
    trips.forEach(
      ({clientName, startTime, endTime, lineDescription, longRemarks}) => {
        const [date, beginning] = startTime.split(' ');
        const [_, end] = endTime.split(' ');
        const newMsg = `
*${t('tripDate')}*: ${moment(date).format('D.M.YYYY')}
*${t('startTime')}*: ${beginning} 
*${t('endTime')}*: ${end} 
*${t('clientName')}*: ${clientName} 
*${t('lineDescription')}*: 	${lineDescription}
*${t('comments')}*: 	${longRemarks}
			`;
        messages.push(newMsg);
      },
    );

    return messages.join('\n');
  };

  return {sendToDrivers, sendOnWhatsapp, deleteTrips};
};

export default useActionsModal;
