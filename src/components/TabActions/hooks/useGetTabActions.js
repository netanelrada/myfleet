import React, {useEffect, useState, useMemo} from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {
  editTripsSelector,
  tabFoucsSelector,
} from '../../../store/selectors/tripsSelectors';
import {setTabFoucs} from '../../../store/actions/actionTrips';
import {useTheme} from 'styled-components';

const useGetTabActions = ({setIsOptionActive, setisPassengesModalVisable}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const onSetTabFoucs = (payload) => dispatch(setTabFoucs(payload));
  const {navigate, push} = useNavigation();
  const [isMultipleEdit, setisMultiplelEdit] = useState(false);
  const editTrips = useSelector((state) => editTripsSelector(state));
  const tabFoucs = useSelector((state) => tabFoucsSelector(state));

  useEffect(() => {
    editTrips.length > 1 ? setisMultiplelEdit(true) : setisMultiplelEdit(false);
  }, [editTrips.length]);

  const tabActions = useMemo(() => {
    return [
      {
        key: 'details',
        label: t('details'),
        labelColor: tabFoucs.key === 'details' ? colors.blueLight : 'black',
        icon: {
          iconName: tabFoucs.key === 'details' ? 'blueDetails' : 'details',
          style: {},
        },
        action: () => {
          onSetTabFoucs({key: 'details', clickOnTab: true});
          setIsOptionActive(false);
          navigate('Home', {
            screen: 'SingleTrip',
            params: {trip: editTrips[0]},
          });
        },
        disabled: isMultipleEdit,
      },
      {
        key: 'map',
        label: t('map'),
        labelColor: tabFoucs.key === 'map' ? colors.blueLight : 'black',
        icon: {
          iconName: tabFoucs.key === 'map' ? 'blueMap' : 'map',
          style: {},
        },
        action: () => {
          onSetTabFoucs({key: 'map', clickOnTab: true});
          setIsOptionActive(false);
          navigate('Home', {
            screen: 'SingleTrip',
            params: {trip: editTrips[0]},
          });
        },
        disabled: isMultipleEdit,
      },

      {
        key: 'passengers',
        label: t('stations'),
        labelColor: tabFoucs.key === 'passengers' ? colors.blueLight : 'black',
        icon: {
          iconName:
            tabFoucs.key === 'passengers'
              ? 'blueStationsAndPassengers'
              : 'stationsAndPassengers',
          style: {},
        },
        action: () => {
          //	setIsOptionActive(false);
          //	setisPassengesModalVisable(true);
          onSetTabFoucs({key: 'passengers', clickOnTab: true});

          navigate('Home', {
            screen: 'StationsAndPassengers',
            params: {trip: editTrips[0]},
          });
        },
        disabled: isMultipleEdit,
        count: !isMultipleEdit && editTrips[0]?.passQty,
      },
      {
        key: 'options',
        label: t('options'),
        labelColor: 'black',
        icon: {
          iconName: 'hamburgerMenu',
          style: {},
        },
        action: () => {
          setIsOptionActive((preState) => !preState);
        },
        disabled: false,
      },
    ];
  }, [isMultipleEdit, editTrips, tabFoucs]);

  return tabActions;
};

export default useGetTabActions;
