import React from 'react';
import styled, {useTheme} from 'styled-components';
import Icon from '../../common/Icon/Icon';
import Text from '../../common/Text/Text';
import CountWrap from '../../common/CountWrap/CountWrap';

const TabAction = ({
  iconName,
  label,
  disabled = true,
  count = 0,
  action,
  labelColor,
}) => {
  return (
    <CountWrap right={-7} top={25} count={count} isWithBorder={false}>
      <s.Container
        disabledStyle={disabled}
        disabled={disabled}
        onPress={action}>
        <Icon name={iconName} />
        <Text color={labelColor}>{label}</Text>
      </s.Container>
    </CountWrap>
  );
};

const s = {
  Container: styled.TouchableOpacity`
    align-items: center;
    height: 80%;
    justify-content: space-around;
    opacity: ${({disabledStyle}) => (disabledStyle ? 0.4 : 1)};
  `,
};

export default TabAction;
