import React from 'react';
import styled, { useTheme } from 'styled-components';
import { calcHeight, calcWidth } from '../../../../../utils/dimensions';
import Text from '../../../../common/Text/Text';
import Icon from '../../../../common/Icon/Icon';

const MenuItem = ({ label, icon, action, disabled }) => {
	const { colors } = useTheme();
	return (
		<s.Container disabled={disabled} onPress={action}>
			<s.IconContainer {...icon.style} disabled={disabled}>
				<Icon name={icon.iconName} {...icon.style} />
			</s.IconContainer>
			<Text color={disabled ? colors.gray5 : null} textAlign='right'>
				{label}
			</Text>
			<s.View />
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		z-index: 99999999;
		flex-direction: row-reverse;
		justify-content: space-between;
		align-items: center;
		flex: 1;
		width: ${calcWidth(150)}px;
		align-self: flex-end;
	`,
	IconContainer: styled.View`
		width: ${calcWidth(36)}px;
		aspect-ratio: 1;
		border-radius: ${calcWidth(18)}px;
		background-color: ${({
			theme,
			disabled,
			colorIconContainer = 'yellowLight',
		}) => (disabled ? theme.colors.gray4 : theme.colors[colorIconContainer])};
		justify-content: center;
		align-items: center;
	`,
	View: styled.View``,
};

export default MenuItem;
