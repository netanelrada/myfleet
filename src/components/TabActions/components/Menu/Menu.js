import React, {useEffect, useRef, useState} from 'react';
import {Animated, Platform, InteractionManager} from 'react-native';
import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';
import useGetOptions from '../../hooks/useGetOptions';
import MenuItem from './MenuItem/MenuItem';
import ModalConfirm from '../../../common/ModalConfirm/ModalConfirm';
import Text from '../../../common/Text/Text';
import {useTranslation} from 'react-i18next';
import {PlatfromOS} from '../../../../utils/utilis';
import AlertsAndErrors from '../../../AlertsAndErrors/AlertsAndErrors';

const initHeightOptions = calcHeight(390);
const bottom = PlatfromOS === 'ios' ? -calcHeight(80) : 0;

export const initStateModal = {
  isVisible: false,
  onConfirm: () => {},
  iconName: null,
  textConfirnBtn: null,
  component: null,
  iconStyle: {},
};

const widtsLan = {
  he: 150,
  en: 166,
  ru: 180,
  ar: 150,
};

const Menu = ({isVisabe = false}) => {
  const {t, i18n} = useTranslation();
  const [stateModal, setStateModal] = useState(initStateModal);
  const {options, modalState, onCloseModal} = useGetOptions(setStateModal);
  const animatedController = useRef(new Animated.Value(initHeightOptions))
    .current;
  const isOpen = useRef(true);
  const width = widtsLan[i18n.language];

  useEffect(() => {
    toggleListItem();
  }, []);

  useEffect(() => {
    toggleListItem();
  }, [isVisabe]);

  const toggleListItem = () => {
    if (isOpen.current) {
      close();
    } else {
      open();
    }
    isOpen.current = !isOpen.current;
  };

  const close = () => {
    Animated.timing(animatedController, {
      duration: 300,
      toValue: bottom,
      useNativeDriver: true,
    }).start();
  };

  const open = () => {
    Animated.timing(animatedController, {
      duration: 300,
      toValue: initHeightOptions,
      useNativeDriver: true,
    }).start();
  };

  const onCancel = () => {
    setStateModal((preState) => ({
      ...preState,
      isVisible: false,
    }));
  };
  return (
    <>
      <ModalConfirm
        isVisible={stateModal.isVisible}
        iconName={stateModal.iconName}
        onCancel={onCancel}
        onConfirm={stateModal.onConfirm}
        textConfirnBtn={stateModal.textConfirnBtn}
        textCancelBtn={t('cancel')}
        iconStyle={stateModal.iconStyle}>
        {stateModal.component}
      </ModalConfirm>

      <AlertsAndErrors
        closeModal={onCloseModal}
        onConfirm={stateModal.onConfirm}
        {...modalState}
      />
      <Animated.View
        style={{
          transform: [{translateY: animatedController}],
          width: calcWidth(150),
          alignSelf: 'flex-end',
        }}>
        <s.Container width={width} isIos={Platform.OS === 'ios'}>
          <s.BoardContainer width={width}></s.BoardContainer>
          {options.map(({key, label, icon, action, disabled}) => (
            <MenuItem
              key={key}
              label={label}
              icon={icon}
              action={action}
              disabled={disabled}
            />
          ))}
        </s.Container>
      </Animated.View>
    </>
  );
};

const s = {
  View: styled.View``,
  Container: styled.View`
    z-index: 9999999;
    height: ${initHeightOptions}px;
    width: ${({width}) => calcWidth(width + 20)}px;

    align-self: flex-end;
    justify-content: space-around;
    ${({isIos}) => isIos && 'position: absolute;'}
    bottom: ${bottom}px;
    right: 0;
  `,
  BoardContainer: styled.View`
    height: ${calcHeight(384)}px;
    width: ${({width}) => calcWidth(width)}px;

    background: ${({theme}) => theme.colors.white};
    position: absolute;
    top: 0;
    border-radius: 4px;
    margin-horizontal: 10px;
  `,
};

export default Menu;
