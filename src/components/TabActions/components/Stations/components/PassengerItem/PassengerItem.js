import React, { useState } from 'react';
import s from './styles';
import { useTranslation } from 'react-i18next';
import Text from '../../../../../common/Text/Text';
import Icon from '../../../../../common/Icon/Icon';
import { useTheme } from 'styled-components';
import useCall from '../../../../../../hooks/useCall';
import PhonesModal from '../PhonesModal/PhonesModal';

const PassengerItem = ({ passenger }) => {
	const { t } = useTranslation();
	const { call } = useCall();
	const [isVisableModalPhone, setisVisableModalPhone] = useState(false);
	const { lastName = '', firstName = '', phone1 = '', phone2 = '' } = passenger;
	const fullName = `${lastName} ${firstName}`;

	const onPressDial = () => {
		if (phone1 && phone2) {
			setisVisableModalPhone(true);
		} else if (phone1) {
			call(phone1);
		} else if (phone2) {
			call(phone2);
		}
	};

	return (
		<s.Container>
			<s.Row>
				<Text>
					<s.Label>{`${t('name')}  `}</s.Label>
					{fullName}
				</Text>
			</s.Row>
			<s.Row>
				<Text>
					<s.Label>{`${t('phone')}  `}</s.Label>
					{`${phone1} , ${phone2}`}
				</Text>
				<s.CallContainer onPress={onPressDial}>
					<Icon name='phone' />
					<s.Text>{t('call')}</s.Text>
				</s.CallContainer>
			</s.Row>

			<PhonesModal
				isVisible={isVisableModalPhone}
				call={call}
				phone1={phone1}
				phone2={phone2}
				onBackdropPress={() => setisVisableModalPhone(false)}
			/>
		</s.Container>
	);
};

export default PassengerItem;
