import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: 100%;
`;

styles.Row = styled.View`
	flex-direction: row-reverse;
	justify-content: space-between;
	height: ${calcHeight(20)}px;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	color: ${({ theme }) => theme.colors.gray6};
`;

styles.CallContainer = styled.TouchableOpacity`
	flex-direction: row-reverse;
	justify-content: space-between;
`;

styles.Text = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	color: ${({ theme }) => theme.colors.blueLight};
	margin-right: 5px;
`;

export default styles;
