import styled from 'styled-components';
import { calcWidth } from '../../../../../../utils/dimensions';
import { TouchableOpacity } from 'react-native';

const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(344)}px;
	aspect-ratio: ${344 / 112};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	justify-content: space-around;
	align-items: center;
`;

styles.Hr = styled.View`
	border-bottom-color: rgba(0, 0, 0, 0.12);
	border-bottom-width: 1px;
	width: 100%;
`;

styles.TouchableOpacity = styled.TouchableOpacity``;

export default styles;
