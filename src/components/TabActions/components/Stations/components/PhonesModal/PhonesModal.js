import React from 'react';
import Modal from 'react-native-modal';
import s from './styles';
import Text from '../../../../../common/Text/Text';

const PhonesModal = ({
	isVisible = false,
	onBackdropPress = () => {},
	phone1 = '',
	phone2 = '',
	call = () => {},
}) => {
	return (
		<Modal
			isVisible={isVisible}
			style={{ alignItems: 'center', justifyContent: 'center' }}
			animationIn='fadeIn'
			animationOut='fadeOut'
			backdropTransitionOutTiming={0}
			animationOutTiming={600}
			animationInTiming={600}
			onBackdropPress={onBackdropPress}
		>
			<s.Container>
				<s.TouchableOpacity onPress={() => call(phone1)}>
					<Text fontSize='s18' fontFamily='Rubik-Medium'>
						{phone1}
					</Text>
				</s.TouchableOpacity>
				<s.Hr />
				<s.TouchableOpacity onPress={() => call(phone2)}>
					<Text fontSize='s18' fontFamily='Rubik-Medium'>
						{phone2}
					</Text>
				</s.TouchableOpacity>
			</s.Container>
		</Modal>
	);
};

export default PhonesModal;
