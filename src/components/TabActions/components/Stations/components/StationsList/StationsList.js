import React from 'react';
import { ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';
import s from './styles';
import Text from '../../../../../common/Text/Text';
import Icon from '../../../../../common/Icon/Icon';
import StationItem from '../StationItem/StationItem';
import useCall from '../../../../../../hooks/useCall';

const StationsList = ({ passengersData = [], passQty = 0 }) => {
	const { t } = useTranslation();
	const { call } = useCall();
	const stationsLength = passengersData.length;
	return (
		<s.Container>
			<s.Header>
				<Text fontSize='s20'>{t('stationsList')}</Text>
				<s.IconsContainer>
					<s.IconContainer>
						<s.CountText fontSize='s20'>{passQty}</s.CountText>
						<Icon name='passenger' />
					</s.IconContainer>

					<s.IconContainer>
						<s.CountText fontSize='s20'>{stationsLength}</s.CountText>
						<Icon name='stations' />
					</s.IconContainer>
				</s.IconsContainer>
			</s.Header>
			<ScrollView>
				{passengersData.map((item, index) => (
					<StationItem
						call={call}
						key={index}
						index={index}
						passengerData={item}
					/>
				))}
			</ScrollView>
		</s.Container>
	);
};

export default StationsList;
