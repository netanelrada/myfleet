import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	width: ${calcWidth(332)}px;
	background-color: ${({ theme }) => theme.colors.ghostWhite};
	align-items: center;
	border-radius: 9px;
	padding-horizontal: ${calcWidth(14)}px;
`;

styles.Header = styled.View`
	height: ${calcHeight(45)}px;
	width: 100%;
	align-items: center;
	flex-direction: row-reverse;
	justify-content: space-between;
`;

styles.IconsContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-between;
	width: 35%;
`;

styles.IconContainer = styled.View`
	flex-direction: row-reverse;
	align-items: center;
`;
styles.CountText = styled.Text`
	font-size: ${({ theme }) => theme.fontSizes.s20}px;
	font-family: Rubik-Regular;
	color: ${({ theme }) => theme.colors.black};
	margin-left: 5px;
`;

export default styles;
