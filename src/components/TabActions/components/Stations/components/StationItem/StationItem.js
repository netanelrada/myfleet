import React, { useState } from 'react';
import s from './styles';
import { useTranslation } from 'react-i18next';
import Text from '../../../../../common/Text/Text';
import Icon from '../../../../../common/Icon/Icon';
import { useTheme } from 'styled-components';
import PhonesModal from '../PhonesModal/PhonesModal';
import PassengerItem from '../PassengerItem/PassengerItem';

const StationItem = ({ passengerData = {}, call = () => {}, index = 0 }) => {
	const { t } = useTranslation();
	const {
		colors: { blueLight },
	} = useTheme();

	const { city = '', street = '', house = '', passengers = [] } = passengerData;

	return (
		<s.Container>
			<s.IndexContainer>
				<Text>{index + 1}</Text>
			</s.IndexContainer>
			<s.DetailsContainer>
				<s.Row>
					<Text
						fontFamily='Rubik-Medium'
						fontWeight='500'
					>{`${street} ${house}, ${city}`}</Text>
				</s.Row>
				{passengers.map(passenger => (
					<PassengerItem passenger={passenger} />
				))}
			</s.DetailsContainer>
		</s.Container>
	);
};

export default StationItem;
