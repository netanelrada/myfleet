import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	width: ${calcWidth(299)}px;
	align-items: center;
	padding-vertical: ${calcWidth(16)}px;
	border-bottom-width: 1px;
	border-bottom-color: ${({ theme }) => theme.colors.gray7};
	flex-direction: row-reverse;
`;

styles.DetailsContainer = styled.View`
	flex: 0.9;
	justify-content: space-between;
	height: 100%;
`;

styles.IndexContainer = styled.View`
	flex: 0.1;
	height: 100%;
	align-items: center;
	justify-content: flex-start;
`;

styles.Row = styled.View`
	flex-direction: row-reverse;
`;
styles.CallRow = styled.TouchableOpacity`
	flex-direction: row-reverse;
	justify-content: space-around;
	width: 100%;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	color: ${({ theme }) => theme.colors.gray6};
	margin-left: 5px;
`;

export default styles;
