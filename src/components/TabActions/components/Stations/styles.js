import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.SafeAreaView`
	flex: 1;
	width: 100%;
	background-color: ${({ theme }) => theme.colors.backgroundModalRemarks};
	align-items: center;
	padding-horizontal: ${calcWidth(14)}px;
`;

styles.Header = styled.View`
	width: 100%;
	height: 10%;
	justify-content: space-between;
	flex-direction: row-reverse;
	align-items: center;
	width: ${calcWidth(332)}px;
`;

styles.CloseBtn = styled.TouchableOpacity`
	width: ${calcWidth(40)}px;
	aspect-ratio: 1;
	background-color: ${({ theme }) => theme.colors.blueLight};
	border-radius: 9px;
	justify-content: center;
	align-items: center;
`;

export default styles;
