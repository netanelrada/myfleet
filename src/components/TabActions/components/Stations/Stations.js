import React, { useContext, useState, useEffect } from 'react';
import Modal from '../../../common/Modal/Modal';
import s from './styles';
import Text from '../../../common/Text/Text';
import StationsList from './components/StationsList/StationsList';
import Icon from '../../../common/Icon/Icon';
import { stations } from '../../../../utils/data';
import { DetailsContext } from '../../../../screens/SingleTrip/DetailsContext';
import { useSelector } from 'react-redux';
import { editTripsSelector } from '../../../../store/selectors/tripsSelectors';
import useGetLineData from '../../hooks/useGetLineData';

const Stations = ({ isVisable = false, closeModal = () => {} }) => {
	const { tripDetalis = {} } = useContext(DetailsContext);
	const { onGetTripData } = useGetLineData();
	const editTrip = useSelector(state => editTripsSelector(state));
	const [stationsData, setStationsData] = useState({
		stations: [],
		passQty: null,
	});

	useEffect(() => {
		if (isVisable) {
			const { stations = null, passQty = 0 } = tripDetalis;
			if (stations) {
				setStationsData({ stations, passQty });
			} else {
				updateTripData();
			}
		}
	}, [isVisable]);

	const updateTripData = async () => {
		try {
			const { lineCode = '' } = editTrip[0];
			const stationRes = await onGetTripData(lineCode);
			const { stations = [], passQty = 0 } = stationRes;
			setStationsData({ stations, passQty });
		} catch (error) {}
	};

	return (
		<Modal isVisable={isVisable} onRequestClose={closeModal}>
			<s.Container>
				<s.Header>
					<s.CloseBtn onPress={closeModal}>
						<Icon name='whiteClose' />
					</s.CloseBtn>
				</s.Header>
				<StationsList
					passengersData={stationsData.stations}
					passQty={stationsData.passQty}
				/>
			</s.Container>
		</Modal>
	);
};

export default Stations;
