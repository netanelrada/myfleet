import React from 'react';
import { useTranslation } from 'react-i18next';
import ModalConfirm from '../../common/ModalConfirm/ModalConfirm';
import Text from '../../common/Text/Text';

const ErrorModal = ({ isVisible = false, closeModal = () => {} }) => {
	const { t } = useTranslation();

	return (
		<ModalConfirm
			isVisible={isVisible}
			onCancel={closeModal}
			isConfirmHidden={true}
			textCancelBtn={t('loginAgain')}
		>
			<Text textAlign='center' fontSize='s20'>
				{t('userDisconnect')}
			</Text>
			<Text textAlign='center' fontSize='s16'>
				{t('needToLoginAgain')}
			</Text>
		</ModalConfirm>
	);
};

export default ErrorModal;
