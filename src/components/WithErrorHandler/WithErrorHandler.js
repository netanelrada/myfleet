import React, {useState, useEffect} from 'react';
import ErrorModal from './ErrorModal/ErrorModal';
import {instance} from '../../api/middleware';
import {navigate, navigationRef} from '../../navigation/navigationService';
import {useDispatch} from 'react-redux';
import {restUserData} from '../../store/actions/actionLogin';
import ServerErrorModal from './ServerErrorModal/ServerErrorModal';
import Offline from './Offline/Offline';
import {useTranslation} from 'react-i18next';

const WithErrorHandler = ({children}) => {
  const [visable, setVisable] = useState(false);
  const {t} = useTranslation();
  const [visableServerError, setVisableServerError] = useState({
    visable: false,
    message: '',
  });
  const dispatch = useDispatch();
  const onRestUserData = () => dispatch(restUserData());

  useEffect(() => {
    const reqInterceptor = instance.interceptors.request.use((req) => {
      return req;
    });

    const resInterceptor = instance.interceptors.response.use(
      (res) => {
        const {data = {}} = res;
        if (data.response === '1') setVisable(true);
        else if (data.response === '99')
          setVisableServerError({visable: true, message: data.data});
        else if (typeof data === 'string' && data.includes('SQL error'))
          setVisableServerError({visable: true, message: data});
        else return res;
      },
      (err) => {
        console.log({err});
        const {isIgnorError = false} = err.config;
        !isIgnorError &&
          setVisableServerError({visable: true, message: t('errorServer')});
        return err;
      },
    );

    return () => {
      instance.interceptors.request.eject(reqInterceptor);
      instance.interceptors.response.eject(resInterceptor);
    };
  }, []);

  const closeModal = () => {
    setVisable(false);
    onRestUserData();
    navigationRef.current?.reset({
      index: 0,
      routes: [{name: 'Initial'}],
    });
  };

  return (
    <>
      <ErrorModal isVisible={visable} closeModal={closeModal} />
      <ServerErrorModal
        isVisible={visableServerError.visable}
        message={visableServerError.message}
        closeModal={() => setVisableServerError(false)}
      />
      <Offline />
      {children}
    </>
  );
};

export default WithErrorHandler;
