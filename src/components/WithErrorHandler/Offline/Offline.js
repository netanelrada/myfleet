import React from 'react';
import { useTranslation } from 'react-i18next';
import s from './styles';
import Text from '../../common/Text/Text';
import Icon from '../../common/Icon/Icon';
import { useNetInfo } from '@react-native-community/netinfo';

const Offline = () => {
	const { t } = useTranslation();
	const { isConnected = true } = useNetInfo();

	return (
		<>
			{!isConnected && (
				<s.Container>
					<Text>{t('offline')}</Text>
					<s.IconContainer>
						<Icon name='noWifi' />
					</s.IconContainer>
				</s.Container>
			)}
		</>
	);
};

export default Offline;
