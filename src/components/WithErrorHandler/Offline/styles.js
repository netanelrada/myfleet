import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(360)}px;
	height: ${calcHeight(32)}px;
	margin-top: ${calcHeight(20)}px;
	align-items: center;
	justify-content: flex-end;
	padding-right: 15px;
	flex-direction: row;
	background-color: ${({ theme }) => theme.colors.white};
`;

styles.IconContainer = styled.View`
	margin-left: 5px;
`;

export default styles;
