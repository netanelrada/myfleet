import React from 'react';
import { useTranslation } from 'react-i18next';
import ModalConfirm from '../../common/ModalConfirm/ModalConfirm';
import Text from '../../common/Text/Text';

const ServerErrorModal = ({
	isVisible = false,
	closeModal = () => {},
	message = '',
}) => {
	const { t } = useTranslation();

	return (
		<ModalConfirm
			isVisible={isVisible}
			onCancel={closeModal}
			isConfirmHidden={true}
			textCancelBtn={t('close')}
		>
			<Text textAlign='center' fontSize='s20'>
				{t('somethingWrong')}
			</Text>
			<Text textAlign='center'>{message}</Text>
		</ModalConfirm>
	);
};

export default ServerErrorModal;
