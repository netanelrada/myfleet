import React from 'react';
import s from './styles';
import {calcWidth} from '../../utils/dimensions';
import Icon from '../common/Icon/Icon';

const Header = ({
  onGoBack = () => {},
  titleText = '',
  iconName = '',
  leftText = '',
  iconStyle = {
    width: calcWidth(16),
    height: calcWidth(16),
  },
  backStyle = {
    width: calcWidth(16),
    height: calcWidth(16),
  },
}) => {
  return (
    <s.Container>
      <s.RightContainer onPress={onGoBack}>
        <s.Title>{titleText}</s.Title>
        <Icon name="blackBack" {...backStyle} />
      </s.RightContainer>
      <s.LeftContainer>
        {!!iconName && <Icon name={iconName} {...iconStyle} />}
        <s.LeftText>{leftText}</s.LeftText>
      </s.LeftContainer>
    </s.Container>
  );
};

export default Header;
