import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../utils/dimensions';
const styles = {};

styles.Icon1Container = styled.View`
	position: absolute;
	left: ${calcWidth(25)}px;
	top: ${calcHeight(67)}px;
`;

styles.Icon2Container = styled.View`
	position: absolute;
	left: ${calcWidth(155)}px;
	top: ${calcHeight(295)}px;
`;

styles.Icon3Container = styled.View`
	position: absolute;
	right: ${calcWidth(5)}px;
	bottom: ${calcHeight(50)}px;
`;

styles.NumberVersionContainer = styled.View`
	position: absolute;
	bottom: ${calcHeight(20)}px;
	align-self: center;
`;

export default styles;
