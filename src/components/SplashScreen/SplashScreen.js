import React, { useState, useEffect, useRef } from 'react';
import { ImageBackground, Animated } from 'react-native';
import { images } from '../../assets';
import Icon from '../common/Icon/Icon';
import s from './styles';
import NumberVersion from '../common/NumberVersion/NumberVersion';

const SplashScreen = ({ children, setIsSplashFinish }) => {
	const fadeAnim1 = useRef(new Animated.Value(0)).current;
	const fadeAnim2 = useRef(new Animated.Value(0)).current;
	const fadeAnim3 = useRef(new Animated.Value(0)).current;

	const getAnimationConfig = (toValue = 1) => ({
		toValue,
		duration: 300,
		useNativeDriver: true,
	});

	useEffect(() => {
		setTimeout(() => {
			Animated.sequence([
				Animated.timing(fadeAnim1, getAnimationConfig(1)),
				Animated.timing(fadeAnim1, getAnimationConfig(0)),
				Animated.timing(fadeAnim3, getAnimationConfig(1)),
				Animated.timing(fadeAnim3, getAnimationConfig(0)),
				Animated.timing(fadeAnim2, getAnimationConfig(1)),
				Animated.timing(fadeAnim2, getAnimationConfig(0)),
				Animated.timing(fadeAnim1, getAnimationConfig(1)),
				Animated.timing(fadeAnim3, getAnimationConfig(1)),
			]).start(() => {
				setIsSplashFinish(true);
			});
		}, 1000);
	}, []);

	return (
		<ImageBackground
			style={{ flex: 1, width: '100%' }}
			source={images.backgroundSplash}
		>
			<Animated.View style={[, { opacity: fadeAnim1 }]}>
				<s.Icon1Container>
					<Icon name='location' />
				</s.Icon1Container>
			</Animated.View>
			<Animated.View style={[, { opacity: fadeAnim2 }]}>
				<s.Icon2Container>
					<Icon name='location' />
				</s.Icon2Container>
			</Animated.View>
			<s.Icon3Container>
				<Animated.View style={[, { opacity: fadeAnim3 }]}>
					<Icon name='location' />
				</Animated.View>
			</s.Icon3Container>
			{children}
			<s.NumberVersionContainer>
				<NumberVersion />
			</s.NumberVersionContainer>
		</ImageBackground>
	);
};

export default SplashScreen;
