import { Linking } from 'react-native';
import { useCallback } from 'react';

const useCall = () => {
	const call = useCallback(number => {
		const url = `tel:${number}`;
		Linking.canOpenURL(url)
			.then(() => {
				Linking.openURL(url);
			})
			.catch(error => console.log(error));
	}, []);
	return { call };
};

export default useCall;
