import {} from 'react';
import {useDispatch} from 'react-redux';
import {setEditMode} from '../store/actions/actionTrips';
import {useNavigation} from '@react-navigation/native';
import useRefresh from './useRefresh';

const useHandleResponse = ({enableGoBack = true}) => {
  const dispatch = useDispatch();
  const onSetEditMode = (payload) => dispatch(setEditMode(payload));
  const {goBack} = useNavigation();
  const {onRefresh, onRefreshByLineCode} = useRefresh();

  const handleResponse = (res) => {
    if (res.response == '0') {
      onSetEditMode(false);
      if (res.lineCode) onRefreshByLineCode(res.lineCode, true);
      else onRefreshByLineCode(res.updateLines);
      //	onRefresh();
      enableGoBack && goBack();
    }
  };

  return {handleResponse};
};

export default useHandleResponse;
