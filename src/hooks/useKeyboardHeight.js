import { useEffect, useState } from 'react';
import { Keyboard } from 'react-native';

const useKeyboardHeight = () => {
	const [keyboardHeight, setKeyboardHeight] = useState(0);

	useEffect(() => {
		const keyboardListener = Keyboard.addListener('keyboardDidShow', e => {
			setKeyboardHeight(e.endCoordinates.height);
		});
		const keyboardListenerHide = Keyboard.addListener('keyboardDidHide', () =>
			setKeyboardHeight(0)
		);
		return () => {
			keyboardListener.remove();
			keyboardListenerHide.remove();
		};
	}, []);

	return keyboardHeight;
};

export default useKeyboardHeight;
