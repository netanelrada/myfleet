import { useState } from 'react';

const useErrorAlertsModals = () => {
	const [modalState, setModalState] = useState({
		isVisible: false,
		res: {
			errors: 0,
			alerts: 0,
			lines: [],
		},
		errorMessages: [],
	});

	const onCloseModal = () => {
		setModalState(preState => ({
			...preState,
			isVisible: false,
		}));
	};

	const actionComplete = ({ errors = 0, alerts = 0 }) => !errors && !alerts;

	return { modalState, onCloseModal, setModalState, actionComplete };
};

export default useErrorAlertsModals;
