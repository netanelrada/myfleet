import {useMemo} from 'react';
import {useTranslation} from 'react-i18next';

const useGetErrorMsg = () => {
  const {t} = useTranslation();

  const deleteErrorMsg = useMemo(
    () => ({
      0: t('error'),
      1: t('driverLock'),
      2: t('clientLock'),
      3: t('lockedDriver'),
      4: t('notAuthDeletionTrip'),
      5: t('fixedPriceTrip'),
      6: t('invoiceIssuedTrip'),
    }),
    [t],
  );

  const updateStatusErrorMsg = useMemo(
    () => ({
      0: t('error'),
      1: t('driverNotConnect'),
      2: t('notAuthChangeOldTrip'),
      3: t('notAuthChangeEndedTrip'),
    }),
    [t],
  );

  const updateCarErrorMsg = useMemo(
    () => ({
      0: t('error'),
      1: t('driverLock'),
      2: t('notAuthChangeTrips'),
      3: t('notAuthChangeOldTrip'),
      4: t('notAuthChangeEndedTrip'),
      5: t('absenceCar'),
      6: t('carNotAuthTakeKids'),
      7: t('overlapCar'),
      8: t('permissionDendForDriverInCar'),
    }),
    [t],
  );

  const updateDriverErrorMsg = useMemo(
    () => ({
      0: t('error'),
      1: t('driverLock'),
      2: t('notAuthChangeTrips'),
      3: t('notAuthChangeOldTrip'),
      4: t('notAuthChangeEndedTrip'),
      5: t('absenceDriver'),
      6: t('driverNotAuthTakeKids'),
      7: t('overlapBetweenTrips'),
      8: t('maxWorkHours'),
      9: t('driverNotAuthFromSafetyOfficer'),
      10: t('driverNotAuthFromHR'),
      11: t('notActiveDriver'),
      12: t('notActiveTrip'),
      13: t('permissionDendForDriverInCar'),
    }),
    [t],
  );

  const newTripErrorMsg = useMemo(
    () => ({
      0: t('error'),
      1: t('notAuthAddTrip'),
      2: t('notAuthAddOldTrip'),
      3: t('regulation'),
      4: t('overlapDriver'),
      5: t('overlapCar'),
      6: t('absenceDriver'),
      7: t('absenceCar'),
    }),
    [t],
  );

  const messagesLock = useMemo(
    () => ({
      9: t('permissionDendForDriverInCar'),
      10: t('tripLockByClientLock'),
      11: t('invoiceGenerated'),
      12: t('branchLock'),
      13: t('attendanceLock'),
      14: t('notAuthChangeOldTrip'),
      15: t('notAuthUpdateDetails'),
      16: t('notAuthUpdateOrderData'),
      20: t('tripLockByDriverLock'),
      30: t('LockCourseTrip'),
      31: t('lockByTypeOrder'),
      40: t('lockByLong'),
      50: t('tripNotOrder'),
    }),
    [t],
  );

  return {
    deleteErrorMsg,
    updateStatusErrorMsg,
    updateCarErrorMsg,
    updateDriverErrorMsg,
    newTripErrorMsg,
    messagesLock,
  };
};

export default useGetErrorMsg;
