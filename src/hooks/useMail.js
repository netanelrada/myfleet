import { Linking } from 'react-native';
import { useCallback } from 'react';

const useMail = () => {
	const mailTo = useCallback(email => {
		const url = `mailto:${email}`;
		Linking.canOpenURL(url)
			.then(() => {
				Linking.openURL(url);
			})
			.catch(error => console.log(error));
	}, []);
	return { mailTo };
};

export default useMail;
