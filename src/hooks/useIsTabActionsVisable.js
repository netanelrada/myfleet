import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
	editTripsSelector,
	isEditModeSelector,
} from '../store/selectors/tripsSelectors';
import { useNavigationState } from '@react-navigation/native';

const useIsTabActionsVisable = () => {
	const [isTabActionVisable, setisTabActionVisable] = useState(false);
	const editTrips = useSelector(state => editTripsSelector(state));
	const isEditMode = useSelector(state => isEditModeSelector(state));

	useEffect(() => {
		if (editTrips.length > 0 && isEditMode) {
			setisTabActionVisable(true);
		} else {
			setisTabActionVisable(false);
		}
	}, [editTrips.length, isEditMode]);

	return isTabActionVisable;
};

export default useIsTabActionsVisable;
