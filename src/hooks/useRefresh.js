import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { userSelector } from '../store/selectors/loginSelectors';
import { dateSelector } from '../store/selectors/tripsSelectors';
import { initGetTrips, getTripsByLineCode } from '../store/actions/actionTrips';

const useRefresh = () => {
	const dispatch = useDispatch();
	const onInitGetTrips = payload => dispatch(initGetTrips(payload));
	const onGetTripsByLineCode = payload => dispatch(getTripsByLineCode(payload));
	const user = useSelector(state => userSelector(state));
	const date = useSelector(state => dateSelector(state));

	const onRefresh = () => {
		const { userToken, wsString } = user;
		const relativeDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
		const payload = {
			userToken,
			wsString,
			relativeDate,
		};

		onInitGetTrips(payload);
	};

	const onRefreshByLineCode = (lineCode = '', isNewTrip = false) => {
		const { userToken, wsString } = user;
		const relativeDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
		const payload = {
			userToken,
			wsString,
			relativeDate,
			lineCode,
			isNewTrip,
		};

		onGetTripsByLineCode(payload);
	};

	return { onRefresh, onRefreshByLineCode };
};

export default useRefresh;
