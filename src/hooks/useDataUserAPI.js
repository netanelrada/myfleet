import {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import moment from 'moment';
import {userSelector} from '../store/selectors/loginSelectors';
import {
  dateSelector,
  editTripsSelector,
} from '../store/selectors/tripsSelectors';
import {fetchDataStatusE} from '../utils/enums';

const useDataUserAPI = ({promise}) => {
  const [data, setData] = useState([]);
  const [filterData, setFilterData] = useState([]);
  const [statusData, setStatusData] = useState(null);
  const user = useSelector((state) => userSelector(state));
  const date = useSelector((state) => dateSelector(state));
  const editTrips = useSelector((state) => editTripsSelector(state));

  useEffect(() => {
    onGetDrivers({onlyFree: 0});
  }, []);

  const onGetDrivers = async ({onlyFree = 0}) => {
    setStatusData(fetchDataStatusE.LOADING);
    try {
      const {userToken, wsString} = user;
      const relativeDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
      const payload = {
        userToken,
        wsString,
        relativeDate,
        onlyFree,
      };

      const res = await promise(payload);
      setData(res.data.data);
      setFilterData(res.data.data);
      setStatusData(fetchDataStatusE.SUCCESS);
    } catch (error) {
      setStatusData(fetchDataStatusE.FAIL);
      console.log(error);
    }
  };

  const updateLines = async ({promise, payloadProp}) => {
    try {
      const {userToken, wsString} = user;
      const lines = editTrips.map(({lineCode}) => lineCode).join(',');
      const payload = {
        wsString,
        userToken,
        lines,
        ...payloadProp,
      };

      const res = await promise(payload);
      res.data.updateLines = lines;

      return res;
    } catch (error) {
      console.log('error', error);
      return error;
    }
  };

  const onFilterData = (onlyFree) => {
    setStatusData(fetchDataStatusE.LOADING);
    if (onlyFree) {
      const filterData = data?.filter(({freeNow}) => freeNow === '1');
      setFilterData(filterData);
    } else {
      setFilterData([...data]);
    }
    setTimeout(() => {
      setStatusData(fetchDataStatusE.SUCCESS);
    }, 300);
  };

  return {filterData, updateLines, onGetDrivers, statusData, onFilterData};
};

export default useDataUserAPI;
