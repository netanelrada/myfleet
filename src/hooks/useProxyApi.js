import React from 'react';
import { useSelector } from 'react-redux';
import { userSelector } from '../store/selectors/loginSelectors';

const useProxyApi = () => {
	const user = useSelector(state => userSelector(state));

	const proxyApi = ({ cb = () => {}, restProp = {} }) => {
		const { wsString, userToken } = user;

		return cb({ wsString, userToken, ...restProp });
	};
	return { proxyApi };
};

export default useProxyApi;
