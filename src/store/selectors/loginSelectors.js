export const loginSelector = state => state.loginReducer;
export const isConnectSelector = state => loginSelector(state).isConnect;
export const isAuthenticatedSelector = state =>
	loginSelector(state).isAuthenticated;
export const isFinshedInitAuthSelector = state =>
	loginSelector(state).isFinshedInitAuth;
export const statusFailSelector = state => loginSelector(state).statusFail;
export const userSelector = state => loginSelector(state).user;
export const loginByUUIDStatusSelector = state =>
	loginSelector(state).loginByUUIDStatus;
