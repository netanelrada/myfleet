export const settingsSelector = (state) => state.settingsReducer;
export const fontSizeSelector = (state) => settingsSelector(state).fontSize;
export const languageSelector = (state) => settingsSelector(state).language;
