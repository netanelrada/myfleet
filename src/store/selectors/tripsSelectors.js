export const tripsSelector = (state) => state.tripsReducer;
export const allTripsSelector = (state) => tripsSelector(state).allTrips;
export const filterTripsSelector = (state) => tripsSelector(state).filterTrips;
export const tripsStatusSelector = (state) => tripsSelector(state).tripsStatus;
export const isEditModeSelector = (state) => tripsSelector(state).isEditMode;
export const dateSelector = (state) => tripsSelector(state).date;
export const editTripsSelector = (state) => tripsSelector(state).editTrips;
export const countFilterSelector = (state) => tripsSelector(state).countFilter;
export const filterBySelector = (state) => tripsSelector(state).filterBy;
export const tabFoucsSelector = (state) => tripsSelector(state).tabFoucs;
export const newLineCodeSelector = (state) => tripsSelector(state).newLineCode;
export const isHandleActionSelector = (state) =>
  tripsSelector(state).isHandleAction;
export const branchesSelector = (state) => tripsSelector(state).branches;
