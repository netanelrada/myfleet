export const persistenceFormDataSelector = state =>
	state.persistenceFormDataReducer;
export const lastSelectedClientsListSelector = state =>
	persistenceFormDataSelector(state).lastSelectedClientsList;

export const defaultValuesSelector = state =>
	persistenceFormDataSelector(state).defaultValues;

///lastSelectedClients
