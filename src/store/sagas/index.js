import { all } from 'redux-saga/effects';
// Import your saga files here
import loginSaga from './loginSaga';
import tripsSaga from './tripsSaga';

export default function* watchRootSaga() {
	yield all([
		loginSaga(),
		tripsSaga(),
		// Call your sagas here
	]);
}
