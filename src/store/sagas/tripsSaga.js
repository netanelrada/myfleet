import {takeLatest, put, select} from 'redux-saga/effects';
import * as APIActions from '../../api/api';
import * as actionTypes from '../actions/actionTypes';
import * as actions from '../actions/actionTrips';
import {filterBySelector} from '../selectors/tripsSelectors';
import {typeOrderByE} from '../../utils/enums';
import {sortBy} from 'lodash';
import moment from 'moment';
import {sortByTime} from '../../utils/time';

function* initGetTrips({payload}) {
  try {
    const filterBy = yield select((state) => filterBySelector(state));

    const filterParam = Object.keys(filterBy).reduce((acc, curr) => {
      if (curr === 'lineDescription' && filterBy[curr].text) {
        acc[curr] = filterBy[curr].text;
      } else if (
        curr !== 'lineDescription' &&
        curr !== 'orderBy' &&
        filterBy[curr]?.key
      ) {
        acc[curr] = filterBy[curr].key;
      }
      return acc;
    }, {});
    filterParam.branchCode = filterBy.branchCode;
    filterParam.isOperationGroup = filterBy.isOperationGroup;

    const response = yield APIActions.getTrips({...payload, ...filterParam});

    if (response.data) {
      let data = response.data.data;
      if (filterBy.orderBy === typeOrderByE.HOUR) {
        data = data.sort((a, b) => sortByTime(a, b));
      } else {
        data = data.sort((a, b) => {
          if (a[filterBy.orderBy] < b[filterBy.orderBy]) {
            return -1;
          }
          if (a[filterBy.orderBy] > b[filterBy.orderBy]) {
            return 1;
          }
          if (a[filterBy.orderBy] === b[filterBy.orderBy]) {
            return sortByTime(a, b);
          }
        });
      }
      yield put(actions.getTripsSuccess(data));
    } else yield put(actions.getTripsFail());
  } catch (error) {
    console.log({error});
    yield put(actions.getTripsFail());
  }
}

function* getTripsByLineCode({payload}) {
  try {
    const {isNewTrip = false, ...restPayload} = payload;
    const response = yield APIActions.getTrips({...restPayload});
    if (response.data) {
      const data = response.data.data;
      yield put(
        actions.getTripsByLineCodeSuccess({
          data,
          lines: payload.lineCode,
          isNewTrip,
        }),
      );
    }
  } catch (error) {
    console.log({error});
  }
}

function* getBranches({payload}) {
  try {
    const response = yield APIActions.getBranches(payload);
    if (response.data) {
      yield put(actions.getBranchesSuccess(response.data.branches));
    }
  } catch (error) {
    console.log({error});
  }
}
export default function* tripsSaga() {
  yield takeLatest(actionTypes.INIT_GET_TRIPS, initGetTrips);
  yield takeLatest(actionTypes.GET_TRIPS_BY_LINE_CODE, getTripsByLineCode);
  yield takeLatest(actionTypes.INIT_GET_BRANCHES, getBranches);
}
