import {takeLatest, put} from 'redux-saga/effects';
import * as APIActions from '../../api/api';
import * as actionTypes from '../actions/actionTypes';
import * as actions from '../actions/actionLogin';
import {setFilterByBranch} from '../actions/actionTrips';

function* initConnect({payload}) {
  try {
    const response = yield APIActions.login(payload);

    if (response.data.companyCode) {
      yield put(actions.connectSuccess(response.data));
      const {branchCode = '', isOperationGroup = ''} = JSON.parse(
        response.data.fcResponse,
      );
      yield put(setFilterByBranch({branchCode, isOperationGroup}));
    } else {
      const statusFail = Math.abs(response.data.response);
      yield put(actions.connectFail(statusFail));
    }
  } catch (error) {
    yield put(actions.connectFail(0));
  }
}

function* connectByUUID({payload}) {
  try {
    const response = yield APIActions.loginByUUID();
    if (response.data.companyCode) {
      yield put(actions.connectByUUIDSuccess(response.data));
      const {branchCode = '', isOperationGroup = ''} = JSON.parse(
        response.data.fcResponse,
      );
      yield put(setFilterByBranch({branchCode, isOperationGroup}));
    } else {
      yield put(actions.connectByUUIDFail());
    }
  } catch (error) {
    yield put(actions.connectByUUIDFail());
  }
}

export default function* loginSaga() {
  yield takeLatest(actionTypes.INIT_CONNECT, initConnect);
  yield takeLatest(actionTypes.CONNECT_BY_UUID, connectByUUID);
}
