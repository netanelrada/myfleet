import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import {composeWithDevTools} from 'redux-devtools-extension';
import {rootReducer} from './reducers/index';
import rootSaga from './sagas/index';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['persistenceFormDataReducer', 'settingsReducer'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const Saga = createSagaMiddleware();
const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(Saga)),
);

Saga.run(rootSaga);
export default {store, persistor: persistStore(store)};
