import * as actionTypes from './actionTypes';

export const initConnect = payload => {
	return {
		type: actionTypes.INIT_CONNECT,
		payload,
	};
};

export const connectSuccess = payload => {
	return {
		type: actionTypes.CONNECT_SUCCESS,
		payload,
	};
};

export const connectFail = payload => {
	return {
		type: actionTypes.CONNECT_FAIL,
		payload,
	};
};

export const connectByUUID = () => {
	return {
		type: actionTypes.CONNECT_BY_UUID,
	};
};

export const connectByUUIDSuccess = payload => {
	return {
		type: actionTypes.CONNECT_BY_UUID_SUCCESS,
		payload,
	};
};

export const connectByUUIDFail = () => {
	return {
		type: actionTypes.CONNECT_BY_UUID_FAIL,
	};
};

export const restIsFinshedInitAuth = () => {
	return {
		type: actionTypes.REST_IS_FINSHED_INIT_AUTH,
	};
};

export const restUserData = () => {
	return {
		type: actionTypes.REST_USER_DATA,
	};
};
