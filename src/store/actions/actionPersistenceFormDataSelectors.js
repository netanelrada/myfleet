import * as actionTypes from './actionTypes';

export const addClient = payload => {
	return {
		type: actionTypes.ADD_CLIENT,
		payload,
	};
};

export const setDefaultValues = payload => {
	return {
		type: actionTypes.SET_DEFAULT_VALUES,
		payload,
	};
};

export const restDefaultValues = payload => {
	return {
		type: actionTypes.REST_DEFAULT_VALUES,
		payload,
	};
};

export const restAllPersistenceData = payload => {
	return {
		type: actionTypes.REST_ALL_PERSISTENCE_DATA,
		payload,
	};
};
