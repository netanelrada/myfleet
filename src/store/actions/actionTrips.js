import * as actionTypes from './actionTypes';

export const initGetTrips = (payload) => {
  return {
    type: actionTypes.INIT_GET_TRIPS,
    payload,
  };
};

export const getTripsSuccess = (payload) => {
  return {
    type: actionTypes.GET_TRIPS_SUCCESS,
    payload,
  };
};

export const getTripsFail = (payload) => {
  return {
    type: actionTypes.GET_TRIPS_FAIL,
    payload,
  };
};

export const filterTripsByClinetName = (payload) => {
  return {
    type: actionTypes.FILTER_TRIPS_BY_CLIENT_NAME,
    payload,
  };
};

export const toggleIsEditMode = () => {
  return {
    type: actionTypes.TOGGLE_IS_EDIT_MODE,
  };
};

export const setEditMode = (payload) => {
  return {
    type: actionTypes.SET_EDIT_MODE,
    payload,
  };
};

export const setDate = (payload) => {
  return {
    type: actionTypes.SET_DATE,
    payload,
  };
};

export const addEditTrip = (payload) => {
  return {
    type: actionTypes.ADD_EDIT_TRIP,
    payload,
  };
};

export const removeEditTrip = (payload) => {
  return {
    type: actionTypes.REMOVE_EDIT_TRIP,
    payload,
  };
};

export const setFilterBy = (payload) => {
  return {
    type: actionTypes.SET_FILTER_BY,
    payload,
  };
};

export const selectAll = (payload) => {
  return {
    type: actionTypes.SELECT_ALL,
    payload,
  };
};

export const cancelAll = (payload) => {
  return {
    type: actionTypes.CANCEL_ALL,
    payload,
  };
};
export const setFilterTrips = (payload) => {
  return {
    type: actionTypes.SET_FILTER_TRIPS,
    payload,
  };
};

export const setTabFoucs = (payload) => {
  return {
    type: actionTypes.SET_TAB_FOUCS,
    payload,
  };
};

export const setSingleEditTrip = (payload) => {
  return {
    type: actionTypes.SET_SINGLE_EDIT_TRIP,
    payload,
  };
};

export const getTripsByLineCode = (payload) => {
  return {
    type: actionTypes.GET_TRIPS_BY_LINE_CODE,
    payload,
  };
};

export const getTripsByLineCodeSuccess = (payload) => {
  return {
    type: actionTypes.GET_TRIPS_BY_LINE_CODE_SUCCESS,
    payload,
  };
};

export const addEditSingleTrip = (payload) => {
  return {
    type: actionTypes.ADD_EDIT_SINGLE_TRIP,
    payload,
  };
};

export const initGetBranches = (payload) => {
  return {
    type: actionTypes.INIT_GET_BRANCHES,
    payload,
  };
};

export const getBranchesSuccess = (payload) => {
  return {
    type: actionTypes.GET_BRANCHES_SUCCESS,
    payload,
  };
};

export const getBranchesFail = (payload) => {
  return {
    type: actionTypes.GET_BRANCHES_FAIL,
    payload,
  };
};

export const setFilterByBranch = (payload) => {
  return {
    type: actionTypes.SET_FILTER_BY_BRANCH,
    payload,
  };
};
