import * as actionTypes from './actionTypes';

export const setFontSize = (payload) => {
  return {
    type: actionTypes.SET_FONT_SIZE,
    payload,
  };
};

export const setLanguage = (payload) => {
  return {
    type: actionTypes.SET_LANGUAGE,
    payload,
  };
};
