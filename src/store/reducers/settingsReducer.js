import * as actionTypes from '../actions/actionTypes';
import {fontSizesE} from '../../utils/enums';

const initialState = {
  fontSize: fontSizesE.REGULAR,
  language: 'he',
};

const setFontSize = (state, payload) => {
  return {
    ...state,
    fontSize: payload,
  };
};

const setLanguage = (state, payload) => {
  return {
    ...state,
    language: payload,
  };
};

const handlerTypes = {
  [actionTypes.SET_FONT_SIZE]: setFontSize,
  [actionTypes.SET_LANGUAGE]: setLanguage,
};

const settingsReducer = (state = initialState, {type, payload}) => {
  const handler = handlerTypes[type];
  if (handler) {
    return handler(state, payload);
  }
  return state;
};

export default settingsReducer;
