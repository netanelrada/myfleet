import {combineReducers} from 'redux';

import loginReducer from './loginReducer';
import tripsReducer from './tripsReducer';
import persistenceFormDataReducer from './persistenceFormDataReducer';
import settingsReducer from './settingsReducer';

export const rootReducer = combineReducers({
  // Put your imported reducers here.
  loginReducer,
  tripsReducer,
  persistenceFormDataReducer,
  settingsReducer,
});
