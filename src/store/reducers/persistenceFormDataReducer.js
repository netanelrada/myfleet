import * as actionTypes from '../actions/actionTypes';

export const initialState = {
  lastSelectedClientsList: [],
  defaultValues: {
    lineDate: '',
    startTime: '00:00',
    endTime: '00:00',
    clientCode: {key: '', text: ''},
    lineDescription: {key: '', text: ''},
    driverCode: {key: '', text: ''},
    carTypeCode: {key: '', text: ''},
    passQty: {key: '', text: ''},
    carCode: {key: '', text: ''},
    visaNumber: {key: '', text: ''},
    shortRemarks: {key: '', text: ''},
    longRemarks: {key: '', text: ''},
  },
};

const addClient = (state, payload) => {
  const newLastSelectedClientsList = [...state.lastSelectedClientsList];
  const isExists =
    newLastSelectedClientsList.findIndex(
      ({clientCode}) => clientCode === payload.clientCode,
    ) !== -1;
  if (isExists) return {...state};

  newLastSelectedClientsList.unshift(payload);

  return {
    ...state,
    lastSelectedClientsList: newLastSelectedClientsList.slice(0, 3),
  };
};

const setDefaultValues = (state, payload) => {
  return {
    ...state,
    defaultValues: {...payload},
  };
};

const restDefaultValues = (state, payload) => {
  return {
    ...state,
    defaultValues: {...initialState.defaultValues},
  };
};

const restAllPersistenceData = (state, payload) => {
  return {
    ...initialState,
  };
};

const handlerTypes = {
  [actionTypes.ADD_CLIENT]: addClient,
  [actionTypes.SET_DEFAULT_VALUES]: setDefaultValues,
  [actionTypes.REST_DEFAULT_VALUES]: restDefaultValues,
  [actionTypes.REST_ALL_PERSISTENCE_DATA]: restAllPersistenceData,
};

const persistenceFormDataReducer = (state = initialState, {type, payload}) => {
  const handler = handlerTypes[type];
  if (handler) {
    return handler(state, payload);
  }
  return state;
};

export default persistenceFormDataReducer;
