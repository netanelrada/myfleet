import moment from 'moment';
import {sortBy} from 'lodash';
import * as actionTypes from '../actions/actionTypes';
import {tripsStatusE, typeOrderByE} from '../../utils/enums';
import {updatePropInArrById} from '../../utils/utilis';
import {sortByTime} from '../../utils/time';

const initialState = {
  allTrips: [],
  filterTrips: [],
  tripsStatus: null,
  date: moment().format('D.M.YYYY'),
  isEditMode: false,
  editTrips: [],
  filterBy: {
    orderBy: typeOrderByE.HOUR,
    driverCode: {key: '', text: ''},
    clientCode: {key: '', text: ''},
    carCode: {key: '', text: ''},
    lineDescription: {key: '', text: ''},
    lineTypeCode: {key: '', text: ''},
    branchCode: '1',
    isOperationGroup: '0',
  },
  countFilter: 0,
  tabFoucs: {key: null, clickOnTab: false},
  newLineCode: null,
  isHandleAction: false,
  branches: [],
};

const initGetTrips = (state, payload) => {
  return {
    ...state,
    tripsStatus: tripsStatusE.LOADING,
  };
};

const getTripsSuccess = (state, payload) => {
  return {
    ...state,
    allTrips: payload.map((trip) => ({...trip, isSelectedToEdit: false})),
    filterTrips: payload.map((trip) => ({...trip, isSelectedToEdit: false})),
    tripsStatus: tripsStatusE.SUCCESS,
    editTrips: [],
  };
};

const getTripsByLineCode = (state, payload) => {
  return {
    ...state,
    isHandleAction: true,
  };
};

const getTripsByLineCodeSuccess = (state, payload) => {
  let newAllTrips = [...state.allTrips];

  if (payload.isNewTrip) {
    const newTrip = payload.data[0];

    const index = newAllTrips.findIndex(
      (trip) => trip.lineCode === newTrip.lineCode,
    );
    if (index !== -1) {
      newAllTrips[index] = newTrip;
    } else {
      newAllTrips.push(newTrip);
      if (state.filterBy.orderBy === typeOrderByE.HOUR) {
        newAllTrips = newAllTrips.sort((a, b) => sortByTime(a, b));
      } else {
        newAllTrips = newAllTrips.sort((a, b) => {
          if (a[state.filterBy.orderBy] < b[state.filterBy.orderBy]) {
            return -1;
          }
          if (a[state.filterBy.orderBy] > b[state.filterBy.orderBy]) {
            return 1;
          }
          if (a[state.filterBy.orderBy] === b[state.filterBy.orderBy]) {
            return sortByTime(a, b);
          }
        });
        //sortBy(newAllTrips, [state.filterBy.orderBy, typeOrderByE.HOUR]);
      }
    }
    return {
      ...state,
      allTrips: newAllTrips,
      filterTrips: newAllTrips,
      newLineCode: payload.lines.toString(),
      isHandleAction: false,
    };
  }

  if (payload.data.length === 0) {
    const removeLines = payload.lines.toString();
    const arrRemoveLines = removeLines.split(',');
    newAllTrips = newAllTrips.filter(
      ({lineCode}) => !arrRemoveLines.includes(lineCode.toString()),
    );
  }

  payload.data.forEach((line) => {
    const index = newAllTrips.findIndex(
      (trip) => trip.lineCode === line.lineCode,
    );
    if (index !== -1) {
      line.isSelectedToEdit = false;
      newAllTrips[index] = line;
    }
  });

  return {
    ...state,
    allTrips: newAllTrips,
    filterTrips: newAllTrips,
    isHandleAction: false,
  };
};

const getTripsFail = (state, payload) => {
  return {
    ...state,
    tripsStatus: tripsStatusE.FAIL,
  };
};

const filterTripsByClinetName = (state, payload) => {
  return {
    ...state,
    filterTrips: state.filterTrips.filter((trip) =>
      trip.client_name.includes(payload),
    ),
  };
};

const toggleIsEditMode = (state, payload) => {
  const isNotEmptyTrips = state.filterTrips.length === 0;
  if (isNotEmptyTrips) return {...state, isEditMode: false};
  return {
    ...state,
    isEditMode: !state.isEditMode,
    filterTrips: state.filterTrips.map((trip) => ({
      ...trip,
      isSelectedToEdit: false,
    })),
    editTrips: [],
  };
};
const setEditMode = (state, payload) => {
  const isNotEmptyTrips = state.filterTrips.length === 0;
  if (isNotEmptyTrips) return {...state, isEditMode: false};
  return {
    ...state,
    isEditMode: payload,
    filterTrips: state.filterTrips.map((trip) => ({
      ...trip,
      isSelectedToEdit: false,
    })),
    editTrips: [],
  };
};

const setDate = (state, payload) => {
  return {
    ...state,
    date: moment(payload, ['D.M.YYYYY', 'YYYY-MM-DD']).format('D.M.YYYY'),
    isEditMode: false,
    editTrips: [],
  };
};

const addEditTrip = (state, payload) => {
  updatePropInArrById(state.allTrips, payload.lineCode, true);
  updatePropInArrById(state.filterTrips, payload.lineCode, true);

  return {...state, editTrips: [...state.editTrips, payload]};
};

const removeEditTrip = (state, payload) => {
  updatePropInArrById(state.allTrips, payload, false);
  updatePropInArrById(state.filterTrips, payload, false);
  return {
    ...state,
    editTrips: state.editTrips.filter((trip) => trip.lineCode !== payload),
  };
};

const setFilterBy = (state, payload) => {
  return {
    ...state,
    filterBy: {
      ...state.filterBy,
      ...payload,
    },
    countFilter: Object.values(payload).reduce((acc, curr, index) => {
      let sum = 0;
      if (index > 0) sum += !!curr.text;

      return acc + sum;
    }, 0),
  };
};

const selectAll = (state, payload) => {
  return {
    ...state,
    filterTrips: state.filterTrips.map((trip) => {
      const findedTrip = state.allTrips.find(
        (el) => el.lineCode === trip.lineCode,
      );
      if (findedTrip) findedTrip.isSelectedToEdit = true;
      return {
        ...trip,
        isSelectedToEdit: true,
      };
    }),
    editTrips: [...state.filterTrips],
  };
};

const cancelAll = (state, payload) => {
  return {
    ...state,
    allTrips: state.allTrips.map((trip) => {
      return {
        ...trip,
        isSelectedToEdit: false,
      };
    }),
    filterTrips: state.filterTrips.map((trip) => {
      return {
        ...trip,
        isSelectedToEdit: false,
      };
    }),
    editTrips: [],
  };
};

const setFilterTrips = (state, payload) => {
  return {
    ...state,
    filterTrips: payload,
  };
};

const setTabFoucs = (state, payload) => {
  return {
    ...state,
    tabFoucs: payload,
  };
};

const setSingleEditTrip = (state, payload) => {
  return {
    ...state,
    editTrips: [{...payload}],
    isEditMode: false,
  };
};

const addEditSingleTrip = (state, payload) => {
  return {
    ...state,
    editTrips: [payload],
  };
};

const getBranchesSuccess = (state, payload) => {
  return {
    ...state,
    branches: [...payload],
  };
};

const setFilterByBranch = (state, payload) => {
  return {
    ...state,
    filterBy: {
      ...state.filterBy,
      ...payload,
    },
  };
};

const handlerTypes = {
  [actionTypes.INIT_GET_TRIPS]: initGetTrips,
  [actionTypes.GET_TRIPS_SUCCESS]: getTripsSuccess,
  [actionTypes.GET_TRIPS_FAIL]: getTripsFail,
  [actionTypes.FILTER_TRIPS_BY_CLIENT_NAME]: filterTripsByClinetName,
  [actionTypes.TOGGLE_IS_EDIT_MODE]: toggleIsEditMode,
  [actionTypes.SET_EDIT_MODE]: setEditMode,
  [actionTypes.SET_DATE]: setDate,
  [actionTypes.ADD_EDIT_TRIP]: addEditTrip,
  [actionTypes.REMOVE_EDIT_TRIP]: removeEditTrip,
  [actionTypes.SET_FILTER_BY]: setFilterBy,
  [actionTypes.SELECT_ALL]: selectAll,
  [actionTypes.CANCEL_ALL]: cancelAll,
  [actionTypes.SET_FILTER_TRIPS]: setFilterTrips,
  [actionTypes.SET_TAB_FOUCS]: setTabFoucs,
  [actionTypes.SET_SINGLE_EDIT_TRIP]: setSingleEditTrip,
  [actionTypes.GET_TRIPS_BY_LINE_CODE]: getTripsByLineCode,
  [actionTypes.GET_TRIPS_BY_LINE_CODE_SUCCESS]: getTripsByLineCodeSuccess,
  [actionTypes.ADD_EDIT_SINGLE_TRIP]: addEditSingleTrip,
  [actionTypes.GET_BRANCHES_SUCCESS]: getBranchesSuccess,
  [actionTypes.SET_FILTER_BY_BRANCH]: setFilterByBranch,
};

const loginReducer = (state = initialState, {type, payload}) => {
  const handler = handlerTypes[type];
  if (handler) {
    return handler(state, payload);
  }
  return state;
};

export default loginReducer;
