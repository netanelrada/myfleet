import * as actionTypes from '../actions/actionTypes';
import { loginByUUIDStatusE } from '../../utils/enums';

const initialState = {
	isAuthenticated: false,
	isFinshedInitAuth: false,
	statusFail: null,
	user: null,
	loginByUUIDStatus: null,
};

const initConnect = (state, payload) => {
	return {
		...state,
		isFinshedInitAuth: false,
	};
};

const connectSuccess = (state, payload) => {
	return {
		...state,
		isAuthenticated: true,
		isFinshedInitAuth: true,
		user: { ...payload },
	};
};

const connectFail = (state, payload) => {
	return {
		...state,
		isAuthenticated: false,
		isFinshedInitAuth: true,
		statusFail: payload,
	};
};

const connectByUUID = (state, payload) => {
	return {
		...state,
		loginByUUIDStatus: loginByUUIDStatusE.LOADING,
	};
};

const connectByUUIDSuccess = (state, payload) => {
	return {
		...state,
		loginByUUIDStatus: loginByUUIDStatusE.SUCCESS,
		user: { ...payload },
	};
};

const connectByUUIDFail = (state, payload) => {
	return {
		...state,
		loginByUUIDStatus: loginByUUIDStatusE.FAIL,
	};
};

const restIsFinshedInitAuth = (state, payload) => {
	return {
		...state,
		isFinshedInitAuth: false,
	};
};

const restUserData = (state, payload) => {
	return {
		...initialState,
	};
};

const handlerTypes = {
	[actionTypes.INIT_CONNECT]: initConnect,
	[actionTypes.CONNECT_SUCCESS]: connectSuccess,
	[actionTypes.CONNECT_FAIL]: connectFail,
	[actionTypes.CONNECT_BY_UUID]: connectByUUID,
	[actionTypes.CONNECT_BY_UUID_SUCCESS]: connectByUUIDSuccess,
	[actionTypes.CONNECT_BY_UUID_FAIL]: connectByUUIDFail,
	[actionTypes.REST_IS_FINSHED_INIT_AUTH]: restIsFinshedInitAuth,
	[actionTypes.REST_USER_DATA]: restUserData,
};

const loginReducer = (state = initialState, { type, payload }) => {
	const handler = handlerTypes[type];
	if (handler) {
		return handler(state, payload);
	}
	return state;
};

export default loginReducer;
