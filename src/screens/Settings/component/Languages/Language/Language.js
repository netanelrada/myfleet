import React from 'react';
import styled from 'styled-components';
import { calcWidth } from '../../../../../utils/dimensions';
import Text from '../../../../../components/common/Text/Text';

const Language = ({
	name = 'English',
	isSelected = false,
	onSelect,
	isDisabled = false,
}) => {
	return (
		<s.Container
			disabled={isDisabled}
			isDisabled={isDisabled}
			onPress={onSelect}
			isSelected={isSelected}
		>
			<Text>{name}</Text>
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		${({ isDisabled }) => (isDisabled ? 'opacity: 0.4' : 'opacity: 1')}
		width: ${calcWidth(72)}px;
		aspect-ratio: ${72 / 28};
		background-color: ${({ theme }) => theme.colors.white};
		border: 1px solid
			${({ theme: { colors }, isSelected }) =>
				isSelected ? colors.blueLight : colors.gray7};
		border-radius: 4px;
		justify-content: center;
		align-items: center;
	`,
};

export default Language;
