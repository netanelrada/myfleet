import styled from 'styled-components';
import { calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(332)}px;
	aspect-ratio: ${332 / 85};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
	elevation: 3;
	justify-content: center;
	padding-horizontal: ${calcWidth(15)}px;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	color: ${({ theme }) => theme.colors.gray6};
	text-align: right;
`;

styles.OptionsLanguages = styled.View`
	flex-direction: row-reverse;
	justify-content: space-around;
`;

export default styles;
