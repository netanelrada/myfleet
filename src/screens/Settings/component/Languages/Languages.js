import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector, useDispatch} from 'react-redux';
import s from './styles';
import {languageSelector} from '../../../../store/selectors/settingsSelectors';
import {setLanguage} from '../../../../store/actions/actionsSettings';
import {languageSettings} from '../../../../i18n/locales/translations';
import SelectOption from '../../../../components/common/SelectOption/SelectOption';
import Language from './Language/Language';

const Languages = () => {
  const {t, i18n} = useTranslation();
  const dispathc = useDispatch();
  const onSetLanguage = (payload) => dispathc(setLanguage(payload));
  const currLan = useSelector((state) => languageSelector(state));

  const changeLan = (key) => {
    // i18n.changeLanguage(key);
    onSetLanguage(key);
  };

  return (
    <s.Container>
      <s.Label>{t('language')}</s.Label>
      <s.OptionsLanguages>
        <SelectOption
          updateSelectedOption={changeLan}
          value={currLan}
          options={Object.keys(languageSettings).map((key) => ({key}))}>
          {Object.keys(languageSettings).map((key) => (
            <Language
              key={key}
              name={languageSettings[key].language}
              isDisabled={false}
            />
          ))}
        </SelectOption>
      </s.OptionsLanguages>
    </s.Container>
  );
};

export default Languages;
