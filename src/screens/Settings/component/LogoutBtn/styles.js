import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.TouchableOpacity`
	width: ${calcWidth(332)}px;
	height: ${calcHeight(48)}px;
	border-radius: 9px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	justify-content: center;
	align-items: center;
	align-self: center;
`;

export default styles;
