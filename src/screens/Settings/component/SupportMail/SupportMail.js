import React from 'react';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import Text from '../../../../components/common/Text/Text';
import { SUPPORT_MAIL } from '../../../../utils/enums';
import useMail from '../../../../hooks/useMail';

const SupportMail = () => {
	const { mailTo } = useMail();
	return (
		<s.Container onPress={() => mailTo(SUPPORT_MAIL)}>
			<Text fontFamily='Rubik-Medium'>{SUPPORT_MAIL}</Text>
			<Icon name='mail' />
		</s.Container>
	);
};

export default SupportMail;
