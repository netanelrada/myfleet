import styled from 'styled-components';
import { calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.TouchableOpacity`
	width: ${calcWidth(180)}px;
	aspect-ratio: ${180 / 49};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
	elevation: 3;
	justify-content: space-around;
	align-items: center;
	flex-direction: row;
`;

export default styles;
