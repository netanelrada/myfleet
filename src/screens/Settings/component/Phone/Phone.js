import React from 'react';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import Text from '../../../../components/common/Text/Text';
import { SUPPORT_PHONE } from '../../../../utils/enums';
import useCall from '../../../../hooks/useCall';

const Phone = () => {
	const { call } = useCall();
	return (
		<s.Container onPress={() => call(SUPPORT_PHONE)}>
			<Text fontFamily='Rubik-Medium'>{SUPPORT_PHONE}</Text>
			<Icon name='phone' />
		</s.Container>
	);
};

export default Phone;
