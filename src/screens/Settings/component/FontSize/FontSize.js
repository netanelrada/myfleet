import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector, useDispatch} from 'react-redux';
import {fontSizeSelector} from '../../../../store/selectors/settingsSelectors';
import styled from 'styled-components';
import {calcWidth} from '../../../../utils/dimensions';
import SwitchText from '../../../../components/common/SwitchText/SwitchText';
import {setFontSize} from '../../../../store/actions/actionsSettings';
import {fontSizesE} from '../../../../utils/enums';

const FontSize = ({}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const onSetFontSize = (fontSize) => dispatch(setFontSize(fontSize));
  const fontSize = useSelector((state) => fontSizeSelector(state));

  const onClickSwitch = (value) => {
    if (value) {
      onSetFontSize(fontSizesE.BIG);
    } else {
      onSetFontSize(fontSizesE.REGULAR);
    }
  };

  return (
    <s.Container>
      <s.Label>{t('fontSize')}</s.Label>
      <SwitchText
        state={fontSize === fontSizesE.BIG}
        setState={onClickSwitch}
      />
    </s.Container>
  );
};

const s = {
  Container: styled.View`
    width: ${calcWidth(332)}px;
    aspect-ratio: ${332 / 49};
    background-color: ${({theme}) => theme.colors.white};
    border: 1px solid
      ${({theme: {colors}, isSelected}) =>
        isSelected ? colors.blueLight : colors.gray7};
    border-radius: 9px;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: center;
    padding-horizontal: ${calcWidth(15)}px;
  `,
  Label: styled.Text`
    font-family: Rubik-Regular;
    color: ${({theme}) => theme.colors.gray6};
  `,
};

export default FontSize;
