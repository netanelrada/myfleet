import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { calcWidth } from '../../../../utils/dimensions';
import Switch from '../../../../components/common/Switch/Switch';

const DarkMode = ({}) => {
	const { t } = useTranslation();
	const [isDarkMode, setisDarkMode] = useState(false);
	return (
		<s.Container pointerEvents='none'>
			<s.Label>{t('darkMode')}</s.Label>
			<Switch
				isEnabled={isDarkMode}
				toggleSwitch={() => setisDarkMode(preSate => !preSate)}
			/>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		width: ${calcWidth(332)}px;
		aspect-ratio: ${332 / 49};
		background-color: ${({ theme }) => theme.colors.white};
		border: 1px solid
			${({ theme: { colors }, isSelected }) =>
				isSelected ? colors.blueLight : colors.gray7};
		border-radius: 9px;
		flex-direction: row-reverse;
		justify-content: space-between;
		align-items: center;
		padding-horizontal: ${calcWidth(15)}px;
	`,
	Label: styled.Text`
		font-family: Rubik-Regular;
		color: ${({ theme }) => theme.colors.gray6};
	`,
};

export default DarkMode;
