import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {logout} from '../../api/api';
import {userSelector} from '../../store/selectors/loginSelectors';
import s from './styles';
import Text from '../../components/common/Text/Text';
import Header from '../../components/common/Header/Header';
import {useTranslation} from 'react-i18next';
import Phone from './component/Phone/Phone';
import SupportMail from './component/SupportMail/SupportMail';
import Languages from './component/Languages/Languages';
import Icon from '../../components/common/Icon/Icon';
import {WEB_SITE, APP_VERSION_NUMBER} from '../../utils/enums';
import DarkMode from './component/DarkMode/DarkMode';
import FontSize from './component/FontSize/FontSize';
import LogoutBtn from './component/LogoutBtn/LogoutBtn';
import useBackHandler from '../../hooks/useBackHandler';
import useProxyApi from '../../hooks/useProxyApi';
import {useNavigation} from '@react-navigation/native';
import {restUserData} from '../../store/actions/actionLogin';

const Settings = ({navigation}) => {
  const {t} = useTranslation();
  const {reset} = useNavigation();
  const dispatch = useDispatch();
  const onRestUserData = () => dispatch(restUserData());
  const {proxyApi} = useProxyApi();
  const {companyName = ''} = useSelector((state) => userSelector(state)) || {};
  const {goBack} = navigation;
  useBackHandler(() => {
    goBack();
    return true;
  });

  const onLogout = async () => {
    try {
      const res = await proxyApi({
        cb: logout,
      });
      if (res.data) {
        onRestUserData();
        reset({
          index: 0,
          routes: [{name: 'Initial'}],
        });
      }
    } catch (error) {
      console.log({error});
    }
  };

  const iconSize = {
    width: 50,
    height: 50,
  };

  return (
    <s.Container>
      <s.SafeAreaView>
        <Header onBack={goBack} title={t('settingsAndContact')} />
        <s.SettingsContainer>
          <s.ContactUS>
            <SupportMail />
            <Phone />
          </s.ContactUS>
          <Languages />
          <DarkMode />
          <FontSize />
        </s.SettingsContainer>
        <s.FooterContainer>
          <LogoutBtn onPress={onLogout} />
          <Text fontSize="s16">{companyName}</Text>
          <s.BottomContainer>
            <s.LogoContainer>
              <Icon {...iconSize} name="logo" />
              <Text fontSize="s16">{WEB_SITE}</Text>
            </s.LogoContainer>
            <Text fontSize="s16">{`${t(
              'version',
            )}: ${APP_VERSION_NUMBER}`}</Text>
          </s.BottomContainer>
        </s.FooterContainer>
      </s.SafeAreaView>
    </s.Container>
  );
};

export default Settings;
