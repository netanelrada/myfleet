import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({theme}) => theme.colors.white};
`;

styles.SafeAreaView = styled.SafeAreaView`
  flex: 1;
  width: 100%;
`;
styles.SettingsContainer = styled.View`
  height: 50%;
  width: 100%;
  align-items: center;
  justify-content: space-around;
  padding-horizontal: ${calcWidth(14)};
`;

styles.ContactUS = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
`;

styles.FooterContainer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
  align-items: center;
  padding-horizontal: ${calcWidth(14)}PX;
`;
styles.BottomContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  align-items: center;
`;

styles.LogoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export default styles;
