import { getLineData, setLineRemarks } from '../../../../api/api';
import { useSelector } from 'react-redux';
import { userSelector } from '../../../../store/selectors/loginSelectors';
import useHandleResponse from '../../../../hooks/useHandleResponse';

const useAPIRemarks = () => {
	const { userToken, wsString } = useSelector(state => userSelector(state));
	const { handleResponse } = useHandleResponse({ enableGoBack: true });

	const getRemarks = async lineCode => {
		try {
			const payload = {
				wsString,
				userToken,
				lineCode,
			};
			const res = await getLineData(payload);
			const { shortRemarks = '', longRemarks = '' } = res.data.data[0];


			return {
				short: shortRemarks,
				long: longRemarks,
			};
		} catch (error) {
			return {
				short: '',
				long: '',
			};
		}
	};

	const setRemarks = async ({ shortRemarks, longRemarks, lines }) => {
		try {
			const payload = {
				wsString,
				userToken,
				lines,
				shortRemarks,
				longRemarks,
				action: 2,
			};
			const res = await setLineRemarks(payload);
			res.data.updateLines = lines;
			handleResponse(res.data);
			console.log('res set remarks', res);
			return res;
		} catch (error) {
			console.log(error);
		}
	};
	return { getRemarks, setRemarks };
};

export default useAPIRemarks;
