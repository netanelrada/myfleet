import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.Pressable`
	flex: 1;
	align-items: center;
	padding-horizontal: ${calcWidth(13)}px;
`;

styles.CommentsContainer = styled.View`
	background-color: ${({ theme }) => theme.colors.white};
	width: ${calcWidth(332)}px;
	height: ${calcHeight(447)}px;
	border-radius: 9px;
	padding-horizontal: ${calcWidth(16)}px;
	flex-direction: row;
`;
styles.CommentsContentContainer = styled.View`
	flex: 1;
	padding-top: ${calcHeight(10)}px;
	height: ${calcHeight(447)}px;
	justify-content: flex-start;
`;

styles.Label = styled.Text`
	text-align: right;
	font-family: Rubik-Regular;
	margin-bottom: 3px;
`;

styles.ShortCommentsInput = styled.TextInput`
	border: 1px solid ${({ theme }) => theme.colors.borderColorInput};
	border-radius: 4px;
	height: ${calcHeight(33)}px;
	text-align: ${({ textAlign = 'right' }) => textAlign};
`;

styles.LongCommentsInput = styled.TextInput`
	border: 1px solid ${({ theme }) => theme.colors.borderColorInput};
	border-radius: 4px;
	height: ${calcHeight(150)}px;
	padding-horizontal: ${calcWidth(7)}px;
	text-align-vertical: top;
	text-align: ${({ textAlign = 'right' }) => textAlign};
`;

styles.ShortRemarksContainer = styled.View``;

styles.LongRemarksContainer = styled.View`
	margin-top: ${calcHeight(5)}px;
`;

styles.AddComment = styled.TouchableOpacity`
	align-self: flex-end;
	flex-direction: row;
	align-items: center;
`;
styles.TabContainer = styled.View`
	width: 100%;
	position: absolute;
	bottom: 0;
`;
export default styles;
