import React, {useState, useEffect} from 'react';
import {Keyboard} from 'react-native';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import Header from '../../components/Header/Header';
import {useSelector} from 'react-redux';
import s from './styles';
import {useTheme} from 'styled-components';
import Text from '../../components/common/Text/Text';
import Icon from '../../components/common/Icon/Icon';
import CustomKeyboardAvoidView from '../../components/common/CustomKeyboardAvoidView/CustomKeyboardAvoidView';
import useAPIRemarks from './components/hooks/useAPIRemarks';
import {editTripsSelector} from '../../store/selectors/tripsSelectors';
import TabConfirm from '../../components/common/TabConfirm/TabConfirm';
import useBackHandler from '../../hooks/useBackHandler';

const EditComments = ({navigation}) => {
  const {goBack} = navigation;
  useBackHandler(() => {
    goBack();
    return true;
  });
  const {t} = useTranslation();
  const editTrips = useSelector((state) => editTripsSelector(state))[0];
  const {getRemarks, setRemarks} = useAPIRemarks();

  const [comments, setComments] = useState({
    short: '',
    long: '',
  });

  useEffect(() => {
    initComments();
  }, []);

  const initComments = async () => {
    const res = await getRemarks(editTrips.lineCode);
    setComments(res);
  };
  const onChangeShortComments = (text) => {
    setComments((preState) => ({...preState, short: text}));
  };
  const onChangeLongComments = (text) => {
    setComments((preState) => ({...preState, long: text}));
  };

  const onSave = async () => {
    const {short, long} = comments;
    const res = await setRemarks({
      shortRemarks: short,
      longRemarks: long,
      lines: editTrips.lineCode,
    });
  };

  const closeKeyboard = () => {
    Keyboard.dismiss();
  };

  return (
    <ImageBackground>
      <s.Container onPress={closeKeyboard}>
        <Header
          onGoBack={goBack}
          titleText={t('commetns')}
          iconName="message"
        />
        <s.CommentsContainer>
          <s.CommentsContentContainer>
            <s.ShortRemarksContainer>
              <s.Label>{t('shortRemarks')}</s.Label>
              <s.ShortCommentsInput
                onChangeText={(text) => onChangeShortComments(text)}
                value={comments.short}
              />
            </s.ShortRemarksContainer>
            <s.LongRemarksContainer>
              <s.Label>{t('longRemarks')}</s.Label>
              <s.LongCommentsInput
                onChangeText={(text) => onChangeLongComments(text)}
                value={comments.long}
                multiline
              />
            </s.LongRemarksContainer>
          </s.CommentsContentContainer>
        </s.CommentsContainer>
      </s.Container>
      <s.TabContainer>
        <TabConfirm onCancel={goBack} onSave={onSave} />
      </s.TabContainer>
    </ImageBackground>
  );
};

export default EditComments;
