import React, {useState, useEffect, useRef} from 'react';
import moment from 'moment';
import s from './styles';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import Header from './components/Header/Header';
import LineDescription from './components/LineDescription/LineDescription';
import SerachList from '../../components/SerachList/SerachList';
import {useTranslation} from 'react-i18next';
import {data} from '../../utils/data';
import TableData from './components/TableData/TableData';
import {getDrivers, setLineDriver} from '../../api/api';
import {userSelector} from '../../store/selectors/loginSelectors';
import {
  dateSelector,
  editTripsSelector,
} from '../../store/selectors/tripsSelectors';
import {useSelector} from 'react-redux';
import ModalConfirm from '../../components/common/ModalConfirm/ModalConfirm';
import {calcWidth} from '../../utils/dimensions';
import ContentModalConfirm from './components/ContentModalConfirm/ContentModalConfirm';
import useDataUserAPI from '../../hooks/useDataUserAPI';
import OnlyFree from './components/OnlyFree/OnlyFree';
import useHandleResponse from '../../hooks/useHandleResponse';
import useGetErrorMsg from '../../hooks/useGetErrorMsg';
import useErrorAlertsModals from '../../hooks/useErrorAlertsModals';
import AlertsAndErrors from '../../components/AlertsAndErrors/AlertsAndErrors';
import {setStatusE} from '../../utils/enums';
import Text from '../../components/common/Text/Text';
import useBackHandler from '../../hooks/useBackHandler';

const DriverReplacement = ({navigation}) => {
  const {goBack} = navigation;
  useBackHandler(() => {
    goBack();
    return true;
  });
  const {updateDriverErrorMsg} = useGetErrorMsg();
  const {
    modalState,
    onCloseModal,
    setModalState,
    actionComplete,
  } = useErrorAlertsModals();
  const {
    filterData: driversData,
    updateLines,
    statusData,
    onFilterData,
  } = useDataUserAPI({
    promise: getDrivers,
  });
  const {handleResponse} = useHandleResponse({});
  const [isModalVisable, setisModalVisable] = useState({
    visable: false,
    isRemove: false,
  });
  const [isUpdateCar, setIsUpdateCar] = useState(true);
  const {t} = useTranslation();
  const editTrips = useSelector((state) => editTripsSelector(state));
  const driverSelectedRef = useRef(null);
  const singleTrip = editTrips.length === 1 ? editTrips[0] : false;
  const driverName = singleTrip?.driverName;

  const onPressDriver = (driverSelected) => {
    driverSelectedRef.current = driverSelected;
    validateUpdate();
  };

  const validateUpdate = async (isRemove = false) => {
    try {
      const {data = {}} = await updateLines({
        promise: setLineDriver,
        payloadProp: {
          driverCode: driverSelectedRef.current.driverCode,
          action: '0', // need to change
        },
      });
      const isComplete = actionComplete(data);

      if (!isComplete) {
        setModalState({
          isVisible: true,
          res: data,
        });
      } else setisModalVisable({isRemove, visable: true});
    } catch (error) {
      console.log(error);
    }
  };
  const onUpdateDriver = async () => {
    try {
      const {driverCode} = driverSelectedRef.current;
      const {data = {}} = await updateLines({
        promise: setLineDriver,
        payloadProp: {
          driverCode,
          updateCar: isUpdateCar ? '1' : '0',
          action: '2', // need to change
        },
      });
      closeModal();
      onCloseModal();
      handleResponse(data);
    } catch (error) {
      console.log(error);
    }
  };

  const onToggleSwitch = async (newValue) => {
    onFilterData(newValue);
    //	await onGetDrivers({ onlyFree: Number(newValue) });
  };

  const closeModal = () =>
    setisModalVisable((preState) => ({...preState, visable: false}));

  const filter = ({nickName}, text) =>
    nickName.toLowerCase().includes(text.toLowerCase());
  const tableHead = [t('freeFrom'), t('driverName'), t('comments')];
  const arrStyle = [
    {textAlign: 'right', flex: 1},
    {textAlign: 'right', flex: 2},
    {textAlign: 'center', flex: 0.7},
  ];

  const removeDriver = () => {
    const emptyData = {
      driverCode: '',
    };
    driverSelectedRef.current = emptyData;
    validateUpdate(true);
  };

  const getRemoveText = () => {
    if (!isModalVisable.isRemove) return '';
    if (singleTrip) {
      return t('removeDriver');
    } else {
      return t('removeDrivers');
    }
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header
          onGoBack={goBack}
          iconName="driver"
          leftText={driverName}
          disableDelete={!editTrips.some(({driverName}) => !!driverName)}
          onPressRemove={removeDriver}
          titleText={t('DriverReplacement')}
        />

        {singleTrip && (
          <LineDescription lineDescription={singleTrip.lineDescription} />
        )}
        <SerachList
          tableHead={tableHead}
          tableData={driversData}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressDriver}
          statusData={statusData}
          keyItem="driverCode"
        />

        <ModalConfirm
          isVisible={isModalVisable.visable}
          iconName="driver"
          onCancel={closeModal}
          onConfirm={onUpdateDriver}
          textConfirnBtn={t('ok')}
          textCancelBtn={t('cancel')}
          iconStyle={{
            width: calcWidth(28),
            height: calcWidth(28),
          }}>
          <ContentModalConfirm
            isSingleTrip={!!singleTrip}
            previousDriver={driverName}
            newDriver={driverSelectedRef.current?.nickName}
            isRemoveMode={isModalVisable.isRemove}
            removeText={getRemoveText()}
            checkBoxValue={isUpdateCar}
            onSetCheckboxValue={setIsUpdateCar}
          />
        </ModalConfirm>
        <AlertsAndErrors
          isVisible={modalState.isVisible}
          res={modalState.res}
          errorMessages={updateDriverErrorMsg}
          closeModal={onCloseModal}
          onConfirm={onUpdateDriver}
        />
      </s.Container>
      <OnlyFree toggleSwitch={onToggleSwitch} />
    </ImageBackground>
  );
};

export default DriverReplacement;
