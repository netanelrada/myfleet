import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	flex: 1;
	padding-horizontal: ${calcWidth(13)}px;
`;
styles.RemoveContainer = styled.TouchableOpacity`
	margin-left: 5px;
`;
styles.Text = styled.Text`
	font-size: ${({ theme }) => theme.fontSizes.s14};
	color: ${({ theme }) => theme.colors.errorColor};
	text-align: left;
`;

export default styles;
