import React from 'react';
import styled from 'styled-components';
import { calcHeight } from '../../../../utils/dimensions';
import Text from '../../../../components/common/Text/Text';
import Icon from '../../../../components/common/Icon/Icon';

const TableData = ({
	arrStyle = [],
	data: { freeFrom, nickName, absence, freeNow },
	onPress,
}) => {
	return (
		<s.Container onPress={onPress}>
			<s.Cell flex={arrStyle[0].flex}>
				<Text textAlign={arrStyle[0].textAlign}>{freeFrom}</Text>
			</s.Cell>
			<s.Cell flex={arrStyle[1].flex}>
				<Text
					color={freeNow == '0' ? '#EBB835' : null}
					textAlign={arrStyle[1].textAlign}
				>
					{nickName}
				</Text>
			</s.Cell>
			<s.Cell align={arrStyle[2].textAlign} flex={arrStyle[2].flex}>
				{absence == '1' && <Icon name='ourOfWork' />}
			</s.Cell>
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		width: 100%;
		height: ${calcHeight(52)}px;
		border-bottom-color: rgba(180, 190, 201, 0.3);
		border-bottom-width: 1px;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: flex-start;
	`,
	Cell: styled.View`
		flex: ${({ flex }) => flex};
		${({ align }) => `align-items: ${align};`}
	`,
};

export default TableData;
