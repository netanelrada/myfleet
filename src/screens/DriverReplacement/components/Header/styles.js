import styled from 'styled-components';
import { calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: 100%;
	height: 10%;
	flex-direction: row-reverse;
	justify-content: space-between;
	align-items: center;
`;

styles.RightContainer = styled.TouchableOpacity`
	flex-direction: row;
	align-items: center;
`;
styles.LeftContainer = styled.TouchableOpacity`
	flex-direction: row;
	align-items: center;
`;

styles.Title = styled.Text`
	font-family: Rubik-Medium;
	font-size: ${({ theme }) => theme.fontSizes.s18}px;
	color: ${({ theme }) => theme.colors.black};
	font-weight: 500;
	align-content: center;
	margin-right: ${calcWidth(8)}px;
`;

styles.LeftText = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s16}px;
	color: ${({ theme }) => theme.colors.black};
	margin-right: ${calcWidth(8)}px;
`;

export default styles;
