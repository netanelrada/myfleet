import React from 'react';
import s from './styles';
import { calcWidth } from '../../../../utils/dimensions';
import Icon from '../../../../components/common/Icon/Icon';

const Header = ({
	onGoBack = () => {},
	onPressRemove = () => {},
	titleText = '',
	iconName = '',
	leftText = '',
	disableDelete = false,
	iconStyle = {
		width: calcWidth(16),
		height: calcWidth(16),
	},
}) => {
	return (
		<s.Container>
			<s.RightContainer onPress={onGoBack}>
				<s.Title>{titleText}</s.Title>
				<Icon name='blackBack' {...iconStyle} />
			</s.RightContainer>
			<s.LeftContainer disabled={disableDelete} onPress={onPressRemove}>
				<s.LeftText>{leftText}</s.LeftText>
				{!!iconName && <Icon name='redTrash' {...iconStyle} />}
			</s.LeftContainer>
		</s.Container>
	);
};

export default Header;
