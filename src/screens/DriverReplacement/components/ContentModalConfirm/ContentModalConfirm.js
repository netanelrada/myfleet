import React from 'react';
import {Platform} from 'react-native';
import {useTranslation} from 'react-i18next';
import styled from 'styled-components';
import CheckBox from '@react-native-community/checkbox';
import Text from '../../../../components/common/Text/Text';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';

const ContentModalConfirm = ({
  isSingleTrip = false,
  previousDriver = '',
  newDriver = '',
  removeText = '',
  checkBoxValue = true,
  onSetCheckboxValue = () => {},
}) => {
  const {t} = useTranslation();

  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(20)};

  return (
    <s.Container>
      <Text fontSize="s18">{t('DriverReplacement')}</Text>
      {isSingleTrip ? (
        <s.SingleTripContainer>
          {!!removeText ? (
            <Text fontSize="s16" fontWeight="400">
              {removeText}
            </Text>
          ) : (
            <>
              <s.Row>
                <s.Label>{t('previousDriver')}: </s.Label>
                <Text fontSize="s16" fontWeight="400">
                  {previousDriver}
                </Text>
              </s.Row>
              <s.Row>
                <s.Label>{t('newDriver')}: </s.Label>
                <Text fontSize="s16" fontWeight="400">
                  {newDriver}
                </Text>
              </s.Row>
            </>
          )}
        </s.SingleTripContainer>
      ) : !!removeText ? (
        <Text fontSize="s16" fontWeight="400">
          {removeText}
        </Text>
      ) : (
        <Text>{t('acceptDriverReplacement')}</Text>
      )}
      <s.CheckBoxContainer>
        <CheckBox
          style={[Platform.OS === 'ios' ? checkBoxStyle : null]}
          value={checkBoxValue}
          onValueChange={(newValue) => onSetCheckboxValue(newValue)}
          onAnimationType="stroke"
        />
        <Text>{` ${t('includeUpdateCar')}`}</Text>
      </s.CheckBoxContainer>
    </s.Container>
  );
};
const s = {
  Container: styled.View`
    align-items: center;
    height: 90%;
    justify-content: space-between;
    margin-top: 5px;
  `,
  CheckBoxContainer: styled.View`
    flex-direction: row-reverse;
    align-items: center;
    align-self: flex-end;
    margin-bottom: ${calcHeight(5)}px;
  `,
  SingleTripContainer: styled.View``,
  Row: styled.View`
    flex-direction: row-reverse;
  `,
  Label: styled.Text`
    font-family: Rubik-Regular;
    color: ${({theme}) => theme.colors.gray6};
    font-size: ${({theme}) => theme.fontSizes.s16};
    font-weight: 400;
  `,
};
export default ContentModalConfirm;
