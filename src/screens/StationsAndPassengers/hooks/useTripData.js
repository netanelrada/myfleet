import {useEffect, useState} from 'react';
import useProxyApi from '../../../hooks/useProxyApi';
import {getLineData} from '../../../api/api';
import {fetchDataStatusE} from '../../../utils/enums';
import {
  setSingleEditTrip,
  getTripsByLineCodeSuccess,
} from '../../../store/actions/actionTrips';
import {useDispatch} from 'react-redux';
import useRefresh from '../../../hooks/useRefresh';

const useTripData = (initTrip) => {
  const [tripDetalis, setTripDetalis] = useState({...initTrip});
  const [stations, setstations] = useState([]);
  const [stationReqStatus, setStationReqStatus] = useState(null);
  const {proxyApi} = useProxyApi();
  const dispatch = useDispatch();
  const onSetSingleEditTrip = (payload) => dispatch(setSingleEditTrip(payload));
  const {onRefreshByLineCode} = useRefresh();

  useEffect(() => {
    onGetTripData();
  }, []);

  const onGetTripData = async () => {
    const {lineCode} = tripDetalis;
    try {
      setStationReqStatus(fetchDataStatusE.LOADING);
      const res = await proxyApi({
        cb: getLineData,
        restProp: {
          lineCode,
        },
      });

      const lineData = res.data.data[0];
      const {stations} = lineData;
      setstations([...stations]);
      setTripDetalis({...lineData});
      onRefreshByLineCode(lineData.lineCode);
      onSetSingleEditTrip({...lineData});
      setStationReqStatus(fetchDataStatusE.SUCCESS);
    } catch (error) {
      console.log(error);
      setStationReqStatus(fetchDataStatusE.FAIL);
    }
  };

  return {tripDetalis, onGetTripData, stations, stationReqStatus};
};

export default useTripData;
