import {useState, useEffect, useMemo} from 'react';
import {useTranslation} from 'react-i18next';
import useApiStations from './useApiStations';
import {useNavigation} from '@react-navigation/native';

const useGetMenuAction = ({
  onGetTripData = () => {},
  navigateToAddPassengersScreen,
  navigateToddStation,
}) => {
  const {t} = useTranslation();
  const {deleteStaion} = useApiStations();
  const menuAction = useMemo(
    () => [
      {
        text: t('editStation'),
        action: (station) => {
          navigateToddStation(station);
        },
      },
      {
        text: t('deleteStation'),
        action: async (station) => {

          const res = await deleteStaion(station);
          if (res) {
            onGetTripData();
          }
        },
      },
      {
        text: t('addPassengersToStations'),
        action: (station) => {
          navigateToAddPassengersScreen({station});
        },
      },
    ],
    [],
  );
  return {menuAction};
};

export default useGetMenuAction;
