import {useState, useEffect} from 'react';
import {cloneDeep} from 'lodash';

const useGetStations = (stations) => {
  const [stationsAndPassengers, setStationsAndPassengers] = useState([]);

  useEffect(() => {
    restStationsAndPassengers();
  }, [stations]);

  const restStationsAndPassengers = () => {
    // const res = stations.map((s) => ({
    //   ...s,
    //   passengers: s.passengers.map((p) => ({...p, isCheck: false})),
    // }));

    // const copyStations = cloneDeep(stations);
    // setStationsAndPassengers([...copyStations]);

    setStationsAndPassengers(
      stations.map((s) => ({
        ...s,
        passengers: s.passengers.map((p) => ({
          ...p,
          isCheck: false,
        })),
      })),
    );
  };

  return {
    stationsAndPassengers,
    setStationsAndPassengers,
    restStationsAndPassengers,
  };
};

export default useGetStations;
