import {useCallback} from 'react';
import useProxyApi from '../../../hooks/useProxyApi';
import {useSelector} from 'react-redux';
import {deleteStationsAndPassengers, setDiaryStation} from '../../../api/api';
import useTripData from './useTripData';
import {editTripsSelector} from '../../../store/selectors/tripsSelectors';
import {userSelector} from '../../../store/selectors/loginSelectors';

const useApiStations = (clientCode = '') => {
  const {lineCode = ''} = useSelector((state) => editTripsSelector(state))[0];
  const {proxyApi} = useProxyApi();

  const deleteStaion = useCallback(async (station) => {
    try {
      const codes = [];
      codes.push(station.code);
      station.passengers.forEach((s) => {
        codes.push(s.code);
      });
      const stationsCodes = codes.join(',');
      const res = await proxyApi({
        cb: deleteStationsAndPassengers,
        restProp: {stationsCodes, lineCode},
      });
      if (res.data.response === 0) return true;

      return false;
    } catch (error) {
      console.log('e', error);
    }
  }, []);

  const deletePassengers = async (passengers) => {
    try {
      const codes = [];
      passengers.forEach((p) => {
        codes.push(p.code);
      });
      const passCodes = codes.join(',');
      const res = await proxyApi({
        cb: deleteStationsAndPassengers,
        restProp: {passCodes, lineCode},
      });
      if (res.data.response === 0) return true;

      return false;
    } catch (error) {
      console.log('e', error);
    }
  };

  const onAddNewStation = async (stationData) => {
    try {
      const payload = {
        clientCode,
        lineCode,
        ...stationData,
      };
      return await proxyApi({
        cb: setDiaryStation,
        restProp: payload,
      });
    } catch (error) {
      console.log('err', error);
    }
  };

  return {deleteStaion, deletePassengers, onAddNewStation};
};

export default useApiStations;
