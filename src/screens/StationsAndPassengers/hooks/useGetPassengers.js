import {useState, useEffect} from 'react';
import {getPassengersDetails, setDiaryStation} from '../../../api/api';
import useProxyApi from '../../../hooks/useProxyApi';
import {fetchDataStatusE} from '../../../utils/enums';

const useGetPassengers = ({clientCode, line = {}, station = {}}) => {
  const [passengers, setPassengers] = useState([]);
  const [passengersStatus, setPassengersStatus] = useState(null);
  const {proxyApi} = useProxyApi();

  useEffect(() => {
    onGetPassengersData();
  }, []);

  const onGetPassengersData = async () => {
    try {
      setPassengersStatus(fetchDataStatusE.LOADING);
      const payload = {clientCode};
      const res = await proxyApi({
        cb: getPassengersDetails,
        restProp: payload,
      });
      const {passList = []} = res.data;
      setPassengers([...passList]);
      setPassengersStatus(fetchDataStatusE.SUCCESS);
    } catch (error) {
      console.log('err', error);
      setPassengersStatus(fetchDataStatusE.FAIL);
    }
  };

  const onAddPassengers = async (selctedPassengers) => {
    try {
      const {lineCode = ''} = line;
      const {code = '', time} = station;

      let stationPayload = {};
      if (code) {
        const {city, street, house, code, remarks = '', passengers} = station;
        stationPayload = {city, street, house, remarks};
        if (passengers.length === 0) {
          stationPayload.stationCode = code;
        }
      }

      const startTime = code ? time : line.startTime;
      const passCodes = selctedPassengers.map((p) => p.passCode).join(',');

      const payload = {
        clientCode,
        lineCode,
        startTime,
        passCodes,
        ...stationPayload,
      };
      return await proxyApi({
        cb: setDiaryStation,
        restProp: payload,
      });
    } catch (error) {
      console.log('err', error);
    }
  };

  const onAddNewPassenger = async (passengerData) => {
    try {
      const {lineCode = ''} = line;
      const [date] = line.startTime.split(' ');
      const payload = {
        clientCode,
        lineCode,
        ...passengerData,
      };
      return await proxyApi({
        cb: setDiaryStation,
        restProp: payload,
      });
    } catch (error) {
      console.log('err', error);
    }
  };

  return {passengers, onAddPassengers, onAddNewPassenger, passengersStatus};
};

export default useGetPassengers;
