import React, {useState} from 'react';
import s from './styles';
import TabActions from '../../components/TabActions/TabActions';
import Header from './components/Header/Header';
import LineDescription from '../../components/LineDescription/LineDescription';
import Stations from './components/Stations/Stations';
import useTripData from './hooks/useTripData';
import useGetStations from './hooks/useGetStations';
import {StationsContext} from './Context';
import PassengersEdit from './components/PassengersEdit/PassengersEdit';
import NewBtn from './components/NewBtn/NewBtn';
import useApiStations from './hooks/useApiStations';
import Overlay from '../../components/common/Overlay/Overlay';
import {editTripsSelector} from '../../store/selectors/tripsSelectors';
import {useSelector} from 'react-redux';

const StationsAndPassengers = ({route, navigation}) => {
  //const {lineDescription} = route.params?.trip;
  const trip = useSelector((state) => editTripsSelector(state))[0];
  const {
    tripDetalis: {lineDescription, clientCode, passQty = 0},
    onGetTripData,
    stations,
    stationReqStatus,
  } = useTripData(trip);
  const {deletePassengers} = useApiStations();
  const {
    stationsAndPassengers,
    setStationsAndPassengers,
    restStationsAndPassengers,
  } = useGetStations(stations);
  const [passengersSelected, setPassengersSelected] = useState([]);
  const [isOpenNewModal, setIsOpenNewModal] = useState(false);

  const navigateToEditPassengerScreen = ({passenger = {}} = {}) => {
    navigation.push('EditPassenger', {
      passenger,
      clientCode,
      onGetStations,
      setPassengersSelected,
    });
  };
  const navigateToAddPassengersScreen = ({
    passenger = {},
    station = {},
  } = {}) => {
    navigation.push('AddPassengers', {
      line: trip,
      clientCode,
      onGetStations,
      station,
      passenger,
    });
  };

  const navigateToddStation = (station = {}) => {
    navigation.push('AddStation', {
      line: trip,
      clientCode,
      onGetStations,
      station,
    });
  };

  const onRemovePassengers = async () => {
    const res = await deletePassengers(passengersSelected);
    if (res) {
      setPassengersSelected([]);
      await onGetTripData();
    }

    //TODDO
  };

  const value = {
    stationsAndPassengers,
    stationReqStatus,
    setStationsAndPassengers,
    onGetTripData,
    passengersSelected,
    setPassengersSelected,
    navigateToAddPassengersScreen,
    navigateToddStation,
    onRemovePassengers,
    navigateToEditPassengerScreen,
  };

  const onCancelSelections = () => {
    setPassengersSelected([]);
    restStationsAndPassengers();
  };

  const onGetStations = async () => {
    setIsOpenNewModal(false);
    await onGetTripData();
  };

  const getActionComponents = () => {
    if (passengersSelected.length === 1) return null;
    if (passengersSelected.length > 0) {
      return (
        <PassengersEdit
          onCancelSelections={onCancelSelections}
          onRemovePassengers={onRemovePassengers}
        />
      );
    }
    return (
      <NewBtn
        isOpen={isOpenNewModal}
        setIsOpen={setIsOpenNewModal}
        onAddPassengers={navigateToAddPassengersScreen}
        onAddStation={navigateToddStation}
      />
    );
  };

  return (
    <StationsContext.Provider value={value}>
      <s.Container>
        <s.Body>
          <Header
            onGoBack={navigation.goBack}
            numberOfPassengers={passQty}
            numberOfStattions={stations.length}
          />
          <LineDescription lineDescription={lineDescription} />
          <Stations />
          <Overlay
            isVisable={isOpenNewModal}
            onBackgroundClick={() => setIsOpenNewModal(false)}
          />
          <s.ActionContainer>{getActionComponents()}</s.ActionContainer>
        </s.Body>
        <TabActions isVisable={true} />
      </s.Container>
    </StationsContext.Provider>
  );
};

export default StationsAndPassengers;
