import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex-direction: row;
`;

styles.CancelSelected = styled.TouchableOpacity`
  flex-direction: row-reverse;
  border-radius: 8px;
  width: ${calcWidth(82)}px;
  height: ${calcHeight(44)}px;
  background-color: ${({theme}) => theme.colors.blueLight};
  justify-content: center;
  align-items: center;
`;

const RADIUS = 22;

styles.RemoveBtn = styled.TouchableOpacity`
  flex-direction: row-reverse;
  border-radius: ${calcHeight(RADIUS)}px;
  aspect-ratio: 1;
  height: ${calcHeight(RADIUS * 2)}px;
  background-color: ${({theme}) => theme.colors.yellowLight};
  justify-content: center;
  align-items: center;
  margin-right: 10px;
`;

export default styles;
