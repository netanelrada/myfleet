import React from 'react';
import s from './styles';
import {useTranslation} from 'react-i18next';
import Text from '../../../../components/common/Text/Text';
import Icon from '../../../../components/common/Icon/Icon';

const trashIconSize = {
  width: 20,
  height: 20,
};
const PassengersEdit = ({
  onCancelSelections = () => {},
  onRemovePassengers = () => {},
}) => {
  const {t} = useTranslation();
  return (
    <s.Container>
      <s.CancelSelected onPress={onCancelSelections}>
        <Text color="white">{t('cancelAll')}</Text>
      </s.CancelSelected>
      <s.RemoveBtn onPress={onRemovePassengers}>
        <Icon {...trashIconSize} name="trash" />
      </s.RemoveBtn>
    </s.Container>
  );
};

export default PassengersEdit;
