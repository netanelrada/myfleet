import React from 'react';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import {useTranslation} from 'react-i18next';

const Header = ({
  numberOfStattions = 0,
  numberOfPassengers = 0,
  onGoBack = () => {},
}) => {
  const {t} = useTranslation();
  return (
    <s.Container>
      <s.Btn onPress={onGoBack}>
        <Icon name="back" />
      </s.Btn>
      <Icon name="stations" />
      <s.Text>{`${numberOfStattions} ${t('stations')}`}</s.Text>
      <Icon name="passenger" />
      <s.Text>{`${numberOfPassengers} ${t('passenger')}`}</s.Text>
    </s.Container>
  );
};

export default Header;
