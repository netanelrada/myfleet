import styled from 'styled-components';
import {calcWidth} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  height: 10%;
  width: 100%;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row-reverse;
`;

styles.Text = styled.Text`
  font-family: Rubik-Medium;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  display: flex;
  align-items: center;
  text-align: right;
  margin: 0 5px 0 15px;
`;

styles.Btn = styled.TouchableOpacity`
  width: ${calcWidth(40)}px;
  aspect-ratio: 1;
  border-radius: 9px;
  background-color: ${({theme}) => theme.colors.blueLight};
  justify-content: center;
  align-items: center;
  margin-left: ${calcWidth(8)}px;
`;

export default styles;
