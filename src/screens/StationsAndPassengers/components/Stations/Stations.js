import React, {useContext, useState} from 'react';
import {ActivityIndicator, FlatList} from 'react-native';
import s from './styles';
import Station from './components/Station/Station';
import {StationsContext} from '../../Context';
import {fetchDataStatusE} from '../../../../utils/enums';
import {calcHeight} from '../../../../utils/dimensions';

const Stations = () => {
  const {
    stationsAndPassengers,
    setStationsAndPassengers,
    setPassengersSelected,
    stationReqStatus,
  } = useContext(StationsContext);

  const onCheckPassenger = (pass, checkValue) => {
    const newStaions = stationsAndPassengers.reduce((acc, curr) => {
      const indexPass = curr.passengers.findIndex(
        (p) => p.passCode === pass.passCode,
      );
      if (indexPass !== -1) {
        curr.passengers[indexPass].isCheck = checkValue;
      }

      return [...acc, curr];
    }, []);
    updatePassengersSelected(pass, checkValue);
    setStationsAndPassengers([...newStaions]);
  };

  const updatePassengersSelected = (pass, checkValue) => {
    setPassengersSelected((preState) => {
      let newPassengersSelected = [];
      if (checkValue) {
        newPassengersSelected = [...preState, pass];
      } else {
        newPassengersSelected = preState.filter(
          (p) => p.passId !== pass.passId,
        );
      }
      return newPassengersSelected;
    });
  };

  return (
    <s.Container>
      {fetchDataStatusE.LOADING === stationReqStatus ? (
        <ActivityIndicator
          style={{marginTop: calcHeight(15)}}
          size="large"
          color="#40A8E2"
        />
      ) : (
        <FlatList
          style={{paddingBottom: calcHeight(100)}}
          data={stationsAndPassengers}
          renderItem={({item}) => (
            <Station
              key={item.code}
              station={item}
              onCheckPassenger={onCheckPassenger}
            />
          )}
        />
      )}
    </s.Container>
  );
};

export default Stations;
