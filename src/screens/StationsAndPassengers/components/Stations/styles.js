import styled from 'styled-components';
import {calcHeight} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.ScrollView`
  flex: 1;
  width: 100%;
  background-color: ${({theme}) => theme.colors.white};
  border-radius: 9px;
  margin-top: ${calcHeight(25)}px;
  margin-bottom: ${calcHeight(60)}px;
`;

export default styles;
