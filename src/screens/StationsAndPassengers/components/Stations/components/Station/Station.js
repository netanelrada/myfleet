import React from 'react';
import Header from './components/Header/Header';
import Passenger from './components/Passenger/Passenger';
import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';

const Station = ({station = {}, onCheckPassenger = () => {}}) => {
  const {passengers = []} = station;

  return (
    <S.Container>
      <Header station={station} />
      {passengers.map((pass) => (
        <Passenger
          key={pass.passCode}
          passenger={pass}
          onPressCheckBox={(newValue) => onCheckPassenger(pass, newValue)}
        />
      ))}
    </S.Container>
  );
};

const S = {
  Container: styled.View`
    padding-horizontal: ${calcWidth(10)}px;
  `,
};
export default Station;
