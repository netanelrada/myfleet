import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  width: 100%;
  justify-content: space-between;
  flex-direction: row-reverse;
  margin-bottom: ${calcHeight(10)}px;
`;

styles.Row = styled.View`
  flex-direction: row-reverse;
`;

styles.LeftContainer = styled.View`
  flex-direction: row-reverse;
`;

styles.CheckboxContainer = styled.View`
  width: ${calcWidth(20)}px;
  margin-left: 7px;
`;

styles.DataContainer = styled.View``;

styles.RightContainer = styled.View`
  flex-direction: row;
`;

styles.Label = styled.Text`
  font-family: Rubik-Regular;
  font-size: ${({theme}) => theme.fontSizes.s14}px;
  color: ${({theme}) => theme.colors.gray6};
  margin-left: 5px;
`;

const RADIUS = 18;
styles.Btn = styled.TouchableOpacity`
  width: ${calcWidth(RADIUS * 2)}px;
  height: ${calcWidth(RADIUS * 2)}px;
  border-radius: ${RADIUS}px;
  border: 1px solid ${({theme}) => theme.colors.darkGray3};
  justify-content: center;
  align-items: center;
  margin-horizontal: 5px;
`;

export default styles;
