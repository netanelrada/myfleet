import React, {useState, useContext} from 'react';
import Tooltip from 'react-native-walkthrough-tooltip';
import s from './styles';
import Text from '../../../../../../../../components/common/Text/Text';
import Icon from '../../../../../../../../components/common/Icon/Icon';
import useGetMenuAction from '../../../../../../hooks/useGetMenuAction';
import {StationsContext} from '../../../../../../Context';

const Header = ({station}) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const {
    onGetTripData,
    navigateToAddPassengersScreen,
    navigateToddStation,
  } = useContext(
    StationsContext,
    navigateToAddPassengersScreen,
    navigateToddStation,
  );
  const {menuAction} = useGetMenuAction({
    onGetTripData,
    navigateToAddPassengersScreen,
    navigateToddStation,
  });
  const {time, city, street, house} = station;

  return (
    <s.Container>
      <s.TextContainer>
        <Text>{time}</Text>
        <s.Space />
        <Text fontWeight="700">
          {`${street.replace('\n', ' ')} ${house.replace(
            '\n',
            ' ',
          )}, ${city.replace('\n', ' ')}`}
        </Text>
      </s.TextContainer>
      <Tooltip
        isVisible={isOpenMenu}
        onClose={() => setIsOpenMenu(false)}
        placement="right"
        content={
          <s.TooltipContainer>
            {menuAction.map((option, i) => (
              <s.BtnOption
                key={i.toString()}
                onPress={() => {
                  setIsOpenMenu(false);
                  option.action(station);
                }}>
                <Text>{option.text}</Text>
              </s.BtnOption>
            ))}
          </s.TooltipContainer>
        }>
        <s.Btn onPress={() => setIsOpenMenu(true)}>
          <Icon name="threePoint" />
        </s.Btn>
      </Tooltip>
    </s.Container>
  );
};

export default Header;
