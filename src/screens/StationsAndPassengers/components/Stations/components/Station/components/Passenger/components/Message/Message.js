import React, {useState} from 'react';
import Tooltip from 'react-native-walkthrough-tooltip';
import s from './styles';
import Icon from '../../../../../../../../../../components/common/Icon/Icon';
import Text from '../../../../../../../../../../components/common/Text/Text';

const Message = ({message = '', style = {}, isDisable = false}) => {
  const [isMessageTollTipVisable, setIsMessageTollTipVisable] = useState(false);
  return (
    <Tooltip
      isVisible={isMessageTollTipVisable}
      onClose={() => setIsMessageTollTipVisable(false)}
      content={
        <s.ToolTipContentContainer>
          <Text textAlign="center">{message}</Text>
        </s.ToolTipContentContainer>
      }
      placement="top">
      <s.Btn
        onPress={() => setIsMessageTollTipVisable(true)}
        {...style}
        disabled={isDisable}
        isDisable={isDisable}>
        <Icon name="message" />
      </s.Btn>
    </Tooltip>
  );
};

export default Message;
