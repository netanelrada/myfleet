import styled from 'styled-components';
import {calcWidth} from '../../../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View``;

styles.ToolTipContentContainer = styled.View``;

const RADIUS = calcWidth(18);

styles.Btn = styled.TouchableOpacity`
  width: ${RADIUS * 2}px;
  height: ${RADIUS * 2}px;
  border-radius: ${RADIUS}px;
  border: 1px solid ${({theme}) => theme.colors.darkGray3};
  justify-content: center;
  align-items: center;
  margin-horizontal: 5px;
  ${({backgroundColor}) =>
    backgroundColor && `background-color: ${backgroundColor}`}
  ${({isDisable, theme}) =>
    isDisable && `background-color: ${theme.colors.gray5}`}
`;

export default styles;
