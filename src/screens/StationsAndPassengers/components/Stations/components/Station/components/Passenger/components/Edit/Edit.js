import React from 'react';
import s from './styles';
import Icon from '../../../../../../../../../../components/common/Icon/Icon';
import Message from '../Message/Message';
import {useTheme} from 'styled-components';

const Edit = ({
  isVisable = false,
  callToPassenger = () => {},
  onEdit = () => {},
  onDelete = () => {},
  message = 'בדיקה',
}) => {
  const {colors} = useTheme();

  return (
    <>
      {isVisable && (
        <s.Container>
          <s.IconContainer onPress={callToPassenger}>
            <Icon name="blackPhone" />
          </s.IconContainer>
          <Message
            message={message}
            style={{backgroundColor: colors.yellowLight}}
            isDisable={!message}
          />
          <s.IconContainer onPress={onEdit}>
            <Icon name="blackEdit" />
          </s.IconContainer>
          <s.IconContainer onPress={onDelete}>
            <Icon name="trash" />
          </s.IconContainer>
        </s.Container>
      )}
    </>
  );
};

export default Edit;
