import React, {useContext, useState, useEffect} from 'react';
import {Platform} from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import CheckBox from '@react-native-community/checkbox';
import {useTranslation} from 'react-i18next';
import s from './styles';
import Text from '../../../../../../../../components/common/Text/Text';
import Icon from '../../../../../../../../components/common/Icon/Icon';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';
import Message from './components/Message/Message';
import Edit from './components/Edit/Edit';
import {StationsContext} from '../../../../../../Context';
import useCall from '../../../../../../../../hooks/useCall';

const Passenger = ({passenger, onPressCheckBox = () => {}}) => {
  const {t} = useTranslation();
  const [isEditMode, setIsEditMode] = useState(false);
  const {
    passengersSelected,
    onRemovePassengers,
    navigateToEditPassengerScreen,
  } = useContext(StationsContext);
  const {call} = useCall();
  const {
    code,
    lastName,
    firstName,
    passCode,
    isCheck = false,
    remarks = '',
    phone1 = '',
  } = passenger;

  useEffect(() => {
    if (
      passengersSelected.length === 1 &&
      passengersSelected[0].code === code
    ) {
      setIsEditMode(true);
    } else {
      setIsEditMode(false);
    }
  }, [passengersSelected.length]);

  const callToPassenger = () => {
    call(phone1);
  };

  const onEditPassenger = () => {
    navigateToEditPassengerScreen({passenger});
  };

  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(15)};

  return (
    <s.Container>
      <s.LeftContainer>
        <s.CheckboxContainer>
          <CheckBox
            disabled={false}
            style={[Platform.OS === 'ios' ? checkBoxStyle : null]}
            value={isCheck}
            onValueChange={(newValue) => onPressCheckBox(newValue)}
            onAnimationType="stroke"
          />
        </s.CheckboxContainer>
        <s.DataContainer>
          <s.Row>
            <s.Label>{t('name')}</s.Label>
            <Text textAlign="right">{`${lastName} ${firstName}`}</Text>
          </s.Row>
          <s.Row>
            <s.Label>{t('employeeCode')}</s.Label>
            <Text textAlign="right">{passCode}</Text>
          </s.Row>
          <s.Row>
            <s.Label>{t('phone')}</s.Label>
            <Text textAlign="right">{phone1}</Text>
          </s.Row>
        </s.DataContainer>
      </s.LeftContainer>
      <s.RightContainer>
        <s.Btn onPress={callToPassenger}>
          <Icon name="phone" />
        </s.Btn>
        {!!remarks && <Message message={remarks} />}
      </s.RightContainer>
      <Edit
        isVisable={isEditMode}
        callToPassenger={callToPassenger}
        onEdit={onEditPassenger}
        onDelete={onRemovePassengers}
        message={remarks}
      />
    </s.Container>
  );
};

export default Passenger;
