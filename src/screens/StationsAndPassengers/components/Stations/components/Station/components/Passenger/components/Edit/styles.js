import styled from 'styled-components';
import {calcWidth} from '../../../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  height: 100%;
  width: 60%;
  position: absolute;
  right: 0;
  background-color: ${({theme}) => theme.colors.white};
  flex-direction: row;
  justify-content: space-between;
`;
const BORDER = calcWidth(18);

styles.IconContainer = styled.TouchableOpacity`
  width: ${BORDER * 2}px;
  height: ${BORDER * 2}px;
  border-radius: ${BORDER};
  background-color: ${({theme, isDisable = false}) =>
    isDisable ? theme.colors.gray5 : theme.colors.yellowLight};
  justify-content: center;
  align-items: center;
`;

export default styles;
