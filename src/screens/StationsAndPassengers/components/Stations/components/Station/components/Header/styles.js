import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  width: 100%;
  height: ${calcHeight(40)}px;
  flex-direction: row-reverse;
  justify-content: space-between;
  align-items: center;
  border-bottom-color: ${({theme}) => theme.colors.gray7};
  border-bottom-width: 1px;
  margin-bottom: 10px;
`;

styles.TextContainer = styled.View`
  flex-direction: row-reverse;
`;

styles.Btn = styled.TouchableOpacity`
  justify-content: center;
  height: 100%;
  align-items: center;
  width: ${calcWidth(20)}px;
`;

styles.Space = styled.View`
  margin-left: 5px;
`;

styles.TooltipContainer = styled.View`
  width: ${calcWidth(155)}px;
  border-radius: 9px;
`;

styles.BtnOption = styled.TouchableOpacity`
  justify-content: center;
  height: ${calcHeight(44)}px;
  align-items: center;
  width: 100%;
`;

export default styles;
