import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View``;

styles.NewBtn = styled.TouchableOpacity`
  width: ${calcWidth(52)}px;
  height: ${calcHeight(52)}px;
  border-radius: 17px;
  background-color: ${({theme, isOpen}) =>
    isOpen ? theme.colors.white : theme.colors.blueLight};
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  ${({isOpen, theme}) =>
    isOpen && `border: 1px solid ${theme.colors.blueLight}`};
`;

styles.ContainerMenu = styled.View`
  background-color: ${({theme}) => theme.colors.white};
  width: ${calcWidth(141)}px;
  height: ${calcHeight(112)}px;
  border-radius: 4px;
  align-items: center;
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
`;

styles.RowMenu = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  height: 50%;
`;

const BORDER = calcWidth(18);

styles.TextContainer = styled.View`
  margin-right: ${calcWidth(25)}px;
`;

styles.IconContainer = styled.View`
  width: ${BORDER * 2}px;
  height: ${BORDER * 2}px;
  border-radius: ${BORDER}px;
  background-color: ${({theme}) => theme.colors.yellowLight};
  justify-content: center;
  align-items: center;
  position: absolute;
  right: ${-BORDER}px;
`;

styles.Overlay = styled.View`
  position: absolute;
  right: 0;
  left: 0;
  bottom: 0;
  top: 0;
  background-color: red;
`;

export default styles;
