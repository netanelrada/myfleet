import React, {useState} from 'react';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import Text from '../../../../components/common/Text/Text';
import {useTranslation} from 'react-i18next';

const NewBtn = ({
  isOpen = false,
  setIsOpen = () => {},
  onAddStation = () => {},
  onAddPassengers = () => {},
}) => {
  const {t} = useTranslation();

  const menuOption = [
    {text: t('addStationsToTrip'), iconName: 'stations', onPress: onAddStation},
    {
      text: t('addPassengersToTrip'),
      iconName: 'passenger',
      onPress: onAddPassengers,
    },
  ];
  return (
    <>
      {isOpen && (
        <s.ContainerMenu>
          {menuOption.map(({text, iconName, onPress}) => (
            <s.RowMenu key={text} onPress={onPress}>
              <s.TextContainer>
                <Text textAlign="center" fontSize="s12">
                  {text}
                </Text>
              </s.TextContainer>
              <s.IconContainer>
                <Icon name={iconName} />
              </s.IconContainer>
            </s.RowMenu>
          ))}
        </s.ContainerMenu>
      )}
      <s.NewBtn
        onPress={() => setIsOpen((preState) => !preState)}
        isOpen={isOpen}>
        <Icon name={isOpen ? 'blackPlus' : 'whitePlus'} />
      </s.NewBtn>
    </>
  );
};

export default NewBtn;
