import React from 'react';
import Header from '../../../../components/Header/Header';
import {useTranslation} from 'react-i18next';
import {useForm, Controller} from 'react-hook-form';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';
import s from './styles';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import Input from '../AddPassengers/components/NewPassenger/components/Input/Input';
import TimeInput from '../AddPassengers/components/NewPassenger/components/TimeInput/TimeInput';
import GooglePlacesInput from '../AddPassengers/components/NewPassenger/components/GooglePlacesInput/GooglePlacesInput';
import SubmitBtn from '../AddPassengers/components/NewPassenger/components/SubmitBtn/SubmitBtn';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {filterObjectWithEmptValue} from '../../../../utils/utilis';
import useApiStations from '../../hooks/useApiStations';

const iconSize = {
  width: calcWidth(22),
  height: calcWidth(16),
};
const fildes = ['startTime', 'city', 'street', 'house', 'remarks'];

const EditPassenger = ({navigation, route}) => {
  const {t} = useTranslation();
  const {goBack} = navigation;
  const {
    passenger = {},
    clientCode,
    onGetStations,
    setPassengersSelected,
  } = route.params;

  const {onAddNewStation: onEditPassenger} = useApiStations(clientCode);
  const initialValues = Object.keys(passenger).reduce((acc, curr) => {
    if (curr === 'time') {
      return {...acc, startTime: passenger[curr]};
    }
    if (fildes.includes(curr)) return {...acc, [curr]: passenger[curr]};

    return acc;
  }, {});
  initialValues.employeeName = `${passenger.firstName} ${passenger.lastName}`;

  const {control, handleSubmit, errors, setValue, getValues, reset} = useForm({
    shouldFocusError: true,
    defaultValues: {
      ...initialValues,
    },
  });

  const onSubmit = async (data) => {
    const {city, street, startTime, remarks, house} = data;
    const payload = {
      city,
      street,
      startTime,
      remarks,
      house,
      stationCode: passenger.code,
    };
    const res = await onEditPassenger(payload);
    if (res.data.response === '0') {
      await onGetStations();
      setPassengersSelected([]);
      goBack();
    }
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header
          titleText={t('editPassenger')}
          iconName="passengersL"
          iconStyle={iconSize}
          onGoBack={goBack}
        />
        <KeyboardAwareScrollView
          style={{width: '100%'}}
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={true}>
          <s.FormContainer>
            <SubmitBtn btnText={t('update')} onPress={handleSubmit(onSubmit)} />
            <Controller
              control={control}
              render={({onChange, value}) => (
                <Input
                  label={t('comments')}
                  inputStyle={{height: 163, multiline: true}}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  error={errors.remarks}
                />
              )}
              name="remarks"
              defaultValue=""
            />

            <Controller
              control={control}
              render={({onChange, value}) => (
                <GooglePlacesInput
                  label={t('city')}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  error={errors.city && t('fieldRequired')}
                />
              )}
              name="city"
              defaultValue=""
              rules={{required: true}}
            />
            <s.Row>
              <Controller
                control={control}
                render={({onChange, value}) => (
                  <GooglePlacesInput
                    label={t('street')}
                    inputStyle={{width: 180}}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    error={errors.street && t('fieldRequired')}
                    type="address"
                  />
                )}
                name="street"
                defaultValue=""
                rules={{required: true}}
              />
              <Controller
                control={control}
                render={({onChange, value}) => (
                  <Input
                    label={t('houseNumber')}
                    inputStyle={{width: 88}}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    error={errors.house}
                  />
                )}
                name="house"
                defaultValue=""
              />
            </s.Row>
            <Controller
              control={control}
              render={({onChange, value}) => (
                <TimeInput
                  label={t('stationTime')}
                  onChange={(value) => onChange(value)}
                  value={value}
                  error={errors.startTime && t('fieldRequired')}
                  width={284}
                />
              )}
              name="startTime"
              defaultValue=""
              rules={{required: true}}
            />
            <Controller
              control={control}
              render={({onChange, value}) => (
                <Input
                  label={t('employeeName')}
                  onChange={(value) => onChange(value)}
                  value={value}
                  error={errors.startTime && t('fieldRequired')}
                  width={284}
                  isDisable={true}
                />
              )}
              name="employeeName"
              defaultValue=""
              rules={{required: true}}
            />
          </s.FormContainer>
        </KeyboardAwareScrollView>
      </s.Container>
    </ImageBackground>
  );
};

export default EditPassenger;
