import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex-direction: row-reverse;
  height: ${calcHeight(40)}px;
  width: ${calcWidth(332)}px;
  background-color: ${({theme}) => theme.colors.white};
`;

styles.Tab = styled.View`
  background-color: ${({theme}) => theme.colors.white};
  height: 100%;
  width: 50%;
  align-items: center;
  justify-content: center;
`;

styles.Vr = styled.View`
  height: 100%;
  width: 1px;
  background-color: ${({theme}) => theme.colors.borderColorInput};
`;

export default styles;
