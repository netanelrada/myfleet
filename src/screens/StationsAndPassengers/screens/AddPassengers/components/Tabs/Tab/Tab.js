import React from 'react';
import styled, {useTheme} from 'styled-components';
import Text from '../../../../../../../components/common/Text/Text';

const Tab = ({text = '', isFoucs = false, onPress = () => {}}) => {
  const {colors} = useTheme();
  const textColor = isFoucs ? colors.blueLight : colors.darkGray;
  return (
    <s.Container onPress={onPress} isFoucs={isFoucs}>
      <Text
        fontFamily={isFoucs ? 'Rubik-Medium' : 'Rubik-Regular'}
        fontWeight={isFoucs ? 'bold' : 400}
        color={textColor}>
        {text}
      </Text>
    </s.Container>
  );
};

const s = {
  Container: styled.TouchableOpacity`
    background-color: ${({theme}) => theme.colors.white};
    height: 100%;
    width: 50%;
    align-items: center;
    justify-content: center;
    border-radius: 8px;
    opacity: ${({isFoucs}) => (isFoucs ? 0.6 : 1)};
    ${({isFoucs}) =>
      isFoucs && 'box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);'};
  `,
};
export default Tab;
