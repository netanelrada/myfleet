import React from 'react';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';
import {useTranslation} from 'react-i18next';
import {useTheme} from 'styled-components';
import {PASSENGERS_TABS} from '../../../../../../utils/enums';
import Tab from './Tab/Tab';

const Tabs = ({currTab = '', onPressTab = () => {}}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();

  return (
    <s.Container>
      <Tab
        isFoucs={currTab === PASSENGERS_TABS.SELECT_PASSENGERS}
        text={t('addPassenger')}
        onPress={() => onPressTab(PASSENGERS_TABS.SELECT_PASSENGERS)}
      />
      <s.Vr />
      <Tab
        isFoucs={currTab === PASSENGERS_TABS.NEW_PASSENGER}
        text={t('newPassenger')}
        onPress={() => onPressTab(PASSENGERS_TABS.NEW_PASSENGER)}
      />
    </s.Container>
  );
};

export default Tabs;
