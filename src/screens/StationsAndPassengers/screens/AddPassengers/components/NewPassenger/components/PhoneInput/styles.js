import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';

calcHeight;

const styles = {};

styles.Container = styled.View`
  align-items: flex-end;
`;

styles.Input = styled.TextInput`
  height: ${({height = 44}) => calcHeight(height)}px;
  width: ${({width = 280}) => calcWidth(width)}px;
  height: ${calcHeight(44)};
  justify-content: center;
  align-items: center;
  text-align: right;
  z-index: -10;
  padding-horizontal: ${calcWidth(4)}px;
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
  border-radius: 4px;
`;

export default styles;
