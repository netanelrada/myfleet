import styled from 'styled-components';
import {calcWidth} from '../../../../../../../../utils/dimensions';

const styles = {};

styles.TimeInputContainer = styled.View`
  align-items: flex-end;
  margin-bottom: 15px;
`;

styles.Container = styled.View`
  width: ${({width}) => calcWidth(width)}px;
  height: ${calcWidth(44)}px;
  border: 1px solid ${({theme: {colors}}) => colors.borderColorInput};
  flex-direction: row-reverse;
  border-radius: 4px;
  ${({backgroundColor, theme}) =>
    backgroundColor && `background-color: ${theme.colors.darkGray1}`}

  ${({isError}) => isError && 'border: 1px solid red'}
`;
styles.InputContainer = styled.View`
  flex-direction: row-reverse;
  flex: 0.75;
  justify-content: center;
  align-items: center;
`;

styles.Input = styled.TextInput`
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

styles.IconContainer = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  height: 100%;
  flex: 0.25;
  border-end-width: 1px;
  border-end-color: ${({theme}) => theme.colors.borderColorInput};
`;

export default styles;
