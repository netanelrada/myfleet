import React, {useState} from 'react';
import Text from '../../../../../../../../components/common/Text/Text';
import s from './styles';
import {TextInputMask} from 'react-native-masked-text';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';
import {useTheme} from 'styled-components';

const PhoneInput = ({
  label,
  inputStyle = {},
  value = '',
  onChangeText = () => {},
  error = '',
}) => {
  const {colors, fontSizes} = useTheme();
  return (
    <s.Container>
      <Text>{label}</Text>
      <TextInputMask
        style={{
          width: calcWidth(280),
          height: calcHeight(44),
          justifyContent: 'center',
          alignItems: 'center',
          textAlign: 'center',
          borderWidth: 1,
          borderColor: colors.borderColorInput,
          borderRadius: 4,
          fontSize: fontSizes.s14,
        }}
        type="cel-phone"
        options={{
          maskType: 'INTERNATIONAL',
          mask: '05 ',
        }}
        selectTextOnFocus
        value={value}
        onChangeText={onChangeText}
      />
    </s.Container>
  );
};

export default PhoneInput;
