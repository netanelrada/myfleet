import React, {useState} from 'react';
import s from './styles';
import {TextInputMask} from 'react-native-masked-text';
import Icon from '../../../../../../../../components/common/Icon/Icon';
import TimePicker from '../../../../../../../../components/TimePicker/TimePicker';
import Text from '../../../../../../../../components/common/Text/Text';

const TimeInput = ({
  onChange,
  onBlur,
  value,
  error = '',
  label = '',
  disable = false,
  width = 181,
}) => {
  const [isShowTimePicker, setIsShowTimePicker] = useState(false);

  const openTimePicker = () => setIsShowTimePicker(true);
  const onSetTimeByTimePicker = (time) => {
    setIsShowTimePicker(false);
    if (!time) {
      return;
    }
    const t = time.toTimeString().split(' ')[0];
    const timeFormat = t.slice(0, 5);
    onChange(timeFormat);
  };
  return (
    <s.TimeInputContainer>
      <Text>{label}</Text>
      <s.Container
        pointerEvents={disable?.isLock ? 'none' : 'auto'}
        backgroundColor={disable?.isLock}
        isError={!!error}
        width={width}>
        <s.InputContainer>
          <TextInputMask
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
              letterSpacing: 5,
            }}
            type={'datetime'}
            options={{
              format: 'HH:MM',
            }}
            selectTextOnFocus
            value={value}
            onChangeText={(text) => onChange(text)}
          />
        </s.InputContainer>
        <s.IconContainer onPress={openTimePicker}>
          <Icon name="blueClock" />
        </s.IconContainer>

        {isShowTimePicker && (
          <TimePicker onConfirm={onSetTimeByTimePicker} label={label} />
        )}
      </s.Container>
      {!!error && (
        <Text fontSize="s12" color="red">
          {error}
        </Text>
      )}
    </s.TimeInputContainer>
  );
};

export default TimeInput;
