import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  align-items: flex-end;
  margin-bottom: 15px;
`;

styles.Input = styled.TextInput`
  height: ${({height = 44}) => calcHeight(height)}px;
  width: ${({width = 280}) => calcWidth(width)}px;
  justify-content: center;
  align-items: center;
  text-align: right;
  z-index: -10;
  padding-horizontal: ${calcWidth(4)}px;
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
  font-size: ${({theme}) => theme.fontSizes.s14};
  border-radius: 4px;
  ${({isDisable, theme}) =>
    isDisable && `background-color: ${theme.colors.darkGray1}`}
`;

export default styles;
