import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';
import {TouchableOpacity} from 'react-native';

const styles = {};

styles.Container = styled.View`
  margin-bottom: 15px;
`;

styles.Body = styled.View`
  align-items: flex-end;
`;

styles.Input = styled.TextInput`
  height: ${({height = 44}) => calcHeight(height)}px;
  width: ${({width = 280}) => calcWidth(width)}px;
  justify-content: center;
  align-items: center;
  text-align: right;
  z-index: -10;
  padding-horizontal: ${calcWidth(4)}px;
  font-size: ${({theme}) => theme.fontSizes.s14};
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
  border-radius: 4px;
  ${({isError}) => isError && 'border: 1px solid red'}
`;
styles.Item = styled.TouchableOpacity`
  height: ${calcHeight(30)}px;
  width: ${({width = 280}) => calcWidth(width)}px;
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
  justify-content: center;
  background-color: white;
`;

export default styles;
