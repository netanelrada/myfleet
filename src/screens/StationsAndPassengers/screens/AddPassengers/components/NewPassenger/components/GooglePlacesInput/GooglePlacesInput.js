import React, {useState} from 'react';
import Text from '../../../../../../../../components/common/Text/Text';
import s from './styles';
import {getPlacesByText} from '../../../../../../../../api/api';
import {FlatList, View} from 'react-native';
import {calcHeight} from '../../../../../../../../utils/dimensions';

const GooglePlacesInput = ({
  label,
  inputStyle = {},
  value = '',
  onChangeText = () => {},
  error = '',
  type = '(cities)',
}) => {
  const [placesList, setPlacesList] = useState([]);

  const onChange = async (text) => {
    onChangeText(text);
    onGetPlacesByText(text);
  };

  const onGetPlacesByText = async (text) => {
    try {
      const res = await getPlacesByText({
        input: text,
        type,
      });
      const {predictions} = res.data;
      const places = predictions.map(
        (place) => place.structured_formatting.main_text,
      );
      setPlacesList([...places]);
    } catch (error) {
      console.log('e', error);
    }
  };

  const onSelectPlace = (place) => {
    onChangeText(place);
    setPlacesList([]);
  };

  return (
    <s.Container>
      <s.Body>
        <Text>{label}</Text>
        <s.Input
          isError={!!error}
          {...inputStyle}
          value={value}
          onChangeText={onChange}
        />
        {!!error && (
          <Text fontSize="s12" color="red">
            {error}
          </Text>
        )}
        <FlatList
          data={placesList}
          style={{left: 0, position: 'absolute', right: 0, top: calcHeight(60)}}
          renderItem={({item}) => (
            <s.Item {...inputStyle} onPress={() => onSelectPlace(item)}>
              <Text textAlign="right">{item}</Text>
            </s.Item>
          )}
        />
      </s.Body>
    </s.Container>
  );
};

export default GooglePlacesInput;
