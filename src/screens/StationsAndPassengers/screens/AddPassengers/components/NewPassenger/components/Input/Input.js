import React from 'react';
import Text from '../../../../../../../../components/common/Text/Text';
import s from './styles';

const Input = ({
  label,
  inputStyle = {},
  value = '',
  onChangeText = () => {},
  error = '',
  isDisable = false,
}) => {
  return (
    <s.Container pointerEvents={isDisable ? 'none' : 'auto'}>
      <Text>{label}</Text>
      <s.Input
        {...inputStyle}
        value={value}
        onChangeText={onChangeText}
        isDisable={isDisable}
      />
      {!!error && <Text>{error}</Text>}
    </s.Container>
  );
};

export default Input;
