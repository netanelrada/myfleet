import React from 'react';
import Text from '../../../../../../components/common/Text/Text';
import GooglePlacesInput from './components/GooglePlacesInput/GooglePlacesInput';
import s from './styles';
import {useForm, Controller} from 'react-hook-form';
import SubmitBtn from './components/SubmitBtn/SubmitBtn';
import {useTranslation} from 'react-i18next';
import useGetInputs from './hooks/useGetInputs';
import Input from './components/Input/Input';
import {ScrollView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import PhoneInput from './components/PhoneInput/PhoneInput';
import TimeInput from './components/TimeInput/TimeInput';

const NewPassenger = ({onPressAdd = () => {}}) => {
  const {t} = useTranslation();
  const {control, handleSubmit, errors, setValue, getValues, reset} = useForm({
    shouldFocusError: true,
  });

  const onSubmit = (data) => {

    const filterData = Object.keys(data).reduce((acc, curr) => {
      if (data[curr]) {
        acc[curr] = data[curr];
      }
      return {
        ...acc,
      };
    }, {});
    onPressAdd(filterData);
  };

  return (
    <KeyboardAwareScrollView
      style={{width: '100%'}}
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={true}>
      <s.Container>
        <SubmitBtn btnText={t('add')} onPress={handleSubmit(onSubmit)} />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <Input
              label={t('comments')}
              inputStyle={{
                height: 163,
                multiline: true,
                textAlignVertical: 'top',
              }}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.comments}
            />
          )}
          name="comments"
          defaultValue=""
        />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <PhoneInput
              label={t('phone')}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.house}
            />
          )}
          name="mobilePhone"
          defaultValue=""
        />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <TimeInput
              label={t('stationTime')}
              onChange={(value) => onChange(value)}
              value={value}
              error={errors.startTime && t('fieldRequired')}
              width={284}
            />
          )}
          name="startTime"
          defaultValue=""
          rules={{required: true}}
        />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <GooglePlacesInput
              label={t('city')}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.city && t('fieldRequired')}
            />
          )}
          name="city"
          defaultValue=""
          rules={{required: true}}
        />
        <s.Row>
          <Controller
            control={control}
            render={({onChange, value}) => (
              <GooglePlacesInput
                label={t('street')}
                inputStyle={{width: 180}}
                onChangeText={(value) => onChange(value)}
                value={value}
                error={errors.street && t('fieldRequired')}
                type="address"
              />
            )}
            name="street"
            defaultValue=""
            rules={{required: true}}
          />
          <Controller
            control={control}
            render={({onChange, value}) => (
              <Input
                label={t('houseNumber')}
                inputStyle={{width: 88}}
                onChangeText={(value) => onChange(value)}
                value={value}
                error={errors.house}
              />
            )}
            name="house"
            defaultValue=""
          />
        </s.Row>

        <Controller
          control={control}
          render={({onChange, value}) => (
            <Input
              label={t('lastName')}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.lastName}
            />
          )}
          name="lastName"
          defaultValue=""
        />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <Input
              label={t('firstName')}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.firstName}
            />
          )}
          name="firstName"
          defaultValue=""
        />
        <Controller
          control={control}
          render={({onChange, value}) => (
            <Input
              label={t('employeeCode')}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors.employeeCode}
            />
          )}
          name="employeeCode"
          defaultValue=""
        />
      </s.Container>
    </KeyboardAwareScrollView>
  );
};

export default NewPassenger;
