import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({theme}) => theme.colors.white};
  align-items: center;
  border-radius: 9px;
  padding-vertical: ${calcHeight(16)}px;
  padding-horizontal: ${calcWidth(38)}px;
  margin-top: ${calcHeight(16)}px;
  flex-direction: column-reverse;
`;

styles.Row = styled.View`
  flex-direction: row-reverse;
  justify-content: space-between;
  width: ${calcWidth(284)};
`;

export default styles;
