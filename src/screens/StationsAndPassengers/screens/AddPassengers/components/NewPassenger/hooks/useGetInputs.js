import {useMemo} from 'react';
import {useTranslation} from 'react-i18next';

const useGetInputs = () => {
  const {t} = useTranslation();
  const inputs = [
    {key: 'passCode', label: t('employeeCode'), style: {}},
    {key: 'lastName', label: t('lastName'), style: {}},
    {key: 'firstName', label: t('firstName'), style: {}},
    {key: 'street', label: t('street'), style: {}},
    {key: 'houseNumber', label: t('houseNumber'), style: {}},
    {key: 'city', label: t('city'), style: {}},
    {key: 'phone', label: t('phone'), style: {}},
    {key: 'startTime', label: t('startTime'), style: {}},
    {key: 'remarks', label: t('comments'), style: {}},
  ];

  return {inputs};
};

export default useGetInputs;
