import React, {useState, useEffect} from 'react';
import {ActivityIndicator} from 'react-native';
import Text from '../../../../../../components/common/Text/Text';
import s from './styles';
import Input from '../../../../../../components/common/Input/Input';
import {useTranslation} from 'react-i18next';
import {useTheme} from 'styled-components';
import Table from './Table/Table';
import {fetchDataStatusE} from '../../../../../../utils/enums';

const SelectPassengers = ({
  passengers = [],
  onAdd = () => {},
  passengersStatus = null,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const [filterPassengers, setFilterPassengers] = useState([]);
  const [selectedPassengers, setSelectedPassengers] = useState([]);
  const [serachText, setSerachText] = useState('');

  useEffect(() => {
    setFilterPassengers(passengers.map((p) => ({...p, isCheck: false})));
  }, [passengers]);

  const onSelectPassenger = (pass, isCheck) => {
    if (isCheck) {
      setSelectedPassengers([...selectedPassengers, pass]);
      setFilterPassengers((prePassengers) =>
        prePassengers.map((p) => ({
          ...p,
          isCheck: p.passCode === pass.passCode ? true : p.isCheck,
        })),
      );
    } else {
      setSelectedPassengers(
        selectedPassengers.filter((p) => p.passCode !== pass.passCode),
      );
      setFilterPassengers((passengers) =>
        passengers.map((p) => ({
          ...p,
          isCheck: p.passCode === pass.passCode ? false : p.isCheck,
        })),
      );
    }
  };

  const onTextChange = (text) => {
    setSerachText(text);
    onFilterPassengers(text);
  };

  const onFilterPassengers = (t) => {
    if (!t) {
      setFilterPassengers([...passengers]);
      return;
    }

    const filterPass = filterPassengers.filter(({fullName}) =>
      fullName.includes(t),
    );
    setFilterPassengers([...filterPass]);
  };

  const inputStyle = {
    width: 300,
    height: 35,
    backgroundColor: colors.grayLight,
    borderRadius: 4,
  };

  const onCancelAll = () => {
    setFilterPassengers(filterPassengers.map((p) => ({...p, isCheck: false})));
    setSelectedPassengers([]);
  };

  const onAddPassengers = () => {
    onAdd(selectedPassengers);
  };

  return (
    <s.Container>
      {fetchDataStatusE.LOADING === passengersStatus ? (
        <ActivityIndicator size="large" color="#40A8E2" />
      ) : (
        <>
          <Input
            value={serachText}
            onChangeText={onTextChange}
            iconName="search"
            placeholder={t('freeSearch')}
            {...inputStyle}
          />
          <Table
            data={filterPassengers}
            onSelectPassenger={onSelectPassenger}
          />

          {selectedPassengers.length > 0 && (
            <>
              <s.CancelBtn onPress={onCancelAll}>
                <Text color="white">{t('cancelAll')}</Text>
              </s.CancelBtn>
              <s.AddBtn onPress={onAddPassengers}>
                <Text color="white" fontSize="s16" fontWeight={500}>
                  {t('add')}
                </Text>
              </s.AddBtn>
            </>
          )}
        </>
      )}
    </s.Container>
  );
};

export default SelectPassengers;
