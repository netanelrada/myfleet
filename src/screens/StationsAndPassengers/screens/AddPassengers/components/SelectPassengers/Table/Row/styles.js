import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  width: 100%;
  flex-grow: 1;
  flex-direction: row-reverse;
  margin-bottom: 10px;
  height: ${calcHeight(52)}px;
  transform: scaleX(-1);
  align-items: center;
`;

styles.CheckBoxContainer = styled.View`
  width: ${calcWidth(30)}px;
  align-items: center;
`;

styles.Col = styled.View`
  width: ${({width = 100}) => calcWidth(width)}px;
  margin-right: 7px;
`;

styles.Text = styled.Text`
  font-size: ${({theme}) => theme.fontSizes.s14}px;
  text-align: right;
`;

export default styles;
