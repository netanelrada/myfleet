import React from 'react';
import {useTranslation} from 'react-i18next';
import s from './styles';

const Titles = () => {
  const {t} = useTranslation();

  return (
    <s.Container>
      <s.CheckBoxContainer />
      <s.Col>
        <s.Text>{t('firstName')}</s.Text>
      </s.Col>
      <s.Col>
        <s.Text>{t('lastName')}</s.Text>
      </s.Col>
      <s.Col width={125}>
        <s.Text>{t('address')}</s.Text>
      </s.Col>
      <s.Col width={125}>
        <s.Text>{t('city')}</s.Text>
      </s.Col>
      <s.Col>
        <s.Text>{t('phone')}</s.Text>
      </s.Col>
      <s.Col>
        <s.Text>{t('commetns')}</s.Text>
      </s.Col>
    </s.Container>
  );
};

export default Titles;
