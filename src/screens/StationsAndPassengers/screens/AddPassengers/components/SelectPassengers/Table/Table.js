import React from 'react';
import s from './styles';
import Row from './Row/Row';
import {FlatList, ScrollView} from 'react-native';
import Titles from './Titles/Titles';

const Table = ({data = [], onSelectPassenger = () => {}}) => {

  return (
    <s.Container>
      <ScrollView horizontal style={{transform: [{scaleX: -1}]}}>
        <FlatList
          data={[{}, ...data]}
          keyExtractor={(item) => item.passCode}
          renderItem={({item, index}) =>
            index === 0 ? (
              <Titles />
            ) : (
              <Row
                {...item}
                onPressCheckBox={(isCheck) => onSelectPassenger(item, isCheck)}
              />
            )
          }
        />
      </ScrollView>
    </s.Container>
  );
};

export default Table;
