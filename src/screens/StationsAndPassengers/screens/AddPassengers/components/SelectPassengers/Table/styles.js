import styled from 'styled-components';
import {calcWidth} from '../../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: ${calcWidth(300)}px;
  margin-top: 15px;
`;

export default styles;
