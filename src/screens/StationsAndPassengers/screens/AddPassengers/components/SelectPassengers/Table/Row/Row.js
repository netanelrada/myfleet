import React from 'react';
import s from './styles';
import Text from '../../../../../../../../components/common/Text/Text';
import CheckBox from '../../../../../../../../components/common/CheckBox/CheckBox';
import Message from '../../../../../../components/Stations/components/Station/components/Passenger/components/Message/Message';

const Row = ({
  firstName = '',
  lastName = '',
  fullName = '',
  city = '',
  street = '',
  houseNum = '',
  phone1 = '',
  phone2 = '',
  isCheck = false,
  remarks = '',
  onPressCheckBox = () => {},
}) => {
  return (
    <s.Container>
      <s.CheckBoxContainer>
        <CheckBox value={isCheck} onPressCheckBox={onPressCheckBox} />
      </s.CheckBoxContainer>
      <s.Col>
        <s.Text>{firstName}</s.Text>
      </s.Col>
      <s.Col>
        <s.Text>{lastName}</s.Text>
      </s.Col>
      <s.Col width={125}>
        <s.Text>{`${street} ${houseNum}`}</s.Text>
      </s.Col>
      <s.Col width={125}>
        <s.Text>{`${city}`}</s.Text>
      </s.Col>
      <s.Col>
        <s.Text>{phone1 ? phone1 : phone2}</s.Text>
      </s.Col>
      <s.Col width={45}>{!!remarks && <Message message={remarks} />}</s.Col>
    </s.Container>
  );
};

export default Row;
