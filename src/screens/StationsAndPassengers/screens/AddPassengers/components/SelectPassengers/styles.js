import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({theme}) => theme.colors.white};
  align-items: center;
  border-radius: 9px;
  padding-vertical: ${calcHeight(16)}px;
  margin-top: ${calcHeight(16)}px;
`;

styles.AddBtn = styled.TouchableOpacity`
  width: 100%;
  height: ${calcHeight(48)}px;
  position: absolute;
  bottom: 0;
  background-color: ${({theme}) => theme.colors.blueLight};
  border-top-left-radius: 9px;
  border-top-right-radius: 9px;
  justify-content: center;
  align-items: center;
`;

styles.CancelBtn = styled.TouchableOpacity`
  width: ${calcWidth(82)}px;
  height: ${calcHeight(44)}px;
  position: absolute;
  bottom: ${calcHeight(76)};
  left: 0;
  background-color: ${({theme}) => theme.colors.blueLight};
  border-radius: 8px;
  justify-content: center;
  align-items: center;
`;

export default styles;
