import React, {useState} from 'react';
import Text from '../../../../components/common/Text/Text';
import Header from '../../../../components/Header/Header';
import {useTranslation} from 'react-i18next';
import s from './styles';
import {PASSENGERS_TABS} from '../../../../utils/enums';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import Tabs from './components/Tabs/Tabs';
import {calcWidth} from '../../../../utils/dimensions';
import SelectPassengers from './components/SelectPassengers/SelectPassengers';
import NewPassenger from './components/NewPassenger/NewPassenger';
import useGetPassengers from '../../hooks/useGetPassengers';

const iconSize = {
  width: calcWidth(22),
  height: calcWidth(16),
};

const AddPassengers = ({navigation, route}) => {
  const [currTab, setCurrTab] = useState(PASSENGERS_TABS.SELECT_PASSENGERS);
  const {t} = useTranslation();
  const {line, station, onGetStations, clientCode} = route.params;
  const {
    passengers,
    onAddPassengers,
    onAddNewPassenger,
    passengersStatus,
  } = useGetPassengers({
    clientCode,
    line,
    station,
  });
  const {goBack} = navigation;

  const submit = async (pass) => {
    const {data} = await onAddPassengers(pass);

    if (data.response === '0') {
      // success
      await onGetStations();
      navigation.goBack();
    }
  };

  const onAddPassenger = async (passengerData) => {
    const {data} = await onAddNewPassenger(passengerData);

    if (data.response === '0') {
      // success
      await onGetStations();

      navigation.goBack();
    }
  };
  const getCurrComponents = () => {
    switch (currTab) {
      case PASSENGERS_TABS.SELECT_PASSENGERS:
        return (
          <SelectPassengers
            passengers={passengers}
            onAdd={submit}
            passengersStatus={passengersStatus}
          />
        );
      case PASSENGERS_TABS.NEW_PASSENGER:
        return <NewPassenger onPressAdd={onAddPassenger} />;
      default:
        return null;
    }
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header
          titleText={t('addPassengers')}
          iconName="passengersL"
          iconStyle={iconSize}
          onGoBack={goBack}
        />
        <Tabs currTab={currTab} onPressTab={setCurrTab} />
        {getCurrComponents()}
      </s.Container>
    </ImageBackground>
  );
};

export default AddPassengers;
