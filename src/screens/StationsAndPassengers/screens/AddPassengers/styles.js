import styled from 'styled-components';
import {calcWidth} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: 100%;
  padding-horizontal: ${calcWidth(13)}px;
`;

export default styles;
