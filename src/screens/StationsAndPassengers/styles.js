import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  flex: 1;
  width: 100%;
`;
styles.Body = styled.View`
  flex: 1;
  width: 100%;
  padding-horizontal: ${calcWidth(15)}px;
`;

styles.Text = styled.Text``;

styles.ActionContainer = styled.View`
  position: absolute;
  bottom: ${calcHeight(80)}px;
  left: ${calcWidth(17)}px;
`;

export default styles;
