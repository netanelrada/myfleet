import React, {useReducer} from 'react';
import {useDispatch} from 'react-redux';
import RadioButton from '../../components/common/RadioButton/RadioButton';
import SelectOption from '../../components/common/SelectOption/SelectOption';
import Text from '../../components/common/Text/Text';
import {useTranslation} from 'react-i18next';
import Input from '../NewTrip/components/DetailsForm/components/Input/Input';
import {setFilterBy} from '../../store/actions/actionTrips';
import s from './styles';
import Header from '../../components/common/Header/Header';
import {useForm, Controller} from 'react-hook-form';
import useGetFormData from './hooks/useGetDataForm';
import useRefresh from '../../hooks/useRefresh';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import useBackHandler from '../../hooks/useBackHandler';
import {calcHeight} from '../../utils/dimensions';

const FilterTrips = ({navigation}) => {
  const {goBack} = navigation;
  useBackHandler(() => {
    goBack();
    return true;
  });
  const {t} = useTranslation();
  const {control, handleSubmit, errors, setValue} = useForm();
  const {filterFileds, orderByFildes, clearForm} = useGetFormData({
    setFiledValue: setValue,
  });
  const dispatch = useDispatch();
  const setFilterbyData = (payload) => dispatch(setFilterBy(payload));
  const {onRefresh} = useRefresh();
  useBackHandler(() => {
    goBack();
    return true;
  });

  const obSubmit = (data) => {
    const payload = Object.keys(data).reduce((acc, curr) => {
      if (curr !== 'orderBy' && data[curr].text === '')
        acc[curr] = {key: '', text: ''};
      else acc[curr] = data[curr];

      return acc;
    }, {});
    setFilterbyData(payload);
    onRefresh();
    goBack();
  };

  const getInputControl = (
    {key, defaultValue, validation = {}, ...rest},
    index,
  ) => {
    return (
      <Controller
        key={key}
        control={control}
        render={({onChange, onBlur, value}) => (
          <s.View>
            {errors[key] && <s.ErrorText>{errors[key]?.message}</s.ErrorText>}
            <Input {...rest} codeProp={key} onChange={onChange} value={value} />
          </s.View>
        )}
        name={key}
        rules={{...validation}}
        defaultValue={defaultValue}
      />
    );
  };

  return (
    <s.Container>
      <s.SafeAreaView>
        <Header onBack={goBack} />
        <KeyboardAwareScrollView
          contentContainerStyle={{flexGrow: 1}}
          enableOnAndroid
          extraHeight={calcHeight(100)}>
          <s.BoadyFormContainer>
            <s.SortContainer>
              <s.Label>{`${orderByFildes.orderBy.label}: `}</s.Label>
              <s.OptionSortContainer>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <SelectOption
                      updateSelectedOption={(key) => onChange(key)}
                      value={value}
                      options={orderByFildes.orderBy.options}>
                      {orderByFildes.orderBy.options.map(({key, label}) => (
                        <RadioButton key={key} label={label} />
                      ))}
                    </SelectOption>
                  )}
                  name="orderBy"
                  rules={{}}
                  defaultValue={orderByFildes.orderBy.defaultValue}
                />
              </s.OptionSortContainer>
            </s.SortContainer>
            <s.FilterContainer>
              <s.FilterHeaderContainer>
                <s.Label>{`${t('filterBy')}: `}</s.Label>
              </s.FilterHeaderContainer>
              <s.InputsContinaer>
                <s.TouchableOpacity onPress={clearForm}>
                  <s.ClearSelected>{t('clearSelect')}</s.ClearSelected>
                </s.TouchableOpacity>
                {filterFileds.map((item, index) =>
                  getInputControl(item, index),
                )}
              </s.InputsContinaer>
            </s.FilterContainer>
          </s.BoadyFormContainer>
        </KeyboardAwareScrollView>
        <s.Btn onPress={handleSubmit(obSubmit)}>
          <Text fontSize="s16" fontWeight={500} color="white">
            {t('ok')}
          </Text>
        </s.Btn>
      </s.SafeAreaView>
    </s.Container>
  );
};

export default FilterTrips;
