import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	width: 100%;
	background-color: white;
`;

styles.SafeAreaView = styled.SafeAreaView`
	flex: 1;
	width: 100%;
`;

styles.BoadyFormContainer = styled.View`
	flex: 1;
	justify-content: space-around;
	padding-horizontal: ${calcWidth(10)}px;
`;

styles.Btn = styled.TouchableOpacity`
	width: 100%;
	height: ${calcHeight(44)}px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	justify-content: center;
	align-items: center;
	position: absolute;
	bottom: 0;
`;

styles.SortContainer = styled.View`
	flex-direction: row-reverse;
	padding-horizontal: ${calcWidth(5)}px;
	align-items: center;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-weight: 500;
	font-size: ${({ theme }) => theme.fontSizes.s16}px;
	margin-left: ${calcWidth(5)}px;
`;

styles.OptionSortContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-around;
	flex: 1;
`;

styles.FilterContainer = styled.View`
	margin-top: ${calcHeight(25)};
	flex: 1;
`;

styles.FilterHeaderContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-between;
	align-items: center;
	padding-horizontal: ${calcWidth(5)}px;
`;

styles.InputsContinaer = styled.View`
	align-items: center;
	flex-direction: column-reverse;
	margin-top: 5px;
	flex: 0.7;
	justify-content: space-around;
`;

styles.ClearSelected = styled.Text`
	margin-top: 10px;
	font-family: Rubik-Regular;
	font-weight: 400;
	color: ${({ theme }) => theme.colors.darkGray};
	text-align: center;
	text-decoration-line: underline;
`;

styles.TouchableOpacity = styled.TouchableOpacity``;

styles.View = styled.View`
	flex-direction: column;
	margin-vertical: 5px;
`;

styles.ErrorText = styled.Text`
	font-family: Rubik-Regular;
	position: absolute;
	right: 0;
	bottom: 0;
	font-size: ${({ theme }) => theme.fontSizes.s12}px;
	color: ${({ theme }) => theme.colors.errorColor};
	text-align: right;
`;

export default styles;
