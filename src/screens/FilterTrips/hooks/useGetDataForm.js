import {useMemo, useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {getClinets, getDrivers, getCars, getLineTypes} from '../../../api/api';
import useProxyApi from '../../../hooks/useProxyApi';
import {useSelector} from 'react-redux';
import {
  dateSelector,
  filterBySelector,
} from '../../../store/selectors/tripsSelectors';
import moment from 'moment';
import {validationSechema} from '../../NewTrip/validations';
import {useNavigation} from '@react-navigation/native';
import {typeOrderByE} from '../../../utils/enums';

const useGetFormData = ({setFiledValue = () => {}}) => {
  const {t} = useTranslation();
  const {proxyApi} = useProxyApi();
  const {navigate} = useNavigation();
  const date = useSelector((state) => dateSelector(state));
  const filterByState = useSelector((state) => filterBySelector(state));
  const [clinetsList, setClinetsList] = useState([]);
  const [driversList, setDriversList] = useState([]);
  const [carsList, setCarsList] = useState([]);
  const [lineTypes, setLineTypes] = useState([]);

  useEffect(() => {
    try {
      onGetClinetsList();
      onGetDriversList();
      onGetCarsList();
      onGetLinesTypes();
    } catch (error) {
      console.log({error});
    }
  }, []);

  const filterFileds = useMemo(
    () => [
      {
        key: 'lineTypeCode',
        label: t('lineType'),
        width: 320,
        action: () => {},
        visableIcon: false,
        autoCompleteData: {
          isVisable: true,
          listData: lineTypes,
          showProp: 'lineType',
          limitData: 5,
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: filterByState.lineTypeCode,
      },
      {
        key: 'lineDescription',
        label: t('lineDescription'),
        width: 320,
        action: () => {},
        visableIcon: false,
        validation: {},
        defaultValue: filterByState.lineDescription,
      },
      {
        key: 'carCode',
        label: t('car'),
        action: () =>
          navigate('NewTripStack', {
            screen: 'CarSelect',
            params: {setFiledValue},
          }),
        width: 320,
        autoCompleteData: {
          isVisable: true,
          listData: carsList,
          showProp: 'carNumber',
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: filterByState.carCode,
      },
      {
        key: 'clientCode',
        label: t('selectClient'),
        width: 320,
        action: () =>
          navigate('NewTripStack', {
            screen: 'ClientSelect',
            params: {clinetsList, setFiledValue},
          }),
        autoCompleteData: {
          isVisable: true,
          listData: clinetsList,
          showProp: 'clientName',
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: filterByState.clientCode,
      },
      {
        key: 'driverCode',
        label: t('driver'),
        width: 320,
        action: () =>
          navigate('NewTripStack', {
            screen: 'DriverSelect',
            params: {setFiledValue},
          }),
        autoCompleteData: {
          isVisable: true,
          listData: driversList,
          showProp: 'nickName',
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: filterByState.driverCode,
      },
    ],
    [clinetsList, driversList, carsList, lineTypes],
  );

  const orderByFildes = useMemo(() => {
    const orderByOptions = [
      {key: 'startTime', label: t('hour')},
      {key: 'clientName', label: t('client')},
      {key: 'driverName', label: t('driver')},
      {key: 'lineDescription', label: t('lineDescription')},
    ];
    return {
      orderBy: {
        label: t('orderBy'),
        options: [...orderByOptions],
        defaultValue: filterByState.orderBy,
      },
    };
  }, []);

  const onGetClinetsList = async () => {
    try {
      const res = await proxyApi({
        cb: getClinets,
        restProp: {},
      });
      const {data = []} = res.data;
      setClinetsList(data);
    } catch (error) {}
  };

  const onGetDriversList = async () => {
    const lineDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
    const res = await proxyApi({
      cb: getDrivers,
      restProp: {lineDate, onlyFree: 0},
    });
    const {data = []} = res.data;
    setDriversList(data);
  };

  const onGetCarsList = async () => {
    const lineDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
    const res = await proxyApi({
      cb: getCars,
      restProp: {lineDate, onlyFree: 0},
    });
    const {data = []} = res.data;
    setCarsList(data);
  };

  const onGetLinesTypes = async () => {
    const res = await proxyApi({
      cb: getLineTypes,
      restProp: {},
    });
    const {data = []} = res.data;
    setLineTypes(data);
  };

  const clearForm = () => {
    Object.keys(filterByState).forEach((item) => {
      if (item === 'orderBy') {
        setFiledValue('orderBy', typeOrderByE.HOUR);
      } else {
        setFiledValue(item, {key: '', text: ''}, {shouldValidate: true});
      }
    });
  };

  return {filterFileds, orderByFildes, clearForm};
};

export default useGetFormData;
