import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.Pressable`
	flex: 1;
	align-items: center;
	padding-horizontal: ${calcWidth(13)}px;
`;

styles.TimeContainer = styled.View`
	width: ${calcWidth(332)}px;
	aspect-ratio: ${332 / 89};
	background: ${({ theme }) => theme.colors.white};
	border: 1px solid ${({ theme }) => theme.colors.borderColorInput};
	border-radius: 9px;
	flex-direction: row-reverse;
	justify-content: space-around;
`;
styles.TimeItemContainer = styled.View`
	justify-content: center;
`;

styles.TabContainer = styled.View`
	width: 100%;
	position: absolute;
	bottom: 0;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14}px;
	color: ${({ theme }) => theme.colors.black};
	text-align: right;
`;
export default styles;
