import { useState } from 'react';
import useProxyApi from '../../../hooks/useProxyApi';
import { setLineDetails } from '../../../api/api';
import useHandleResponse from '../../../hooks/useHandleResponse';
import { isHours, isMinutes, validateTime } from '../../../utils/time';

const useEditScheduleTrip = ({ initStartTime, initEndTime, lineCode }) => {
	const { proxyApi } = useProxyApi();
	const { handleResponse } = useHandleResponse({ enableGoBack: true });
	const [startTime, setStartTime] = useState({
		time: initStartTime,
		isValid: true,
	});
	const [endTime, setEndTime] = useState({ time: initEndTime, isValid: true });

	const onSetTime = async () => {
		try {
			if (!validateTime(startTime.time)) {
				setStartTime(preState => ({ ...preState, isValid: false }));
				return;
			}
			if (!validateTime(endTime.time)) {
				setEndTime(preState => ({ ...preState, isValid: false }));
				return;
			}
			const res = await proxyApi({
				cb: setLineDetails,
				restProp: {
					lineCode,
					startTime: startTime.time,
					endTime: endTime.time,
				},
			});
			res.data.updateLines = lineCode;
			handleResponse(res.data);
			
		} catch (error) {
			console.log(error);
		}
	};

	return {
		onSetTime,
		startTime,
		setStartTime,
		endTime,
		setEndTime,
	};
};

export default useEditScheduleTrip;
