import React from 'react';
import { Keyboard } from 'react-native';
import { useTranslation } from 'react-i18next';
import Header from '../../components/Header/Header';
import s from './styles';
import TabConfirm from '../../components/common/TabConfirm/TabConfirm';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import TimePickerInput from '../../components/common/TimePickerInput/TimeInput';
import useEditScheduleTrip from './hooks/useEditScheduleTrip';
import useBackHandler from '../../hooks/useBackHandler';
import TimeInput from '../NewTrip/components/DateForm/components/TimeInput/DateInput';

const getTime = (startTime = '', endTime = '') => {
	const start = startTime.split(' ').pop();
	const end = endTime.split(' ').pop();

	return { start, end };
};

const EditScheduleTrip = ({ navigation, route }) => {
	const {
		lineCode,
		startTime: fullStartTime,
		endTime: fullEndTime,
	} = route.params.trip;
	const { start: initStartTime, end: initEndTime } = getTime(
		fullStartTime,
		fullEndTime
	);

	const { t } = useTranslation();
	const { goBack } = navigation;
	useBackHandler(() => {
		goBack();
		return true;
	});
	const {
		onSetTime,
		startTime,
		setStartTime,
		endTime,
		setEndTime,
	} = useEditScheduleTrip({ initStartTime, initEndTime, lineCode });

	const closeKeyboard = () => {
		Keyboard.dismiss();
	};

	return (
		<ImageBackground>
			<s.Container onPress={closeKeyboard}>
				<Header
					onGoBack={goBack}
					titleText={t('editScheduleTrip')}
					iconName='clock'
				/>

				<s.TimeContainer>
					<s.TimeItemContainer>
						<s.Label>{t('startTime')}</s.Label>
						<TimeInput
							isValidate={startTime.isValid}
							label={t('startTime')}
							value={startTime.time}
							onChange={t =>
								setStartTime(preState => ({ ...preState, time: t }))
							}
							width={142}
						/>
					</s.TimeItemContainer>
					<s.TimeItemContainer>
						<s.Label>{t('endTime')}</s.Label>
						<TimeInput
							isValidate={endTime.isValid}
							label={t('endTime')}
							value={endTime.time}
							onChange={t => setEndTime(preState => ({ ...preState, time: t }))}
							width={142}
						/>
					</s.TimeItemContainer>
				</s.TimeContainer>
			</s.Container>
			<s.TabContainer>
				<TabConfirm onCancel={goBack} onSave={onSetTime} />
			</s.TabContainer>
		</ImageBackground>
	);
};

export default EditScheduleTrip;
