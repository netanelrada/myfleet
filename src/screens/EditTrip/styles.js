import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.Pressable`
	flex: 1;
	padding-horizontal: ${calcWidth(13)}px;
`;

styles.TextInputContainer = styled.View`
	width: ${calcWidth(332)}px;
	height: ${calcHeight(58)}px;
	background-color: ${({ theme: { colors }, isLock }) =>
		isLock ? colors.gray8 : colors.white};
	border-radius: 9px;
	border: 1px solid ${({ theme }) => theme.colors.gray7};
	flex-direction: row-reverse;
	align-items: center;
	justify-content: space-around;
	margin-top: ${calcWidth(10)}px;
`;
//justify-content: ${({ isLock }) => (isLock ? 'space-around' : 'flex-start')};

styles.TextInput = styled.TextInput`
	flex: ${({ isLock }) => (isLock ? 0.8 : 1)};
	padding-horizontal: ${calcWidth(5)}px;
	height: 80%;
	text-align: right;
`;

styles.IconContainer = styled.View`
	flex: 0.2;
	align-items: center;
`;
styles.TabContainer = styled.View`
	width: 100%;
	position: absolute;
	bottom: ${({ bottom = 0 }) => (bottom ? bottom : 0)}px;
`;

export default styles;
