import React, { useState, useRef } from 'react';
import { Keyboard } from 'react-native';
import s from './styles';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import Header from '../../components/Header/Header';
import { useTranslation } from 'react-i18next';
import { calcWidth } from '../../utils/dimensions';
import LineDescription from './components/LineDescription/LineDescription';
import useEditTrip from './hooks/useEditTrip';
import SerachList from '../../components/SerachList/SerachList';
import TableData from './components/TableData/TableData';
import TabConfirm from '../../components/common/TabConfirm/TabConfirm';
import useKeyboardHeight from '../../hooks/useKeyboardHeight';
import ModalConfirm from '../../components/common/ModalConfirm/ModalConfirm';
import Text from '../../components/common/Text/Text';
import Icon from '../../components/common/Icon/Icon';
import useBackHandler from '../../hooks/useBackHandler';

const EditTrip = ({ navigation, route }) => {
	const { goBack } = navigation;
	useBackHandler(() => {
		goBack();
		return true;
	});
	const { lineCode = '' } = route.params.trip;
	const { t } = useTranslation();
	const {
		lineDescriptionText,
		setLineDescriptionText,
		courseList,
		statusData,
		saveChangeTrip,
		setlineDescriptionByCoursesId,
		isLock,
	} = useEditTrip(lineCode);
	const keyboardHeight = useKeyboardHeight();
	const [isLineDescriptionFocus, setIsLineDescriptionFocus] = useState(false);
	const [isModalVisable, setIsModalVisable] = useState(false);
	const isInitialShowTab = useRef(false);
	const selectedCourseRef = useRef(null);

	const filter = ({ courseDescription, courseCode }, text) =>
		courseDescription.includes(text) || courseCode.includes(text);

	const tableHead = [t('lineDescription'), t('code')];

	const arrStyle = [
		{ textAlign: 'right', flex: 0.8 },
		{ textAlign: 'center', flex: 0.2 },
	];

	const onPressCourse = course => {
		selectedCourseRef.current = course;
		setIsModalVisable(true);
	};

	const closeKeyboard = () => {
		Keyboard.dismiss();
	};

	const onFoucsLineDescriptionText = () => {
		if (!isInitialShowTab.current) isInitialShowTab.current = true;
		setIsLineDescriptionFocus(true);
	};

	const onSave = () => {
		closeKeyboard();
		saveChangeTrip();
	};

	const onConfirmSelectedCourse = () => {
		setIsModalVisable(false);
		setlineDescriptionByCoursesId(selectedCourseRef.current.courseCode);
	};

	return (
		<ImageBackground>
			<s.Container onPress={closeKeyboard}>
				<Header
					onGoBack={goBack}
					iconStyle={{}}
					iconName='road'
					titleText={t('editTrip')}
				/>
				<s.TextInputContainer
					pointerEvents={isLock ? 'none' : 'auto'}
					isLock={isLock}
				>
					<s.TextInput
						onChangeText={text => setLineDescriptionText(text)}
						onFocus={onFoucsLineDescriptionText}
						onEndEditing={() => setIsLineDescriptionFocus(false)}
						value={lineDescriptionText}
						multiline
					/>
					{isLock && (
						<s.IconContainer>
							<Icon name='lock' />
						</s.IconContainer>
					)}
				</s.TextInputContainer>

				<SerachList
					tableHead={tableHead}
					tableData={courseList}
					arrStyle={arrStyle}
					callBackFilter={(data, text) => filter(data, text)}
					RowComponent={<TableData arrStyle={arrStyle} />}
					onClickItem={onPressCourse}
					statusData={statusData}
					keyItem='courseCode'
				/>
			</s.Container>
			{isInitialShowTab.current && (
				<s.TabContainer bottom={isLineDescriptionFocus && keyboardHeight}>
					<TabConfirm onCancel={goBack} onSave={onSave} />
				</s.TabContainer>
			)}
			<ModalConfirm
				isVisible={isModalVisable}
				iconName='road'
				onCancel={() => setIsModalVisable(false)}
				onConfirm={onConfirmSelectedCourse}
				textConfirnBtn={t('ok')}
				textCancelBtn={t('cancel')}
				iconStyle={{
					width: calcWidth(28),
					height: calcWidth(28),
				}}
			>
				<Text textAlign='center' numberOfLines={2} fontSize='s20'>
					{t('confirmChangeTrip')}
				</Text>
			</ModalConfirm>
		</ImageBackground>
	);
};

export default EditTrip;
