import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(332)}px;
	height: ${calcHeight(58)}px;
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	border: 1px solid ${({ theme }) => theme.colors.gray7};
	padding: ${calcWidth(5)}px;
	margin-top: ${calcWidth(10)}px;
`;

styles.TextInput = styled.TextInput`
	text-align: right;
	flex: 1;
`;

export default styles;
