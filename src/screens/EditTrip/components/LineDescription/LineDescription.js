import React from 'react';
import Text from '../../../../components/common/Text/Text';
import s from './styles';

const LineDescription = ({
	lineDescription = '',
	onChangelineDescription = () => {},
}) => {
	return (
		<s.Container>
			<s.TextInput
				onChangeText={text => onChangelineDescription(text)}
				onFocus={() =>{}}
				onEndEditing={() => {}}
				value={lineDescription}
				multiline
			/>
		</s.Container>
	);
};

export default LineDescription;
