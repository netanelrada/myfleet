import React from 'react';
import styled from 'styled-components';
import { calcHeight } from '../../../../utils/dimensions';
import Text from '../../../../components/common/Text/Text';

const TableData = ({
	arrStyle = [],
	data: { courseCode = '', courseDescription = '' },
	onPress = () => {},
}) => {
	return (
		<s.Container onPress={onPress}>
			<s.Cell flex={arrStyle[0].flex}>
				<Text color='black' textAlign={arrStyle[0].textAlign}>
					{courseDescription}
				</Text>
			</s.Cell>
			<s.Cell flex={arrStyle[1].flex}>
				<Text textAlign={arrStyle[1].textAlign}>{courseCode}</Text>
			</s.Cell>
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		height: ${calcHeight(52)}px;
		border-bottom-color: rgba(180, 190, 201, 0.3);
		border-bottom-width: 1px;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: flex-start;
		width: 100%;
	`,
	Cell: styled.View`
		flex: ${({ flex }) => flex};
		${({ align }) => `align-items: ${align};`}
	`,
};

export default TableData;
