import {useState, useEffect} from 'react';
import {getCourses, setLineDetails} from '../../../api/api';
import useProxyApi from '../../../hooks/useProxyApi';
import {fetchDataStatusE} from '../../../utils/enums';
import useHandleResponse from '../../../hooks/useHandleResponse';

const useEditTrip = (lineCode = '') => {
  const {handleResponse} = useHandleResponse({enableGoBack: true});
  const {proxyApi} = useProxyApi();
  const [lineDescriptionText, setLineDescriptionText] = useState('');
  const [courseList, setcourseList] = useState([]);
  const [statusData, setStatusData] = useState(null);
  const [isLock, setIsLock] = useState(false);

  useEffect(() => {
    onGetCourses();
  }, []);

  const onGetCourses = async () => {
    setStatusData(fetchDataStatusE.LOADING);
    try {
      const res = await proxyApi({
        cb: getCourses,
        restProp: {lineCode},
      });
      const {lineDescription, courseList, lockType} = res.data;
      if (lockType !== '0') setIsLock(true);
      setLineDescriptionText(lineDescription);
      setStatusData(fetchDataStatusE.SUCCESS);
      setcourseList(courseList);
    } catch (error) {
      console.log(error);
      setStatusData(fetchDataStatusE.FAIL);
    }
  };

  const saveChangeTrip = async () => {
    try {
      const res = await proxyApi({
        cb: setLineDetails,
        restProp: {lineCode, lineDescription: lineDescriptionText},
      });
      res.data.updateLines = lineCode;
      handleResponse(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  const setlineDescriptionByCoursesId = async (courseCode) => {
    try {
      const res = await proxyApi({
        cb: setLineDetails,
        restProp: {lineCode, courseCode},
      });

      res.data.updateLines = lineCode;
      handleResponse(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    lineDescriptionText,
    setLineDescriptionText,
    courseList,
    statusData,
    saveChangeTrip,
    setlineDescriptionByCoursesId,
    isLock,
  };
};

export default useEditTrip;
