import React from 'react';
import s from './styles';
import { useTranslation } from 'react-i18next';
import { useTheme } from 'styled-components';
import Text from '../../../../components/common/Text/Text';
import Icon from '../../../../components/common/Icon/Icon';
import { calcWidth } from '../../../../utils/dimensions';
import { APP_NAME } from '../../../../utils/enums';

const HeaderForm = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const iconSize = {
		width: calcWidth(44),
		height: calcWidth(44),
	};
	return (
		<s.Container>
			<s.Row>
				<Icon name='logo' {...iconSize} />
				<Text
					fontFamily='RoundedMplus1c-Bold'
					fontSize='s24'
					color={theme.colors.blueLight}
				>
					{APP_NAME}
				</Text>
			</s.Row>
			<Text fontFamily='Rubik-Medium' fontWeight={500} fontSize='s20'>
				{t('login')}
			</Text>
		</s.Container>
	);
};
export default HeaderForm;
