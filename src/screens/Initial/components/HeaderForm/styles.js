import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(296)}px;
	height: ${calcHeight(40)}px;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

styles.Image = styled.Image`
	width: ${calcWidth(118)}px;
	aspect-ratio: ${118 / 30};
`;

styles.Row = styled.View`
	flex-direction: row;
	align-items: center;
`;

export default styles;
