import React from 'react';
import styled, { useTheme } from 'styled-components';
import Text from '../../../../components/common/Text/Text';
import { APP_NAME } from '../../../../utils/enums';
import { useTranslation } from 'react-i18next';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
import Icon from '../../../../components/common/Icon/Icon';

const SuccessForm = () => {
	const { colors } = useTheme();
	const { t } = useTranslation();
	const iconSize = {
		width: calcWidth(44),
		height: calcWidth(44),
	};

	return (
		<s.Container>
			<s.Row>
				<Icon name='logo' {...iconSize} />
				<Text
					fontFamily='RoundedMplus1c-Bold'
					fontSize='s24'
					color={colors.blueLight}
				>
					{APP_NAME}
				</Text>
			</s.Row>
			<s.View>
				<Text fontSize='s18' fontWeight={500} color={colors.textColor}>
					{t('login')}
				</Text>
				<Text fontSize='s18' fontWeight={500} color={colors.textColor}>
					{t('doneSuccessfully')}
				</Text>
			</s.View>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		height: ${calcHeight(100)}px;
		margin-top: ${calcHeight(25)}px;
		justify-content: space-between;
		align-items: center;
	`,
	View: styled.View`
		align-items: center;
	`,
	Row: styled.View`
		flex-direction: row;
		align-items: center;
	`,
};

export default SuccessForm;
