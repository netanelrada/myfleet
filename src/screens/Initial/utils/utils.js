import i18n from '../../../i18n';

export const textErrors = () => [
  i18n.t('error'),
  i18n.t('wrongInputs'),
  i18n.t('errorServer'),
  i18n.t('wrongPassword'),
  i18n.t('expiredUser'),
];

export const getErrorIconNamebyIndex = (index) =>
  index > 3 ? 'devices' : 'warning';
