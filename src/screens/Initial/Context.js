import React, { createContext, useState } from 'react';

export const FillContext = createContext();

export default function FillProvider(props) {
	const [fill, setFill] = useState(2);
	const value = [fill, setFill];
	return <FillContext.Provider value={value} {...props} />;
}
