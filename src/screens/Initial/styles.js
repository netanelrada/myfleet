import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	justify-content: center;
	align-items: center;
	flex: 1;
`;

styles.HeaderContainer = styled.View`
	top: ${calcHeight(50)}px;
`;

styles.FormContainer = styled.View`
	top: ${calcHeight(200)}px;
`;

styles.ProgressContainer = styled.View`
	bottom: 0;
	z-index: 999;
`;

styles.Board = styled.View`
	width: ${calcWidth(308)}px;
	aspect-ratio: ${308 / 388};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
`;

export default styles;
