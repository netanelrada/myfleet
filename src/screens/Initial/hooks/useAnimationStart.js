import { useEffect } from 'react';
import { Animated } from 'react-native';
import { calcHeight } from '../../../utils/dimensions';

const useAnimationStart = (
	isSplashFinish,
	headerAnimRef,
	formAnimRef,
	opacityAnimRef
) => {
	useEffect(() => {
		isSplashFinish && start();
	}, [isSplashFinish]);

	const getConfigAnim = ({ toValue = 1, duration = 1000 }) => ({
		toValue,
		duration,
		useNativeDriver: true,
	});

	const start = () => {
		Animated.parallel([
			Animated.timing(opacityAnimRef, getConfigAnim({ duration: 500 })),
			Animated.timing(
				headerAnimRef,
				getConfigAnim({ toValue: calcHeight(43) })
			),
			Animated.timing(
				formAnimRef,
				getConfigAnim({ toValue: calcHeight(100) * -1 })
			),
		]).start();
	};
};

export default useAnimationStart;
