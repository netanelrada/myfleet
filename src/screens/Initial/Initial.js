import React, {useState, useEffect, useRef} from 'react';
import {Animated, BackHandler} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  initConnect,
  connectByUUID,
  restIsFinshedInitAuth,
} from '../../store/actions/actionLogin';
import {loginSelector} from '../../store/selectors/loginSelectors';
import LoginForm from '../../components/Forms/Login/Login';
import s from './styles';
import HeaderForm from './components/HeaderForm/HeaderForm';
import Progress from '../../components/common/Progress/Progress';
import FillProvider from './Context';
import SplashScreen from '../../components/SplashScreen/SplashScreen';
import ModalConfirm from '../../components/common/ModalConfirm/ModalConfirm';
import Text from '../../components/common/Text/Text';
import {textErrors, getErrorIconNamebyIndex} from './utils/utils';
import {useTheme} from 'styled-components';
import {useTranslation} from 'react-i18next';
import CustomKeyboardAvoidView from '../../components/common/CustomKeyboardAvoidView/CustomKeyboardAvoidView';
import useAnimationStart from './hooks/useAnimationStart';
import SuccessForm from './components/SuccessForm/SuccessForm';
import {loginByUUIDStatusE} from '../../utils/enums';
import useBackHandler from '../../hooks/useBackHandler';
import {restAllPersistenceData} from '../../store/actions/actionPersistenceFormDataSelectors';

const Initial = (props) => {
  const headerAnimRef = useRef(new Animated.Value(0)).current;
  const formAnimRef = useRef(new Animated.Value(0)).current;
  const opacityAnimRef = useRef(new Animated.Value(0)).current;

  const [startLoginAnim, setStartLoginAnim] = useState(false);
  const [isSplashFinish, setIsSplashFinish] = useState(false);
  const [isAuthSuccess, setisAuthSuccess] = useState(false);
  const [modalConfirmState, setModalConfirmState] = useState({
    isVisable: false,
    indexError: null,
  });
  const dispatch = useDispatch();
  const onLogin = (payload) => dispatch(initConnect(payload));
  const onLoginByUUID = () => dispatch(connectByUUID());
  const onRestIsFinshedInitAuth = () => dispatch(restIsFinshedInitAuth());
  const onRestAllPersistenceData = () => dispatch(restAllPersistenceData());
  const {
    isFinshedInitAuth,
    statusFail,
    isAuthenticated,
    loginByUUIDStatus,
  } = useSelector((state) => loginSelector(state));
  const {t} = useTranslation();
  useBackHandler(() => BackHandler.exitApp());
  const {replace} = props.navigation;
  useAnimationStart(startLoginAnim, headerAnimRef, formAnimRef, opacityAnimRef);

  useEffect(() => {
    //	setModalConfirmState({ isVisable: false, indexError: null });
    onLoginByUUID();
  }, []);

  useEffect(() => {
    if (isSplashFinish) {
      if (loginByUUIDStatus === loginByUUIDStatusE.SUCCESS) replace('Home');
      else setStartLoginAnim(true);
    }
  }, [isSplashFinish]);

  useEffect(() => {
    if (isAuthenticated) setSuccessToConnect();
    else if (isFinshedInitAuth) {
      setModalConfirmState({isVisable: true, indexError: statusFail});
      onRestIsFinshedInitAuth();
    }
  }, [isFinshedInitAuth]);

  const setSuccessToConnect = () => {
    setisAuthSuccess(true);
    onRestAllPersistenceData();
    setTimeout(() => {
      replace('Home');
    }, 2500);
  };
  const onSubmit = (data) => {
    onLogin(data);
  };

  const closeModal = () => {
    setModalConfirmState({isVisable: false, indexError: null});
  };

  return (
    <FillProvider>
      <SplashScreen setIsSplashFinish={setIsSplashFinish}>
        <s.Container>
          <CustomKeyboardAvoidView>
            <s.HeaderContainer>
              <Animated.View
                style={{
                  opacity: opacityAnimRef,
                  transform: [
                    {
                      translateY: headerAnimRef,
                    },
                  ],
                }}>
                {!isAuthSuccess && <HeaderForm />}
              </Animated.View>
            </s.HeaderContainer>
            <s.FormContainer>
              <Animated.View
                style={{
                  opacity: opacityAnimRef,
                  transform: [
                    {
                      translateY: formAnimRef,
                    },
                  ],
                }}>
                <s.Board>
                  {isAuthSuccess ? (
                    <SuccessForm />
                  ) : (
                    <LoginForm submit={onSubmit} />
                  )}
                </s.Board>
              </Animated.View>
            </s.FormContainer>
          </CustomKeyboardAvoidView>

          <ModalConfirm
            isVisible={modalConfirmState.isVisable}
            onCancel={closeModal}
            isConfirmHidden={true}
            textCancelBtn={t('close')}
            iconName={getErrorIconNamebyIndex(modalConfirmState.indexError)}>
            <Text
              fontFamily="Rubik-Regular"
              textAlign="center"
              fontSize="s20"
              color="black">
              {textErrors()[modalConfirmState.indexError]}
            </Text>
          </ModalConfirm>
        </s.Container>
        <s.ProgressContainer>
          <Progress />
        </s.ProgressContainer>
      </SplashScreen>
    </FillProvider>
  );
};

export default Initial;
