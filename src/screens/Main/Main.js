import React, {useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {
  initGetBranches,
  initGetTrips,
  setTabFoucs,
} from '../../store/actions/actionTrips';
import {userSelector} from '../../store/selectors/loginSelectors';
import s from './styles';
import Header from './components/Header/Header';
import TripList from './components/TripList/TripList';
import {dateSelector} from '../../store/selectors/tripsSelectors';
import moment from 'moment';
import Tab from './components/Tab/Tab';
import Drawer from './components/Drawer/Drawer';

const Main = () => {
  const dispatch = useDispatch();
  const onInitGetTrips = (payload) => dispatch(initGetTrips(payload));
  const onInitGetBranches = (payload) => dispatch(initGetBranches(payload));
  const user = useSelector((state) => userSelector(state));
  const date = useSelector((state) => dateSelector(state));
  const onSetTabFoucs = (payload) => dispatch(setTabFoucs(payload));

  useFocusEffect(
    useCallback(() => {
      onSetTabFoucs({key: null, clickOnTab: false});
    }, []),
  );

  useEffect(() => {
    onInitGetBranches(user);
  }, [user]);

  useEffect(() => {
    try {
      if (!user) return;
      const {userToken, wsString} = user;
      const relativeDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
      const payload = {
        userToken,
        wsString,
        relativeDate,
      };

      onInitGetTrips(payload);
    } catch (error) {
      console.log(error);
    }
  }, [date, user]);

  return (
    <Drawer>
      <s.Container>
        <TripList />
        <Header />
        <Tab />
      </s.Container>
    </Drawer>
  );
};

export default Main;
