import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';
const styles = {};

styles.Button = styled.View`
  border: 0.5px solid ${({theme}) => theme.colors.gray};
  background-color: ${({theme}) => theme.colors.white};
  width: ${calcWidth(20)}px;
  height: ${calcHeight(40)}px;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 999;
  right: 0;
  top: 50%;
`;

export default styles;
