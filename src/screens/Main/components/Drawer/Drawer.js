import React, {useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Drawer from 'react-native-drawer';
import {branchesSelector} from '../../../../store/selectors/tripsSelectors';
import useRefresh from '../../../../hooks/useRefresh';
import Icon from '../../../../components/common/Icon/Icon';
import RotateAnimation from '../../../../components/common/RotateAnimation/RotateAnimation';
import {setFilterByBranch} from '../../../../store/actions/actionTrips';
import ControlPanel from './ControlPanel/ControlPanel';
import s from './styles';

export default function RNDrawer({children}) {
  const dispatch = useDispatch();
  const onSetFilterByBranch = (payload) => dispatch(setFilterByBranch(payload));
  const {onRefresh} = useRefresh();
  const ref = useRef();
  const [isOpen, setisOpen] = useState(false);
  const branches = useSelector((state) => branchesSelector(state));
  const disableDrwer = branches.length < 2;

  const onToggle = () => {
    ref.current.toggle();
  };

  const onSubmit = async (barnch) => {
    const {branchName, ...restPayload} = barnch;
    await onSetFilterByBranch(restPayload);
    onRefresh();
    onToggle();
  };

  return (
    <Drawer
      ref={ref}
      disabled={disableDrwer}
      type="static"
      onOpenStart={() => setisOpen(true)}
      onCloseStart={() => setisOpen(false)}
      side="right"
      openDrawerOffset={0.2}
      panOpenMask={0.8}
      useInteractionManager
      tapToClose
      captureGestures="open"
      acceptDoubleTap
      content={
        <ControlPanel onToggle={onToggle} onSubmit={onSubmit} isOpen={isOpen} />
      }>
      {!disableDrwer && (
        <s.Button onPress={onToggle} style={{transform: [{rotateY: '180deg'}]}}>
          <RotateAnimation isToggle={isOpen}>
            <Icon name="leftArrow" />
          </RotateAnimation>
        </s.Button>
      )}
      {children}
    </Drawer>
  );
}
