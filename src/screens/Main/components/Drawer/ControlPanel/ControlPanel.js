import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import {useTheme} from 'styled-components';
import {
  branchesSelector,
  filterBySelector,
} from '../../../../../store/selectors/tripsSelectors';
import Text from '../../../../../components/common/Text/Text';
import s from './styles';
import {FlatList} from 'react-native-gesture-handler';

export default function ControlPanel({onToggle, onSubmit, isOpen}) {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const branches = useSelector((state) => branchesSelector(state));
  const {branchCode} = useSelector((state) => filterBySelector(state));
  const [currentSelected, setCurrentSelected] = useState(null);

  useEffect(() => {
    setCurrentSelected(branchCode);
  }, [isOpen]);

  const handleSubmit = () => {
    const barnch = branches.find((b) => b.branchCode === currentSelected);
    if (barnch) onSubmit(barnch);
  };

  return (
    <s.Container>
      <Text fontSize="s18" fontFamily="Rubik-Medium" fontWeight="500">
        {t('filterBranches')}
      </Text>
      <s.ListContainer>
        <FlatList
          data={branches}
          renderItem={({item: {branchName, branchCode, isOperationGroup}}) => (
            <s.Item
              key={branchCode}
              isOperationGroup={Boolean(Number(isOperationGroup))}
              isSelected={currentSelected === branchCode}
              onPress={() => setCurrentSelected(branchCode)}>
              <Text>{branchName}</Text>
            </s.Item>
          )}
        />
      </s.ListContainer>
      <s.SubmitContainer>
        <s.Btn onPress={onToggle}>
          <Text
            fontSize="s14"
            fontFamily="Rubik-Medium"
            fontWeight="500"
            color={colors.darkGray}>
            {t('cancel')}
          </Text>
        </s.Btn>
        <s.Btn onPress={handleSubmit}>
          <Text
            fontSize="s14"
            fontFamily="Rubik-Medium"
            fontWeight="500"
            color={colors.blueLight}>
            {t('ok')}
          </Text>
        </s.Btn>
      </s.SubmitContainer>
    </s.Container>
  );
}
