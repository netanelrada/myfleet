import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
  width: 100%;
  height: 100%;
  align-items: center;
  padding-top: ${calcHeight(20)}px;
`;

styles.ListContainer = styled.View`
  height: 85%;
`;

styles.Item = styled.TouchableOpacity`
  border: 1px solid
    ${({theme, isSelected}) =>
      isSelected ? theme.colors.blueLight : theme.colors.borderColorInput};
  ${({isOperationGroup, theme}) =>
    isOperationGroup &&
    `background-color: ${theme.colors.backgroundColorOnPress};`}
  ${({isSelected, theme}) =>
    isSelected &&
    `background-color: ${theme.colors.white}; box-shadow: 10px 0px 20px rgba(163, 163, 163, 0.25);`}
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  width: ${calcWidth(262)}px;
  height: ${calcWidth(28)}px;
  margin-top: 12px;
`;

styles.SubmitContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: auto;
`;

styles.Btn = styled.TouchableOpacity`
  width: ${calcWidth(65)}px;
  height: ${calcHeight(70)}px;
  justify-content: center;
  align-items: center;
`;
export default styles;
