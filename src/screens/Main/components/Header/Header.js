import React from 'react';
import s from './styles';
import MainHeader from './components/MainHeader/MainHeader';

const Header = () => {
	return (
		<s.Container>
			<MainHeader />
		</s.Container>
	);
};

export default Header;
