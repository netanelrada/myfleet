import styled from 'styled-components';
const styles = {};

styles.Container = styled.View`
	width: 100%;
	flex: 0.12;
	align-items: center;
	background-color: ${({ theme }) => theme.colors.ghostWhite};
`;

export default styles;
