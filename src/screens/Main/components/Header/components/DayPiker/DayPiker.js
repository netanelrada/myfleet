import React, {useState} from 'react';
import moment from 'moment';
import styled, {useTheme} from 'styled-components';
import {useSelector, useDispatch} from 'react-redux';
import {setDate} from '../../../../../../store/actions/actionTrips';
import {dateSelector} from '../../../../../../store/selectors/tripsSelectors';
import CalendarDays from '../../../../../../components/CalendarDays/CalendarDays';
const initStateDate = (dateString) => {
  const [selectedDay, month] = dateString.split('.');
  return {
    selectedDay: Number(selectedDay),
    month: Number(month),
  };
};
const DayPicker = () => {
  const dispatch = useDispatch();
  const onSetDate = (payload) => dispatch(setDate(payload));
  const date = useSelector((state) => dateSelector(state));
  //const [dateState, setdateState] = useState(initStateDate(date));
  const {selectedDay, month} = initStateDate(date);

  const onSetDay = (day) => {
    const format = 'D.M.YYYYY';
    const selectedDate = moment(date, format).set('D', day);

    onSetDate(selectedDate);
  };
  return (
    <s.Container>
      <CalendarDays
        month={month}
        selectedDay={selectedDay}
        onSetDay={onSetDay}
      />
    </s.Container>
  );
};

const s = {
  Container: styled.View`
    align-items: center;
    justify-content: center;
    width: 100%;
  `,
};

export default DayPicker;
