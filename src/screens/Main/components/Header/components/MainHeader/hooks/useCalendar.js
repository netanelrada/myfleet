import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setDate } from '../../../../../../../store/actions/actionTrips';
import { dateSelector } from '../../../../../../../store/selectors/tripsSelectors';

const useCalendar = () => {
	const [isVisable, setIsVisable] = useState(false);
	const dispatch = useDispatch();
	const onSetDate = payload => dispatch(setDate(payload));
	const date = useSelector(state => dateSelector(state));

	const closeCalender = () => setIsVisable(false);
	const openCalender = () => setIsVisable(true);

	return { isVisable, closeCalender, openCalender, onSetDate, date };
};

export default useCalendar;
