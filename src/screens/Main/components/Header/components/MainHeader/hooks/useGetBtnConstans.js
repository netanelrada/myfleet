//import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useTheme } from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { toggleIsEditMode } from '../../../../../../../store/actions/actionTrips';
import {
	isEditModeSelector,
	countFilterSelector,
} from '../../../../../../../store/selectors/tripsSelectors';
import { calcWidth, calcHeight } from '../../../../../../../utils/dimensions';

const useGetBtnConstans = () => {
	const { navigate } = useNavigation();
	const { colors } = useTheme();
	const dispathc = useDispatch();
	const onToggleIsEditMode = () => dispathc(toggleIsEditMode());
	const isEditMode = useSelector(state => isEditModeSelector(state));
	const countFilter = useSelector(state => countFilterSelector(state));

	return [
		{
			key: 'settings',
			iconName: 'settings',
			iconStyle: { width: calcWidth(20), height: calcHeight(20) },
			action: () => navigate('Settings'),
			style: {
				width: calcWidth(40),
				height: calcHeight(37),
				backgroundColor: colors.blueLight,
				borderRadius: 9,
			},
		},
		{
			key: 'edit',
			iconName: isEditMode ? 'blackEdit' : 'edit',
			iconStyle: { width: calcWidth(16), height: calcHeight(20) },
			action: onToggleIsEditMode, // dispatc edit mode,
			style: {
				width: calcWidth(40),
				height: calcHeight(37),
				backgroundColor: isEditMode ? colors.white : colors.blueLight,
				borderRadius: 9,
				borderWidth: isEditMode ? 2 : null,
				borderColor: colors.blueLight,
			},
		},
		{
			key: 'filter',
			iconName: 'filter',
			iconStyle: { width: calcWidth(24), height: calcHeight(24) },
			action: () => navigate('FilterTrips'),
			style: {
				width: calcWidth(40),
				height: calcHeight(37),
				backgroundColor: colors.blueLight,
				borderRadius: 9,
			},
			count: countFilter,
		},
	];
};

export default useGetBtnConstans;
