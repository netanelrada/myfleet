import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: 100%;

	padding-right: ${calcWidth(28)}px;
	padding-left: ${calcWidth(14)}px;
`;
styles.BodyContainer = styled.View`
	width: 100%;
	justify-content: space-between;
	flex-direction: row;
`;

styles.BtnsContainer = styled.View`
	flex-direction: row;
	justify-content: space-around;
	width: 50%;
`;

styles.CalenderContainer = styled.TouchableOpacity``;

// View ===> Image
styles.Image = styled.Image`
	align-self: flex-end;
	height: ${calcHeight(25)}px;
	aspect-ratio: ${88 / 25};
`;

export default styles;
