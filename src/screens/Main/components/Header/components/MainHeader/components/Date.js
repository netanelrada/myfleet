import React from 'react';
import styled, { useTheme } from 'styled-components';
import { useSelector } from 'react-redux';
import { dateSelector } from '../../../../../../../store/selectors/tripsSelectors';
import Text from '../../../../../../../components/common/Text/Text';
import Icon from '../../../../../../../components/common/Icon/Icon';
import { calcWidth, calcHeight } from '../../../../../../../utils/dimensions';

const Date = () => {
	const theme = useTheme();
	const dateSelected = useSelector(state => dateSelector(state));
	const size = {
		width: calcWidth(15),
		height: calcHeight(11),
	};
	return (
		<s.Container>
			<s.Margin>
				<Icon name='downArrow' {...size} />
			</s.Margin>
			<Text
				fontSize='s24'
				fontFamily='Rubik-Regular'
				color={theme.colors.colorText}
			>
				{dateSelected}
			</Text>
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		flex: 1;
		flex-direction: row;
		align-items: center;
	`,
	Margin: styled.View`
		margin-horizontal: 4px;
	`,
};

export default Date;
