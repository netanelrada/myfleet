import { typeOrderByE } from '../../../../../../../../utils/enums';

export const actions = {
	SET_ORDER_BY: 'SET_ORDER_BY',
	SET_DRIVER: 'SET_DRIVER',
	SET_CAR: 'SET_CAR',
	CLEAR: 'CLEAR',
};

export const initialState = {
	orderBy: typeOrderByE.HOUR,
	driver: '',
	car: '',
};

export const getTypeOrderByKey = key => {
	const objTypeOrderBy = {
		hour: typeOrderByE.HOUR,
		client: typeOrderByE.CLIENT,
		driver: typeOrderByE.DRIVER,
		lineDescription: typeOrderByE.LINE_DESCRIPTION,
	};
	return objTypeOrderBy[key];
};
export function reducer(state, action) {
	switch (action.type) {
		case actions.SET_ORDER_BY:
			return { ...state, orderBy: action.payload };
		case actions.SET_DRIVER:
			return { ...state, driver: action.payload };
		case actions.SET_CAR:
			return { ...state, car: action.payload };
		case actions.CLEAR:
			return initialState;
		default:
			throw new Error();
	}
}
