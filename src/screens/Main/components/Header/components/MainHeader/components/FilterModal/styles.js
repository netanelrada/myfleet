import styled from 'styled-components';
import {
	calcHeight,
	calcWidth,
} from '../../../../../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	height: 100%;
	width: 100%;
`;

styles.OverlayContainer = styled.TouchableOpacity`
	flex: 0.15;
	background-color: rgba(0, 0, 0, 0.4);
`;

styles.FormContainer = styled.View`
	flex: 0.85;
	width: 100%;
	background-color: white;
`;

styles.BoadyFormContainer = styled.View`
	justify-content: space-around;
	padding-horizontal: ${calcWidth(10)}px;
	min-height: 35%;
`;

styles.Btn = styled.TouchableOpacity`
	width: 100%;
	height: ${calcHeight(44)}px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	justify-content: center;
	align-items: center;
	position: absolute;
	bottom: 0;
`;

styles.SortContainer = styled.View`
	flex-direction: row-reverse;
	padding-horizontal: ${calcWidth(5)}px;
	align-items: center;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-weight: 500;
	font-size: ${({ theme }) => theme.fontSizes.s16}px;
	margin-left: ${calcWidth(5)}px;
`;

styles.OptionSortContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-around;
	flex: 1;
`;

styles.FilterContainer = styled.View``;
styles.FilterHeaderContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: space-between;
	align-items: center;
	padding-horizontal: ${calcWidth(5)}px;
`;

styles.InputsContinaer = styled.View`
	align-items: center;
	margin-top: 5px;
`;

styles.ClearSelected = styled.Text`
	font-family: Rubik-Regular;
	font-weight: 400;
	color: ${({ theme }) => theme.colors.darkGray};
`;

styles.TouchableOpacity = styled.TouchableOpacity``;

export default styles;
