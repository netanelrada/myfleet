import React, {useReducer} from 'react';
import {Modal} from 'react-native';
import s from './styles';
import {useDispatch} from 'react-redux';
import {PlatfromOS} from '../../../../../../../../utils/utilis';
import RadioButton from '../../../../../../../../components/common/RadioButton/RadioButton';
import SelectOption from '../../../../../../../../components/common/SelectOption/SelectOption';
import Text from '../../../../../../../../components/common/Text/Text';
import {useTranslation} from 'react-i18next';
import useGetConstans from './hooks/useGetConstans';
import Input from '../../../../../../../../components/common/InputIcon/Input';
import {initialState, reducer, actions, getTypeOrderByKey} from './reducer';
import {typeOrderByE} from '../../../../../../../../utils/enums';
import {setFilterBy} from '../../../../../../../../store/actions/actionTrips';

const FilterModal = ({isVisable, closeModal}) => {
  const {t} = useTranslation();
  const formData = useGetConstans();
  const [formState, dispatchFormData] = useReducer(reducer, initialState);
  const dispatch = useDispatch();
  const setFilterbyData = (payload) => dispatch(setFilterBy(payload));

  const obSubmit = () => {
    closeFilterModal();
    setFilterbyData(formState);
  };

  const onChangeText = (t, label) => {
    switch (label) {
      case 'driver':
        dispatchFormData({
          type: actions.SET_DRIVER,
          payload: t,
        });
        break;
      case 'car':
        dispatchFormData({
          type: actions.SET_CAR,
          payload: t,
        });
        break;
      default:
        break;
    }
  };

  const clearForm = () => {
    dispatchFormData({
      type: actions.CLEAR,
    });
  };

  const closeFilterModal = () => {
    closeModal();
  };

  const onSetOrderBy = (key) => {
    dispatchFormData({
      type: actions.SET_ORDER_BY,
      payload: getTypeOrderByKey(key),
    });
  };

  return (
    <Modal visible={isVisable} animationType="slide" transparent>
      <s.Container>
        <s.OverlayContainer onPress={closeFilterModal} />
        <s.FormContainer>
          <s.BoadyFormContainer>
            <s.SortContainer>
              <s.Label>{`${formData.orderBy.label}: `}</s.Label>
              <s.OptionSortContainer>
                <SelectOption updateSelectedOption={(key) => onSetOrderBy(key)}>
                  {formData.orderBy.options.map(({key, label}) => (
                    <RadioButton key={key} label={label} />
                  ))}
                </SelectOption>
              </s.OptionSortContainer>
            </s.SortContainer>
            <s.FilterContainer>
              <s.FilterHeaderContainer>
                <s.Label>{`${formData.filterBy.label}: `}</s.Label>
                <s.TouchableOpacity onPress={clearForm}>
                  <s.ClearSelected>{t('clearSelect')}</s.ClearSelected>
                </s.TouchableOpacity>
              </s.FilterHeaderContainer>
              <s.InputsContinaer>
                {formData.filterBy.options.map(({key, label}) => (
                  <Input
                    key={key}
                    label={label}
                    value={formState[key]}
                    onChangeText={(t) => onChangeText(t, key)}
                  />
                ))}
              </s.InputsContinaer>
            </s.FilterContainer>
          </s.BoadyFormContainer>
          <s.Btn onPress={obSubmit}>
            <Text fontSize="s16" fontWeight={500} color="white">
              {t('ok')}
            </Text>
          </s.Btn>
        </s.FormContainer>
      </s.Container>
    </Modal>
  );
};

export default FilterModal;
