import React from 'react';
import { useTranslation } from 'react-i18next';

const useGetConstans = () => {
	const { t } = useTranslation();
	const formData = {
		orderBy: {
			label: t('orderBy'),
			options: [
				{ key: 'startTime', label: t('hour') },
				{ key: 'client', label: t('client') },
				{ key: 'driver', label: t('driver') },
				{ key: 'lineDescription', label: t('lineDescription') },
			],
		},
		filterBy: {
			label: t('filterBy'),
			options: [
				{ key: 'driver', label: t('driver') },
				{ key: 'car', label: t('car') },
			],
		},
	};

	return formData;
};

export default useGetConstans;
