import React, { useState } from 'react';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { userSelector } from '../../../../../../store/selectors/loginSelectors';
import s from './styles';
import Icon from '../../../../../../components/common/Icon/Icon';
import Button from '../../../../../../components/common/Buttons/Button';
import useGetBtnConstans from './hooks/useGetBtnConstans';
import Date from './components/Date';
import MyCalendar from '../../../../../../components/MyCalendar/MyCalendar';
import useCalendar from './hooks/useCalendar';
import FilterModal from './components/FilterModal/FilterModal';
import CountWrap from '../../../../../../components/common/CountWrap/CountWrap';
import { images } from '../../../../../../assets/index';

const MainHeader = () => {
	const BtnConstans = useGetBtnConstans();
	const { companyLogo = null } =
		useSelector(state => userSelector(state)) || {};
	const {
		isVisable,
		closeCalender,
		openCalender,
		onSetDate,
		date,
	} = useCalendar();

	return (
		<s.Container>
			<s.Image source={{ uri: companyLogo }} resizeMode='cover' />

			<s.BodyContainer>
				<s.BtnsContainer>
					{BtnConstans.map(
						({ key, iconName, action, style, count = 0, iconStyle }) => (
							<CountWrap key={key} count={count}>
								<Button
									onPress={action}
									{...style}
									content={<Icon name={iconName} {...iconStyle} />}
								/>
							</CountWrap>
						)
					)}
				</s.BtnsContainer>
				<s.CalenderContainer onPress={openCalender}>
					<Date />
				</s.CalenderContainer>
			</s.BodyContainer>

			<MyCalendar
				isVisible={isVisable}
				closeCalender={closeCalender}
				setDate={onSetDate}
				initialDate={moment(date)}
			/>
		</s.Container>
	);
};

export default MainHeader;
