import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  FlatList,
  ActivityIndicator,
  BackHandler,
  Animated,
  StyleSheet,
  InteractionManager,
  View,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  filterTripsSelector,
  isEditModeSelector,
  tripsStatusSelector,
  allTripsSelector,
  newLineCodeSelector,
  editTripsSelector,
  dateSelector,
} from '../../../../store/selectors/tripsSelectors';
import {
  addEditTrip,
  removeEditTrip,
  setFilterTrips,
  setTabFoucs,
  setEditMode,
  addEditSingleTrip,
} from '../../../../store/actions/actionTrips';
import s from './styles';
import Filter from './components/Fliter/Filter';
import TripItem from './components/TripItem/TripItem';
import {tripsStatusE} from '../../../../utils/enums';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';
import CancelAllBtn from './components/CancelAllBtn/CancelAllBtn';
import {useNavigation} from '@react-navigation/native';
import Icon from '../../../../components/common/Icon/Icon';
import moment from 'moment';
import useBackHandler from '../../../../hooks/useBackHandler';
import Text from '../../../../components/common/Text/Text';
import ModalConfirm from '../../../../components/common/ModalConfirm/ModalConfirm';
import {useTranslation} from 'react-i18next';
import DayPicker from '../Header/components/DayPiker/DayPiker';
import useAnimatedHader, {HEADER_HEIGHT} from './hooks/useAnimatedHader';
import Header from './components/Header/Header';

const TripList = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {navigate} = useNavigation();
  const onAddEditTrip = (payload) => dispatch(addEditTrip(payload));
  const onRemoveEditTrip = (payload) => dispatch(removeEditTrip(payload));
  const onSetFilterTrips = (payload) => dispatch(setFilterTrips(payload));
  const onSetTabFoucs = (payload) => dispatch(setTabFoucs(payload));
  const onAddEditSingleTrip = (payload) => dispatch(addEditSingleTrip(payload));
  const onSetEditMode = (payload) => dispatch(setEditMode(payload));
  const allTrips = useSelector((state) => allTripsSelector(state));
  const filterTrips = useSelector((state) => filterTripsSelector(state));
  const isEditMode = useSelector((state) => isEditModeSelector(state));
  const tripsStatus = useSelector((state) => tripsStatusSelector(state));
  const newLineCode = useSelector((state) => newLineCodeSelector(state));
  const date = useSelector((state) => dateSelector(state));
  const editTrips = useSelector((state) => editTripsSelector(state)); // it will make the componnent to re render for checkbox
  const [isModalVisable, setIsModalVisable] = useState(false);
  const [isOnScrollEndDrag, setIsOnScrollEndDrag] = useState(false);
  const flatListRef = useRef(null);
  const scrollY = useRef(new Animated.Value(0)).current;

  useBackHandler(() => {
    if (isEditMode) {
      onSetEditMode(false);
      return true;
    } else {
      setIsModalVisable(true);
      return true;
    }
  });

  useEffect(() => {
    setIsOnScrollEndDrag(false);
  }, [date, tripsStatus]);

  useEffect(() => {
    if (newLineCode) focusByLineCode();
  }, [newLineCode]);

  useEffect(() => {
    if (tripsStatus === tripsStatusE.SUCCESS) {
      focusByCurrTime();
    }
  }, [tripsStatus]);

  const focusByLineCode = () => {
    const indexLine = filterTrips.findIndex(
      ({lineCode = ''}) => lineCode == newLineCode,
    );

    if (indexLine !== -1) {
      const scrollToData = {animated: false, index: indexLine};
      flatListRef.current.scrollToIndex(scrollToData);
    }
  };

  const focusByCurrTime = () => {
    const now = moment();
    const indexLine = filterTrips.findIndex(({startTime = ''}) => {
      const [_, sTime] = startTime.split(' ');
      const targetTime = moment(sTime, 'hh:mm');
      const diff = targetTime.diff(now, 'minutes');
      return diff > 0;
    });

    if (indexLine !== -1) {
      const scrollToData = {animated: true, index: indexLine};
      //	setInitialNumToRender(indexLine);
      flatListRef.current.scrollToIndex(scrollToData);
    }
  };

  const onFilterTrips = (textFilter) => {
    if (textFilter) {
      onSetFilterTrips(
        allTrips.filter(({clientName, lineDescription, driverName}) => {
          return [
            clientName.toLowerCase(),
            lineDescription.toLowerCase(),
            driverName.toLowerCase(),
          ].some((str) => str.includes(textFilter.toLowerCase()));
        }),
      );
    } else {
      onSetFilterTrips(allTrips);
    }
  };

  const onPressTrip = (trip) => {
    onSetTabFoucs({key: 'details', clickOnTab: true});
    onAddEditSingleTrip(trip);
    navigate('Home', {
      screen: 'SingleTrip',
      params: {trip},
    });
  };
  const onLongPressTrip = (trip) => {
    onSetEditMode(true);
    onAddEditTrip(trip);
  };

  const curryOnAddEditTrip = useCallback(
    (item) => () => onAddEditTrip(item),
    [],
  );
  const curryOnPressTrip = useCallback((item) => () => onPressTrip(item), []);
  const curryOnLongPressTrip = useCallback(
    (item) => () => onLongPressTrip(item),
    [],
  );
  const curryOnRemoveEditTrip = useCallback(
    (lineCode) => () => onRemoveEditTrip(lineCode),
    [],
  );

  const renderItem = useCallback(
    ({item: {isSelectedToEdit, ...item}, index}) => (
      <TripItem
        trip={item}
        isEdit={isEditMode}
        addEditTrip={curryOnAddEditTrip(item)}
        onPressTrip={curryOnPressTrip(item)}
        onLongPressTrip={curryOnLongPressTrip(item)}
        removeEditTrip={curryOnRemoveEditTrip(item.lineCode)}
        date={moment(date, 'D.M.YYYY').format('YYYY-MM-DD')}
        isSelectedToEdit={isSelectedToEdit}
      />
    ),
    [isEditMode],
  );
  return (
    <s.Container>
      <Header
        scrollY={scrollY}
        onFilterTrips={onFilterTrips}
        isOnScrollEndDrag={isOnScrollEndDrag}
        numbersOfTrips={filterTrips.length}
      />
      {tripsStatus === tripsStatusE.LOADING ? (
        <s.ActivityIndicator>
          <ActivityIndicator size="large" color="#40A8E2" />
        </s.ActivityIndicator>
      ) : (
        <Animated.FlatList
          bounces={false}
          ref={flatListRef}
          style={{width: '100%'}}
          getItemLayout={(data, index) => ({
            length: calcHeight(100),
            offset: calcHeight(100) * index,
            index,
          })}
          removeClippedSubviews={true}
          contentContainerStyle={{
            alignItems: 'center',
            paddingBottom: calcHeight(55),
            paddingTop: HEADER_HEIGHT,
          }}
          data={filterTrips}
          keyExtractor={(item, index) => item.lineCode.toString()}
          renderItem={renderItem}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {y: scrollY},
                },
              },
            ],
            {useNativeDriver: true},
          )}
          onScrollEndDrag={() => {
            !isOnScrollEndDrag && setIsOnScrollEndDrag(true);
          }}
        />
      )}
      <s.CancelAllBtnContainer>
        <CancelAllBtn />
      </s.CancelAllBtnContainer>
      {!isEditMode && (
        <s.PlusBtnContainer onPress={() => navigate('NewTripStack')}>
          <Icon name="whitePlus" />
        </s.PlusBtnContainer>
      )}

      <ModalConfirm
        isVisible={isModalVisable}
        iconName="alert"
        onCancel={() => setIsModalVisable(false)}
        onConfirm={() => BackHandler.exitApp()}
        textConfirnBtn={t('ok')}
        textCancelBtn={t('cancel')}
        iconStyle={{
          width: calcWidth(28),
          height: calcWidth(28),
        }}>
        <Text
          textAlign="center"
          fontSize="s18"
          fontFamily="Rubik-Medium"
          fontWeight="500">
          {t('exit')}
        </Text>
        <Text>{t('confirmExitApp')}</Text>
      </ModalConfirm>
    </s.Container>
  );
};

export default TripList;
