import { useState, useRef } from 'react';
import { Animated } from 'react-native';
import { calcHeight } from '../../../../../utils/dimensions';
export const HEADER_HEIGHT = calcHeight(120);
const interpolateObj = {
	inputRange: [0, 1],
	outputRange: [0, 1],
	extrapolateLeft: 'clamp',
};
const useAnimatedHader = () => {
	const scrollAnim = useRef(new Animated.Value(0));
	const offsetAnim = useRef(new Animated.Value(0));
	const [clampedScroll, setClampedScroll] = useState(
		Animated.diffClamp(
			Animated.add(
				scrollAnim.current.interpolate({
					...interpolateObj,
				}),
				offsetAnim.current
			),
			0,
			1
		)
	);

	const navbarTranslate = clampedScroll.interpolate({
		inputRange: [0, HEADER_HEIGHT],
		outputRange: [0, -HEADER_HEIGHT],
		extrapolate: 'clamp',
	});

	return {
		scrollAnim,
		offsetAnim,
		clampedScroll,
		setClampedScroll,
		navbarTranslate,
	};
};

export default useAnimatedHader;
