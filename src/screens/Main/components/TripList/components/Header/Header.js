import React, { useEffect, useState } from 'react';
import { Animated, StyleSheet, View } from 'react-native';
import DayPicker from '../../../Header/components/DayPiker/DayPiker';
import Filter from '../Fliter/Filter';
import { HEADER_HEIGHT } from '../../hooks/useAnimatedHader';
import Text from '../../../../../../components/common/Text/Text';
import { useTranslation } from 'react-i18next';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';
import { useTheme } from 'styled-components';

const Header = ({
	scrollY,
	onFilterTrips,
	isOnScrollEndDrag = false,
	numbersOfTrips = 0,
}) => {
	const [outputRange, setOutputRange] = useState([0, 0]);
	const [diffClamp, setDiffClamp] = useState(
		Animated.diffClamp(scrollY, 0, HEADER_HEIGHT)
	);

	const { t } = useTranslation();
	const { colors } = useTheme();

	useEffect(() => {
		if (isOnScrollEndDrag) {
			setOutputRange([0, -HEADER_HEIGHT]);
		} else {
			setOutputRange([0, 0]);
		}
		setDiffClamp(Animated.diffClamp(scrollY, 0, HEADER_HEIGHT));
		return () => setOutputRange([0, 0]);
	}, [isOnScrollEndDrag]);

	return (
		<Animated.View
			style={[
				styles.header,
				{
					transform: [
						{
							translateY: diffClamp?.interpolate({
								inputRange: [0, HEADER_HEIGHT],
								outputRange,
								extrapolate: 'clamp',
							}),
						},
					],
				},
			]}
		>
			<DayPicker />
			<Filter onFilterTrips={onFilterTrips} />
			<View style={{ width: calcWidth(332), marginBottom: 5 }}>
				<Text color={colors.darkGray} textAlign='right'>{`${t(
					'tripsDisplay'
				)} (${numbersOfTrips})`}</Text>
			</View>
		</Animated.View>
	);
};

const styles = StyleSheet.create({
	header: {
		height: HEADER_HEIGHT,
		backgroundColor: '#F9F8F8',
		justifyContent: 'space-around',
		alignItems: 'center',
		position: 'absolute',
		top: 0,
		left: 0,
		zIndex: 5,
	},
});

export default Header;
