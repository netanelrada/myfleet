import React from 'react';
import styled from 'styled-components';
import Text from '../../../../../../../../components/common/Text/Text';
import { calcWidth } from '../../../../../../../../utils/dimensions';
import { statusWork } from '../../../../../../../../utils/enums';

const WorkStatus = ({ status }) => {
	let statusElement = null;

	switch (status) {
		case statusWork.NOT_WORK:
			statusElement = <s.Red />;
			break;
		case statusWork.WORK:
			statusElement = <s.Green />;
		default:
			break;
	}

	return statusElement;
};

const Driver = ({ driverName, workStatus = false, label }) => {
	return (
		<>
			{!!driverName && (
				<s.Container>
					{/* <Text fontWeight={500}>{`${driverName}:`}</Text> */}
					<s.Row>
						<WorkStatus status={workStatus} />
						<Text fontWeight={500}>{`${label}:`}</Text>
					</s.Row>
					<s.Text>{driverName}</s.Text>
				</s.Container>
			)}
		</>
	);
};

const s = {
	Container: styled.View`
		flex-direction: row-reverse;

		align-items: center;
	`,
	Row: styled.View`
		flex-direction: row-reverse;
		align-items: center;
	`,

	Red: styled.View`
		width: ${calcWidth(8)}px;
		aspect-ratio: 1;
		background-color: red;
		border-radius: ${calcWidth(4)}px;
		margin: 2px;
	`,
	Green: styled.View`
		width: ${calcWidth(8)}px;
		aspect-ratio: 1;
		background-color: ${({ theme }) => theme.colors.green};
		border-radius: ${calcWidth(4)}px;
		margin: 2px;
	`,
	Text: styled.Text`
		font-weight: 500;
		font-size: ${({ theme }) => theme.fontSizes.s14}px;
		font-family: Rubik-Regular;
		overflow: hidden;
		text-align: center;
	`,
};

export default Driver;
