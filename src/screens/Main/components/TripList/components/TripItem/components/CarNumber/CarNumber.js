import React from 'react';
import styled from 'styled-components';
import Text from '../../../../../../../../components/common/Text/Text';

const CarNumber = ({ carNumber }) => {
	return (
		<>
			{!!carNumber && (
				<s.Container>
					<Text fontWeight={500}>{carNumber}</Text>
				</s.Container>
			)}
		</>
	);
};

const s = {
	Container: styled.View`
		align-items: flex-end;
	`,
};
export default CarNumber;
