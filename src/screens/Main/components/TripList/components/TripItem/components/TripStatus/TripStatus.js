import React from 'react';
import styled from 'styled-components';
import Icon from '../../../../../../../../components/common/Icon/Icon';
import { calcWidth } from '../../../../../../../../utils/dimensions';
import { statusTrip } from '../../../../../../../../utils/enums';

const TripStatus = ({ status }) => {
	let iconName = null;
	const style = {
		width: calcWidth(20),
		height: calcWidth(16),
	};
	switch (status) {
		case statusTrip.UNDEFINED:
			iconName = null;
			break;
		case statusTrip.SEND:
			iconName = 'sended';
			break;
		case statusTrip.ACCEPT:
			iconName = 'accept';
			break;
		case statusTrip.START:
			iconName = 'on_drive';
			break;
		case statusTrip.END:
			iconName = 'ended';
			break;
		case statusTrip.REJECT:
			iconName = 'rejected';
			break;
		default:
			iconName = null;
			break;
	}

	return (
		<>
			{iconName && (
				<s.Container>
					<Icon name={iconName} {...style} />
				</s.Container>
			)}
		</>
	);
};

const s = {
	Container: styled.View`
		background-color: ${({ theme }) => theme.colors.white};
		width: ${calcWidth(40)}px;
		justify-content: center;
		align-items: center;
		aspect-ratio: 1;
		border-radius: ${calcWidth(17.5)}px;
		border: 1px solid ${({ theme }) => theme.colors.darkGray3};
	`,
};

export default TripStatus;
