import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../../../../../utils/dimensions';
const styles = {};

const marginBottom = calcHeight(16);

styles.Container = styled.Pressable`
  width: ${calcWidth(332)}px;
  height: ${({height = 90}) => calcHeight(height)}px;
  background-color: ${({theme, isWithoutDriver}) =>
    isWithoutDriver ? theme.colors.backgroundColorOnPress : theme.colors.white};
  flex-direction: row;
  border-radius: 9px;
  margin-bottom: ${marginBottom}px;
  box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);
  elevation: 6;
  /* ${({isDayAfter}) => isDayAfter && 'border: 1px solid black'}
	${({isSelected}) => isSelected && 'opacity: 0.6'} */
`;

//theme.colors.backgroundColorOnPress
styles.View = styled.View``;

styles.VerticelLine = styled.View`
  width: 1px;
  height: 100%;
  background-color: ${({theme}) => theme.colors.darkGray1};
`;

styles.DetilesContainer = styled.View`
  flex: 0.8;
  align-items: flex-end;
  padding-vertical: ${calcHeight(7)}px;
  padding-right: ${calcWidth(7)}px;
  padding-left: ${calcWidth(13)}px;
  justify-content: space-around;
`;

styles.SubDetilesContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  flex-wrap: wrap-reverse;
  align-items: flex-start;
`;

styles.TimeContainer = styled.View`
  flex: 0.25;
  align-items: center;
  justify-content: space-around;
`;

styles.Start = styled.View`
  width: ${calcWidth(54)}px;
  aspect-ratio: ${54 / 38};
  justify-content: center;
  align-items: center;
  background-color: ${({theme}) => theme.colors.darkGray1};
  border-radius: 9px;
`;

styles.StatusContainer = styled.View`
  position: absolute;
  top: ${-calcWidth(16)}px;
  left: ${-calcWidth(10)}px;
  z-index: 999;
`;

styles.CheckBoxContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex: 0.1;
`;

export default styles;
