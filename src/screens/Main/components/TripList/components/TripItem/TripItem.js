import React, {useState, useEffect, memo} from 'react';
import {StyleSheet, Platform, InteractionManager, View} from 'react-native';
import {useTheme} from 'styled-components';
import {useTranslation} from 'react-i18next';
import CheckBox from '@react-native-community/checkbox';
import moment from 'moment';
import {useSelector} from 'react-redux';
import {fontSizeSelector} from '../../../../../../store/selectors/settingsSelectors';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';
import Driver from './components/Driver/Driver';
import CarNumber from './components/CarNumber/CarNumber';
import TripStatus from './components/TripStatus/TripStatus';
import {calcWidth, calcHeight} from '../../../../../../utils/dimensions';
import {isEqual} from 'lodash';
import {fontSizesE} from '../../../../../../utils/enums';

const TripItem = ({
  trip,
  isEdit = false,
  addEditTrip,
  removeEditTrip,
  onPressTrip,
  onLongPressTrip,
  isSelectedToEdit = false,
  date = '',
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const fontSize = useSelector((state) => fontSizeSelector(state));
  //	const [toggleCheckBox, setToggleCheckBox] = useState(isSelectedToEdit);
  const [isSelected, setisSelected] = useState(false);

  const {
    lineCode,
    clientName,
    startTime,
    endTime,
    lineDescription,
    lineStatus,
    passQty,
    driverName,
    atWork,
    carNumber,
    isActive,
    firstStation,
    lastStation,
    // isSelectedToEdit,
  } = trip;
  const height = fontSize === fontSizesE.BIG ? 110 : 90;

  const onValueChange = () => {
    if (!isSelectedToEdit) addEditTrip();
    else removeEditTrip();
  };

  const [startDate, start] = startTime.split(' ');
  const [endDate, end] = endTime.split(' ');
  const selectedDate = date.split(' ')[0];
  //const isDayAfter = startDate !== selectedDate;
  const isWithoutDriver = !driverName;

  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(20)};

  const getIsOverlap = () => {
    const addDay = moment(startDate).add(-1, 'd').format('YYYY-MM-DD');

    return addDay === selectedDate;
  };
  return (
    <s.Container
      isSelected={!isEdit && isSelected}
      isWithoutDriver={isWithoutDriver}
      isDayAfter={getIsOverlap()}
      onPress={isEdit ? () => {} : onPressTrip}
      onLongPress={isEdit ? () => {} : onLongPressTrip}
      onPressIn={() => setisSelected(true)}
      onPressOut={() => setisSelected(false)}
      height={height}>
      <s.DetilesContainer>
        <Text
          numberOfLines={2}
          textAlign="right"
          fontSize="s16"
          fontWeight={700}>
          {lineDescription}
        </Text>
        <Text numberOfLines={2} textAlign="right" color={colors.darkGray2}>
          {clientName}
        </Text>
        <s.SubDetilesContainer>
          <Driver
            label={t('driver')}
            driverName={driverName}
            workStatus={atWork}
          />
          <CarNumber carNumber={carNumber} />
        </s.SubDetilesContainer>
      </s.DetilesContainer>
      <s.VerticelLine />
      <s.TimeContainer>
        <s.Start>
          <Text fontWeight={500}>{start}</Text>
        </s.Start>
        <Text fontSize="s12">{`${t('until')} ${end}`}</Text>
      </s.TimeContainer>
      {isEdit && (
        <s.CheckBoxContainer onPress={onValueChange}>
          <CheckBox
            disabled={false}
            style={[Platform.OS === 'ios' ? checkBoxStyle : null]}
            value={isSelectedToEdit}
            onValueChange={() =>
              isSelectedToEdit ? removeEditTrip() : addEditTrip()
            }
            onAnimationType="stroke"
          />
        </s.CheckBoxContainer>
      )}

      <s.StatusContainer>
        <TripStatus status={lineStatus} />
      </s.StatusContainer>
    </s.Container>
  );
};

function areEqual(prevProps, nextProps) {
  const isEqualRes = isEqual(prevProps, nextProps);
  return isEqualRes;
}
//export default memo(TripItem, areEqual);
export default TripItem;
