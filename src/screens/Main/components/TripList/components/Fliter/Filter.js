import React, {useState, useEffect} from 'react';
import {Platform} from 'react-native';
import {useTranslation} from 'react-i18next';
import CheckBox from '@react-native-community/checkbox';
import moment from 'moment';
import {useSelector, useDispatch} from 'react-redux';
import {
  selectAll,
  setDate,
  initGetTrips,
  cancelAll,
} from '../../../../../../store/actions/actionTrips';
import {
  dateSelector,
  isEditModeSelector,
  editTripsSelector,
  isHandleActionSelector,
} from '../../../../../../store/selectors/tripsSelectors';
import styled, {useTheme} from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';
import Button from '../../../../../../components/common/Buttons/Button';
import Icon from '../../../../../../components/common/Icon/Icon';
import Input from '../../../../../../components/common/Input/Input';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';
import useRefresh from '../../../../../../hooks/useRefresh';
import {debounce} from 'lodash';

const Filter = ({onFilterTrips}) => {
  const {colors} = useTheme();
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const onSelectAll = () => dispatch(selectAll());
  const onCancelAll = () => dispatch(cancelAll());
  const onSetDate = (payload) => dispatch(setDate(payload));
  const [inputValue, setInputValue] = useState('');
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const date = useSelector((state) => dateSelector(state));
  const isEditMode = useSelector((state) => isEditModeSelector(state));
  const editTrips = useSelector((state) => editTripsSelector(state));
  const isHandleAction = useSelector((state) => isHandleActionSelector(state));
  const {onRefresh} = useRefresh();

  useEffect(() => {
    setInputValue('');
  }, [date, isHandleAction]);

  useEffect(() => {
    setToggleCheckBox(false);
  }, [isEditMode]);

  useEffect(() => {
    editTrips.length === 0 && setToggleCheckBox(false);
  }, [editTrips.length]);

  const setCurrentDayAndRefresh = () => {
    const payload = moment().format('D.M.YYYYY');
    onSetDate(payload);
  };

  const onFilterTripsDebounce = debounce((text) => {
    onFilterTrips(text);
  }, 160);

  const inputStyle = {
    width: isEditMode ? 202 : 273,
    height: 35,
    backgroundColor: colors.grayLight,
    borderRadius: 4,
  };

  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(20)};

  const onChangeText = (text) => {
    setInputValue(text);
    onFilterTripsDebounce(text);
    //	onFilterTripsByClinetName(text);
  };

  const onValueChange = (newValue) => {
    if (newValue) onSelectAll();
    else onCancelAll();
    setToggleCheckBox((preState) => !preState);
  };

  const refresh = () => {
    onRefresh();
    setInputValue('');
  };

  return (
    <s.Container>
      <s.RefershBtn onPress={refresh} onLongPress={setCurrentDayAndRefresh}>
        <Icon name="refresh" />
      </s.RefershBtn>

      <Input
        value={inputValue}
        onChangeText={(text) => onChangeText(text)}
        iconName="search"
        placeholder={t('freeSearch')}
        {...inputStyle}
      />

      {isEditMode && (
        <s.CheckBoxContainer>
          <Text fontSize={i18n.language === 'ru' ? 's10' : 's14'}>
            {t('selectAll')}
          </Text>
          <CheckBox
            style={Platform.OS === 'ios' ? checkBoxStyle : null}
            value={toggleCheckBox}
            onValueChange={(newValue) => onValueChange(newValue)}
            onAnimationType="stroke"
          />
        </s.CheckBoxContainer>
      )}
    </s.Container>
  );
};

export default Filter;
