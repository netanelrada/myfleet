import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(332)}px;
	flex-direction: row;
	align-items: center;
	height: ${calcHeight(50)}px;
	justify-content: space-between;
`;

styles.CheckBoxContainer = styled.View`
	flex-direction: row;
	align-items: center;
	width: ${calcWidth(75)}px;
	justify-content: space-between;
`;

styles.RefershContainer = styled.View``;

styles.RefershBtn = styled.Pressable`
	width: ${calcWidth(45)}px;
	height: ${calcHeight(35)}px;
	background-color: ${({ theme }) => theme.colors.grayLight};
	border-radius: 9px;
	align-items: center;
	justify-content: center;
`;

styles.BoxShadow = styled.View`
	box-shadow: 0px 2px 5px #fff;
	elevation: 3;
`;

export default styles;
