import React from 'react';
import Button from '../../../../../../components/common/Buttons/Button';
import { useTranslation } from 'react-i18next';
import { useTheme } from 'styled-components';
import Text from '../../../../../../components/common/Text/Text';
import { useDispatch } from 'react-redux';
import { cancelAll } from '../../../../../../store/actions/actionTrips';
import useIsTabActionsVisable from '../../../../../../hooks/useIsTabActionsVisable';

const CancelAllBtn = () => {
	const { t } = useTranslation();
	const { colors, fontSizes } = useTheme();
	const isVisable = useIsTabActionsVisable();
	const dispatch = useDispatch();
	const onCancelAll = () => dispatch(cancelAll());

	const style = {
		width: 82,
		height: 44,
		backgroundColor: colors.blueLight,
		borderRadius: 4,
	};
	return (
		<>
			{isVisable && (
				<Button
					onPress={onCancelAll}
					content={<Text color={colors.white}>{t('cancelAll')}</Text>}
					{...style}
				/>
			)}
		</>
	);
};

export default CancelAllBtn;
