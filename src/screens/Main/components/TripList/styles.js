import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	flex: 0.9;
	width: 100%;
	align-items: center;
	background-color: ${({ theme }) => theme.colors.ghostWhite};
`;

styles.CancelAllBtnContainer = styled.View`
	position: absolute;
	bottom: ${calcHeight(80)}px;
	left: ${calcWidth(15)}px;
`;

styles.PlusBtnContainer = styled.TouchableOpacity`
	position: absolute;
	bottom: ${calcHeight(30)}px;
	width: ${calcWidth(52)}px;
	aspect-ratio: 1;
	left: ${calcWidth(15)}px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	border-radius: 17px;
	justify-content: center;
	align-items: center;
`;
styles.ActivityIndicator = styled.View`
	margin-top: ${calcHeight(130)}px;
`;

export default styles;
