import styled from 'styled-components';
import { calcHeight } from '../../utils/dimensions';
const styles = {};

styles.SafeAreaView = styled.SafeAreaView`
	flex: 1;
`;

styles.Container = styled.View`
	flex: 1;
	flex-direction: column-reverse;
	background-color: ${({ theme }) => theme.colors.ghostWhite};
`;

styles.TabContainer = styled.View`
	position: absolute;
	width: 100%;
	bottom: 0;
`;

export default styles;
