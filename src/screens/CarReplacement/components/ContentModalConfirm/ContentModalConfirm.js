import React from 'react';
import {useTranslation} from 'react-i18next';
import styled from 'styled-components';
import CheckBox from '@react-native-community/checkbox';
import Text from '../../../../components/common/Text/Text';
import {calcHeight, calcWidth} from '../../../../utils/dimensions';

const ContentModalConfirm = ({
  isSingleTrip = false,
  previousCar = '',
  newCar = '',
  removeText = '',
  checkBoxValue = true,
  onSetCheckboxValue = () => {},
}) => {
  const {t} = useTranslation();

  const checkBoxStyle = {height: calcWidth(20), width: calcWidth(15)};

  return (
    <s.Container>
      <Text fontSize="s18">{t('CarReplacement')}</Text>
      {isSingleTrip ? (
        <s.SingleTripContainer>
          {!!removeText ? (
            <Text fontSize="s16" fontWeight="400">
              {removeText}
            </Text>
          ) : (
            <>
              <s.Row>
                <s.Label>{t('previousCar')}: </s.Label>
                <Text fontSize="s16" fontWeight="400">
                  {previousCar}
                </Text>
              </s.Row>
              <s.Row>
                <s.Label>{t('newCar')}: </s.Label>
                <Text fontSize="s16" fontWeight="400">
                  {newCar}
                </Text>
              </s.Row>
            </>
          )}
        </s.SingleTripContainer>
      ) : !!removeText ? (
        <Text fontSize="s16" fontWeight="400">
          {removeText}
        </Text>
      ) : (
        <Text>{t('acceptCarReplacement')}</Text>
      )}
      <s.CheckBoxContainer>
        <CheckBox
          style={Platform.OS === 'ios' ? checkBoxStyle : null}
          value={checkBoxValue}
          onValueChange={(newValue) => onSetCheckboxValue(newValue)}
          onAnimationType="stroke"
        />
        <Text>{` ${t('includeUpdateDriver')}`}</Text>
      </s.CheckBoxContainer>
    </s.Container>
  );
};
const s = {
  Container: styled.View`
    align-items: center;
    height: 90%;
    justify-content: space-between;
    margin-top: 5px;
  `,
  CheckBoxContainer: styled.View`
    flex-direction: row-reverse;
    align-items: center;
    align-self: flex-end;
    margin-bottom: ${calcHeight(5)}px;
  `,
  SingleTripContainer: styled.View``,
  Row: styled.View`
    flex-direction: row-reverse;
  `,
  Label: styled.Text`
    font-family: Rubik-Regular;
    color: ${({theme}) => theme.colors.gray6};
    font-size: ${({theme}) => theme.fontSizes.s16};
    font-weight: 400;
  `,
};
export default ContentModalConfirm;
