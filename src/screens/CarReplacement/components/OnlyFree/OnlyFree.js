import React, { useState } from 'react';
import s from './styles';
import Text from '../../../../components/common/Text/Text';
import { useTranslation } from 'react-i18next';
import { useTheme } from 'styled-components';
import Switch from '../../../../components/common/Switch/Switch';

const OnlyFree = ({ toggleSwitch = () => {} }) => {
	const { t } = useTranslation();
	const [isEnabled, setisEnabled] = useState(false);

	const onToggleSwitch = newValue => {
		setisEnabled(!newValue);
		toggleSwitch(!newValue);
	};

	return (
		<s.Container>
			<Text>{t('onlyFreeCars')}</Text>
			<Switch
				isEnabled={isEnabled}
				toggleSwitch={newValue => onToggleSwitch(newValue)}
			/>
		</s.Container>
	);
};

export default OnlyFree;
