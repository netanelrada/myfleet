import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	position: absolute;
	bottom: 0;
	box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
	elevation: 3;
	width: 100%;
	height: ${calcHeight(44)}px;
	flex-direction: row-reverse;
	justify-content: space-between;
	align-items: center;
	background-color: ${({ theme }) => theme.colors.white};
	padding-horizontal: ${calcWidth(13)}px;
`;

styles.Switch = styled.Switch``;

export default styles;
