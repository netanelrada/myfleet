import React, { useState } from 'react';
import styled from 'styled-components';
import Tooltip from 'react-native-walkthrough-tooltip';
import Icon from '../../../../../components/common/Icon/Icon';
import { calcWidth, calcHeight } from '../../../../../utils/dimensions';
import Text from '../../../../../components/common/Text/Text';
import CountWrap from '../../../../../components/common/CountWrap/CountWrap';
import { useTranslation } from 'react-i18next';

const ToolTipContent = ({ dayRemarks, endOfAbsence }) => {
	const { t } = useTranslation();
	const [, endAbsence] = endOfAbsence.split(' ');
	return (
		<s.ToolTipContentContainer>
			{!!dayRemarks && <Text textAlign='center'>{dayRemarks}</Text>}
			<s.Hr />
			{!!endAbsence && (
				<Text textAlign='center'>{`${t('absenceTime')}: ${endAbsence}`}</Text>
			)}
		</s.ToolTipContentContainer>
	);
};

const Comments = ({ dayRemarks = '', absence = '', endOfAbsence = '' }) => {
	const [toolTipVisable, settoolTipVisable] = useState(false);

	const getCommentComponent = () => {
		if (absence !== '1' && !dayRemarks) return null;
		let CommentComponent = null;
		const iconName = absence == '1' ? 'OutOfService' : 'message';
		const isRemarksAndAbsence = absence === '1' && dayRemarks ? 2 : 0;

		CommentComponent = (
			<>
				<Tooltip
					isVisible={toolTipVisable}
					content={
						<ToolTipContent
							dayRemarks={dayRemarks}
							endOfAbsence={endOfAbsence}
						/>
					}
					placement='top'
					onClose={() => settoolTipVisable(false)}
				>
					<CountWrap
						width={20}
						isWithBorder={false}
						count={isRemarksAndAbsence}
					>
						<s.IconContainer onPress={() => settoolTipVisable(true)}>
							<Icon name={iconName} />
						</s.IconContainer>
					</CountWrap>
				</Tooltip>
			</>
		);

		return CommentComponent;
	};

	return <>{getCommentComponent()}</>;
};

const s = {
	ToolTipContentContainer: styled.View`
		justify-content: space-around;
	`,
	Hr: styled.View`
		border-bottom-color: rgba(217, 226, 233, 0.5);
		border-bottom-width: 1px;
	`,
	IconContainer: styled.TouchableOpacity`
		width: ${calcWidth(36)}px;
		aspect-ratio: 1;
		align-items: center;
		justify-content: center;
		border-radius: ${calcWidth(18)}px;
		background-color: ${({ theme }) => theme.colors.white};
		border: 1px solid rgba(180, 190, 201, 0.303017);
		box-shadow: 0px 5px 6px rgba(217, 226, 233, 0.5);
	`,
};
export default Comments;
