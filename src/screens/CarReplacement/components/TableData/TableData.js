import React from 'react';
import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';
import Text from '../../../../components/common/Text/Text';
import Comments from './Comments/Comments';

const TableData = ({
	arrStyle = [],
	data: {
		carId = '',
		carNumber = '',
		carType = '',
		freeFrom,
		absence,
		endOfAbsence = '2020-12-10 08:20',
		dayRemarks,
		freeNow,
	},
	onPress,
}) => {
	return (
		<s.Container onPress={onPress}>
			<s.Cell flex={arrStyle[0].flex}>
				<Text textAlign={arrStyle[0].textAlign}>{freeFrom}</Text>
			</s.Cell>
			<s.Cell flex={arrStyle[1].flex}>
				<Text textAlign={arrStyle[1].textAlign}>{carId}</Text>
			</s.Cell>
			<s.Cell flex={arrStyle[2].flex}>
				<Text
					color={freeNow == '0' ? '#EBB835' : null}
					textAlign={arrStyle[2].textAlign}
				>
					{carNumber}
				</Text>
				<Text textAlign={arrStyle[2].textAlign}>{carType}</Text>
			</s.Cell>
			<s.Cell align={arrStyle[3].textAlign} flex={arrStyle[3].flex}>
				<Comments
					absence={absence}
					endOfAbsence={endOfAbsence}
					dayRemarks={dayRemarks}
				/>
			</s.Cell>
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		width: 100%;
		height: ${calcHeight(52)}px;
		border-bottom-color: rgba(180, 190, 201, 0.3);
		border-bottom-width: 1px;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: flex-start;
	`,
	Cell: styled.View`
		flex: ${({ flex }) => flex};
		height: 70%;
		${({ align }) => `align-items: ${align};`};
	`,
	IconContainer: styled.TouchableOpacity`
		width: ${calcWidth(36)}px;
		aspect-ratio: 1;
		align-items: center;
		justify-content: center;
		border-radius: ${calcWidth(18)}px;
		background-color: ${({ theme }) => theme.colors.white};
		border: 1px solid rgba(180, 190, 201, 0.303017);
		box-shadow: 0px 5px 6px rgba(217, 226, 233, 0.5);
	`,
	Row: styled.TouchableOpacity``,
};

export default TableData;
