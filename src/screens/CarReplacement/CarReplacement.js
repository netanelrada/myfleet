import React, {useState, useRef} from 'react';
import {useSelector} from 'react-redux';
import s from './styles';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import Header from './components/Header/Header';
import LineDescription from './components/LineDescription/LineDescription';
import SerachList from '../../components/SerachList/SerachList';
import {useTranslation} from 'react-i18next';
import TableData from './components/TableData/TableData';
import {getCars, setLineCar} from '../../api/api';
import {
  dateSelector,
  editTripsSelector,
} from '../../store/selectors/tripsSelectors';
import useDataUserAPI from '../../hooks/useDataUserAPI';
import ModalConfirm from '../../components/common/ModalConfirm/ModalConfirm';
import ContentModalConfirm from './components/ContentModalConfirm/ContentModalConfirm';
import {calcWidth} from '../../utils/dimensions';
import OnlyFree from './components/OnlyFree/OnlyFree';
import useHandleResponse from '../../hooks/useHandleResponse';
import AlertsAndErrors from '../../components/AlertsAndErrors/AlertsAndErrors';
import useGetErrorMsg from '../../hooks/useGetErrorMsg';
import useErrorAlertsModals from '../../hooks/useErrorAlertsModals';
import useBackHandler from '../../hooks/useBackHandler';

const CarReplacement = ({navigation}) => {
  const {goBack} = navigation;
  useBackHandler(() => {
    goBack();
    return true;
  });

  const {t} = useTranslation();
  const {updateCarErrorMsg} = useGetErrorMsg();
  const [isModalVisable, setisModalVisable] = useState({
    visable: false,
    isRemove: false,
  });
  const [isUpdateDriver, setIsUpdateDriver] = useState(true);
  const {
    modalState,
    onCloseModal,
    setModalState,
    actionComplete,
  } = useErrorAlertsModals();
  const {
    filterData: carsData,
    updateLines,
    statusData,
    onFilterData,
  } = useDataUserAPI({
    promise: getCars,
  });
  const {handleResponse} = useHandleResponse({});
  const carSelectedRef = useRef(null);
  const editTrips = useSelector((state) => editTripsSelector(state));
  const singleTrip = editTrips.length === 1 ? editTrips[0] : false;
  const carNumber = singleTrip?.carNumber;

  const onPressCar = (car) => {
    carSelectedRef.current = car;
    validateUpdate();
  };

  const validateUpdate = async (isRemove = false) => {
    try {
      const {data = {}} = await updateLines({
        promise: setLineCar,
        payloadProp: {
          carCode: carSelectedRef.current.carCode,
          action: '0', // need to change
        },
      });
      const isComplete = actionComplete(data);

      if (!isComplete) {
        setModalState({
          isVisible: true,
          res: data,
        });
      } else setisModalVisable({isRemove, visable: true});
    } catch (error) {
      console.log(error);
    }
  };

  const onUpdateCar = async () => {
    try {
      const {data = {}} = await updateLines({
        promise: setLineCar,
        payloadProp: {
          carCode: carSelectedRef.current.carCode,
          updateDriver: isUpdateDriver ? '1' : '0',
          action: '2', // need to change
        },
      });
      closeModal();
      onCloseModal();
      handleResponse(data);
    } catch (error) {
      console.log(error);
    }
  };

  const onToggleSwitch = async (newValue) => {
    onFilterData(newValue);
  };

  const closeModal = () => {
    setisModalVisable((preState) => ({...preState, visable: false}));
  };

  const filter = ({carNumber = '', carId = ''}, text) =>
    [carNumber, carId].some((str) => str.includes(text));
  const tableHead = [
    t('freeFrom'),
    t('carCode'),
    t('carCodeAndCarType'),
    t('comments'),
  ];

  const arrStyle = [
    {textAlign: 'center', flex: 1},
    {textAlign: 'center', flex: 0.8},
    {textAlign: 'center', flex: 2},
    {textAlign: 'center', flex: 0.8},
  ];

  const removeCar = () => {
    const emptyData = {
      carCode: '',
    };
    carSelectedRef.current = emptyData;

    validateUpdate(true);
  };

  const getRemoveText = () => {
    if (!isModalVisable.isRemove) return '';
    if (singleTrip) {
      return t('removeCar');
    } else {
      return t('removeCars');
    }
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header
          onGoBack={goBack}
          onPressRemove={removeCar}
          deleteDisabled={!editTrips.some(({carNumber}) => !!carNumber)}
          leftText={carNumber}
          iconName="car"
          titleText={t('CarReplacement')}
        />

        {singleTrip && (
          <LineDescription lineDescription={singleTrip.lineDescription} />
        )}
        <SerachList
          tableHead={tableHead}
          tableData={carsData}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressCar}
          statusData={statusData}
          keyItem="carCode"
        />
        <ModalConfirm
          isVisible={isModalVisable.visable}
          iconName="car"
          onCancel={closeModal}
          onConfirm={onUpdateCar}
          textConfirnBtn={t('ok')}
          textCancelBtn={t('cancel')}
          iconStyle={{
            width: calcWidth(28),
            height: calcWidth(28),
          }}>
          <ContentModalConfirm
            isSingleTrip={!!singleTrip}
            previousCar={carNumber}
            newCar={carSelectedRef.current?.carNumber}
            removeText={getRemoveText()}
            checkBoxValue={isUpdateDriver}
            onSetCheckboxValue={setIsUpdateDriver}
          />
        </ModalConfirm>
        <AlertsAndErrors
          isVisible={modalState.isVisible}
          res={modalState.res}
          errorMessages={updateCarErrorMsg}
          closeModal={onCloseModal}
          onConfirm={onUpdateCar}
        />
      </s.Container>
      <OnlyFree toggleSwitch={onToggleSwitch} />
    </ImageBackground>
  );
};

export default CarReplacement;
