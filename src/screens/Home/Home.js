import React, { useCallback } from 'react';
import s from './styles';
import TripStack from '../../navigation/Stacks/TripStack';
import Tab from './components/Tab/Tab';

const Home = () => {
	return (
		<s.SafeAreaView>
			<s.Container>
				<TripStack />
			</s.Container>
		</s.SafeAreaView>
	);
};

export default Home;
