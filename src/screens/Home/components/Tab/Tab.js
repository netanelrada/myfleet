import React from 'react';
import useIsTabActionsVisable from '../../../../hooks/useIsTabActionsVisable';
import TabActions from '../../../../components/TabActions/TabActions';

const Tab = () => {
	const isTabActionVisable = useIsTabActionsVisable();

	return <TabActions isVisable={isTabActionVisable} />;
};

export default Tab;
