import React, {useEffect} from 'react';
import {ThemeProvider} from 'styled-components';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {
  fontSizeSelector,
  languageSelector,
} from '../../store/selectors/settingsSelectors';
import defaultTheme, {bigFontSizes} from '../../theme/defalutTheme';
import MainStack from '../../navigation';
import WithErrorHandler from '../../components/WithErrorHandler/WithErrorHandler';
import {fontSizesE} from '../../utils/enums';

const InitScreen = (props) => {
  const {i18n} = useTranslation();
  const lan = useSelector((state) => languageSelector(state));
  const fontSize = useSelector((state) => fontSizeSelector(state));

  useEffect(() => {
    i18n.changeLanguage(lan);
  }, [lan]);

  const getTheme = () => {
    console.log({fontSize});
    return fontSize === fontSizesE.BIG
      ? {...defaultTheme, fontSizes: bigFontSizes}
      : defaultTheme;
  };

  return (
    <ThemeProvider theme={getTheme()}>
      <WithErrorHandler>
        <MainStack isConnect={false} />
      </WithErrorHandler>
    </ThemeProvider>
  );
};

export default InitScreen;
