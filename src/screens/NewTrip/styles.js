import styled from 'styled-components';
import { calcWidth } from '../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	width: 100%;
`;

styles.DateContainer = styled.View`
	align-self: center;
	width: ${calcWidth(308)}px;
	aspect-ratio: ${308 / 196};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
`;

styles.DateRowContainer = styled.View`
	flex-direction: row;
`;
 
styles.SubmitBtn = styled.TouchableOpacity``;

export default styles;
