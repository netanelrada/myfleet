import React from 'react';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import {useTranslation} from 'react-i18next';
import Header from '../../../../components/Header/Header';
import s from './styles';
import SerachList from '../../../../components/SerachList/SerachList';
import OnlyFree from './components/OnlyFree/OnlyFree';
import TableData from './components/TableData/TableData';
import {getDrivers} from '../../../../api/api';
import useDataUserAPI from '../../../../hooks/useDataUserAPI';

const DriverSelect = ({navigation, route}) => {
  const {t} = useTranslation();
  const {goBack} = navigation;
  const {
    filterData: driversData = [],
    onGetDrivers,
    statusData,
    onFilterData,
  } = useDataUserAPI({
    promise: getDrivers,
  });
  const {setFiledValue} = route.params;

  const onToggleSwitch = (newValue) => {
    onFilterData(newValue);
  };

  const filter = ({nickName}, text) =>
    nickName.toLowerCase().includes(text.toLowerCase());

  const tableHead = [t('freeFrom'), t('driverName'), t('comments')];
  const arrStyle = [
    {textAlign: 'right', flex: 1},
    {textAlign: 'right', flex: 2},
    {textAlign: 'center', flex: 0.7},
  ];

  const onPressDriver = (item) => {
    const {driverCode = '', nickName = '', carCode = '', carNumber = ''} = item;
    setFiledValue(
      'driverCode',
      {key: driverCode, text: nickName},
      {shouldValidate: true},
    );
    carCode &&
      setFiledValue(
        'carCode',
        {key: carCode, text: carNumber},
        {shouldValidate: true},
      );
    goBack();
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header
          onGoBack={goBack}
          iconName="driver"
          titleText={t('selectDriver')}
        />
        <SerachList
          tableHead={tableHead}
          tableData={driversData}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressDriver}
          statusData={statusData}
          keyItem="driverCode"
        />
      </s.Container>
      <OnlyFree toggleSwitch={onToggleSwitch} />
    </ImageBackground>
  );
};

export default DriverSelect;
