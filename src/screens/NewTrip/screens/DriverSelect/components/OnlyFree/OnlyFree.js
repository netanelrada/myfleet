import React, { useState } from 'react';
import s from './styles';
import { useTranslation } from 'react-i18next';
import Switch from '../../../../../../components/common/Switch/Switch';
import Text from '../../../../../../components/common/Text/Text';

const OnlyFree = ({ toggleSwitch = () => {} }) => {
	const { t } = useTranslation();
	const [isEnabled, setisEnabled] = useState(false);

	const onToggleSwitch = newValue => {
		setisEnabled(!newValue);
		toggleSwitch(!newValue);
	};

	return (
		<s.Container>
			<Text>{t('onlyFreeDrivers')}</Text>
			<Switch
				isEnabled={isEnabled}
				toggleSwitch={newValue => onToggleSwitch(newValue)}
			/>
		</s.Container>
	);
};

export default OnlyFree;
