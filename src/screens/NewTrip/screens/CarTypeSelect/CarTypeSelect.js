import React from 'react';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import {useTranslation} from 'react-i18next';
import Header from '../../../../components/Header/Header';
import s from './styles';
import SerachList from '../../../../components/SerachList/SerachList';
import TableData from './components/TableData/TableData';
import {fetchDataStatusE} from '../../../../utils/enums';

const CarTypeSelect = ({navigation, route}) => {
  const {t} = useTranslation();
  const {goBack} = navigation;
  const {carsTypeList = [], setFiledValue} = route.params;

  const filter = ({carType = ''}, text) => carType.includes(text);

  const tableHead = [t('carType')];

  const arrStyle = [{textAlign: 'right', flex: 1}];

  const onPressCarType = (item) => {
    const {carTypeCode = '', carType = ''} = item;
    setFiledValue(
      'carTypeCode',
      {key: carTypeCode, text: carType},
      {shouldValidate: true},
    );
    goBack();
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header onGoBack={goBack} titleText={t('orderCarType')} />
        <SerachList
          tableHead={tableHead}
          tableData={carsTypeList}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressCarType}
          statusData={fetchDataStatusE.SUCCESS}
          keyItem="carTypeCode"
        />
      </s.Container>
    </ImageBackground>
  );
};

export default CarTypeSelect;
