import React from 'react';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import {useTranslation} from 'react-i18next';
import Header from '../../../../components/Header/Header';
import s from './styles';
import SerachList from '../../../../components/SerachList/SerachList';
import TableData from './components/TableData/TableData';
import {fetchDataStatusE} from '../../../../utils/enums';

const ClientSelect = ({navigation, route}) => {
  const {t} = useTranslation();
  const {goBack} = navigation;
  const {clinetsList = [], setFiledValue} = route.params;

  const filter = ({clientName = ''}, text) => clientName.includes(text);

  const tableHead = [t('clientName'), t('code')];
  const arrStyle = [
    {textAlign: 'right', flex: 0.8},
    {textAlign: 'center', flex: 0.2},
  ];

  const onPressItem = (item) => {
    const {clientCode = '', clientName = ''} = item;
    setFiledValue(
      'clientCode',
      {key: clientCode, text: clientName},
      {shouldValidate: true},
    );
    goBack();
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header onGoBack={goBack} titleText={t('selectClient')} />
        <SerachList
          tableHead={tableHead}
          tableData={clinetsList}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressItem}
          statusData={fetchDataStatusE.SUCCESS}
          keyItem="clientCode"
        />
      </s.Container>
    </ImageBackground>
  );
};

export default ClientSelect;
