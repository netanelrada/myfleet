import React from 'react';
import styled from 'styled-components';
import Text from '../../../../../../components/common/Text/Text';
import { calcHeight } from '../../../../../../utils/dimensions';

const TableData = ({
	arrStyle = [],
	data: { clientCode = '', clientName = '' },
	onPress = () => {},
}) => {
	return (
		<s.Container onPress={onPress}>
			<s.Cell flex={arrStyle[0].flex}>
				<Text textAlign={arrStyle[0].textAlign}>{clientName}</Text>
			</s.Cell>
			<s.Cell flex={arrStyle[1].flex}>
				<Text textAlign={arrStyle[1].textAlign}>{clientCode}</Text>
			</s.Cell>
		</s.Container>
	);
};

const s = {
	Container: styled.TouchableOpacity`
		width: 100%;
		height: ${calcHeight(52)}px;
		border-bottom-color: rgba(180, 190, 201, 0.3);
		border-bottom-width: 1px;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: flex-start;
	`,
	Cell: styled.View`
		flex: ${({ flex }) => flex};
		${({ align }) => `align-items: ${align};`}
	`,
};

export default TableData;
