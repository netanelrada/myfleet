import { useState, useEffect } from 'react';
import useProxyApi from '../../../../../hooks/useProxyApi';
import { fetchDataStatusE } from '../../../../../utils/enums';
import { getCourses } from '../../../../../api/api';

const useLineDescriptionSelect = ({ clientCode = '' }) => {
	const { proxyApi } = useProxyApi();
	const [courseList, setCourseList] = useState([]);
	const [statusData, setStatusData] = useState(null);

	useEffect(() => {
		onGetCourses();
	}, []);

	const onGetCourses = async () => {
		setStatusData(fetchDataStatusE.LOADING);
		try {
			const res = await proxyApi({
				cb: getCourses,
				restProp: { clientCode },
			});
			const { courseList } = res.data;
			setStatusData(fetchDataStatusE.SUCCESS);
			setCourseList(courseList);
		} catch (error) {
			console.log(error);
			setStatusData(fetchDataStatusE.FAIL);
		}
	};

	return { courseList, statusData };
};

export default useLineDescriptionSelect;
