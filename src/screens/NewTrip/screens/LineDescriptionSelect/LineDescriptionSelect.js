import React from 'react';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import {useTranslation} from 'react-i18next';
import Header from '../../../../components/Header/Header';
import s from './styles';
import SerachList from '../../../../components/SerachList/SerachList';
import TableData from './components/TableData/TableData';
import useLineDescriptionSelect from './hooks/useLineDescriptionSelect';

const LineDescriptionSelect = ({navigation, route}) => {
  const {t} = useTranslation();
  const {goBack} = navigation;
  const {clientCode, setFiledValue} = route.params;

  const {courseList = [], statusData} = useLineDescriptionSelect({clientCode});

  const tableHead = [t('lineDescription'), t('code')];

  const arrStyle = [
    {textAlign: 'right', flex: 0.8},
    {textAlign: 'center', flex: 0.2},
  ];

  const filter = ({courseDescription, courseCode}, text) =>
    courseDescription.includes(text) || courseCode.includes(text);

  const onPressCourse = (item) => {
    const shouldValidate = {shouldValidate: true};
    const {
      courseCode = '',
      courseDescription = '',
      driverCode = '',
      nickName = '',
      carTypeCode = '',
      carType = '',
      carCode = '',
      carNumber = '',
    } = item;
    setFiledValue(
      'lineDescription',
      {
        key: courseCode,
        text: courseDescription,
      },
      shouldValidate,
    );
    driverCode &&
      setFiledValue(
        'driverCode',
        {
          key: driverCode,
          text: nickName,
        },
        shouldValidate,
      );
    carTypeCode &&
      setFiledValue(
        'carTypeCode',
        {
          key: carTypeCode,
          text: carType,
        },
        shouldValidate,
      );
    carCode &&
      setFiledValue(
        'carCode',
        {key: carCode, text: carNumber},
        {shouldValidate: true},
      );

    goBack();
  };
  return (
    <ImageBackground>
      <s.Container>
        <Header
          onGoBack={goBack}
          iconName="road"
          titleText={t('lineDescriptionSelect')}
        />
        <SerachList
          tableHead={tableHead}
          tableData={courseList}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressCourse}
          statusData={statusData}
          keyItem="courseCode"
        />
      </s.Container>
    </ImageBackground>
  );
};

export default LineDescriptionSelect;
