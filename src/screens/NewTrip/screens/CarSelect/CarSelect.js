import React from 'react';
import ImageBackground from '../../../../components/common/ImageBackground/ImageBackground';
import {useTranslation} from 'react-i18next';
import Header from '../../../../components/Header/Header';
import s from './styles';
import SerachList from '../../../../components/SerachList/SerachList';
import OnlyFree from '../../../CarReplacement/components/OnlyFree/OnlyFree';
import TableData from '../../../CarReplacement/components/TableData/TableData';
import {getCars} from '../../../../api/api';
import useDataUserAPI from '../../../../hooks/useDataUserAPI';

const CarSelect = ({navigation, route}) => {
  const {t} = useTranslation();
  const {filterData: carsData = [], statusData, onFilterData} = useDataUserAPI({
    promise: getCars,
  });
  const {goBack} = navigation;
  const {setFiledValue} = route.params;

  const onToggleSwitch = (newValue) => {
    onFilterData(newValue);
  };

  const onPressCar = (item) => {
    const {carCode = '', carNumber = '', driverCode = '', nickName = ''} = item;

    setFiledValue(
      'carCode',
      {key: carCode, text: carNumber},
      {shouldValidate: true},
    );
    driverCode &&
      setFiledValue(
        'driverCode',
        {key: driverCode, text: nickName},
        {shouldValidate: true},
      );
    goBack();
  };

  const filter = ({carNumber = '', carId = ''}, text) =>
    [carNumber, carId].some((str) => str.includes(text));
  const tableHead = [
    t('freeFrom'),
    t('carCode'),
    t('carCodeAndCarType'),
    t('comments'),
  ];
  const arrStyle = [
    {textAlign: 'center', flex: 1},
    {textAlign: 'center', flex: 0.8},
    {textAlign: 'center', flex: 2},
    {textAlign: 'center', flex: 0.8},
  ];

  return (
    <ImageBackground>
      <s.Container>
        <Header onGoBack={goBack} iconName="car" titleText={t('selectCar')} />
        <SerachList
          tableHead={tableHead}
          tableData={carsData}
          arrStyle={arrStyle}
          callBackFilter={(data, text) => filter(data, text)}
          RowComponent={<TableData arrStyle={arrStyle} />}
          onClickItem={onPressCar}
          statusData={statusData}
          keyItem="carCode"
        />
      </s.Container>
      <OnlyFree toggleSwitch={onToggleSwitch} />
    </ImageBackground>
  );
};

export default CarSelect;
