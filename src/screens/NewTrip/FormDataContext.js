import React, { createContext, useState } from 'react';

export const FormDataContext = createContext();

const initialState = {
	date: '',
	startTime: '',
	endTime: '',
};

export default function FormDataProvider(props) {
	const [formData, setFormData] = useState(initialState);
	const value = [formData, setFormData];

	return <FormDataContext.Provider value={value} {...props} />;
}
