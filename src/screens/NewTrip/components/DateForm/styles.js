import styled from 'styled-components';
import {calcWidth} from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  align-self: center;
  width: ${calcWidth(308)}px;
  aspect-ratio: ${308 / 196};
  background-color: ${({theme}) => theme.colors.white};
  border-radius: 9px;
  padding-horizontal: ${calcWidth(23)}px;
  justify-content: space-around;
  box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
  elevation: 3;
`;

styles.FiledContainer = styled.View`
  flex-direction: row-reverse;
  align-items: center;
  justify-content: space-between;
  ${({disable}) => disable && 'opacity: 0.4'}
`;

styles.Label = styled.Text`
  font-family: Rubik-Regular;
  font-size: ${({theme, fontSize = 's10'}) => theme.fontSizes[fontSize]}px;
  color: ${({theme}) => theme.colors.gray6};
  max-width: 100px;
`;

styles.ErrorText = styled.Text`
  font-family: Rubik-Regular;
  font-size: ${({theme}) => theme.fontSizes.s12}px;
  color: ${({theme}) => theme.colors.errorColor};
`;

styles.View = styled.View`
  align-items: flex-end;
`;

export default styles;
