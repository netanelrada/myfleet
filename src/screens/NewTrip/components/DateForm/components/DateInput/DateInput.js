import React, { useState } from 'react';
import s from './styles';
import Icon from '../../../../../../components/common/Icon/Icon';
import MyCalendar from '../../../../../../components/MyCalendar/MyCalendar';
import moment from 'moment';
import useCalendar from '../../../../../Main/components/Header/components/MainHeader/hooks/useCalendar';
import { TextInputMask } from 'react-native-masked-text';

const DateInput = ({
	onChange,
	onBlur,
	value,
	isValidate = true,
	disable = false,
}) => {
	const { isVisable, closeCalender, openCalender, date } = useCalendar();

	const onSetDate = dateSelected => {
		const d = moment(dateSelected).format('DD/MM/YYYY');
		onChange(d);
		closeCalender();
	};

	return (
		<>
			<s.Container
				pointerEvents={disable?.isLock ? 'none' : 'auto'}
				backgroundColor={disable?.isLock}
				isValidate={isValidate}
			>
				<s.InputContainer>
					<TextInputMask
						style={{
							width: '100%',
							height: '100%',
							justifyContent: 'center',
							alignItems: 'center',
							textAlign: 'center',
						}}
						type={'datetime'}
						options={{
							format: 'DD/MM/YYYY',
						}}
						selectTextOnFocus
						value={value}
						onChangeText={text => onChange(text)}
					/>
				</s.InputContainer>
				<s.IconContainer onPress={openCalender}>
					<Icon name='calendar' />
				</s.IconContainer>
			</s.Container>
			<MyCalendar
				isVisible={isVisable}
				closeCalender={closeCalender}
				setDate={onSetDate}
				initialDate={moment(date)}
			/>
		</>
	);
};

export default DateInput;
