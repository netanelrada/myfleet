import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../../../utils/dimensions';
const styles = {};

styles.Container = styled.View`
	width: ${({ width }) => calcWidth(width)}px;
	height: ${calcWidth(44)}px;
	border: 1px solid
		${({ theme: { colors }, isValidate }) =>
			isValidate ? colors.borderColorInput : colors.errorColor};
	flex-direction: row-reverse;
	border-radius: 4px;
	${({ backgroundColor, theme }) =>
		backgroundColor && `background-color: ${theme.colors.darkGray1}`}
`;
styles.InputContainer = styled.View`
	flex-direction: row-reverse;
	flex: 0.75;
	justify-content: center;
	align-items: center;
`;

styles.Input = styled.TextInput`
	width: 100%;
	height: 100%;
	justify-content: center;
	align-items: center;
	text-align: center;
`;

styles.IconContainer = styled.TouchableOpacity`
	justify-content: center;
	align-items: center;
	height: 100%;
	flex: 0.25;
	border-end-width: 1px;
	border-end-color: ${({ theme }) => theme.colors.borderColorInput};
`;

export default styles;
