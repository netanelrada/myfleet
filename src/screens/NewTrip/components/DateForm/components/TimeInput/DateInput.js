import React, { useState } from 'react';
import s from './styles';
import Icon from '../../../../../../components/common/Icon/Icon';
import moment from 'moment';
import { TextInputMask } from 'react-native-masked-text';
import TimePicker from '../../../../../../components/TimePicker/TimePicker';

const TimeInput = ({
	onChange,
	onBlur,
	value,
	isValidate = true,
	label = '',
	disable = false,
	width = 181,
}) => {
	const [isShowTimePicker, setIsShowTimePicker] = useState(false);

	const openTimePicker = () => setIsShowTimePicker(true);
	const onSetTimeByTimePicker = time => {
		setIsShowTimePicker(false);
		if (!time) {
			return;
		}
		const t = time.toTimeString().split(' ')[0];
		const timeFormat = t.slice(0, 5);
		onChange(timeFormat);
	};
	return (
		<s.Container
			pointerEvents={disable?.isLock ? 'none' : 'auto'}
			backgroundColor={disable?.isLock}
			isValidate={isValidate}
			width={width}
		>
			<s.InputContainer>
				<TextInputMask
					style={{
						width: '100%',
						height: '100%',
						justifyContent: 'center',
						alignItems: 'center',
						textAlign: 'center',
						letterSpacing: 5,
					}}
					type={'datetime'}
					options={{
						format: 'HH:MM',
					}}
					selectTextOnFocus
					value={value}
					onChangeText={text => onChange(text)}
				/>
			</s.InputContainer>
			<s.IconContainer onPress={openTimePicker}>
				<Icon name='blueClock' />
			</s.IconContainer>

			{isShowTimePicker && (
				<TimePicker onConfirm={onSetTimeByTimePicker} label={label} />
			)}
		</s.Container>
	);
};

export default TimeInput;
