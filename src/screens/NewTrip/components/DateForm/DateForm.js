import React from 'react';
import s from './styles';
import {useTranslation} from 'react-i18next';
import TimePickerInput from '../../../../components/common/TimePickerInput/TimeInput';
import DateInput from './components/DateInput/DateInput';
import {Controller} from 'react-hook-form';
import Text from '../../../../components/common/Text/Text';
import TimeInput from './components/TimeInput/DateInput';
import WithLock from '../WithLock/WithLock';

const DateForm = ({control, errors, dateFileds}) => {
  const {t} = useTranslation();

  return (
    <s.Container>
      <s.FiledContainer>
        <s.Label>{t('tripDate')}</s.Label>
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <s.View>
              <WithLock placement="bottom" {...dateFileds.lineDate.disable}>
                <DateInput
                  onChange={onChange}
                  value={value}
                  isValidate={!errors.lineDate}
                  disable={dateFileds.lineDate.disable}
                />
              </WithLock>
              {errors.lineDate && (
                <s.ErrorText>{errors.lineDate.message}</s.ErrorText>
              )}
            </s.View>
          )}
          name="lineDate"
          rules={{
            required: {
              value: true,
              message: t('fieldRequired'),
            },
            ...dateFileds.lineDate.validation,
          }}
          defaultValue={dateFileds.lineDate.defaultValue}
        />
      </s.FiledContainer>
      <s.FiledContainer>
        <s.Label>{t('startTime')}</s.Label>
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <s.View>
              <WithLock {...dateFileds.startTime.disable}>
                <TimeInput
                  label={t('startTime')}
                  value={value}
                  onChange={onChange}
                  isValidate={!Boolean(errors.startTime?.message)}
                  disable={dateFileds.startTime.disable}
                />
              </WithLock>
              {errors.startTime && (
                <s.ErrorText>{errors.startTime.message}</s.ErrorText>
              )}
            </s.View>
          )}
          name="startTime"
          rules={{
            required: {value: true, message: t('fieldRequired')},
            ...dateFileds.startTime.validation,
          }}
          defaultValue={dateFileds.startTime.defaultValue}
        />
      </s.FiledContainer>
      <s.FiledContainer>
        <s.Label>{t('endTime')}</s.Label>
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <s.View>
              <WithLock {...dateFileds.endTime.disable}>
                <TimeInput
                  label={t('endTime')}
                  value={value}
                  onChange={onChange}
                  isValidate={!errors.endTime}
                  disable={dateFileds.endTime.disable}
                />
              </WithLock>
              {errors.endTime && (
                <s.ErrorText>{errors.endTime.message}</s.ErrorText>
              )}
            </s.View>
          )}
          name="endTime"
          rules={{
            required: {value: true, message: t('fieldRequired')},
            ...dateFileds.endTime.validation,
          }}
          defaultValue={dateFileds.endTime.defaultValue}
        />
      </s.FiledContainer>
    </s.Container>
  );
};

export default DateForm;
