import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View``;

styles.ToolTipContainer = styled.View`
	position: absolute;
	left: ${-calcWidth(12)}px;
	top: ${-calcWidth(10)}px;
	z-index: 99999;
`;

styles.IconContainer = styled.TouchableOpacity`
	width: ${calcWidth(24)}px;
	aspect-ratio: 1;
	border-radius: ${calcWidth(12)}px;
	justify-content: center;
	align-items: center;
	background-color: ${({ theme }) => theme.colors.yellowLight};
`;

styles.ToolTipContentContainer = styled.View`
	justify-content: space-around;
`;
styles.Text = styled.Text`
	text-align: right;
	text-align-vertical: top;
	margin-bottom: 15px;
	border: 1px solid red;
`;

export default styles;
