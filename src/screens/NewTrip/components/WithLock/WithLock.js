import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import useGetErrorMsg from '../../../../hooks/useGetErrorMsg';
import Text from '../../../../components/common/Text/Text';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';
import { textErrors } from '../../../Initial/utils/utils';

const WithLock = ({
	children,
	isLock = false,
	locksIds = [],
	inputHeight = 44,
	placement = 'top',
}) => {
	const [toolTipVisable, setToolTipVisable] = useState(false);
	const { messagesLock } = useGetErrorMsg();

	const sizeIcon = {
		width: calcWidth(15),
		height: calcWidth(15),
	};
	return (
		<s.Container>
			{children}
			{!!isLock && (
				<s.ToolTipContainer inputHeight={inputHeight}>
					<Tooltip
						isVisible={toolTipVisable}
						content={
							<ScrollView
								contentContainerStyle={{
									maxHeight: calcHeight(55),
								}}
							>
								{locksIds.map(id => (
									<Text
										textAlign='right'
										key={id}
									>{`• ${messagesLock[id]}`}</Text>
								))}
							</ScrollView>
						}
						placement={placement}
						onClose={() => setToolTipVisable(false)}
					>
						<s.IconContainer onPress={() => setToolTipVisable(true)}>
							<Icon {...sizeIcon} name='warning' />
						</s.IconContainer>
					</Tooltip>
				</s.ToolTipContainer>
			)}
		</s.Container>
	);
};

export default WithLock;
