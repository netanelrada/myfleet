import React from 'react';
import s from './styles';
import Text from '../../../../components/common/Text/Text';
import { useTranslation } from 'react-i18next';

const CancelBtn = ({ onPress }) => {
	const { t } = useTranslation();
	return (
		<s.Container onPress={onPress}>
			<Text fontWeight='500' color='gray'>
				{t('cancel')}
			</Text>
		</s.Container>
	);
};

export default CancelBtn;
