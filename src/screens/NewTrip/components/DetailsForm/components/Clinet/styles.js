import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex-direction: column-reverse;
`;

styles.ClientsContainer = styled.View`
	flex-direction: row-reverse;
	justify-content: flex-start;
	margin-top: ${calcHeight(15)}px;
	${({ disable }) => disable && 'opacity: 0.4'}
`;

styles.ClientContainer = styled.TouchableOpacity`
	width: ${calcWidth(82)}px;
	aspect-ratio: ${82 / 28};
	justify-content: center;
	margin-left: 5px;
	border: 1px solid ${({ theme }) => theme.colors.borderColorInput};
	border-radius: 4px;
`;

styles.ErrorText = styled.Text`
	font-family: Rubik-Regular;
	position: absolute;
	top: 0;
	left: 0;
	font-size: ${({ theme }) => theme.fontSizes.s12}px;
	color: ${({ theme }) => theme.colors.errorColor};
	text-align: right;
`;

export default styles;
