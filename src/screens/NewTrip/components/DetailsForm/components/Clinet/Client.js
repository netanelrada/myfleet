import React from 'react';
import { Controller } from 'react-hook-form';
import Input from '../Input/Input';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';
import { useSelector } from 'react-redux';
import { lastSelectedClientsListSelector } from '../../../../../../store/selectors/persistenceFormDataSelectors';
import { useTranslation } from 'react-i18next';

const Client = ({
	errors,
	control,
	data: { key, defaultValue, validation = {}, ...rest },
}) => {
	const { t } = useTranslation();
	const lastSelectedClientsList = useSelector(state =>
		lastSelectedClientsListSelector(state)
	);

	return (
		<Controller
			key={key}
			control={control}
			rules={{ ...validation }}
			render={({ onChange, onBlur, value }) => (
				<s.Container>
					<s.ClientsContainer
						pointerEvents={rest.disable?.isLock ? 'none' : 'auto'}
						disable={rest.disable}
					>
						{lastSelectedClientsList.map((item, index) => (
							<s.ClientContainer
								key={index}
								onPress={() =>
									onChange({ key: item.clientCode, text: item.clientName })
								}
							>
								<Text numberOfLines={1} textAlign='center'>
									{item.clientName}
								</Text>
							</s.ClientContainer>
						))}
					</s.ClientsContainer>
					<Input
						marginBottom={0}
						{...rest}
						value={value}
						codeProp={key}
						onChange={onChange}
						isValidate={!errors[key]}
						errorMessage={errors[key]?.message}
					/>
				</s.Container>
			)}
			name={key}
			defaultValue={defaultValue}
		/>
	);
};

export default Client;
