import styled from 'styled-components';
import {calcHeight, calcWidth} from '../../../../../../utils/dimensions';
const styles = {};

styles.FiledContainer = styled.View`
  margin-bottom: ${({marginBottom}) => calcHeight(marginBottom)}px;
  align-self: flex-end;
  ${({disable}) => disable && 'opacity: 0.4'}
`;

styles.Container = styled.View`
  width: ${({width}) => calcWidth(width)}px;
  height: ${({height}) => calcWidth(height)}px;
  border: 1px solid
    ${({theme: {colors}, isValidate}) =>
      isValidate ? colors.borderColorInput : colors.errorColor};
  flex-direction: row-reverse;
  border-radius: 4px;
  ${({backgroundColor, theme}) =>
    backgroundColor && `background-color: ${theme.colors.darkGray1}`}
  align-self: flex-end;
`;

styles.InputContainer = styled.View`
  flex-direction: row-reverse;
  flex: 0.85;
  justify-content: center;
  align-items: center;
`;

styles.Input = styled.TextInput`
  height: ${calcHeight(44)}px;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  text-align: right;
  z-index: -10;
  padding-horizontal: ${calcWidth(4)}px;
`;

styles.IconContainer = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  height: 100%;
  flex: 0.15;
  border-end-width: 1px;
  border-end-color: ${({theme}) => theme.colors.borderColorInput};
`;

styles.CloseContainer = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  height: 100%;
  flex: 0.1;
`;

styles.LabelContainer = styled.View`
  flex-direction: row-reverse;
  align-self: flex-end;
  max-width: ${calcWidth(120)}px;
`;

styles.TouchableOpacity = styled.TouchableOpacity`
  height: ${calcWidth(25)}px;
  z-index: 999;
`;

styles.ListContainer = styled.View`
  z-index: 9999999;
  width: 100%;
  position: absolute;
  top: 0;
  border: 1px solid ${({theme}) => theme.colors.borderColorInput};
  background-color: white;
`;

styles.View = styled.View``;
styles.InputLabelContainer = styled.View`
  flex-direction: column-reverse;
`;

styles.ErrorText = styled.Text`
  font-family: Rubik-Regular;
  position: absolute;
  top: 0;
  right: 0;
  font-size: ${({theme}) => theme.fontSizes.s12}px;
  color: ${({theme}) => theme.colors.errorColor};
  text-align: right;
`;

export default styles;
