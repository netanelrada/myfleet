import React, {useState} from 'react';
import s from './styles';
import Icon from '../../../../../../components/common/Icon/Icon';
import Text from '../../../../../../components/common/Text/Text';
import Autocomplete from 'react-native-autocomplete-input';
import {driversList} from '../../../../../../utils/data';
import {
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import {PlatfromOS} from '../../../../../../utils/utilis';
import {View, Pressable} from 'react-native';
import {calcHeight} from '../../../../../../utils/dimensions';
import WithLock from '../../../WithLock/WithLock';

const Input = ({
  onChange = () => {},
  action = () => {},
  value = '',
  width = 264,
  height = 44,
  marginBottom = 15,
  visableIcon = true,
  isValidate = true,
  label = 'שם שדה',
  errorMessage = '',
  codeProp = '',
  autoCompleteData = {
    isVisable: false,
    listData: [],
    showProp: '',
    limitData: 10,
  },
  inputProps = {},
  disable = false,
  next = () => {},
}) => {
  const [data, setData] = useState([]);

  const onChangeText = (text) => {
    if (!text) {
      setData([]);
      onChange({key: '', text});
      return;
    }

    const newData = autoCompleteData.listData.filter((el) =>
      el[autoCompleteData.showProp].includes(text),
    );
    setData(newData.slice(0, autoCompleteData.limitData));
    onChange({key: '', text});
  };

  const onPressItem = (item) => {
    setData([]);
    onChange({key: item[codeProp], text: item[autoCompleteData.showProp]});
    next(item);
  };

  return (
    <s.FiledContainer marginBottom={marginBottom}>
      <s.View>
        <s.LabelContainer>
          <Text textAlign="right">{label}</Text>
        </s.LabelContainer>
        <WithLock inputHeight={height} {...disable}>
          <s.Container
            pointerEvents={disable?.isLock ? 'none' : 'auto'}
            backgroundColor={disable?.isLock}
            width={width}
            height={height}
            isValidate={isValidate}>
            <s.InputContainer>
              <s.Input
                {...inputProps}
                value={value.text}
                onChangeText={(text) => onChangeText(text)}
              />
            </s.InputContainer>
            {!!value.key && (
              <s.CloseContainer onPress={() => onChange({key: '', text: ''})}>
                <Icon name="inputClose" />
              </s.CloseContainer>
            )}
            {visableIcon && (
              <s.IconContainer onPress={action}>
                <Icon name="blueSearch" />
              </s.IconContainer>
            )}
          </s.Container>
        </WithLock>
      </s.View>
      <s.View>
        <s.ErrorText>{errorMessage}</s.ErrorText>
        {autoCompleteData.isVisable && data.length > 0 ? (
          <s.ListContainer>
            {data.map((item, index) => (
              <TouchableHighlight
                key={index}
                style={{height: calcHeight(18)}}
                onPress={() => onPressItem(item)}>
                <Text textAlign="right">{item[autoCompleteData.showProp]}</Text>
              </TouchableHighlight>
            ))}
          </s.ListContainer>
        ) : null}
      </s.View>
    </s.FiledContainer>
  );
};

export default Input;
