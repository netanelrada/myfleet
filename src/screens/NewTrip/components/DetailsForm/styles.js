import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	align-self: center;
	flex-direction: column-reverse;
	flex-wrap: wrap;
	justify-content: space-between;
	margin-vertical: ${calcWidth(16)}px;
	width: ${calcWidth(308)}px;
	aspect-ratio: ${308 / 1048};
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	padding-horizontal: ${calcWidth(21)}px;
	padding-vertical: ${calcHeight(15)}px;
	box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.1);
	elevation: 3;
`;

styles.FiledContainer = styled.View`
	flex-direction: row-reverse;
	align-items: center;
	width: 100%;
	justify-content: space-between;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	font-size: ${({ theme }) => theme.fontSizes.s14};
	color: ${({ theme }) => theme.colors.gray6};
`;

styles.View = styled.View`
	flex-direction: column;
`;

styles.ErrorText = styled.Text`
	font-family: Rubik-Regular;
	position: absolute;
	right: 0;
	bottom: 0;
	font-size: ${({ theme }) => theme.fontSizes.s12}px;
	color: ${({ theme }) => theme.colors.errorColor};
	text-align: right;
`;

export default styles;
