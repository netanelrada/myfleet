import React from 'react';
import s from './styles';
import useGetFormData from '../../hooks/useGetFormData';
import Input from './components/Input/Input';
import {Controller} from 'react-hook-form';
import Client from './components/Clinet/Client';

const DetailsForm = ({control, errors, detailsFileds, clientFiled}) => {
  const getInputControl = (
    {key, defaultValue, validation = {}, ...rest},
    index,
  ) => {
    return (
      <Controller
        key={key}
        control={control}
        render={({onChange, onBlur, value}) => (
          <s.View>
            {errors[key] && <s.ErrorText>{errors[key]?.message}</s.ErrorText>}
            <Input {...rest} codeProp={key} onChange={onChange} value={value} />
          </s.View>
        )}
        name={key}
        rules={{...validation}}
        defaultValue={defaultValue}
      />
    );
  };

  return (
    <s.Container>
      {detailsFileds.map((item, index) => (
        <s.FiledContainer key={index}>
          {Array.isArray(item)
            ? item.map((item, index) => getInputControl(item, index))
            : getInputControl(item, index)}
        </s.FiledContainer>
      ))}
      <Client data={clientFiled} control={control} errors={errors} />
    </s.Container>
  );
};

export default DetailsForm;
