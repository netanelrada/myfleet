import React from 'react';
import { useTranslation } from 'react-i18next';
import s from './styles';
import Text from '../../../../components/common/Text/Text';

const SubmitBtn = ({ onPress = () => {}, btnText = '' }) => {
	return (
		<s.Container onPress={onPress}>
			<Text fontWeight='500' color='white'>
				{btnText}
			</Text>
		</s.Container>
	);
};

export default SubmitBtn;
