import React from 'react';
import ModalConfirm from '../../../../components/common/ModalConfirm/ModalConfirm';
import Text from '../../../../components/common/Text/Text';
import { useTranslation } from 'react-i18next';

const SaveModal = ({
	isVisible = false,
	onCancel = () => {},
	onSave = () => {},
	onCancelSave = () => {},
}) => {
	const { t } = useTranslation();
	return (
		<ModalConfirm
			isVisible={isVisible}
			onConfirm={onSave}
			onCancel={onCancelSave}
			onPressExtraBtn={onCancel}
			textConfirnBtn={t('save')}
			textCancelBtn={t('dontSave')}
			textExtraBtn={t('cancel')}
			isExtraBtnHidden={false}
		>
			<Text>{t('noDataSaved')}</Text>
		</ModalConfirm>
	);
};

export default SaveModal;
