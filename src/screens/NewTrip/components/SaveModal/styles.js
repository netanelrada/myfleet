import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.TouchableOpacity`
	width: ${calcWidth(264)}px;
	height: ${calcHeight(48)}px;
	border-radius: 9px;
	justify-content: center;
	align-items: center;
	align-self: center;
`;

export default styles;
