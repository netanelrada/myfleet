export const validationSechema = {
	required: message => {
		return { validate: ({ text }) => (!text ? message : true) };
	},
	onlyNumber: message => {
		return { validate: ({ text }) => (isNaN(text) ? message : true) };
	},
	existOnListAndRequired: (requiredMessage, notExistMessage) => {
		return {
			validate: ({ key, text }) => {
				if (text === '') return requiredMessage;
				return text !== '' && key === '' ? notExistMessage : true;
			},
		};
	},
	existOnList: notExistMessage => {
		return {
			validate: ({ key, text }) =>
				text !== '' && key === '' ? notExistMessage : true,
		};
	},
};
