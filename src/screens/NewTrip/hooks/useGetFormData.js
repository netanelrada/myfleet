import {useMemo, useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {
  getClinets,
  getDrivers,
  getCars,
  getCarsType,
  getLineData,
} from '../../../api/api';
import useProxyApi from '../../../hooks/useProxyApi';
import {useSelector} from 'react-redux';
import {dateSelector} from '../../../store/selectors/tripsSelectors';
import moment from 'moment';
import {validationSechema} from '../validations';
import {useNavigation} from '@react-navigation/native';
import {defaultValuesSelector} from '../../../store/selectors/persistenceFormDataSelectors';
import {initialState} from '../../../store/reducers/persistenceFormDataReducer';
import {DATE_REGEX, TIME_REGEX} from '../../../utils/time';

const initialDefaultValues = ({defaultValues, date, isEditMode = false}) => {
  if (isEditMode) return initialState.defaultValues;
  const {lineDate = ''} = defaultValues;

  if (!lineDate)
    return {
      ...defaultValues,
      lineDate: moment(date, 'DD.MM.YYYY').format('DD/MM/YYYY'),
    };
  return defaultValues;
};

const useGetFormData = ({
  setFiledValue = () => {},
  getFiledValues = () => {},
  resetFiledsValues,
  isEditMode = false,
  lineCode = '',
}) => {
  const {t} = useTranslation();
  const {proxyApi} = useProxyApi();
  const {navigate} = useNavigation();
  const date = useSelector((state) => dateSelector(state));
  const defaultValues = initialDefaultValues({
    defaultValues: useSelector((state) => defaultValuesSelector(state)),
    date,
    isEditMode,
  });
  const [disabled, setDisabled] = useState({});

  const [clinetsList, setClinetsList] = useState([]);
  const [driversList, setDriversList] = useState([]);
  const [carsList, setCarsList] = useState([]);
  const [carsTypeList, setCarsType] = useState([]);
  const [initilDriver, setInitilDriver] = useState(true);
  const [initilCar, setInitilCar] = useState(true);

  useEffect(() => {
    isEditMode && onSetDefaultValues();
  }, []);

  useEffect(() => {
    try {
      onGetClinetsList();
      onGetDriversList();
      onGetCarsList();
      onGetCarsTypeList();
    } catch (error) {
      console.log({error});
    }
  }, []);

  const dateFileds = useMemo(
    () => ({
      lineDate: {
        label: 'date',
        validation: {
          pattern: {
            value: DATE_REGEX,
            message: t('invalidDate'),
          },
        },
        defaultValue: defaultValues.lineDate,
        disable: isEditMode && disabled?.lineDate,
      },
      startTime: {
        label: 'startTime',
        validation: {
          pattern: {
            value: TIME_REGEX,
            message: t('invalidTime'),
          },
        },
        defaultValue: defaultValues.startTime,
        disable: isEditMode && disabled?.startTime,
      },
      endTime: {
        label: 'endTime',
        validation: {
          pattern: {
            value: TIME_REGEX,
            message: t('invalidTime'),
          },
        },
        defaultValue: defaultValues.endTime,
        disable: isEditMode && disabled?.endTime,
      },
    }),
    [disabled],
  );

  const detailsFileds = useMemo(
    () => [
      {
        key: 'longRemarks',
        label: t('longRemarks'),
        height: 163,
        action: () => {},
        visableIcon: false,
        validation: {required: false},
        inputProps: {multiline: true},
        defaultValue: defaultValues.longRemarks,
        disable: isEditMode && disabled?.longRemarks,
      },
      {
        key: 'shortRemarks',
        label: t('shortRemarks'),
        action: () => {},
        visableIcon: false,
        validation: {required: false},
        inputProps: {},
        defaultValue: defaultValues.shortRemarks,
        disable: isEditMode && disabled?.shortRemarks,
      },
      {
        key: 'visaNumber',
        label: t('visaNumber'),
        action: () => {},
        visableIcon: false,
        validation: {...validationSechema.onlyNumber(t('onlyNumber'))},
        inputProps: {},
        defaultValue: defaultValues.visaNumber,
        disable: isEditMode && disabled?.visaNumber,
      },
      {
        key: 'carCode',
        label: t('selectCar'),
        action: () => navigate('CarSelect', {setFiledValue}),
        autoCompleteData: {
          isVisable: true,
          listData: carsList,
          showProp: 'carNumber',
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: defaultValues.carCode,
        disable: isEditMode && disabled?.carCode,
        next: (item) => {
          const {driverCode = '', nickName = ''} = item;

          if (driverCode && initilCar) {
            setFiledValue(
              'driverCode',
              {key: driverCode, text: nickName},
              {shouldValidate: true},
            );
            setInitilCar(false);
          }
        },
      },
      [
        {
          key: 'passQty',
          label: t('passQty'),
          width: 82,
          action: () => {},
          visableIcon: false,
          validation: {
            ...validationSechema.onlyNumber(t('onlyNumber')),
          },
          inputProps: {},
          defaultValue: defaultValues.passQty,
          disable: isEditMode && disabled?.passQty,
        },
        {
          key: 'carTypeCode',
          label: t('orderCarType'),
          width: 173,
          action: () =>
            navigate('CarTypeSelect', {carsTypeList, setFiledValue}),
          autoCompleteData: {
            isVisable: true,
            listData: carsTypeList,
            showProp: 'carType',
          },
          validation: {...validationSechema.existOnList(t('notExistOnList'))},
          inputProps: {},
          defaultValue: defaultValues.carTypeCode,
          disable: isEditMode && disabled?.carTypeCode,
        },
      ],
      {
        key: 'driverCode',
        label: t('driver'),
        action: () => navigate('DriverSelect', {setFiledValue}),
        autoCompleteData: {
          isVisable: true,
          listData: driversList,
          showProp: 'nickName',
        },
        validation: {...validationSechema.existOnList(t('notExistOnList'))},
        defaultValue: defaultValues.driverCode,
        disable: isEditMode && disabled?.driverCode,
        next: (item) => {
          const {carCode = '', carNumber = ''} = item;
          if (carCode && initilDriver) {
            setFiledValue(
              'carCode',
              {key: carCode, text: carNumber},
              {shouldValidate: true},
            );
            setInitilDriver(false);
          }
        },
      },
      {
        key: 'lineDescription',
        label: t('lineDescription'),
        height: 80,
        action: () =>
          navigate('LineDescriptionSelect', {
            clientCode: getFiledValues('clientCode')?.key,
            setFiledValue,
          }),
        validation: {...validationSechema.required(t('fieldRequired'))},
        inputProps: {multiline: true},
        defaultValue: defaultValues.lineDescription,
        disable: isEditMode && disabled?.lineDescription,
      },
    ],
    [driversList, carsList, carsTypeList, disabled, initilDriver, initilCar],
  );

  const clientFiled = useMemo(
    () => ({
      key: 'clientCode',
      label: t('selectClient'),
      action: () => navigate('ClientSelect', {clinetsList, setFiledValue}),
      autoCompleteData: {
        isVisable: true,
        listData: clinetsList,
        showProp: 'clientName',
      },
      validation: {
        ...validationSechema.existOnListAndRequired(
          t('fieldRequired'),
          t('notExistOnList'),
        ),
      },
      inputProps: {},
      defaultValue: defaultValues.clientCode,
      disable: isEditMode && disabled?.clientCode,
    }),
    [clinetsList, disabled],
  );

  const onSetDefaultValues = async () => {
    try {
      const {data = []} = await proxyApi({
        cb: getLineData,
        restProp: {lineCode},
      });
      const {
        carCode,
        carNumber,
        clientCode,
        clientName,
        courseCode,
        driverCode,
        driverName,
        endTime: eTime,
        lineDescription,
        longRemarks,
        carTypeCode,
        carTypeName,
        passQty,
        relativeDate,
        shortRemarks,
        startTime: sTime,
        visaNumber,
        clientLocks: {clientLock, clientLockIDs},
        driverLocks: {driverLock, driverLockIDs},
        courseLocks: {courseLock, courseLockIDs},
        driveTimeLocks: {driveTimeLock, driveTimeLockIDs},
        orderLocks: {orderLock, orderLockIDs},
      } = data.data[0];

      const [, startTime] = sTime.split(' ');
      const [, endTime] = eTime.split(' ');
      const defaultValuesPayload = {
        lineDate: moment(relativeDate).format('DD/MM/YYYY'),
        startTime,
        endTime,
        clientCode: {key: clientCode, text: clientName},
        lineDescription: {key: courseCode, text: lineDescription},
        driverCode: {key: driverCode, text: driverName},
        carTypeCode: {key: carTypeCode, text: carTypeName},
        passQty: {key: '', text: passQty},
        carCode: {key: carCode, text: carNumber},
        visaNumber: {key: '', text: visaNumber},
        shortRemarks: {key: '', text: shortRemarks},
        longRemarks: {key: '', text: longRemarks},
      };

      const disabledPayload = {
        lineDate: {
          isLock: clientLock || driverLock || courseLock || orderLock,
          locksIds: [
            ...new Set([
              ...clientLockIDs,
              ...driverLockIDs,
              ...courseLockIDs,
              ...orderLockIDs,
            ]),
          ],
        },
        startTime: {
          isLock: clientLock || courseLock,
          locksIds: [...new Set([...clientLockIDs, ...courseLockIDs])],
        },
        endTime: {
          isLock: clientLock || courseLock || driveTimeLock,
          locksIds: [
            ...new Set([
              ...clientLockIDs,
              ...courseLockIDs,
              ...driveTimeLockIDs,
            ]),
          ],
        },
        clientCode: {
          isLock: clientLock || courseLock || orderLock,
          locksIds: [
            ...new Set([...clientLockIDs, ...courseLockIDs, ...orderLockIDs]),
          ],
        },
        lineDescription: {
          isLock: clientLock || courseLock,
          locksIds: [...new Set([...clientLockIDs, ...courseLockIDs])],
        },
        driverCode: {isLocks: driverLock, locksIds: [...driverLockIDs]},
        carTypeCode: {
          isLock: clientLock || courseLock,
          locksIds: [...new Set([...clientLockIDs, ...courseLockIDs])],
        },
        passQty: {isLock: clientLock, locksIds: [...clientLockIDs]},
        carCode: {isLocks: driverLock, locksIds: [...driverLockIDs]},
        visaNumber: {isLock: clientLock, locksIds: [...clientLockIDs]},
        shortRemarks: {
          isLock: clientLock || driverLock,
          locksIds: [...new Set([...clientLockIDs, ...driverLockIDs])],
        },
        longRemarks: {
          isLock: clientLock || driverLock,
          locksIds: [...new Set([...clientLockIDs, ...driverLockIDs])],
        },
      };

      resetFiledsValues(defaultValuesPayload);
      setDisabled(disabledPayload);
    } catch (error) {
      console.log(error);
    }
  };

  const onGetClinetsList = async () => {
    try {
      const res = await proxyApi({
        cb: getClinets,
        restProp: {},
      });
      const {data = []} = res.data;
      setClinetsList(data);
    } catch (error) {}
  };

  const onGetDriversList = async () => {
    const lineDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
    const res = await proxyApi({
      cb: getDrivers,
      restProp: {lineDate, onlyFree: 0},
    });
    const {data = []} = res.data;
    setDriversList(data);
  };

  const onGetCarsList = async () => {
    const lineDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
    const res = await proxyApi({
      cb: getCars,
      restProp: {lineDate, onlyFree: 0},
    });
    const {data = []} = res.data;
    setCarsList(data);
  };

  const onGetCarsTypeList = async () => {
    const lineDate = moment(date, 'D.M.YYYY').format('YYYY-M-D');
    const res = await proxyApi({
      cb: getCarsType,
      restProp: {lineDate, onlyFree: 0},
    });
    const {data = []} = res.data;
    setCarsType(data);
  };

  return {dateFileds, detailsFileds, clientFiled, defaultValues};
};

export default useGetFormData;
