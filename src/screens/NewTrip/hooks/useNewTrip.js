import useProxyApi from '../../../hooks/useProxyApi';
import {setNewLine} from '../../../api/api';
import useHandleResponse from '../../../hooks/useHandleResponse';
import {useDispatch} from 'react-redux';
import {
  addClient,
  setDefaultValues,
  restDefaultValues,
} from '../../../store/actions/actionPersistenceFormDataSelectors';
import moment from 'moment';

const useNewTrip = ({lineCode = null, setModalState, actionComplete}) => {
  const dispatch = useDispatch();
  const onSaveDefaultValues = (payload) => dispatch(setDefaultValues(payload));
  const onRestDefaultValues = () => dispatch(restDefaultValues());
  const onAddClient = (payload) => dispatch(addClient(payload));
  const {proxyApi} = useProxyApi();
  const {handleResponse} = useHandleResponse({enableGoBack: true});

  const onSubmit = async (data, e, action = 1) => {
    try {
      const payload = Object.keys(data).reduce((acc, curr) => {
        const currInput = data[curr];
        if (curr === 'lineDescription') {
          acc[curr] = data[curr].text;
          if (currInput.key) acc.courseCode = currInput.key;
        } else {
          acc[curr] =
            typeof currInput === 'string'
              ? currInput
              : currInput.key
              ? currInput.key
              : currInput.text;
        }

        return acc;
      }, {});
      payload.lineDate = moment(payload.lineDate, 'DD/MM/YYYY').format(
        'YYYY-MM-DD',
      );

      payload.action = action;

      if (lineCode) payload.lineCode = lineCode;

      const res = await proxyApi({
        cb: setNewLine,
        restProp: payload,
      });

      const isComplete = actionComplete(res.data);

      if (!isComplete && action !== 2) {
        setModalState({
          isVisible: true,
          res: res.data,
        });
        return;
      }
      if (action === 2) {
        setModalState((pre) => ({...pre, isVisible: false}));
      }

      if (res.data.response == '0') {
        const {key, text} = data.clientCode;
        onAddClient({clientCode: key, clientName: text});
      }
      handleResponse(res.data);
    } catch (error) {
      console.log('error', error);
    }
  };

  return {onSubmit, onSaveDefaultValues, onRestDefaultValues};
};

export default useNewTrip;
