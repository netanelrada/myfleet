import React, {useEffect, useState, useCallback, useRef} from 'react';
import ImageBackground from '../../components/common/ImageBackground/ImageBackground';
import Header from '../../components/common/Header/Header';
import s from './styles';
import {useTranslation} from 'react-i18next';
import useGetFormData from './hooks/useGetFormData';
import FormDataProvider from './FormDataContext';
import DateForm from './components/DateForm/DateForm';
import {useForm, Controller} from 'react-hook-form';
import SubmitBtn from './components/SubmitBtn/SubmitBtn';
import CancelBtn from './components/CancelBtn/CancelBtn';
import DetailsForm from './components/DetailsForm/DetailsForm';
import {ScrollView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import useNewTrip from './hooks/useNewTrip';
import SaveModal from './components/SaveModal/SaveModal';
import {calcHeight} from '../../utils/dimensions';
import {isEqual} from 'lodash';
import useErrorAlertsModals from '../../hooks/useErrorAlertsModals';
import AlertsAndErrors from '../../components/AlertsAndErrors/AlertsAndErrors';
import useGetErrorMsg from '../../hooks/useGetErrorMsg';
import useBackHandler from '../../hooks/useBackHandler';

const NewTrip = ({navigation, route: {params = {}}}) => {
  const {t} = useTranslation();
  const {lineCode = null} = params;

  const isEditMode = !!lineCode;
  const scrollViewRef = useRef(null);
  const {
    modalState,
    onCloseModal,
    setModalState,
    actionComplete,
  } = useErrorAlertsModals();
  const {newTripErrorMsg} = useGetErrorMsg();
  const {control, handleSubmit, errors, setValue, getValues, reset} = useForm({
    shouldFocusError: true,
  });

  const {onSubmit, onSaveDefaultValues, onRestDefaultValues} = useNewTrip({
    lineCode,
    setModalState,
    actionComplete,
  });
  const [isModalVisable, setisModalVisable] = useState(false);
  const {
    dateFileds,
    detailsFileds,
    clientFiled,
    defaultValues,
  } = useGetFormData({
    setFiledValue: setValue,
    getFiledValues: getValues,
    resetFiledsValues: reset,
    lineCode,
    isEditMode,
  });
  const {goBack} = navigation;
  const title = isEditMode ? t('editingTrip') : t('createNewTrip');
  const submitBtnText = isEditMode ? t('save') : t('add');
  useBackHandler(() => {
    goBack();
    return true;
  });
  const onGoBack = () => {
    const data = getValues();
    const isFormNotChanged = isEqual(data, defaultValues);
    if (!isFormNotChanged && !isEditMode) {
      setisModalVisable(true);
    } else {
      goBack();
    }
  };

  const closeModal = () => setisModalVisable(false);

  const dontSave = () => {
    closeModal();
    onRestDefaultValues();
    goBack();
  };

  const onSave = () => {
    const data = getValues();
    onSaveDefaultValues(data);
    closeModal();
    goBack();
  };

  const onError = (data) => {
    const topFormKeys = [
      'lineDate',
      'startTime',
      'endTime',
      'clientCode',
      'lineDescription',
    ];
    const centerFormKeys = ['driverCode', 'carTypeCode', 'passQty'];
    const errorObjKeys = Object.keys(data);

    const isErrorOnTopForm = errorObjKeys.some((key) =>
      topFormKeys.includes(key),
    );
    const isErrorOnCenterForm = errorObjKeys.some((key) =>
      centerFormKeys.includes(key),
    );

    if (isErrorOnTopForm) scrollViewRef.current.scrollToPosition(0, 0, true);
    else if (isErrorOnCenterForm)
      scrollViewRef.current.scrollToPosition(0, calcHeight(450), true);
  };

  return (
    <ImageBackground>
      <s.Container>
        <Header title={title} onBack={onGoBack} />
        <KeyboardAwareScrollView ref={scrollViewRef} enableResetScrollToCoords>
          <DateForm control={control} errors={errors} dateFileds={dateFileds} />
          <DetailsForm
            control={control}
            errors={errors}
            detailsFileds={detailsFileds}
            clientFiled={clientFiled}
          />
          <SubmitBtn
            btnText={submitBtnText}
            onPress={handleSubmit(onSubmit, onError)}
          />
          <CancelBtn onPress={onGoBack} />
        </KeyboardAwareScrollView>
        <SaveModal
          isVisible={isModalVisable}
          onCancel={closeModal}
          onCancelSave={dontSave}
          onSave={onSave}
        />
        <AlertsAndErrors
          isVisible={modalState.isVisible}
          res={modalState.res}
          errorMessages={newTripErrorMsg}
          closeModal={onCloseModal}
          onConfirm={handleSubmit((data, e) => onSubmit(data, e, 2))}
        />
      </s.Container>
    </ImageBackground>
  );
};

export default NewTrip;
