import React, {useState} from 'react';
import Header from './components/Header/Header';
import LineDescription from './components/LineDescription/LineDescription';
import CarInfo from './components/CarInfo/CarInfo';
import GroupInfo from './components/GroupInfo/GroupInfo';
import Remarks from './components/Remarks/Remarks';
import s from './styles';
import RemarksModal from './components/RemarksModal/RemarksModal';
import {useNavigation} from '@react-navigation/native';

const Details = ({onPressNext = () => {}, onPressPrevious = () => {}}) => {
  const {navigate} = useNavigation();
  const [isModalRemarksVisable, setIsModalRemarksVisable] = useState(false);

  const navigateToEditRemarks = () => {
    navigate('EditComments');
    setIsModalRemarksVisable(false);
  };

  const proxysetIsModalRemarksVisable = (e) => {
    setIsModalRemarksVisable(true);
  };
  const proxyclose = () => {
    setIsModalRemarksVisable(false);
  };

  return (
    <s.Container>
      <Header />
      <LineDescription
        onPressNext={onPressNext}
        onPressPrevious={onPressPrevious}
      />
      <CarInfo />
      <GroupInfo />
      <Remarks openModal={proxysetIsModalRemarksVisable} />
      <RemarksModal
        isVisable={isModalRemarksVisable}
        closeModal={proxyclose}
        navigateToEditRemarks={navigateToEditRemarks}
      />
    </s.Container>
  );
};

export default Details;
