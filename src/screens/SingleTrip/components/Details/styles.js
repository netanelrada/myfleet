import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	width: 100%;
	align-items: center;
	z-index: 40;
	padding-horizontal: ${calcWidth(14)}px;
`;

export default styles;
