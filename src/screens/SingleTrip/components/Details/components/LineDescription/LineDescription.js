import React, { useContext } from 'react';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';
import { DetailsContext } from '../../../../DetailsContext';
import Icon from '../../../../../../components/common/Icon/Icon';
import { calcHeight, calcWidth } from '../../../../../../utils/dimensions';

const LineDescription = ({ onPressNext, onPressPrevious }) => {
	const {
		tripDetalis: { lineDescription = '' },
	} = useContext(DetailsContext);

	return (
		<s.Container>
			{/* <s.NextBtn onPress={onPressNext}>
				<Icon name='rightArrow' />
			</s.NextBtn> */}
			<s.TextContainer>
				<Text textAlign='right'>{lineDescription}</Text>
			</s.TextContainer>
			{/* <s.PrevBtn onPress={onPressPrevious}>
				<Icon name='leftArrow' />
			</s.PrevBtn> */}
		</s.Container>
	);
};

export default LineDescription;
