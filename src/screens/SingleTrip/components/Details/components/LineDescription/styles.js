import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};
//height: ${calcHeight(58)}px;

styles.Container = styled.View`
	width: ${calcWidth(332)}px;

	height: ${calcHeight(58)}px;
	border-radius: 9px;
	background-color: ${({ theme }) => theme.colors.white};
	padding-horizontal: ${calcWidth(24)}px;
	padding-vertical: ${calcWidth(8)}px;
	justify-content: center;
	margin-top: ${calcHeight(8)}px;
	margin-bottom: ${calcHeight(20)}px;
	box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);
	elevation: 6;
`;

styles.TextContainer = styled.View`
	flex: 1;
`;

styles.NextBtn = styled.TouchableOpacity`
	position: absolute;
	right: ${-calcWidth(12)}px;
	width: ${calcWidth(24)}px;
	aspect-ratio: 1;
	background-color: ${({ theme }) => theme.colors.yellowLight};
	border-radius: ${calcWidth(12)}px;
	justify-content: center;
	align-items: center;
`;

styles.PrevBtn = styled.TouchableOpacity`
	position: absolute;
	left: ${-calcWidth(12)}px;
	width: ${calcWidth(24)}px;
	align-self: center;
	aspect-ratio: 1;
	background-color: ${({ theme }) => theme.colors.yellowLight};
	justify-content: center;
	align-items: center;
	border-radius: ${calcWidth(12)}px;
`;

export default styles;
