import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex-wrap: wrap;
	flex-direction: row;
	width: ${calcWidth(332)}px;
	height: ${calcHeight(115)}px;
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 9px;
	margin-top: ${calcHeight(10)}px;
	margin-bottom: ${calcHeight(15)}px;
	box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);
	elevation: 6;
`;

export default styles;
