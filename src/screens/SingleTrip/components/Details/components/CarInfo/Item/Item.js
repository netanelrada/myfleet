import React from 'react';
import styled, { useTheme } from 'styled-components';
import Text from '../../../../../../../components/common/Text/Text';
import Icon from '../../../../../../../components/common/Icon/Icon';
import useCall from '../../../../../../../hooks/useCall';

const Item = ({ label = '', text = '', driverPhone = '' }) => {
	const { colors } = useTheme();
	const { call } = useCall();

	return (
		<s.Container>
			{!!driverPhone && (
				<s.CallContainer onPress={() => call(driverPhone)}>
					<Icon name='phone' />
				</s.CallContainer>
			)}
			<Text color={colors.gray6}>{label}</Text>
			<Text>{text}</Text>
			{!!driverPhone && <Text>{driverPhone}</Text>}
		</s.Container>
	);
};

const s = {
	Container: styled.View`
		height: 50%;
		width: 50%;
		align-items: center;
		justify-content: space-around;
		border: 1px solid ${({ theme }) => theme.colors.darkGray1};
	`,
	CallContainer: styled.TouchableOpacity`
		position: absolute;
		width: 50%;
		left: 5px;
		top: 5px;
	`,
};
export default Item;
