import React, { useContext } from 'react';
import { DetailsContext } from '../../../../DetailsContext';
import s from './styles';
import { useTranslation } from 'react-i18next';
import Item from './Item/Item';

const CarInfo = () => {
	const { t } = useTranslation();
	const {
		tripDetalis: {
			carNumber = '',
			driverName = '',
			carTypeName = '',
			actualCarTypeName = '',
			driverPhone = '',
		},
	} = useContext(DetailsContext);

	return (
		<s.Container>
			<Item label={t('driver')} text={driverName} driverPhone={driverPhone} />
			<Item label={t('carNumber')} text={carNumber} />
			<Item label={t('actualCar')} text={actualCarTypeName} />
			<Item label={t('expecteCar')} text={carTypeName} />
		</s.Container>
	);
};

export default CarInfo;
