import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(332)}px;
	min-height: ${calcHeight(90)}px;
	flex-direction: row;
	border-radius: 9px;
	background-color: ${({ theme }) => theme.colors.backgroundRemarks};
	padding-horizontal: ${calcWidth(24)}px;
	padding-vertical: ${calcWidth(8)}px;
`;

styles.IconContainer = styled.TouchableOpacity`
	position: absolute;
	z-index: 9999;
	left: ${calcWidth(9)}px;
	top: ${calcWidth(9)}px;
`;

styles.TextContainer = styled.View`
	width: 95%;
`;

styles.MetaDataContainer = styled.View`
	position: absolute;
	width: ${calcHeight(80)}px;
	right: ${-calcWidth(25)}px;
	align-self: center;
	align-items: center;
	justify-content: space-around;
	flex-direction: row-reverse;
	opacity: 0.3;
	transform: rotate(90deg);
`;
styles.MarginButtom = styled.View`
	margin-bottom: 5px;
`;
export default styles;
