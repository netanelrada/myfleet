import React, { useContext } from 'react';
import { DetailsContext } from '../../../../DetailsContext';
import s from './styles';
import Icon from '../../../../../../components/common/Icon/Icon';
import Text from '../../../../../../components/common/Text/Text';
import { useTranslation } from 'react-i18next';
import { ScrollView } from 'react-native';

const Remarks = ({ openModal, showExpandBtn = true }) => {
	const { t } = useTranslation();
	const {
		tripDetalis: { shortRemarks = '', longRemarks = '' },
	} = useContext(DetailsContext);
	const remarksCount = !!shortRemarks + !!longRemarks;
	return (
		<ScrollView>
			<s.Container>
				{showExpandBtn && (
					<s.IconContainer onPress={e => openModal(e)}>
						<Icon name='expand' />
					</s.IconContainer>
				)}
				<s.MetaDataContainer>
					<Icon name='message' />
					<Text>{t('comments')}</Text>
					<Text>{`(${remarksCount})`}</Text>
				</s.MetaDataContainer>
				<s.TextContainer>
					{!!shortRemarks && (
						<Text
							textAlign='right'
							fontWeight='400'
						>{`• ${shortRemarks}`}</Text>
					)}
					<s.MarginButtom />
					{!!longRemarks && (
						<Text textAlign='right' fontWeight='400'>{`• ${longRemarks}`}</Text>
					)}
				</s.TextContainer>
			</s.Container>
		</ScrollView>
	);
};

export default Remarks;
