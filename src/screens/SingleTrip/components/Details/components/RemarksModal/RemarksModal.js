import React from 'react';
import CustomModal from '../../../../../../components/common/Modal/Modal';
import s from './styles';
import Remarks from '../Remarks/Remarks';
import Icon from '../../../../../../components/common/Icon/Icon';

const RemarksModal = ({ isVisable, closeModal, navigateToEditRemarks }) => {
	return (
		<CustomModal isVisable={isVisable}>
			<s.Container>
				<s.Header>
					<s.CloseBtn onPress={closeModal}>
						<Icon name='whiteClose' />
					</s.CloseBtn>
					<s.EditRemarks onPress={navigateToEditRemarks}>
						<Icon name='boxEdit' />
					</s.EditRemarks>
				</s.Header>
				<Remarks showExpandBtn={false} />
			</s.Container>
		</CustomModal>
	);
};

export default RemarksModal;
