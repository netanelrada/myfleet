import React, {useContext} from 'react';
import {DetailsContext} from '../../../../DetailsContext';
import s from './styles';
import {getTimeFromFullDate} from '../../../../../../utils/time';
import Icon from '../../../../../../components/common/Icon/Icon';
import Text from '../../../../../../components/common/Text/Text';
import {calcWidth} from '../../../../../../utils/dimensions';

const Header = () => {
  const {
    tripDetalis: {
      lineCode = '',
      clientName = '',
      orderStartTime = '',
      orderEndTime = '',
      actualStartTime = '',
      actualEndTime = '',
      isDisplayOrder = '0',
    },
  } = useContext(DetailsContext);
  const iconSize = {
    width: calcWidth(18),
    height: calcWidth(18),
  };

  const getRangTime = (date1, date2) =>
    `${getTimeFromFullDate(date1)}-${getTimeFromFullDate(date2)}`;

  const orderRangeTime = getRangTime(orderStartTime, orderEndTime);
  const actualRangeTime = getRangTime(actualStartTime, actualEndTime);

  const boldTime = isDisplayOrder ? orderRangeTime : actualRangeTime;
  const lightTime = isDisplayOrder ? actualRangeTime : orderRangeTime;
  return (
    <s.Container>
      <s.LeftContainer>
        <Icon name="clock" {...iconSize} />
        {!!orderStartTime && !!actualStartTime && (
          <s.TimeComtainer>
            <s.Text>{boldTime}</s.Text>
            <s.LightText>{lightTime}</s.LightText>
          </s.TimeComtainer>
        )}
      </s.LeftContainer>
      <s.RightContainer>
        <Text textAlign="right" fontSize="s16">
          {clientName}
        </Text>
        <s.LightText>{lineCode}</s.LightText>
      </s.RightContainer>
    </s.Container>
  );
};

export default Header;
