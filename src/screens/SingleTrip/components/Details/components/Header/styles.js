import styled from 'styled-components';
import {calcWidth, calcHeight} from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  margin-top: ${calcHeight(56)}px;
  flex-wrap: wrap;
`;

styles.LeftContainer = styled.View`
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
  flex: 0.35;
`;

styles.RightContainer = styled.View`
  flex: 0.65;
  align-items: flex-end;
`;

styles.Text = styled.Text`
  font-family: Rubik-Regular;
  font-size: ${({theme}) => theme.fontSizes.s15}px;
  margin-left: ${calcWidth(7)}px;
`;

styles.LightText = styled.Text`
  font-family: Rubik-Regular;
  font-size: ${({theme}) => theme.fontSizes.s12}px;
  text-align: center;
  color: ${({theme}) => theme.colors.darkGray};
  margin-left: ${calcWidth(7)}px;
`;

styles.TimeComtainer = styled.View``;

export default styles;
