import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	width: ${calcWidth(332)}px;
	height: ${calcHeight(82)}px;
	background-color: ${({ theme }) => theme.colors.white};
	justify-content: space-around;
	border-radius: 9px;
	margin-bottom: ${calcHeight(15)}px;
	box-shadow: 0px 5px 12px rgba(217, 226, 233, 0.5);
	elevation: 6;
`;

styles.Row = styled.View`
	flex-direction: row-reverse;
	justify-content: flex-start;
	padding-horizontal: ${calcWidth(15)}px;
`;

styles.Item = styled.View`
	flex-direction: row-reverse;
	flex: 1;
`;

styles.Label = styled.Text`
	font-family: Rubik-Regular;
	color: ${({ theme }) => theme.colors.gray6};
	font-size: ${({ theme }) => theme.fontSizes.s14};
	margin-left: ${calcWidth(4)}px;
`;

export default styles;
