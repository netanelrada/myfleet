import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { DetailsContext } from '../../../../DetailsContext';
import s from './styles';
import Text from '../../../../../../components/common/Text/Text';

const GroupInfo = () => {
	const { t } = useTranslation();
	const {
		tripDetalis: { groupName = '', departmentName, visaNumber },
	} = useContext(DetailsContext);

	return (
		<s.Container>
			<s.Row>
				<s.Item>
					<s.Label>{t('group')}</s.Label>
					<Text>{groupName}</Text>
				</s.Item>
				<s.Item>
					<s.Label>{t('department')}</s.Label>
					<Text>{departmentName}</Text>
				</s.Item>
			</s.Row>
			<s.Row>
				<s.Item>
					<s.Label>{t('visaNumber')}</s.Label>
					<Text>{visaNumber}</Text>
				</s.Item>
			</s.Row>
		</s.Container>
	);
};

export default GroupInfo;
