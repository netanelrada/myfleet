import React, {useRef, useContext, useState, useEffect} from 'react';
import {Animated, PanResponder, TouchableOpacity, Text} from 'react-native';
import styled from 'styled-components';
import {
  calcHeight,
  calcWidth,
  deviceHeight,
} from '../../../../utils/dimensions';
import useCall from '../../../../hooks/useCall';
import {DetailsContext} from '../../DetailsContext';
import Icon from '../../../../components/common/Icon/Icon';
import {useSelector, useDispatch} from 'react-redux';
import {tabFoucsSelector} from '../../../../store/selectors/tripsSelectors';
import {setTabFoucs} from '../../../../store/actions/actionTrips';
import {PlatfromOS} from '../../../../utils/utilis';

const panResponderStateE = {
  TOP: 'TOP',
  CENTER: 'CENTER',
  BOTTOM: 'BOTTOM',
};

const centerBottomFixed =
  PlatfromOS === 'ios' ? calcHeight(400) : calcHeight(410);
const bottomFixed = PlatfromOS === 'ios' ? calcHeight(515) : calcHeight(530);

const isOnTopScreen = (value) => {
  const top = calcHeight(192);
  return value < top;
};

const isOnCenterScreen = (value) => {
  const topCenter = calcHeight(192);
  const bottomCenter = calcHeight(400);

  return value > topCenter && value < bottomCenter;
};

const isOnBottomScreen = (value) => {
  const bottomCenter = calcHeight(400);

  return value > bottomCenter;
};

const AnimationSlide = ({children}) => {
  const pan = useRef(new Animated.ValueXY(0, 0)).current;
  const dispatch = useDispatch();
  const onSetTabFoucs = (payload) => dispatch(setTabFoucs(payload));
  const tabFocus = useSelector((state) => tabFoucsSelector(state));
  const [panResponderState, setPanResponderState] = useState(
    panResponderStateE.TOP,
  );
  const {
    tripDetalis: {driverName = ''},
  } = useContext(DetailsContext);
  const {call} = useCall();

  useEffect(() => {
    switch (tabFocus.key) {
      case 'details':
        tabFocus.clickOnTab && animatedToTop();
        break;
      case 'map':
        tabFocus.clickOnTab && animatedToBottom();
        break;
      default:
        break;
    }
  }, [tabFocus]);

  const panResponder = useRef(
    PanResponder.create({
      onShouldBlockNativeResponder: () => false,
      onMoveShouldSetPanResponder: (evt, gestureState) => {
        const {dx, dy} = gestureState;
        return (dy > 12 || dy < -12) && dy != 0;
      },
      onPanResponderGrant: () => {
        pan.setOffset({
          y: pan.y._value,
        });
      },
      onPanResponderMove: (_, gesture) => {
        pan.y.setValue(gesture.dy);
      },
      onPanResponderRelease: (_, gesture) => {
        pan.flattenOffset();
        if (isOnTopScreen(pan.getLayout().top._value)) {
          onSetTabFoucs({key: 'details', clickOnTab: false});
          animatedToTop();
        } else if (isOnCenterScreen(pan.getLayout().top._value)) {
          onSetTabFoucs({key: 'map', clickOnTab: false});
          animatedToCenter();
        } else if (isOnBottomScreen(pan.getLayout().top._value)) {
          onSetTabFoucs({key: 'map', clickOnTab: false});
          animatedToBottom();
        }
      },
    }),
  ).current;

  const animatedToTop = () => {
    setPanResponderState(panResponderStateE.TOP);
    Animated.spring(pan, {
      duration: 300,
      toValue: {x: 0, y: 0},
      useNativeDriver: false,
    }).start();
  };

  const animatedToCenter = () => {
    Animated.spring(pan, {
      duration: 300,
      toValue: {x: 0, y: centerBottomFixed},
      useNativeDriver: false,
    }).start();
    setPanResponderState(panResponderStateE.CENTER);
  };

  const animatedToBottom = () => {
    Animated.spring(pan, {
      duration: 300,
      toValue: {x: 0, y: bottomFixed},
      useNativeDriver: false,
    }).start();
    setPanResponderState(panResponderStateE.BOTTOM);
  };

  return (
    <Animated.View
      style={[
        {
          zIndex: 0,
          height: '100%',
          width: '100%',
          transform: [{translateY: pan.y}],
        },
      ]}
      {...panResponder.panHandlers}>
      <s.SlideContainer>
        {panResponderState !== panResponderStateE.CENTER && (
          <s.IndacitorContainer>
            {panResponderState === panResponderStateE.BOTTOM ? (
              <Icon name="yellowUpArrow" />
            ) : (
              <Icon name="yellowDownArrow" />
            )}
          </s.IndacitorContainer>
        )}
        {!!driverName && panResponderState === panResponderStateE.CENTER && (
          <s.PhoneContainer onPress={() => call(driverName)}>
            <Icon name="phone" />
            <s.Text>{driverName}</s.Text>
          </s.PhoneContainer>
        )}
        {children}
      </s.SlideContainer>
    </Animated.View>
  );
};

const s = {
  Container: styled.View`
    flex: 1;
    width: 100%;
  `,
  SlideContainer: styled.View`
    flex: 1;
    width: 100%;
    background-color: ${({theme}) => theme.colors.backgroundDetailes};
  `,
  PhoneContainer: styled.TouchableOpacity`
    position: absolute;
    flex-direction: row;
    top: ${calcHeight(25)}px;
    left: ${calcWidth(15)}px;
    z-index: 999;
  `,
  Text: styled.Text`
    font-family: Rubik-Regular;
    font-size: ${({theme}) => theme.fontSizes.s14}px;
    margin-left: 8px;
  `,
  IndacitorContainer: styled.View`
    width: ${calcWidth(64)}px;
    background-color: ${({theme}) => theme.colors.white};
    box-shadow: 3px 3px 7px rgba(0, 0, 0, 0.47);
    justify-content: center;
    align-items: center;
    align-self: center;
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    aspect-ratio: ${64 / 22};
    elevation: 3;
  `,
};
export default AnimationSlide;
