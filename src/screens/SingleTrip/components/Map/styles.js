import styled from 'styled-components';
import { calcHeight, calcWidth } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	flex: 1;
`;

styles.ContainerImage = styled.View`
	width: ${calcWidth(60)}px;
	aspect-ratio: 1;
	align-items: center;
	justify-content: center;
	border-radius: ${calcWidth(30)}px;
`;

styles.Image = styled.Image`
	height: ${calcHeight(20)}px;
	aspect-ratio: ${47 / 20};
	transform: ${({ heading }) => `rotate(${heading - 270}deg)`};
`;

styles.ImageFocus = styled.Image``;

styles.Overlay = styled.View`
	background-color: ${({ theme }) => theme.colors.transparentBlack};
	position: absolute;
	left: 0;
	right: 0;
	top: 0;
	bottom: 0;
`;

export default styles;
