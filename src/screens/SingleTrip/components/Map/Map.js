import React, {
  useEffect,
  useState,
  useRef,
  useContext,
  useCallback,
} from 'react';
import {StyleSheet, Platform} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

import {images} from '../../../../assets/index';
import s from './styles';
import {DetailsContext} from '../../DetailsContext';
import {getLocation, getGpsData} from '../../../../api/api';
import {useSelector} from 'react-redux';
import {userSelector} from '../../../../store/selectors/loginSelectors';
import {getTimeFromFullDate, getDateFromFullDate} from '../../../../utils/time';
import {useTranslation} from 'react-i18next';
import {get} from 'lodash';
import {tabFoucsSelector} from '../../../../store/selectors/tripsSelectors';
import moment from 'moment';
import Foucs from './Foucs/Foucs';

const initialUser = {
  gpsServer: '',
  gpsToken: '',
  companyCode: '',
};

const ISREAL_REGION = {
  latitude: 31.5601263,
  longitude: 34.8558051,
  latitudeDelta: 1.4,
  longitudeDelta: 2,
};
const API_KEY = 'AIzaSyDgzBYAL9ZP6lJs5egZenJgzUpMhvENt7k';

const Map = () => {
  const {t} = useTranslation();
  const {tripDetalis = {}, setToolTipMap} = useContext(DetailsContext);
  const driverCode = get(tripDetalis, 'driverCode', '');
  const isFutureTrips = get(tripDetalis, 'isFutureTrips', false);
  const from = get(tripDetalis, 'startTime', '');
  const to = get(tripDetalis, 'endTime', '');

  const {gpsServer = '', gpsToken: token = '', companyCode: accountCode = ''} =
    useSelector((state) => userSelector(state)) || initialUser;
  const [coordinateDriver, setCoordinateDriver] = useState([]);
  const [isNotAvaliable, setIsNotAvaliable] = useState(false);
  const [isOutOfFoucs, setIsOutOfFoucs] = useState(false);
  const [historyLocations, setHistoryLocation] = useState([]);
  const lastIDLocation = useRef('');
  const mapRef = useRef(null);
  const intervalId = useRef(null);
  const {key} = useSelector((state) => tabFoucsSelector(state));
  const isMapFoucs = key === 'map';

  useEffect(() => {
    if (driverCode && isMapFoucs && !isFutureTrips && !isOutOfFoucs) {
      drwaRouteHistory();
      onGetLoocation(2500);
      intervalId.current = setInterval(() => {
        onGetLoocation();
      }, 10000);
    }
    return clearIntervalGetLocation;
  }, [driverCode, isMapFoucs, isFutureTrips, isOutOfFoucs]);

  const clearIntervalGetLocation = () => clearInterval(intervalId.current);

  const drwaRouteHistory = async () => {
    try {
      const payload = {
        gpsServer,
        token,
        driverCode,
        accountCode,
        formID: 0,
        from,
        to,
      };
      const resGpsData = await getGpsData(payload);

      const {gpsData, response} = resGpsData.data;

      if (response !== 0 && response !== 3) {
        setIsNotAvaliable(true);
        setToolTipMap(errorsLocation[response]);
        return;
      }
      lastIDLocation.current = gpsData[gpsData.length - 1]?.id;
      setHistoryLocation(
        gpsData.map(({lt: latitude, lg: longitude}) => ({latitude, longitude})),
      );
      if (gpsData.length === 0) return;
      const {lt: latitude, lg: longitude} = gpsData[gpsData.length - 1];

      animateToRegion({latitude, longitude});
    } catch (error) {
      console.log({error});
    }
  };

  const onGetLoocation = async (duration) => {
    try {
      if (!driverCode) return;
      const payload = {
        gpsServer,
        token,
        driverCode,
        accountCode,
        // formID: lastIDLocation.current,
        from,
        to,
      };
      const resGpsData = await getGpsData(payload);
      const {currentPos, response} = resGpsData.data;

      if (response !== 0 && response !== 3) {
        setIsNotAvaliable(true);
        setToolTipMap(errorsLocation[response]);
        return;
      }
      if (response === 3) {
        setToolTipMap(errorsLocation[response]);
        return;
      }
      const {lt: latitude, lg: longitude, t: time, h: heading} = currentPos;
      setHistoryLocation((preState) => [...preState, {latitude, longitude}]);
      animateToRegion({latitude, longitude}, duration);
      setCoordinateDriver([
        {
          latitude,
          longitude,
          heading,
        },
      ]);

      const lastUpdateTime = getTimeFromFullDate(time).slice(0, 5);
      const date = moment(getDateFromFullDate(time)).format('DD.MM.YYYY');

      setToolTipMap(`${t('locationDriverUpdate')} ${date} ${lastUpdateTime}`);
      setIsNotAvaliable(false);
    } catch (error) {
      console.log('error', error);
    }
  };

  const animateToRegion = ({latitude, longitude}, duration = 1000) => {
    mapRef.current.animateToRegion(
      {
        latitude,
        longitude,
        latitudeDelta: 0.002,
        longitudeDelta: 0.002,
      },
      duration,
    );
  };

  const onFoucs = () => {
    setIsOutOfFoucs(false);
    const {latitude, longitude} = coordinateDriver[0];
    animateToRegion({latitude, longitude});
  };

  const errorsLocation = {
    1: t('invalidCurrentLocation'),
    2: t('endTrip'),
    3: t('currentLocationNotUpdate'),
    4: t('endTripDisableLocation'),
  };

  return (
    <s.Container>
      <MapView
        provider="google"
        ref={mapRef}
        style={[styles.map]}
        zoomEnabled={true}
        showsMyLocationButton={false}
        initialRegion={ISREAL_REGION}
        mapPadding={{
          top: 0,
          right: 10,
          bottom: 0,
          left: 0,
        }}
        onPanDrag={() => setIsOutOfFoucs(true)}>
        {coordinateDriver.map((coord) => (
          <Marker.Animated
            key={1}
            coordinate={{
              latitude: coord.latitude,
              longitude: coord.longitude,
            }}
            anchor={{x: 0.5, y: 0.5}}>
            <s.ContainerImage>
              <s.Image
                heading={coord.heading}
                source={images.minibus}
                resizeMode="cover"
              />
            </s.ContainerImage>
          </Marker.Animated>
        ))}
        <MapViewDirections
          origin={historyLocations[0]}
          // waypoints={historyLocations.slice(1, 20)}
          destination={historyLocations[historyLocations.length - 1]}
          strokeWidth={8}
          strokeColor="green"
          apikey={API_KEY}
          optimizeWaypoints
          onError={(error) => console.log({error})}
        />
      </MapView>
      {isOutOfFoucs && !!coordinateDriver[0] && <Foucs onFoucs={onFoucs} />}
      {isNotAvaliable && <s.Overlay />}
    </s.Container>
  );
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});

export default Map;
