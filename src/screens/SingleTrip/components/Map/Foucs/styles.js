import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../../utils/dimensions';

const styles = {};

const BORDER_RADIUS = 26;

styles.Container = styled.TouchableOpacity`
	position: absolute;
	top: ${calcHeight(80)};
	right: ${calcWidth(10)};
	border-radius: ${BORDER_RADIUS};
	width: ${calcWidth(BORDER_RADIUS * 2)}px;
	aspect-ratio: 1;
	background: white;
	justify-content: center;
	align-items: center;
`;

export default styles;
