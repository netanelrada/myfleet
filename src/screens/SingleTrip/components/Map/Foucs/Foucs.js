import React from 'react';
import s from './styles';
import Icon from '../../../../../components/common/Icon/Icon';

const Foucs = ({ onFoucs = () => {} }) => {
	return (
		<s.Container onPress={onFoucs}>
			<Icon name='foucs' />
		</s.Container>
	);
};

export default Foucs;
