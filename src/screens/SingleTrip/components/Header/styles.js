import styled from 'styled-components';
import { calcWidth, calcHeight } from '../../../../utils/dimensions';

const styles = {};

styles.Container = styled.View`
	position: absolute;
	top: 0;
	z-index: 999;
	width: 100%;
	height: ${calcHeight(45)};
	flex-direction: row;
	justify-content: ${({ isMapFoucs }) =>
		isMapFoucs ? 'space-between' : 'flex-end'};
	align-items: center;
	padding-horizontal: ${calcWidth(14)}px;
	margin-top: ${calcHeight(15)}px;
`;

styles.Btn = styled.TouchableOpacity`
	width: ${calcWidth(40)}px;
	aspect-ratio: 1;
	border-radius: 9px;
	background-color: ${({ theme }) => theme.colors.blueLight};
	justify-content: center;
	align-items: center;
`;

styles.Title = styled.Text`
	font-family: Rubik-Regular;
	font-weight: 500;
	font-size: ${({ theme }) => theme.fontSizes.s18}px;
	margin-right: ${calcWidth(9)}px;
`;

styles.MapStatusContainer = styled.View`
	background-color: ${({ theme }) => theme.colors.ghostWhite};
	border-radius: 9px;
	width: ${calcWidth(274)}px;
	aspect-ratio: ${274 / 40};
	justify-content: center;
	align-items: center;
`;

export default styles;
