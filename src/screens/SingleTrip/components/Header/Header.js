import React, {useContext} from 'react';
import s from './styles';
import Icon from '../../../../components/common/Icon/Icon';
import {DetailsContext} from '../../DetailsContext';
import Text from '../../../../components/common/Text/Text';
import {useSelector} from 'react-redux';
import {tabFoucsSelector} from '../../../../store/selectors/tripsSelectors';

const Header = ({onBack = () => {}, title}) => {
  const {
    toolTipMap,
    tripDetalis: {driverCode = '', isFutureTrips = false},
  } = useContext(DetailsContext);
  const {key} = useSelector((state) => tabFoucsSelector(state));
  const isMapFoucs = key === 'map';
  const showToolTipHeader = isMapFoucs && !!driverCode && !isFutureTrips;
  return (
    <s.Container isMapFoucs={showToolTipHeader}>
      {showToolTipHeader ? (
        <s.MapStatusContainer>
          <Text textAlign="right" fontSize="s15" color="black">
            {toolTipMap}
          </Text>
        </s.MapStatusContainer>
      ) : (
        <s.Title>{title}</s.Title>
      )}
      <s.Btn onPress={onBack}>
        <Icon name="back" />
      </s.Btn>
    </s.Container>
  );
};

export default Header;
