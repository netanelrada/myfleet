import React, { createContext, useState, useRef } from 'react';
import s from './styles';
import useSingleTrip from './hooks/useSingleTrip';
import Header from './components/Header/Header';
import Details from './components/Details/Details';
import { useSelector } from 'react-redux';
import { dateSelector } from '../../store/selectors/tripsSelectors';
import AnimationSlide from './components/AnimationSlide/AnimationSlide';
import Map from './components/Map/Map';
import { DetailsContext } from './DetailsContext';
import TabActions from '../../components/TabActions/TabActions';
import useBackHandler from '../../hooks/useBackHandler';
import moment from 'moment';
 import { diffBetweenDatesByDays } from '../../utils/time';

const SingleTrip = ({ route, navigation }) => {
	const { goBack } = navigation;
	const date = useSelector(state => dateSelector(state));
	const { trip = {} } = route.params;
	const { tripDetalis, onPressNext, onPressPrevious } = useSingleTrip({
		...trip,
		isFutureTrips: moment().isBefore(trip.startTime.split(' ')[0]),
	});
	const [toolTipMap, setToolTipMap] = useState('');

	useBackHandler(() => {
		goBack();
		return true;
	});

	return (
		  <DetailsContext.Provider value={{ tripDetalis, toolTipMap, setToolTipMap }}>
			<s.Container>
				<Header onBack={goBack} title={date} />
				<s.MapContainer>
					<Map />
				</s.MapContainer>
				<AnimationSlide>
					<Details
						onPressNext={onPressNext}
						onPressPrevious={onPressPrevious}
					/>
				</AnimationSlide>
				<TabActions isVisable={true} />
			</s.Container>
		</DetailsContext.Provider>
	);
};  

export default SingleTrip;
