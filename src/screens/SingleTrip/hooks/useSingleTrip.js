import {useEffect, useState} from 'react';
import useProxyApi from '../../../hooks/useProxyApi';
import {getLineData} from '../../../api/api';
import {useSelector, useDispatch} from 'react-redux';
import {allTripsSelector} from '../../../store/selectors/tripsSelectors';
import {setSingleEditTrip} from '../../../store/actions/actionTrips';

const useSingleTrip = (initTrip) => {
  const dispatch = useDispatch();
  const onSetSingleEditTrip = (payload) => dispatch(setSingleEditTrip(payload));
  const allTrips = useSelector((state) => allTripsSelector(state));
  const [index, setIndex] = useState(() =>
    allTrips.findIndex((el) => el.lineCode === initTrip.lineCode),
  );
  const [tripDetalis, setTripDetalis] = useState({...initTrip});
  const {proxyApi} = useProxyApi();

  useEffect(() => {
    const currTrip = allTrips[index];
    onSetSingleEditTrip(currTrip);
    onGetTripData(currTrip.lineCode);
  }, [allTrips]);

  const onGetTripData = async (lineCode) => {
    try {
      const res = await proxyApi({
        cb: getLineData,
        restProp: {
          lineCode,
        },
      });

      const restData = res.data.data[0];
      setTripDetalis((preState) => ({...preState, ...restData}));
    } catch (error) {
      setTripDetalis({});
      console.log(error);
    }
  };
  const onPressNext = () => {
    const newIndex = index + 1;
    if (newIndex < allTrips.length) {
      setIndex(newIndex);
    }
  };

  const onPressPrevious = () => {
    const newIndex = index - 1;
    if (0 <= newIndex) {
      setIndex(newIndex);
    }
  };
  return {tripDetalis, onPressNext, onPressPrevious, onGetTripData};
};

export default useSingleTrip;
