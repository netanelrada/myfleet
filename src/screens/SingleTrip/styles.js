import styled from 'styled-components';

const styles = {};

styles.Container = styled.View`
	flex: 1;
	align-items: center;
`;

styles.MapContainer = styled.View`
	position: absolute;
	width: 100%;
	height: 100%;
`;

export default styles;
