export const statusTrip = {
  /// tripStatus
  UNDEFINED: 0,
  SEND: 1,
  ACCEPT: 2,
  START: 3,
  END: 4,
  REJECT: 5,
};

export const statusWork = {
  //workStatus
  NOT_WORK: '0',
  WORK: '2',
  UNDEFINED: '3',
};

export const tripsStatusE = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
};

export const loginByUUIDStatusE = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
};

export const typeOrderByE = {
  HOUR: 'startTime',
  CLIENT: 'clientName',
  DRIVER: 'driverName',
  LINE_DESCRIPTION: 'lineDescription',
};

export const fetchDataStatusE = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
};
export const typeModalE = {
  SEND_TO_DRIVERS: 'SEND_TO_DRIVERS',
  SEND_TO_WHATSAPP: 'SEND_TO_WHATSAPP',
  DELETE: 'DELETE',
};
export const setStatusE = {
  FAILD: 'FAILD',
  SUCCESS: 'SUCCESS',
  INITIAL: 'INITIAL',
};

export const PASSENGERS_TABS = {
  SELECT_PASSENGERS: 'SELECT_PASSENGERS',
  NEW_PASSENGER: 'NEW_PASSENGER',
};

export const fontSizesE = {
  REGULAR: 'REGULAR',
  BIG: 'BIG',
};

export const APP_VERSION_NUMBER = '2.3.6';

export const APP_NAME = 'My Fleet';
export const SUPPORT_MAIL = 'support@y-it.co.il';
export const SUPPORT_PHONE = '08-9464288';
export const WEB_SITE = 'Y-IT.co.il';
