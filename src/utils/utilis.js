import {Platform} from 'react-native';

export const updatePropInArrById = (arr, lineCode, value = false) => {
  const resFind = arr.find((trip) => trip.lineCode === lineCode);
  if (resFind) resFind.isSelectedToEdit = value;
};

export const filterObjectWithEmptValue = (obj) => {
  return Object.keys(obj).reduce((acc, curr) => {
    if (obj[curr]) {
      acc[curr] = obj[curr];
    }
    return {
      ...acc,
    };
  }, {});
};

export const PlatfromOS = Platform.OS;
