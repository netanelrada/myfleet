import moment from 'moment';
import I18n from '../i18n';

export const getDate = () => {
  return moment().format('D.M.YYYY');
};

export const generateDatesOfAMonth = (month = moment().month()) => {
  let start_date_of_month = moment().set('M', month).format('YYYY-MM-01'),
    end_date_of_month =
      moment().set('M', month).format('YYYY-MM-') + moment().daysInMonth(),
    dayArray = [];

  while (start_date_of_month <= end_date_of_month) {
    const momentData = moment(start_date_of_month);
    dayArray.push({dayInWeek: momentData.day(), date: momentData.date()});

    start_date_of_month = moment(start_date_of_month)
      .add(1, 'days')
      .format('YYYY-MM-DD');
  }
  return dayArray;
};

export const getCurrentDate = () => moment().date();
export const formatToTwoDig = (value) => (value < 10 ? '0' + value : value);

export const isMinutes = (min) => min >= 0 && min < 60 && min !== '';
export const isHours = (hours) => hours >= 0 && hours < 24 && hours !== '';

export const getTimeFromFullDate = (fullDate) => fullDate.split(' ')[1];
export const getDateFromFullDate = (fullDate) => fullDate.split(' ')[0];

export const validateTime = (time) => {
  let isValid = false;
  const [hours, min] = time.split(':');
  if (isHours(hours) && isMinutes(min)) isValid = true;

  return isValid;
};

export const sortByTime = (a, b) => {
  let dateA = moment(a.startTime);
  let dateB = moment(b.startTime);
  return dateA.diff(dateB);
};
export const diffBetweenDatesByDays = (a) => {
  let dateA = moment(a);
  let dateB = moment();

  return dateA.diff(dateB, 'd', true);
};

export const DATE_REGEX = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
export const TIME_REGEX = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;

export const getDays = () => [
  I18n.t('sunday'),
  I18n.t('monday'),
  I18n.t('tuesday'),
  I18n.t('wednesday'),
  I18n.t('thrusday'),
  I18n.t('friday'),
  I18n.t('saturday'),
];

export const getMonths = () => [
  I18n.t('january'),
  I18n.t('february'),
  I18n.t('march'),
  I18n.t('april'),
  I18n.t('may'),
  I18n.t('june'),
  I18n.t('july'),
  I18n.t('august'),
  I18n.t('september'),
  I18n.t('october'),
  I18n.t('november'),
  I18n.t('december'),
];

export const getShortDays = () => [
  I18n.t('sun'),
  I18n.t('mon'),
  I18n.t('tue'),
  I18n.t('wed'),
  I18n.t('thr'),
  I18n.t('fri'),
  I18n.t('sat'),
];
