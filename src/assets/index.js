import backgroundSplash from './images/Background.png';
import maps from './images/Maps.png';
import minibus from './images/Minibus.png';

import dont_see from './icons/dont_see.svg';
import see from './icons/see.svg';
import back from './icons/back.svg';
import blackBack from './icons/blackBack.svg';
import changeCar from './icons/ChangeCar.svg';
import devices from './icons/Devices.svg';
import driverIcon from './icons/DriverIcon.svg';
import remove from './icons/Remove.svg';
import vector from './icons/Vector.svg';
import warning from './icons/Warning.svg';
import whatsapp from './icons/Whatsapp.svg';
import edit from './icons/Edit.svg';
import filter from './icons/Filter.svg';
import settings from './icons/Settings.svg';
import downArrow from './icons/DownArrow.svg';
import yellowDownArrow from './icons/YellowDownArrow.svg';
import yellowUpArrow from './icons/YellowUpArrow.svg';
import refresh from './icons/Refresh.svg';
import search from './icons/search.svg';
import sended from './icons/sended.svg';
import rejected from './icons/rejected.svg';
import accept from './icons/accept.svg';
import on_drive from './icons/on_drive.svg';
import ended from './icons/ended.svg';
import location from './icons/Location.svg';
import logo from './icons/yit_logo.svg';
import blackEdit from './icons/BlackEdit.svg';
import close from './icons/close.svg';
import calendar from './icons/calendar.svg';
import details from './icons/Details.svg';
import blueDetails from './icons/BlueDetails.svg';
import map from './icons/Map.svg';
import blueMap from './icons/BlueMap.svg';
import passenger from './icons/Passenger.svg';
import hamburgerMenu from './icons/HamburgerMenu.svg';
import blueSearch from './icons/blueSearch.svg';
import ourOfWork from './icons/OurOfWork.svg';
import OutOfService from './icons/OutOfService.svg';
import plus from './icons/plus.svg';
import whitePlus from './icons/WhitePlus.svg';
import blackPlus from './icons/blackPlus.svg';
import inputClose from './icons/InputClose.svg';
import threePoint from './icons/three_point.svg';
import passengersL from './icons/PassengersL.svg';

//
import car from './icons/Car.svg';
import clock from './icons/Clock.svg';
import trash from './icons/Delete.svg';
import redTrash from './icons/Red_Trash.svg';
import driver from './icons/Driver.svg';
import message from './icons/Message.svg';
import road from './icons/Road.svg';
import share from './icons/Share.svg';
import blueClock from './icons/BlueClock.svg';
import lock from './icons/Lock.svg';
import rightArrow from './icons/RightArrow.svg';
import leftArrow from './icons/LeftArrow.svg';
import expand from './icons/Expand.svg';
import boxEdit from './icons/boxEdit.svg';
import whiteClose from './icons/WhiteClose.svg';
import phone from './icons/phone.svg';
import blackPhone from './icons/blackPhone.svg';
import mail from './icons/Mail.svg';
import alert from './icons/Alert.svg';
import stations from './icons/Stations.svg';
import stationsPassengers from './icons/StationsPassengers.svg';
import noWifi from './icons/NoWifiIcon.svg';
import foucs from './icons/foucs.svg';
import blueStationsAndPassengers from './icons/blueStationsAndPassengers.svg';
import stationsAndPassengers from './icons/stationsAndPassengers.svg';

export const images = {backgroundSplash, maps, minibus};

export const icons = {
  back,
  blackBack,
  dont_see,
  see,
  changeCar,
  devices,
  driverIcon,
  remove,
  vector,
  warning,
  whatsapp,
  settings,
  edit,
  filter,
  downArrow,
  refresh,
  search,
  sended,
  rejected,
  accept,
  on_drive,
  ended,
  location,
  logo,
  blackEdit,
  close,
  calendar,
  details,
  map,
  passenger,
  hamburgerMenu,
  car,
  clock,
  trash,
  redTrash,
  driver,
  message,
  road,
  share,
  blueSearch,
  ourOfWork,
  OutOfService,
  plus,
  blueClock,
  lock,
  rightArrow,
  leftArrow,
  expand,
  boxEdit,
  whiteClose,
  phone,
  blackPhone,
  blueDetails,
  blueMap,
  whitePlus,
  blackPlus,
  mail,
  alert,
  stations,
  stationsPassengers,
  inputClose,
  yellowDownArrow,
  yellowUpArrow,
  noWifi,
  foucs,
  threePoint,
  passengersL,
  blueStationsAndPassengers,
  stationsAndPassengers,
};
