import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {I18nManager, Text} from 'react-native';
import {Provider} from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import {I18nextProvider} from 'react-i18next';
import RNRestart from 'react-native-restart';
import storeObj from './store/index';
import InitScreen from './screens/Init/Init';
import i18n from './i18n';

I18nManager.forceRTL(false);
I18nManager.allowRTL(false);

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

const App = () => {
  useEffect(() => {
    if (I18nManager.isRTL) {
      RNRestart.Restart();
    }
    SplashScreen.hide();
  }, []);

  return (
    <>
      <Provider store={storeObj.store}>
        <I18nextProvider i18n={i18n}>
          <InitScreen />
        </I18nextProvider>
      </Provider>
    </>
  );
};

export default App;
